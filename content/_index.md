+++
title = "Home"

[menu.main]
title = "homepage of site"
weight = -100
+++

This site exists just for me to dump a bunch of tier lists onto with arbitrary data. For the most part it exists to rank isekai manga but i could add other types and mix and match everything together if i really wanted to.

If you have suggestions for the site, or something is broken, let me know. Styling is still a work in progress but i'm not putting too much effort into it yet. If you want to rec something to me to prioritize it to put it on this list as well that would also be cool since there's too much out there to reasonably catch everything that is great.

I have a [mangaupdates here as well](https://www.mangaupdates.com/member/oo00vwv/xelivous) categorized in custom lists.
