+++
title = "Stale Tracker"
layout = "stale-tracker"

[menu.main]
title = "A list of manga sorted by last checked date and only if they're still ongoing/hiatus"
weight = 90
+++

This is a list of manga on the site but listed from when I last checked them to try and find things that are out of date.
