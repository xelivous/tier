+++
title = "Partial"
+++

These works have furigana on certain kanji, either due to alternative meanings or if they're uncommon kanji in general, but a good portion of kanji do not have any furigana on them. Sometimes the kanji that have furigana are just completely random trivial kanji that just about everybody should know, and then harder kanji just don't have furigana. 

![example bubble showing partial furigana](example1.png)
