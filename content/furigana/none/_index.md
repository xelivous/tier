+++
title = "None"
+++

Practically none of the kanji have furigana, excluding name kanji, or a few obscure meme readings/usages.

![example bubble showing meme furi](example1.png) ![example bubble showing dot furi + name furi](example2.png)
