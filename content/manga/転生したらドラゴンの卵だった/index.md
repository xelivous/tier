+++
title = "転生したらドラゴンの卵だった"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["partial"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147652"
raw = "http://comic-earthstar.jp/detail/doratama/"
md = "https://mangadex.org/title/23774/tensei-shitara-dragon-no-tamago-datta-ibara-no-dragon-road"
bw = "https://bookwalker.jp/series/164419/"

[chapters]
released = 34
read = 20.7

[lists]
recommend = "B"
+++

This is fairly similar to 蜘蛛ですが, except the protag gets to interact with other people *far* quicker.

<!--more-->
