+++
title = "奪う者 奪われる者"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2021-09-19
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=133712"
raw = "https://web-ace.jp/youngaceup/contents/1000021/"
md = "https://mangadex.org/title/ed95d175-0e50-4315-8044-3dccbcd63748/ubau-mono-ubawareru-mono"
#bw = ""

[chapters]
released = 8.1
read = 8.1

[lists]
recommend = "F"
+++

Protag gets strangled to death by his abusive dad and fed tons of pills so they can cash out on insurance money, and in the process of trying to run away he gets isekai'd. Once there he gets found by an old granny who lives on the outskirts of town, but even in an isekai his luck isn't that much better and he gets discriminated for his hair color whenever he tries to visit town.

<!--more-->

It's possible that it might've been decent at some point, but it got axed before it ever really started so i'm not really sure where to put it, and since the artist started working on some separate series while he was working on this one they released like 6 pages a month for ages up until it finally got axed ruining the pacing. Booba oneechan is decent but the interactions between her and the protag are weirdge.
