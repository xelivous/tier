+++
title = "魔王学院の不適合者"
title_en = ""
statuses = ["axed"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=151642"
raw = "https://magazine.jp.square-enix.com/mangaup/original/maougakuin/"
md = "https://mangadex.org/title/31962/maou-gakuin-no-futekigousha"
bw = "https://bookwalker.jp/series/181411/"

[chapters]
released = 15
read = 0

[lists]
recommend = ""
+++

<!--more-->
