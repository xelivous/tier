+++
title = "姫騎士がクラスメート"
title_en = ""
categories = []
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = []
schedules = []
lastmod = 2023-09-28
tags = [
    "basically-porn",
    "nudity",
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=121222"
raw = "http://www.comic-valkyrie.com/modules/web_valkyrie/classmate/"
md = "https://mangadex.org/title/3584eeb4-7ce4-4aed-b5e4-5f4a26da55d7/himekishi-ga-classmate"
bw = "https://bookwalker.jp/series/75022"

[chapters]
released = 59
read = 0

[lists]
recommend = ""
+++



<!--more-->

