+++
title = "シルシア＝コード"
title_en = "Cylcia=Code"
categories = ["reverse-isekai", "game"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2024-05-31
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/wfbgsq8/cylcia-code"
#raw = ""
#md = ""
#bw = ""

[chapters]
released = 32
read = 32

[lists]
recommend = "D"
+++

Protag lives in the shadow of his epic ikemen of a twin brother his entire life and the only one who saw him as an actual person was the cute sick childhood friend (girl) of his that he saved after she has an accident in the woods. One day the childhood friend has to go away to a hospital to possibly cure her of her life-threatening condition, but the protag hasn't had contact with her for years and doesn't know what she's up to. At least until she sends him a random note with details about an online VRMMO and asks for help inside of the game. However this is a super realistic VRMMO where you can feel all of the sensations inside of it, and somehow the protagonist was given an incredibly sexy girl avatar by the childhood friend.

<!--more-->

Wild nonsensical series where the only redeeming aspect is the cute character designs. Kind of based that the protag is slowly mindbroken into being okay with being a girl and it's basically somewhat trans-positive as a result? The ending is bafflingly bad since it just kind of got axed without really resolving anything, and a lot of chapters are just kind of filler fanservice as well. The backgrounds in this story are also almost completely nonexistent so the only thing in this story is the character art and text bubbles. The character designs are cute but like a story needs more than that.

