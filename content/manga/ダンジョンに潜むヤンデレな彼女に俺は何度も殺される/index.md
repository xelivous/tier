+++
title = "ダンジョンに潜むヤンデレな彼女に俺は何度も殺される"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2025-01-13
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/eid3qwi"
raw = "https://comic-walker.com/contents/detail/KDCW_CW01203885010000_68"
md = "https://mangadex.org/title/ea0e7020-2b2f-4f17-9251-fee3632dc135/dungeon-ni-hisomu-yandere-na-kanojo-ni-ore-wa-nando-mo-korosareru"
bw = "https://bookwalker.jp/series/431585/"

[chapters]
released = 22.1
read = 21.2

[lists]
recommend = "B"
+++

Protagonist is some random dude living his life in a fantasy world bullied and ostracized in his little medieval town for having the haircolor of a despised person, but pushes on regardless. However one day his crush and love of his life tells him she's going to be married to his scumbag bully of a village leader's kid and right before they are going to elope she ends up getting raped/killed, the protagonist gets framed for her death, and gets sentenced to death in the hardest dungeon in the world that is conveniently right next to their village that nobody has ever cleared. However as soon as he enters a random girls offers him a skill to save/reset his position as if it were rezero, and as a result dies repeatedly over and over while he brute forces his way forward despite having no actual intrinsic skills or abilities of his own.

<!--more-->

Actually kind of a fascinating mellycore work that I enjoyed a lot for the ride. You never know what it's going to do next and the author is just cooking up whatever he finds to be interesting. The heroines are all hot, the art is great in general. Honestly I just like it overall and think it's a fun popcorn read. I don't know if the plot will be able to hold up much longer and I might need to lower the rating a bit but i'll trust the author to cook some more at least.

Would rec if you want an even edgier re:zero (somehow) with hot heroines.
