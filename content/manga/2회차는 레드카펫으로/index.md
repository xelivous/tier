+++
title = "2회차는 레드카펫으로"
title_en = "Second Life on the Red Carpet"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["axed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2022-12-26
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=192947"
raw = "https://page.kakao.com/home?seriesId=58419968"
md = "https://mangadex.org/title/8ca87b5a-ccc9-4d5c-a0f9-b111e04cc368/second-life-on-the-red-carpet"

[chapters]
released = 81
read = 41

[lists]
recommend = "B"
+++

Protag isekais into a novel plot, tries to follow the plot 1:1 even if it means sacrificing any innocent lives that are taken in front of her if they canonically die in the work, and succeeds in reaching the end. Except after the ending she wakes up years in the past and basically has to redo everything from the beginning for some reason, which is where this work starts off; To top it off the princess she was protecting for the entire time also went back in time with her and they commit to making an ideal world unlike the events of the first timeline (the princess doesn't know it's a novel though).

<!--more-->

It's a decent enough romance with a relatively powerful female lead that still feels elegant. It's done pretty well. 
