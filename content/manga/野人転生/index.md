+++
title = "野人転生"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=162091"
raw = "https://comic-walker.com/contents/detail/KDCW_AM05200934010000_68/"
md = "https://mangadex.org/title/b4615bd9-7cbd-4243-a8a2-5c24671663e0/yajin-tensei-karate-survivor-in-another-world"
bw = "https://bookwalker.jp/series/234146"

[chapters]
released = 43.2
read = 0

[lists]
recommend = ""
+++



<!--more-->

