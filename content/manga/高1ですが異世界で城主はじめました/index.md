+++
title = "高1ですが異世界で城主はじめました"
title_en = ""
statuses = ["axed"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=120279"
raw = "https://firecross.jp/ebook/series/180"
md = "https://mangadex.org/title/18753/kou-1-desu-ga-isekai-de-joushu-hajimemashita"
bw = "https://bookwalker.jp/series/56564/"

[chapters]
released = 46
read = 43

[lists]
recommend = "D"
+++

Always fun to have two people isekai at the same time, but the 2nd dude is kind of pointless, and a lot of the plot points are kind of mediocre. Main selling point of this manga is BOOBA. Would rec for BOOBA. Alternatively follow the artist's [twitter](https://twitter.com/rikak)/[pixiv](https://www.pixiv.net/en/users/3650)/[whatever](http://galvas.net/).

<!--more-->
