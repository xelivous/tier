+++
title = "転生貴族 鑑定スキルで成り上がる"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169081"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666634269"
md = "https://mangadex.org/title/51376/reincarnated-as-an-aristocrat-with-an-appraisal-skill"
bw = "https://bookwalker.jp/series/275621/"

[chapters]
released = 113
read = 81

[lists]
recommend = "D"
+++

Protag dies and gets reincarnated in a medieval european fantasy world straight out of his strategy games he loved to play, and he also gets to quantify everybody's abilities just like his strategy games. The higher the numbers and the higher the stat multiplier, the better! So he just walks around finding people with high potential and multiplier and it all just kind of works out for him with no problems! 

<!--more-->

Mage girl is hot at least but her clothes are meme. 

The biggest question I have in this story is: where the fuck is the protag's mom in this story. The protag all of a sudden has younger brothers at one point, but at no point do you ever see who their mother is, and when {{% spoiler %}}the protag's dad starts dieing{{% /spoiler %}} their mother is still nowhere to be seen anywhere. It's utterly baffling.

He also gets an asspull powerup out of nowhere later on to give him a basic amount of helpful information to move the plot along. Instead of carefully observing his surroundings, making logical deductions, and doing a basic amount of research, he is instead just gifted the power of knowing background details about people he sees which will then carry him through basic negotations the moment he needs it so he can be perceived as even more of a cool competent guy. It's lazy writing at best.
