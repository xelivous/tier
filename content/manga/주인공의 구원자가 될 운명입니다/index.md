+++
title = "주인공의 구원자가 될 운명입니다"
title_en = "It's My Destiny to Be Hero's Savior"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-08-11
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/vfcwfde"
raw = "https://page.kakao.com/content/59494826"
md = "https://mangadex.org/title/dbcee2f0-2ea2-42ea-800d-7942e33840c8/it-s-my-destiny-to-be-hero-s-savior"

[chapters]
released = 109
read = 83

[lists]
recommend = "C"
+++

Protag gets reincarnated as a high ranking noble girl and lives until the age of 16, except she gets regularly tortured and experimented on to the point that the protag is an extreme socio/psychopath who doesn't understand relationships at all, but manages to reincarnate again to hopefully find a way out of her abusive parental relationship. She manages to get saved by legit good people who just wants her to be happy so a large portion of the early work is her healing her trauma of being abused. She also just introduces a breakthrough elixer and makes lodes of emone so that she'll never have a problem in the future, and sponsors the protag to go on his campaign of suffering so that he can be powerful enough to save the world because reasons.

<!--more-->

Biggest problem with this work is that it becomes melodramatic yandere boyfriend central the longer the work goes on, which I can understand the appeal of for certain demographics at least. While this work is largely about the protagonist slowly trying to build up a party of epic adventurers to tackle  the Grand Labyrinth, a majority of the work is spent having angsty scenes with the male leads instead. The protagonist is unfortunately kind of dumb at times and the only reason why the story kind of goes to shit is because of her lack of foresight (despite having full knowledge of both past/future at her hands).
