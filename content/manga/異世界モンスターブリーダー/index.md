+++
title = "異世界モンスターブリーダー"
title_en = ""
statuses = ["axed"]
demographics = ["seinen"]
furigana = ["none"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-03
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165476"
raw = "https://seiga.nicovideo.jp/comic/46482"
md = "https://mangadex.org/title/47296/isekai-monster-breeder"
bw = "https://bookwalker.jp/series/253816/"

[chapters]
released = 79
read = 79

[lists]
recommend = "D"
+++

Protag gets reincarnated by the goddess of beauty as a monster tamer and constantly lambasts him since "monster tamer is an utterly useless trash job and he's going to have such a terrible life in this new isekai which is fitting for a neet getting reincarnated serves him right!"; however he immediately throws his capture ball at the goddess and captures her, then he gets auto-summoned down into the isekai while still keeping her captured. He's been tasked with killing the demon lord so that he can eventually go back to earth and he even gets to retain his abilities, so his first order of priority is capturing as many hot babes in his pokeball as he can and bring them back to earth to have sexy harem time 24/7.

<!--more-->

The art is the main highlight of this work however even then a lot of the panels have blank solid color backgrounds so even that isn't much of a highlight. It's kind of a comedy but also not really, and it's more of a wish fulfillment shounen harem adventure manga and nothing more; the protag never struggles and just captures everything while letting other people do the work as more and more cute girls get enslaved by him and he can command every aspects of their life with no disobeyal. A cute glasses girl joins like 25 chapters in so i could maybe rec to tsukai at least. The chapters become shorter and shorter as the work goes on and the end of the work is kind of baffling and rushed. Don't really know who I would rec this to outside of the minor glasses appearances for tsukai.
