+++
title = "目覚めたら最強装備と宇宙船持ちだったので、一戸建て目指して傭兵として自由に生きたい"
title_en = ""
categories = ["isekai", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=161337"
raw = "https://seiga.nicovideo.jp/comic/44873"
md = "https://mangadex.org/title/8791c515-0245-4fcc-81cc-22863a0aba6b/mezametara-saikyou-soubi-to-uchuusen-mochi-datta-no-de-ikkodate-mezashite-youhei-toshite-jiyuu-ni-ikitai"
bw = "https://bookwalker.jp/series/245044"

[chapters]
released = 34.2
read = 34.1

[lists]
recommend = "B"
+++

Protag is an average salaryman who goes to sleep one day and the next he wakes up in the cockpit of a spaceship with no idea how he got there, but is vaguely similar to a game he occasionally plays (basically EVE online), and the spaceship he is in is the ship he bought in the game recently.

<!--more-->

What is an isekai anyways? Can you really guarantee that it's actually another world and not just a simulation that someone slapped the protag's concious into? Does it even matter? It has all of the standard isekai tropes of a Guild with ~~adventurer~~ mercenary ranks and constant harem antics and regular fights against ~~bandits~~ pirates. The constant harem antics are kind of annoying tbh but if you want to read an isekai adaptation of eve online this is your pick. Additionally it kind of feels like it handwaves some plotpoints away like the casual AI Uprising and subsequent status quo of them kind of just living together with humans instead of near infinitely propogating.

Most baffling of all is the comments on MU thinking this is based off of Elite Dangerous instead of Eve Online when the parody game is literally called Stella Online. Zoomers these days...
