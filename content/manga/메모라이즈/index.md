+++
title = "메모라이즈"
title_en = "Memorize"
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172303"
raw = "https://page.kakao.com/home?seriesId=55626889"
md = "https://mangadex.org/title/87acdf66-30a9-4800-8a03-754c64e382b1/memorize"
#bw = ""

[chapters]
released = 100
read = 100

[lists]
recommend = "D"
+++

So the first few chapters are abysmally bad; basically a totally nonsensical bang start that doesn't serve to kindle any interest in the series at all and barely even sets up the protag's "previous life". It's nice to have a full party/group but it's kind of annoying how they're all ultra-reliant on the protag and they basically have a parent/child relationship instead of being actual friends; this doesn't change no matter how far into the story you go. It's also rather disappointing how harem-y the story gets as it goes on. I personally like it a fair amount but there's too many bad parts strewn about the work and there's basically never a point where the protag has any hardship; he just tells the kids he's grooming what to do and they follow him.

<!--more-->

