+++
title = "マーダープリンセス"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-24
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=2697"
#raw = ""
md = "https://mangadex.org/title/84b654e4-7d53-4425-8f92-1cad0b2a9d05/murder-princess"
bw = "https://bookwalker.jp/series/3292/"

[chapters]
released = 11
read = 0

[lists]
recommend = ""
+++

<!--more-->
