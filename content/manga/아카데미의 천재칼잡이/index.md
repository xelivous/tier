+++
title = "아카데미의 천재칼잡이"
title_en = "The Academy's Genius Swordsman"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-03
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/iq7rqvq/the-academy-s-genius-swordsman"
raw = "https://series.naver.com/comic/detail.series?productNo=10002986"
md = "https://mangadex.org/title/633d470a-4146-4dd3-b841-93dd648c23a5/the-academy-s-genius-swordsman"
wt = "https://www.webtoons.com/en/fantasy/the-academys-genius-swordsman/list?title_no=5752"

[chapters]
released = 38
read = 23

[lists]
recommend = "C"
+++

Protagonist is an angsty edgelord fighting off against giant dieties that invaded his land, and after a long and bloody battle in which everybody is dead except for him he finalizes everything and kills the last of the dieties only to live alone as the sole protector left standing. However little did he know that the 3 dieties were nothing more than a scout party and the real force were coming later. The protagonist isn't discouraged and tries to rouse whatever is left of his strength in his dying breath, which causes the only other survivor that has given in to despair to bestow her power of time regression to go back and hopefully change the outcome in place of her after she attempted to change this outcome multiple times already.

<!--more-->

It's honestly not that bad. The protagonist is a bit of a cocky asshole considering he kept most of his techniques from his past life but he is still a shounen protagonist who fights for his friends in the end kind of. Sidenote the red hair shota who totally looks like a girl is extremely ambiguously hot as well.

Nothing particularly out of the ordinary that is worth reccomending this work over anything else but if you want to read about a dude self-righteously slashing dudes as he marches onwards towards the impending end of the earth it's an okay work.
