+++
title = "狼は眠らない"
title_en = ""
categories = []
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-08-14
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=154268"
raw = "https://www.kadokawa.co.jp/product/321810000560/"
md = "https://mangadex.org/title/9038211c-65b3-43b8-9471-9bda65049220/ookami-wa-nemuranai"
bw = "https://bookwalker.jp/series/213111"

[chapters]
released = 16
read = 0

[lists]
recommend = ""
+++


<!--more-->
