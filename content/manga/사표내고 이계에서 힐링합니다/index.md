+++
title = "사표내고 이계에서 힐링합니다"
title_en = "Resigning and Healing in Another World"
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-28
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/cqi5bo6/resigning-and-healing-in-another-world"
raw = "https://series.naver.com/comic/detail.series?productNo=8905038"
md = "https://mangadex.org/title/63605dd2-d8ca-4ff2-967f-9fc33d1dbd6f/i-ll-resign-and-have-a-fresh-start-in-this-world"
wt = "https://www.webtoons.com/en/fantasy/resigning-and-healing-in-another-world/list?title_no=5264"

[chapters]
released = 70
read = 48

[lists]
recommend = "C"
+++

Dude is your average corporate coder hating his life in the depths of depression after his parents died in a car accident when all of a sudden a portal to another world appears in his apartment. He can go back and forth freely through this portal, and he can set down a few more portals at locations of his choosing but they're limited in how many times he can do that. He then tries experiencing the Nice Country Life free from modern worries and just wants to kind of chill out in the isekai like a farmer instead.

<!--more-->

This is ryancore, and even has an elf wife. It's a relatively wholesome chungus manga about a dude just wanting to chill in the countryside after seeing the horrors of modern programming and corporate life. He does end up going along the meme nobility path treadmill and also just whips up food constantly that gets handwaved away but otherwise relatively solid.
