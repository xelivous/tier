+++
title = "닳고 닳은 뉴비"
title_en = "Worn and Torn Newbie"
categories = ["reincarnation", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-05-14
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/cvl4x5j/worn-and-torn-newbie"
raw = "https://page.kakao.com/content/56109623"
md = ""
#wt = ""

[chapters]
released = 187
read = 109

[lists]
recommend = "D"
+++

Protagonist plays a game well past its prime, constantly takes out loans from loan sharks to feed his addiction and tries to make all of his money back in this depreciating game, only to spend all of it on a single weapon that he then proceeds to break while enhancing (because it's a shit korean mmo). As a result he falls into the depths of despair and the loan sharks come after him. But he ends up getting another chance and goes back 15 years in time to when all of this started, because, reasons, and using his knowledge of all of the strats and future events he'll become the very best and take all of the unique legendary equipment before everybody else so that nobody can even touch him.

<!--more-->

Average korean vrmmo garbage where the protagonist is epic god emperor and everybody thinks he's super cool. There is no actual reason or premise for him to have been reincarnated/regressed and it just kind of happens out of nowhere. Nobody ever questions why he knows the things that he knows, The game balance is extremely terrible. Nothing really makes any sense. No battles really matter since the protag just steamrolls through everything since he already hyperoptimized everything offscreen in his previous life by reading the wikis from top players.

Only real merit to this work is that the main heroine(?) is kind of unconventional since she's made out to be the minor reoccuring villain for a majority of the work and the main instigator for why his life was ruined in his previous life, while she slowly gets redeemed(?) over the course of the story as being a victim of circumstance instead of someone actually evil. Somewhere around half of the work is focused on her character and she pops up constantly in every arc but typically in an antagonistic role, and the only way for the two are able to reconcile their differences is because the protagonist leads a double life as another person for most of the plot simultaneously. 

I would say overall it's not a particularly compelling or entertaining work but there's little bits of brilliance buried within the story that kept me reading instead of dropping it. Might be interested in reading another work by this author with a different setting/premise.
