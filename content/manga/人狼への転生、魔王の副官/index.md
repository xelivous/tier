+++
title = "人狼への転生、魔王の副官"
title_en = ""
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=141118"
raw = "https://comic.pixiv.net/works/3023"
md = "https://mangadex.org/title/38337/jinrou-e-no-tensei-maou-no-fukkan-hajimari-no-shou"
bw = "https://bookwalker.jp/series/110787/"

[chapters]
released = 37
read = 0

[lists]
recommend = ""
+++


<!--more-->
