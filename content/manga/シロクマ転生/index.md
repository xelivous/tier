+++
title = "シロクマ転生"
title_en = ""
statuses = ["hiatus"]
demographics = ["seinen"]
furigana = []
categories = ["isekai", "reincarnation"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-04-13
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144860"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000051010000_68/"
md = "https://mangadex.org/title/22322/shirokuma-tensei-mori-no-shugoshin-ni-natta-zo-dense"
bw = "https://bookwalker.jp/series/150081/"

[chapters]
released = 31
read = 24

[lists]
recommend = "C"
+++

The story follows a bunch of cute wolfgirls and also the protag who got reincarnated as a polar bear after climbing a mountain. Why is a polar bear in the middle of a random forest? Good question. He's also a really strong bear that can beat up just about anybody and doesn't really struggle ever. Either way, the protag saves the eldest of the wolfgirls while she was facing off against the Knight Scum that was chasing her family, and he starts living with them. As a result they become extremely horny for the protag and want the D, except the protag is like 28 and can't see them that way at all so you just get constant sexual tension/horniness that goes nowhere since the protag doesn't want anything to do with them (and he's a bear). 

<!--more-->

Major highlight of this work is the cute twinbraid wolfgirl. That's about all that I can recommend this work for. It also got put on indefinite hiatus and will likely never resume, so there's even less of a point in trying to read this. However if you fall into the mood of wanting to look at cute horny wolfgirls maybe it's okay.
