+++
title = "世界最強の努力家"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-18
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=173430"
raw = "https://www.comic-earthstar.jp/detail/doryokuka/"
md = "https://mangadex.org/title/bfe0ab22-61fd-4ba1-8667-40c516c2dddd/sekai-saikyou-no-doryokuka-sainou-ga-doryoku-datta-no-de-kouritsu-yoku-kikakugai-no-doryoku-o"
bw = "https://bookwalker.jp/series/287957/"

[chapters]
released = 20
read = 19

[lists]
recommend = "A"
+++

Protag lives in a world where you get assigned jobs/etc by god that kind of make up who you are, but the protag just "workin' hard" as his so he's looked down upon while his cute childhood friend has the strongest. He sets out to prove to everybody that he can work the darn tootenest and show them all he's the coolest ever for the sake of his cute osananajimi.

<!--more-->

The merchant girl is cute though tbh.

The inclusion of this work in this list is a minor spoiler in itself since you won't really understand why for a while but i'd say it's worth reading since it's cool. It's similar to other works that focus on the Meta behind gaining random skills, the sociology that comes about due to it, and the innate properties of Humans. The art can be a little bad at times but it's tolerable. I don't know if it fully adapts the source material or if there's stuff left after it but the manga itself is largely self-contained with an "ending". I do like this work a fair bit overall and it's short enough to not overstay its relatively simple plot.
