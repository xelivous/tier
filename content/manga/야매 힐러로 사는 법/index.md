+++
title = "야매 힐러로 사는 법"
title_en = "Life of a Quack Healer"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-04
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/xdb6oko/life-of-a-quack-healer"
raw = "https://series.naver.com/comic/detail.series?productNo=9676803"
md = "https://mangadex.org/title/e0a49848-1973-4f66-9616-4aab36a43463/life-of-a-quack-healer"
wt = "https://www.webtoons.com/en/fantasy/life-of-a-quack-healer/list?title_no=5557"

[chapters]
released = 50
read = 29

[lists]
recommend = "F"
+++

Protagonist wakes up in a cave in an isekai with a few other people, but he's a weird kind of healer without "healing spells" that can only do normal doctory things which causes him to have lots of issues in this isekai where doctors don't really exist! He also wasn't really a doctor before this so he's just kind of futzing along with doctor skills and throwing whatever works at people that need help. And almost immediately in the story he gets a "conventional" healing spell that involves consuming potions and spraying them out everywhere to heal in an AOE anyways.

<!--more-->

It's just the protag being smug and "i told you so"ing various people over and over since everybody except him is the most incompetent healer imaginable and only he is the epic god emperor of mankind able to save anybody and everybody.
