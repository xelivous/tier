+++
title = "오라버니가 너무 순해서 걱정이다"
title_en = "Don't Hire My Brother, Your Highness!"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2024-07-21
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=180312"
raw = "https://page.kakao.com/home?seriesId=57906830"
md = "https://mangadex.org/title/a9c03a3c-b1e8-4f2c-8336-976961530193/don-t-hire-my-brother-your-highness"

[chapters]
released = 91
read = 82

[lists]
recommend = "C"
+++

Protagonist is an average office worker working for a black company who overworks herself to death, only to find herself waking up as the sister of the supporting character to the male lead of a romance novel. The main premise of which is that the male lead slacks off to have dates with the female lead while his supporting character best friend is stuck doing all of the paperwork and actual work in the palace until the day he dies, for whatever reason. And since the protagonist died of overwork and is all for worker's rights, she will do anything in her power to prevent her sibling from having to work that hellish nightmare.

<!--more-->

This work is 70% doting on the protagonist, 20% romance, and 10% politics. The politics are braindead and more driven by emotion than logic. The romance takes a very long time to bloom and then immediately goes all in. Which basically just leaves this as work of family bonding and friendship with the few main characters. Roger in particular is the most interesting and complex character out of the entire cast which is pretty amazing considering the first impression he gives early on. Another benefit of this work is that the "original female lead" is nowhere to be seen in this work, and doesn't exist to fuck shit up later on, and she isn't a reincarnator or anything either. It's solely a mostly wholesome romance throughout the entire thing. It also has a fairly conclusive ending with decent pacing throughout.

That being said there's quite a few other works that have this exact identical premise but also have better execution/plot/art/etc, so it's hard to generally recommend this.
