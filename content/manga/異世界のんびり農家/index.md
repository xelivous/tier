+++
title = "異世界のんびり農家"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-08-28
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=145283"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01000051010000_68/"
md = "https://mangadex.org/title/879af0bb-ce30-47e4-a74e-cd1ce874c6e3/isekai-nonbiri-nouka"
bw = "https://bookwalker.jp/series/151065"

[chapters]
released = 240
read = 153

[lists]
recommend = "C"
+++

Another 'slow-life' series that isn't really all that slow. It primarily focuses on village-building and the protag has all of the deus ex machina abilities to handle everything that comes at him with no issues; there's no conflict that isn't immediately solved. 99% of the village members are women that want to fuck the protag and eventually end up having his children. The chapters are relatively short but releases every other week and every chapter has a good amount of text in it so it's not that bad.

<!--more-->
