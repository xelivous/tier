+++
title = "내게 종말은 게임이다"
title_en = "The End is a Game to Me"
categories = ["game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-28
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/5knu2ca/the-end-is-a-game-to-me"
raw = "https://series.naver.com/comic/detail.series?productNo=9565051"
md = "https://mangadex.org/title/ec03df55-26de-46c8-98b7-c8df9efda27f/the-end-of-world-is-a-game"
wt = "https://www.webtoons.com/en/action/the-end-is-a-game-to-me/list?title_no=5715"

[chapters]
released = 53
read = 28

[lists]
recommend = "B"
+++

The protagonist is a hikki streamer who streams to a single viewer every time, and is even willing to do dumb things on stream for the tiniest of donations while playing whatever random game of the week. While playing a zombie game and having his character die while doing something dumb for a donation, he ends up getting a weird comment from his only viewer asking if he enjoys streaming to a single viewer, to which the protagonist answers "yes". Next thing the protagonist knows the world is on fire and zombies are all over the place IRL, and he has a floating chat window following him around along with some basic game systems popping up; The protagonist has become the main character of a zombie action rpg.

<!--more-->

It's a standard zombie apocalypse story except the protag regularly has a livestream chat (gods) laughing at him for doing dumb things.

The nurse is good.
