+++
title = "転生王女は今日も旗を叩き折る"
title_en = ""
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148942"
raw = "https://bookwalker.jp/series/203603/"
md = "https://mangadex.org/title/35281/the-reincarnated-princess-strikes-down-flags-today-as-well"
bw = "https://bookwalker.jp/series/156281/"
web = "https://arianrose.jp/comic/?series_id=73"

[chapters]
released = 67
read = 20

[lists]
recommend = "B"
+++



<!--more-->

Okay, this work has a manga version and a webtoon version of it that's borderline the same story except the manga is typical monochrome and the webtoon butchers the panelling but adds color instead. I only ever read the manga version, but both are available in JP on different sites, but apparently the only official english translation of this is of the webtoons version lmao (there's also an old dead fan translation of the manga but haven't checked that out either). Bizarre work.
