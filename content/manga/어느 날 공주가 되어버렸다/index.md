+++
title = "어느 날 공주가 되어버렸다"
title_en = "Who Made Me a Princess"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150780"
raw = "https://page.kakao.com/home?seriesId=51880368"
md = "https://mangadex.org/title/722a45c0-5e55-40f2-929b-ff69b0989edb/who-made-me-a-princess"
bw = "https://bookwalker.jp/series/209753"

[chapters]
released = 125
read = 125

[lists]
recommend = "B"
+++

This is a fairly standard shoujo work primarily about a father/daughter relationship, and "family" in general. There's a tiny bit of romance but it doesn't really go into that ever, and mostly sticks to the aforementioned father/daughter relationship throughout the entire work, along with trying to uncover/resolve the root of the problems of why the protag dies in the first place. There isn't really an overarching plot beyond that, and it's almost exclusively a character-driven/emotional work. According to the post-credits of the last chapter, despite being based on a novel a good portion of this work was changed/manwha-original as well.

<!--more-->
