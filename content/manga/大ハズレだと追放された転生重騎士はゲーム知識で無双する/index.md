+++
title = "大ハズレだと追放された転生重騎士はゲーム知識で無双する"
title_en = ""
categories = ["isekai", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=196742"
raw = "https://yanmaga.jp/comics/大ハズレだと追放された転生重騎士はゲーム知識で無双する"
md = "https://mangadex.org/title/7c5ee2ae-5bed-4e72-8d75-91e3b8e9787a/the-exiled-reincarnated-heavy-knight-is-unrivaled-in-game-knowledge"
#bw = ""

[chapters]
released = 59
read = 51

[lists]
recommend = "C"
+++

Protag grows up as the son of a noble but constantly has dejavu throughout his life, and upon being able to choose his class when coming of age he realizes he has his previous life's memories and that he's in a VRMMO. As a result he ends up choosing his favorite class, but it's an extremely niche class that is considered to be completely dogshit in this world since nobody is going to go through the effort of building it up like autists online would when there's the threat of dying. As a result he gets exiled from his family and sets off to become the strongest with his favorite class.

<!--more-->

The faces are kind of bizarre at times and a lot of the interactions are kind of subpar. The protag has encyclopedic knowledge of the game world he is in and manages to find the exact items he needs to make his OP build, and also gets a convenient party member with ultra broken loot drops for even more rare drops exactly when he needs them. If you want to see someone blast through an MMO by basically following random guides online then it's probably fine.
