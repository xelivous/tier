+++
title = "異邦人、ダンジョンに潜る。"
title_en = ""
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156905"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04201257010000_68/"
md = "https://mangadex.org/title/41850/ihoujin-dungeon-ni-moguru"
bw = "https://bookwalker.jp/series/237223/"

[chapters]
released = 12
read = 12

[lists]
recommend = "D"
+++


<!--more-->
