+++
title = "愛弟子に裏切られて死んだおっさん勇者、史上最強の魔王として生き返る"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["every-other-month"]
lastmod = 2023-09-29
tags = [
    "male-protagonist",
    "human-portagonist",
    "demon-lord-protagonist",
    "former-hero-now-demon-lord",
    "humans-are-evil",
    "ecchi",
    "magic",
    "beach-chapter",
    "obligatory-japanese-land",
    "harem",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=170103"
raw = "https://gammaplus.takeshobo.co.jp/manga/manadeshini/"
md = "https://mangadex.org/title/e23e9bc7-8495-4d9b-9320-0c4bfb68b14c/the-betrayed-hero-who-was-reincarnated-as-the-strongest-demon-lord"
bw = "https://bookwalker.jp/series/286220/list"

[chapters]
released = 15.1
read = 10.2

[lists]
recommend = "D"
+++

Protagonist is an epic badass hero dude who was tasked with defeating the demon lord, and he brings along his disciple to give him some backup, but after defeating the demon lord he ends up getting betrayed by said disciple since they wanted to have the spotlight for defeating the demon lord and he gets killed!! However the demon lord's ressurrection skill bugs out due to this and all of her power gets transferred to the protagonist instead making him even more OP. And to make matters worse, the humans were the evil ones all along and it's the poor weak defenseless demons that are being pushed back and they just want to live in peace despite the protagonist constantly being told that they were the evil ones!!!

<!--more-->

Dude's straight up hakuowlo.

It personally doesn't click with me at all; I can't get into whatever plot it has and I don't care for the characters. Basically just hetare protag with a harem happy funtimes while the protag obliterates people occasionally but never wants to kill anybody since he just wants peace.
