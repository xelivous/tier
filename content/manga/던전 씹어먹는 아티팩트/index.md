+++
title = "던전 씹어먹는 아티팩트"
title_en = "Dungeons & Artifacts"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-04-30
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/yeclv15/dungeons-artifacts"
raw = "https://comic.naver.com/webtoon/list.nhn?titleId=758439"
md = "https://mangadex.org/title/2dc7984a-b6dc-493c-aec1-1b33ab7c686a/dungeons-artifacts"
wt = "https://www.webtoons.com/en/action/dungeons-and-artifacts/list?title_no=2575&page=15"

[chapters]
released = 149
read = 149

[lists]
recommend = "D"
+++

Protag is an Explorer class with no real combat abilities but makes up for it with astounding dungeoneering skills, and is hired by a large noble task force to clear one of the oldest dungeons on the continent. However as soon as they make their way to the end of the dungeon and reach the artifact, the protagonist gets tricked into touching the artifact and gets cursed (/killed) as a result. However while the artifact did kill him, it also revived him by taking away a majority of his skills and stats giving him another chance to get revenge and keep his life. The caveat is that the protagonist needs to stockpile enough mana to keep the artifact going, and unfortunately they're almost out and he only has 12 hours left to live.

<!--more-->

The start of the work sets up the 12 hour thing as some impending doom that will hang over the protag but then it immediately gets rid of that plot point by like the next chapter and he gets almost all of his skills back a few chapters after that, making this mostly pointless and just a power fantasy of the protag getting the most broken artifact possible with no downsides and becoming broken OP almost overnight.

For a work that is 90% epic fights the fights just kind of suck and are boring to read; There's very little planning or hype moments in them, the protag just bulldozes through without thinking every time. The art is servicable at best. There's regularly wonky mistakes in the lineart, the coloring is average, and the backgrounds just generic 3d scenery that just kind of exist. However I do have to give credit that every panel does in fact have backgrounds and most of them are fairly detailed, which is a result of them actually just using 3d environments for everything. There's a lot of works that just slap in random solid color backgrounds with lazy compositing constantly or just action lines/etc as filler but this work never really dips that low ever. Additionally every chapter is fairly long and never really gets any shorter as time goes on.

The main highlight of this work is the elf wife that just kind of sticks around with him for no reason and doesn't really question what he does too deeply. The work regularly forgets to loop the elf in on things so you're left with awkard situations where he has to backtrack and have the elf say shit like "you know i've never thought about it before but an artifact that has sentience is weird!" and then it just moves on without doing anything or acknowledging anything. Shoutout to the random dude who joins their party at near ch100 with the most contrived reason ever.

I could see a story like this working fairly well but the execution of this work in particular leaves a lot to be desired. There's a lot of plot threads but most of them are discarded quickly after barely touching upon them, the characters don't feel particularly compelling, the battles have no strategy to them, the dungeons aren't particularly interesting or have any depth to them and very little is actually shown of them, etc. All in all, would rec if you want to periodically look at a cute booba elf every once in a while but can't really rec beyond that unfortunately. At least it's not offensively bad, just disappointing.
