+++
title = "フシノカミ～辺境から始める文明再生記～"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2025-01-28
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/9zcfajt/fushi-no-kami-henkyou-kara-hajimeru-bunmei-saiseiki"
raw = "https://comic-gardo.com/episode/3269754496561191448"
md = "https://mangadex.org/title/576f3eec-a728-4f36-a87f-dd3fc2342812/fushi-no-kami-rebuilding-civilization-starts-with-a-village"
bw = "https://bookwalker.jp/series/308894/"

[chapters]
released = 38.2
read = 23.6

[lists]
recommend = "C"
+++

The protagonist has memories of a previous life after reincarnating as a poor boy in a random village without any entertainment, but desperately wants to read books. Conveniently a priest recently moved into town that has a whole bunch of books, and manages to convince the priest to lend him a book to read after much cajoling. After months of reading as many books as possible and learning the language so quickly, the priest gives the protag an opportunity to help try to decipher the book of an ancient civilization that the priest was obsessed with to the point that he got sent all the way out to the boonies after showing no results for.

<!--more-->

The protag is fairly annoying with how preachy he is in the first chapter, and the ancient writing system being "kanji" is extremely eyeroll. Bruh the end of volume1 and the protag's response to being stranded in the wilderness is kind of dumb.

This work is basically Dr. Stone except the setting is post-apocalyptic honzuki where the protag loves books more than anything but also has infinite books to read, while everybody is extremely wholesome chungus (outside of the one dude who almost kills the protag); a work dedicated to teaching kids how basic inventions throughout periods of civilization are made that isn't just hand pumps for the 1000th time. Basically the "primitive technology" videos in manga form. It does require you to suspend your disbelief a bit considering that the world has all of these books around that people are mostly actively reading but nobody has managed to piece 2 and 2 together to make 4, when the world clearly had a really advanced civilization before and should at least maybe have some ruins somewhere of this civilization as well. Even if 99% of the world was wiped out through a demon apocalypse there should at least be some ruins if a lot books detailing that time period still exist, right? At least the protag doesn't have encyclopedic knowledge of how to create everything and instead needs to research (using all of these incredibly detailed books that are lying around in libraries) how to make/create all of these industries from scratch again.
