+++
title = "그녀의 소환수"
title_en = "Her Summon"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/5skp6yy"
raw = "http://www.toomics.com/webtoon/episode/toon/4724"
md = "https://mangadex.org/title/27b5a8ba-818c-413f-9b50-ef74e4cbf62c/her-summon"

[chapters]
released = 61
read = 61

[lists]
recommend = "C"
+++

so far it's a drugtrip. It's going hard on the protag's extreme social anxiety and bleak family life after becoming a hikki from that event, and it balances it with the absurdity of the escapism into another world and having every mundane action the protag takes to be over the top OP similar to Kage no Jitsuryokusha in a way. The artist starts to get horny after a certain point and just throws in some weird ecchi panels more and more frequently as i'm reading it so i don't know if it will just go fully downhill or if it will improve

<!--more-->

art is good though

also probably actually the first time i've seen a black guy in a korean isekai webtoon and he's chad

okay finished it. tldr is that the first half of the work feels like the author had a basic idea but didn't know what to do with it other than make something absurd, and then the 2nd half tried to make a proper story out of it since it probably got popular. the oversexualization of some of the women is too much at times as well. The art hard carries the work.
