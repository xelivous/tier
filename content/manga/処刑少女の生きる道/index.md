+++
title = "処刑少女の生きる道"
title_en = ""
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=168091"
raw = "https://magazine.jp.square-enix.com/yg/introduction/syokei/"
md = "https://mangadex.org/title/bed89b51-15d3-44a5-b27a-a13a2f9a6dd8/shokei-shoujo-no-ikirumichi"
bw = "https://bookwalker.jp/series/287476"

[chapters]
released = 25
read = 0

[lists]
recommend = ""
+++



<!--more-->

