+++
title = "転生幼女はあきらめない"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-30
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=167605"
raw = "https://magcomi.com/episode/13933686331650787935"
md = "https://mangadex.org/title/63706448-66b5-4665-b549-b6ad6d568afc/tensei-youjo-wa-akiramenai"
bw = "https://bookwalker.jp/series/266745"

[chapters]
released = 39
read = 0

[lists]
recommend = ""
+++



<!--more-->

