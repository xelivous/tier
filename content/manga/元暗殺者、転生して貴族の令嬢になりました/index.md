+++
title = "元暗殺者、転生して貴族の令嬢になりました"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-30
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/c02jucr"
raw = "https://pash-up.jp/content/00001835"
md = "https://mangadex.org/title/0a29b87c-f6eb-4e6d-98b4-3cd8adb3ab8f/moto-ansatsusha-tensei-shite-kizoku-no-reijou-ni-narimashita"
bw = "https://bookwalker.jp/series/438442"

[chapters]
released = 8.1
read = 0

[lists]
recommend = ""
+++



<!--more-->

