+++
title = "イキリ勇者は救えない！"
title_en = ""
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["partial"]
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-03-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=176984"
raw = "https://ganma.jp/ikiri"
md = "https://mangadex.org/title/301bc2f1-a338-4eb2-bad6-d714df3d8802/ikiri-yuusha-wa-sukuenai"
bw = "https://bookwalker.jp/series/280072/"

[chapters]
released = 47
read = 7

[lists]
recommend = "D"
+++

This is a comedy manga, and it might've been interesting if it was an anime, but i'm not sure how well it works in manga format... The sense of humour is just completely off and doesn't work at all. I'm not sure how desperate you would need to be to be able to power through all 47 chapters of this, but i'm sure this sense of humour clicks with at least someone out there.

<!--more-->
