+++
title = "航宙軍士官、冒険者になる"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166643"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19200711010000_68/"
md = "https://mangadex.org/title/44553/the-galactic-navy-officer-becomes-an-adventurer"
bw = "https://bookwalker.jp/series/208811/"

[chapters]
released = 44
read = 44

[lists]
recommend = "S"
+++

The protagonist is an officer in the intergalatic human resistance fighting against the common enemy threat when one day the ship he is on is attacked by an unknown enemy and they get stranded in space with no way to communicate back or receive help. As a result he ends up getting sent down to a nearby planet with limited resources and he has to fend for himself. Thankfully he ends up managing to find long lost remnants of human civilization on this planet and they are seemingly in the medieval age, and even have a strange technology that is called Magic.

<!--more-->

An interesting take on fantasy isekai where it's actually "sci-fi" (cue wareya saying they're the same thing). Handled pretty competently although it kind of speedruns some of the "boring" sections and skims over them, but handles foreign languages decently enough. The pacing/flashbacks in the beginning is kind of awkward though. Overall I enjoy the work and it's a fairly unique entry since even though it has all of the standard narou isekai tropes it does all of it completely differently than normal.
