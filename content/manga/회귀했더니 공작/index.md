+++
title = "회귀했더니 공작"
title_en = "I Regressed As The Duke"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-23
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=199984"
raw = "https://page.kakao.com/content/59262003"
md = "https://mangadex.org/title/dcd5b8ae-d7e8-453c-b052-6a68064f0572/i-regressed-as-the-duke"

[chapters]
released = 62
read = 40

[lists]
recommend = "D"
+++

Okay so a butler that served the duke reincarnates as the duke, after witnessing how much the duke fucked up his life, so he's given a chance to "do it better" with all of his future knowledge and his OP powers. He curbstomps tons of people and gets help from everybody because he's so cool.

<!--more-->
