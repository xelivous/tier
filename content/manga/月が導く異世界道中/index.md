+++
title = "月が導く異世界道中"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=123153"
raw = "http://www.alphapolis.co.jp/manga/official/48000051"
md = "https://mangadex.org/title/16105/tsuki-ga-michibiku-isekai-douchuu"
bw = "https://bookwalker.jp/series/74772/"

[chapters]
released = 86
read = 86

[lists]
recommend = "B"
+++

Due to a long-standing contract with his family he got summoned to the goddess of another world to be a hero in an isekai but he's so ugly (by the standards of that world) that he gets dropped in the middle of demon territory and left to die instead. As a result of having parents from this isekai and living in the harsh environment that is earth he's actually super strong in this isekai as a result so he doesn't have too many issues regardless.

<!--more-->

The work is part city-building, part the protagonist slowly building up enough strength to slap the goddess who condemned him to a corner of the world while being extremely naive. There isn't really a grand overarching plotline other than the protag just doing whatever the hell he wants while all of the people around him are baffled at how strong he (and everybody with him) is.
