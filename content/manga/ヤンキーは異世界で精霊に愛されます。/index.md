+++
title = "ヤンキーは異世界で精霊に愛されます。"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2021-10-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=140113"
raw = "https://www.alphapolis.co.jp/manga/viewOpening/462000159"
md = "https://mangadex.org/title/46889536-a4c3-400e-a892-804d0dc879dc/yankee-wa-isekai-de-seirei-ni-aisaremasu"
bw = "https://bookwalker.jp/series/142269"

[chapters]
released = 42
read = 42

[lists]
recommend = "A"
+++

This is runefactory without the farming.

<!--more-->
