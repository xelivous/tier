+++
title = "ルパン三世 異世界の姫君 ネイバーワールドプリンセス"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-13
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=187511"
raw = "https://seiga.nicovideo.jp/comic/56765"
md = "https://mangadex.org/title/775979ab-feb9-4f86-a574-3492efb995f4/lupin-iii-neighbor-world-princess"
#bw = ""

[chapters]
released = 36
read = 19

[lists]
recommend = "D"
+++

kind of ryancore but also I don't think it's particularly good. Seems to be riding on using the wellknown characters and then just throws in random garbage.

<!--more-->
