+++
title = "悪役令嬢は隣国の王太子に溺愛される"
title_en = ""
categories = ["reincarnation", "isekai", "game"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-15
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146998"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00200168010000_68/"
md = "https://mangadex.org/title/d7100239-9327-4a2d-af1d-8f48f0facfea/the-villainess-is-adored-by-the-crown-prince-of-the-neighboring-kingdom"
bw = "https://bookwalker.jp/series/164266"

[chapters]
released = 48
read = 24

[lists]
recommend = "C"
+++

It's a cute romance series.

<!--more-->
