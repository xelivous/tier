+++
title = "加護なし令嬢の小さな村"
title_en = ""
categories = []
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-30
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=167359"
raw = "https://comic-walker.com/contents/detail/KDCW_EB03201580010000_68/"
md = "https://mangadex.org/title/5af60d6c-8b8e-4ca3-bc10-3046543c1614/the-small-village-of-the-young-lady-without-blessing"
bw = "https://bookwalker.jp/series/265211"

[chapters]
released = 32.2
read = 0

[lists]
recommend = ""
+++



<!--more-->

