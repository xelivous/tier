+++
title = "ぼくは異世界で付与魔法と召喚魔法を天秤にかける"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2025-01-12
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/f74nwpj/boku-wa-isekai-de-fuyo-mahou-to-shoukan-mahou-wo-tenbin-ni-kakeru"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19202558010000_68/"
md = "https://mangadex.org/title/bd22ac44-3832-4f35-b0d5-9b69d60cb472/boku-wa-isekai-de-fuyo-mahou-to-shoukan-mahou-wo-tenbin-ni-kakeru"
bw = "https://bookwalker.jp/series/344663/"

[chapters]
released = 34
read = 21

[lists]
recommend = "D"
+++

The protagonist gets bullied in school hard enough that he actual sets up a pitfall outside his boarding school to trap his bully in along with a sharpened bamboo spear in an effort to kill the bully and then kill himself afterwards. However when acting out his plan a mysterious earthquake happens and an orc falls into the trap instead which upon being killed transports the protagonist into a weird white room where he can allocate stats/skills. He then kills a few other orcs around with his newfound power, until he comes across a girl being raped by an orc and eventually saves her (after waiting until the last minute, partially due to trust issues). He then opts to actually trust the girl, allow her to level up by boosting her with another orc, and she then falls in love with him almost immediately and wants his children. They then set out to save as many people as possible, but most people are already dead and the only ones left are a few girls who have been getting ceaselessly raped by the orcs for the day they've been grinding out levels.

<!--more-->

This is trash, I'm not sure it's even particularly well-done trash either, but it is at least fascinating enough that I don't feel like it merits a full on F tier rec. On one hand it's a lazy wish fulfillment work where the protag is surrounded and depdended on by a ton of cute girls his age and he gets to act out being the Alpha Male, while he has sex with his main wife on the side and kills some orcs. On the other hand it handles PTSD/trauma decently well and doesn't actually turn it into a full on harem (yet?) with the protag only gunning for the main heroine and keeping everybody else at arms length. His classmate is a fairly complex character as well using both her PTSD of being raped by orcs and understanding of the protag to try and solidify her position while knowing he's too much of a hetare to really do anything especially when he already has his waifu. It tries to stay relatively realistic by not making all of them epic baddasses who can do anything (outside of the main heroine) and most of them are only hanging on by fighting through their trauma at best or using it to persevere.

I have read lots of "ero trash" in this subgenre and I feel like this is probably the best of the worst, for whatever that is worth. The main issue is that the plot progresses too quickly in terms of the relationships but it also progresses insanely slowly outside of that. Like the first 20 chapters alone is just repeating the same core loop over and over where the protag just keeps getting a larger and larger group of girls to tag along with him while he's fighting off orcs while staying behind commanding them as a "support mage". The pacing is quite honestly terrible and only exacerbated by this being a monthly manga. After reading a fair bit of this, I feel like the author focuses a little too much on the wish fulfillment and I kind of want a version of this work that uses the scenario/setting in a more mature way.
