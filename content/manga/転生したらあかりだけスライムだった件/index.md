+++
title = "転生したらあかりだけスライムだった件"
title_en = ""
categories = []
demographics = []
statuses = []
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-08
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=165675"
raw = "https://seiga.nicovideo.jp/comic/51933"
md = "https://mangadex.org/title/a52fde59-7cd3-433b-8a44-3b2ec46dd87f/that-time-only-akari-got-reincarnated-as-a-slime"
bw = "https://bookwalker.jp/series/317034/list"

[chapters]
released = 0
read = 0

[lists]
recommend = ""
+++



<!--more-->

