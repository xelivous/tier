+++
title = "2周目の大魔導士は近接魔法で無双する"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = ["weekly"]
lastmod = 2025-01-21
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/oyaq3a2/2-shuume-no-daimadoushi-wa-kinsetsu-mahou-de-musou-suru"
raw = "https://manga.line.me/product/periodic?id=S140412&t=1729395038988"
md = "https://mangadex.org/title/deb0cef0-4129-4899-939c-6f044589e2ea/regression-of-the-close-combat-mage"
wt = "https://www.webtoons.com/en/fantasy/regression-of-the-close-combat-mage/list?title_no=6780"

[chapters]
released = 48
read = 22

[lists]
recommend = "F"
+++

The evil death god suddenly appears and basically obliterates humanity, and the protagonist is nearly helpless to prevent humanity's doom despite being the top supremo magician of all time. However he was given a magical artifact by his teacher that allows him to undo one regret in his life and just before he dies he goes back in time to when he was first choosing his specialization, because that's the only way to be able to eventually defeat the god. After returning to the past he opts to spec into physical/melee magic instead of ranged/aoe magic as if this was path of exile with a super large skill tree that's annoying to respec, everybody shuns him for "wasting his limitless potential" on this absolutely useless branch of magic.

<!--more-->

Why is this dude in ch3 so comically antagonistic bro. Dude decides to grindset instead of just telling his teacher/headmaster that he's from the future, which the headmaster would clearly know about it. Goes to magic school, teachers antagonize him, teach him nothing, gets put into a meme duel against a dude who's been training his entire life to this style of magic, still wins because he's the most epic chad to ever chad. Why the fuck does he hide this shit from the headmaster bro. Dude the fight in ep11 is the worst thing I have ever had to misfortune to read. At least he finally talked with the headmaster and wasn't a complete dingus.

Honestly the magic system and reasoning for him going back in time to choose physical/melee magic just makes no sense. "Oh yea if it's close range it will allow you to bypass magic nullifcation because it's so quick" like just drop an entire meteor on him with "ranged" dude. black hole the guy.
