+++
title = "魔拳のデイドリーマー"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-01-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=134483"
raw = "https://www.alphapolis.co.jp/manga/viewOpening/87000136"
md = "https://mangadex.org/title/0eefe0f3-ecd6-4766-8ad5-55efd0bee50b/maken-no-daydreamer"
bw = "https://bookwalker.jp/series/102674"

[chapters]
released = 61
read = 47

[lists]
recommend = "C"
+++

The work seemingly wanted to do something along the lines of tackling the subject of being indifferent to those around you, treating everything like a fantasy story/manga like you'd normally see in isekai works, and then making various parallels to other people in the story etc. But along the way it fails at that and just becomes a weird story about an oretueee dude. I respect that the author's fetish seems to be glasses, and being glared at by girls who may or may not be wearing glasses, since glasses are fairly rare in these isekai-ish worlds where you can just cure anything with light magic and not be afflicted by Poor Sight.

<!--more-->
