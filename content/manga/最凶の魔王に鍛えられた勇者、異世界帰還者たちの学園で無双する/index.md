+++
title = "最凶の魔王に鍛えられた勇者、異世界帰還者たちの学園で無双する"
title_en = ""
categories = ["post-isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["light novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2023-10-25
tags = [
    "male-protagonist",
    "multiple-isekaiers",
    "gods-on-planet",
    "hyperbolic-time-chamber",
    "demon-lord-as-pet",
    "demon-lord-wife",
    "school-arc",
    "purposefully-hiding-powerlevel",
    "useless-goddess",
    "human-transformation",
    "arbitrary-magic-system",
    "powerful-unique-skills",
]

[links]
mu = "https://www.mangaupdates.com/series/ijjdf61"
raw = "https://youngchampion.jp/series/4dca94f408517"
md = "https://mangadex.org/title/900b1790-4d86-4030-b410-00db69df4771/saikyou-no-maou-ni-kitaerareta-yuusha-isekai-kikanshatachi-no-gakuen-de-musou-suru"
bw = "https://bookwalker.jp/series/399501/list"

[chapters]
released = 19.2
read = 12

[lists]
recommend = "B"
+++

Protag is someone who has returned from an isekai, and gets invited to a school that is full of people who have returned from isekais, and the protag is accompanied by a super sus cat! Apparently the protag is a part of a super select few students who are separate from the others, but it's not because the protag is an epic badass (he totally is though), it's because he doesn't have a "mark of completion" on his hand stating that he killed his world's demon lord yet still returned. I wonder who the cat could be!!!!

<!--more-->

The start of this work is kind of annoying since the actual start is like at the end of chapter 2 or beginning of chapter 3, and the entire 1st chapter/etc is just way in the future. Terrible pacing honestly.

Beyond that it's a relatively fun spin on the post-isekai genre where society tries to deal with the fact that hundreds of kids return from various worlds with their powers intact.
