+++
title = "完全回避ヒーラーの軌跡"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["partial"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154019"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000160010000_68/"
md = "https://mangadex.org/title/36773/kanzen-kaihi-healer-no-kiseki"
bw = "https://bookwalker.jp/series/223172/"

[chapters]
released = 43
read = 42

[lists]
recommend = "C"
+++

Protag is just chilling playing soshage when all of a sudden he's isekai'd along with two other random people. The protag ends up being a healer class which is highly desired, and his healing stat is off the charts super strong. The protag thinks defense is the wrong choice for a healer since you'll waste heals defending your own HP and thus uses his starting stat points to go all into evasion since his healing stat is already basically maxed. And upon saying he threw all of his stats in evasion everybody yells loudly at him calling him a dumbass, and throws him in jail before he even gets a chance to say his healing stat is already overly broken since he was hiding it thus far. But well the king/etc are dumb anyways so may as well just break out and go on his own anyways, right? haha lol.

<!--more-->

The extremely bizarre hatred towards evasion, along with the lack of the protag ever kind of clarifying to anybody that his healing skill is off the charts becomes annoying quickly. There's a relatively cute pseudo-twinbraid elf later on though; she's a great character, top tier even, if only she had full twinbraids instead of weird halfbraids. Beyond the issues with the evasion stat being meme'd on throughout this series, and the author not really understanding evasion stats in videogames in the first place, it's actually pretty enjoyable overall with some good focus on the relationships between the protag and the other two people who get isekai'd along with, and the archer girl who also meme'd her build.
