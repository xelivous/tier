+++
title = "더 해머"
title_en = "The Hammer"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-04-14
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/r0x6i5t/the-hammer"
raw = "https://series.naver.com/comic/detail.series?productNo=9180636"
md = "https://mangadex.org/title/40881c1c-9bd7-425c-959e-a64799522434/i-picked-a-hammer-to-save-the-world"
wt = "https://www.webtoons.com/en/action/the-hammer/list?title_no=5382"

[chapters]
released = 75
read = 47

[lists]
recommend = "D"
+++

The protagonist is a brainless meathead who worked his way up from the slums to be one of the top knights in the world that is suffering from calamity after calamity, only to eventaully meet his demise after fighting one of the mid-bosses that shows up. However shortly before he dies one of the older noble knights recognizes his talent and uses his artifact to send the protagonist back in time to possibly change the outcome of humanity and get stronger from properly training. However the point in time he was sent back to was directly after the biggest tragedy he faced of losing his sister when he was younger and relents the fact that it wasn't even a few days earlier to prevent that from happening.

<!--more-->

This is extremely painfully shounen with a dumb protagonist who hits before he thinks and didn't care to learn about any of the events that happened in the "past" so that he can now prevent them now that he's gone back in time. He existed solely to bulk up and swing bigger hammers in his previous life, and as a result the protagonist only knows how to beat people up and ask questions later. The antagonists are comically evil and the protagonist is terrible. The art is decent but there's better battle-shounen to be found in manwha. 
