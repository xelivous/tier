+++
title = "野生のラスボスが現れた！"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
categories = ["isekai", "reincarnation"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-16
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144017"
raw = "http://comic-earthstar.jp/detail/wildlastboss/"
md = "https://mangadex.org/title/21944/yasei-no-last-boss-ga-arawareta"
bw = "https://bookwalker.jp/series/140903/"

[chapters]
released = 45
read = 40

[lists]
recommend = "S"
+++

On one hand, having the protag do a minor freakout that he's in a female body now is kind of dumb if they're just going to immediately drop it and do nothing with it at any point in time, but on the other hand if they didn't at least do that there will be people who complain that he did nothing so whatever. I do like the overall setting at lesat. Ch17 and beyond is good; before that is good too but it gets like a lot better at that point. and i'm not saying that simply because ch19 has a cute twinbraid okay. At that point it starts delving very deep into the setting and the characters; It's a well fleshed out series with actual thought put into it, which is sadly kind of rare in the isekai sphere... extremely kino...

<!--more-->

The main issue with this work is that it has an absolutely glacial release pace; like one chapter every few months if you're lucky. It's not Hunter X Hunter tier but still hard to recommend because of that pacing.
