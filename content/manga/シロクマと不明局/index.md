+++
title = "シロクマと不明局"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=98209"
raw = "https://houbunsha.co.jp/comics/detail.php?p=%A5%B7%A5%ED%A5%AF%A5%DE%A4%C8%C9%D4%CC%C0%B6%C9"
md = "https://mangadex.org/title/041478a2-66b2-4f88-8895-eb07bc9bb248/shirokuma-to-fumeikyoku"
bw = "https://bookwalker.jp/series/44795"

[chapters]
released = 23
read = 0

[lists]
recommend = ""
+++



<!--more-->

