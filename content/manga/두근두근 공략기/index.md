+++
title = "두근두근 공략기"
title_en = "Heartbeat Conquest"
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-04
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/lg9jtc6/heartbeat-conquest"
raw = "https://www.peanutoon.com/ko/comic/detail/4923"
md = "https://mangadex.org/title/61ed2b2d-2df7-481c-b45d-4a5ad3c2cb96/heartbeat-conquest"
wt = "https://www.webtoons.com/en/romance/heartbeat-conquest/list?title_no=5268"

[chapters]
released = 131
read = 40

[lists]
recommend = "B"
+++

Protagonist stays up all night playing otome harem games and eventually dies in her sleep only to wake up in the most recent otome harem game she played, with the ability to see the heart meters of everybody person imaginable including dogs. She then sets out to have an epic harem but she's kind of a massive sociopath/psychopath who has murderous urges so things don't work out super well!

<!--more-->

The transgender/crossdressing portion of this work is ultra kino based poggers but the rest of the work is kind of lackluster and it also kind of handwaves a portion of that part almost immediately afterwards which greatly sours whatever impact it had.

Probably one of the best female-oriented harem works that i've read although i'm not really the target demographic so it's hard for me to get super into it. Decent rec if you want a competent female protagonist doing everything she can to make a harem with ikemen/twinks I guess.

The work seems to be completed in korean but the english translation is barely a third of the way done at this point so it'll be interesting to see where it goes as the story continues on. Will probably revisit it in half a year or so.
