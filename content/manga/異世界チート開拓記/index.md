+++
title = "異世界チート開拓記"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158813"
raw = "https://futabanet.jp/list/monster/work/5dd4f8437765616c32070000"
md = "https://mangadex.org/title/e3edf5e4-09bf-480e-9655-dcedbc4e79ff/isekai-cheat-kaitakuki"
bw = "https://bookwalker.jp/series/242973/"

[chapters]
released = 28.1
read = 11

[lists]
recommend = "D"
+++

Protag is tired with life, his parents die in an accident, then spends his life living as a neet until he eventually dies at a young age, only to get reincarnated with super OP magic powers. Okay, but why does his like 9 year old imouto have massive booba (and also not related by blood, haha lol). There's some rly good twinbraids tho.

<!--more-->

The work itself is kind of bizarre since the protagonist has super high MP levels from training since he was a baby (similar to mushoku) but he's generally incompetent at magic and can't control his powers since it's not like he's a genius or anything; he's still just a dumbass neet. There's also a few chapters told from the perspective of an airheaded fairy spirit who regularly watches over the protag like a stalker. The work also starts heading deeply into scifi after a bit. Except it uses that setting to give the protag a harem of battle sex dolls.
