+++
title = "行き倒れもできないこんな異世界じゃ"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=168120"
raw = "https://comic-walker.com/contents/detail/KDCW_EB03201657010000_68/"
md = "https://mangadex.org/title/578ca54a-0aaa-4928-8dd1-219a706299f9/another-world-where-i-can-t-even-collapse-and-die"
bw = "https://bookwalker.jp/series/288900"

[chapters]
released = 12
read = 0

[lists]
recommend = ""
+++



<!--more-->

