+++
title = "ネタキャラ仮プレイのつもりが異世界召喚"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-12-25
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=162818"
raw = "https://seiga.nicovideo.jp/comic/44865"
md = "https://mangadex.org/title/b7021a00-f8d8-45f0-b36b-a1555950b9b3/neta-chara-kari-play-no-tsumori-ga-isekai-shoukan-mayoibito-wa-josei-no-teki-ni-ninteisaremashita"
bw = "https://bookwalker.jp/series/240520"

[chapters]
released = 21
read = 11

[lists]
recommend = "F"
+++

<!--more-->
