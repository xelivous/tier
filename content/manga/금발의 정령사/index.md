+++
title = "금발의 정령사"
title_en = "The Golden-Haired Summoner"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["axed"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["every-10th-day"]
lastmod = 2025-01-26
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/j2zfdh3/the-golden-haired-summoner"
raw = "https://page.kakao.com/content/55326413"
md = "https://mangadex.org/title/da89755c-f069-48b1-90d4-cebf5f0b2a54/the-golden-haired-summoner"
#bw = ""

[chapters]
released = 108
read = 108

[lists]
recommend = "A"
+++

The protagonist is disgruntled at her parents hounding on her to study and regularly getting mad at her for problems well outside her control, like any typical highschool student (although perhaps even worse for korean/asian students in general) soon to take college entrance tests. However on her way home from school the bus she's in has an accident and wakes up in the body of a noble girl in an isekai. However despite her best attempts to live out an easier life she still ends up being deemed a genius (as it's rather hard to hide all 17 years old knowledge in the form of a baby/toddler) and gets shipped off to the prestigious magic school for geniuses regardless. However she will never stop trying to live out a far comfier life this time instead.

<!--more-->

This has easily the best first chapter that I've read in this entire isekai/reincarnation journey. It is worth reading this work for the first chapter alone regardless of anything else. Most of the chapters after it aren't anything too special but there's a turning point some ways into the work where it finally regains some of that brilliance and then finally fastforwards a bit with a timeskip. And that's the point where the work really starts to pick up and become something fairly special. It follows a chaotic neutral protag that simply does whatever she wants without really any kind of moral compass since she's just sick of everyone's shit. Death penalty for anyone doing heinous acts, death penalty for anybody who slights her, death penalty for all evildoers. Fuck letting the badguys live, they don't deserve mercy.
