+++
title = "해골병사는 던전을 지키지 못했다"
title_en = "The Skeleton Soldier Failed to Defend the Dungeon"
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2024-07-26
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149218"
raw = "https://page.kakao.com/home?seriesId=54097394"
md = "https://mangadex.org/title/d993f789-e7e5-4832-92fd-37614220b427/skeleton-soldier-couldn-t-protect-the-dungeon"

[chapters]
released = 290
read = 272

[lists]
recommend = "B"
+++

Okay so if rezero is a roguelike with savescumming, this work is like a roguelite instead. The protag loops and loops but he gets to keep his stats/skills and even gets to unlock some extra features or NPCs to bring back to his savepoints depending on the progress done on the branches, and while he does typically become OP enough to stomp upon that which thwarted him in his previous attempts, there's still tons of ultra powerful instant death enemies that can easily wreck his shit regardless since they too transcend the spacetime continuum in their own ways. The work kind of just branches off in literally every possible direction with an excessive amount of Epic Twists that it's impossible to tell where the story is even headed since it keeps dropping random shit constantly everywhere slowly unveiling the grand scheme of the world and the meaning of life. It's a pretty cool manga with some good moments that plays around with its meta elements of the System decently but goes a little too hard on stat spam at times.

<!--more-->

Season 1 and 2 are kino. Season 3 is good for most of it but starts to drag on near the end when it focuses on a separate character for most of it. Then season 4 just gets totally lost in the sauce and becomes a different work entirely that loses all of the charm and with of the previous 3 seasons.
