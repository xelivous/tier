+++
title = "Re: ゼロから始める異世界生活"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=111686"
raw = "https://comic-walker.com/contents/detail/KDCW_MF02000011010000_68/"
md = "https://mangadex.org/title/13364/re-zero-kara-hajimeru-isekai-seikatsu-daiisshou-outo-no-ichinichi-hen/chapters/"
bw = "https://bookwalker.jp/series/153825/"

[chapters]
released = 0
read = 0

[lists]
recommend = ""
+++

<!--more-->

