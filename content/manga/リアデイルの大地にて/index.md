+++
title = "リアデイルの大地にて"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=164562"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19201033010000_68/"
md = "https://mangadex.org/title/b6b87e4d-c1cc-4d0b-8588-df715dd98624/in-the-land-of-leadale"
bw = "https://bookwalker.jp/series/241361"

[chapters]
released = 29
read = 0

[lists]
recommend = ""
+++



<!--more-->

