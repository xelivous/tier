+++
title = "転生した大聖女は、聖女であることをひた隠す"
title_en = ""
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = ["none"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [
    "female-protagonist",
    "story-starts-as-adult",
    "regain-memories-after-traumatic-event",
    "taming-monsters-through-blood",
    "tamed-overpowered-creature",
    "saintess-protagonist",
    "torture",
    "purposefully-hiding-powerlevel",
    "brainlet-protagonist",
    "intelligent-protagonist",
    "protagonist-in-military",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=159849"
raw = "https://www.comic-earthstar.jp/sp/detail/daiseijo/"
md = "https://mangadex.org/title/50692/the-reincarnated-great-saint-hides-that-she-s-a-saint"
bw = "https://bookwalker.jp/series/243878/"

[chapters]
released = 40
read = 15

[lists]
recommend = "B"
+++

Protagonist gets betrayed by her family/party when subjugating the demon lord since they thought she was useless since she was just a buffer/helear and they did all of the real work by doing the actual fighting (lol?) and ends up getting tortured in the demon lords castle until she eventually dies because reasons. Then she reincarnates 300 years later as a girl in a household of knights that go through a ritual to become adults, and while out on the ritual she comes across a cat who she tries healing with the potion meant to heal her, but it's actually an evil beast/dragon and it damages it, mistakes the healing potion as an attack, and attacks the protagonist. During this attack she then realizes that she was reincarnated and ends up actually healing the dragon who then forms a contract with the protagonist for saving its life, and to protect her. The protagonist doesn't want to be killed again like in her previous life so she will try to hide the fact that she is the reincarnation of the saintess as best as she can from now on.

<!--more-->

The protagonist is an extreme brainlet but also randomly smart at times. Honestly this is the most enigmatic-written female protagonist in any work in this list. The protag is extremely competent but also a complete dumbass in a good way, she's cute but also basically an ossan on the inside at times, etc. Chapter 6 in particular is kino and so is chapter 15.
