+++
title = "悪役令嬢になりました"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["josei"]
statuses = ["axed"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158159"
raw = "https://www.alphapolis.co.jp/manga/official/127000286"
md = "https://mangadex.org/title/b64a0204-24fe-4ee1-b5e7-5c737ed05497/i-became-a-villainess"
#bw = ""

[chapters]
released = 7
read = 7

[lists]
recommend = "F"
+++

Protag gets truck'd and reincarnates as a chubby freckled villainess in the otomege that the protag loves playing, and the god is just casually developing a world based off of that game that loops indefinitely but has tons of oddities and one of the characters even killed themselves, hence the offer to reincarnate. 

<!--more-->

As the protag gets prettier they once again remove the freckles so you don't even get 1 whole chapter of them. The pacing is very fast and regularly timeskips, and then ends abruptly and the characters don't really have any time to be developed so it's overall unsatisfying. The art/panelling in this work is pretty good at least. Honestly not a whole lot to say about it when this work is so short that nothing is even really there to begin with. Can't even really recommend it to anybody unless you're already going out of your way to read every villainess work imaginable and for that you don't even need a rec.
