+++
title = "ヤンデレ系乙女ゲーの世界に転生してしまったようです"
title_en = ""
categories = ["reincarnation", "isekai", "game"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148230"
#raw = ""
md = "https://mangadex.org/title/317837f6-229e-422c-95f3-33b817d74b93/yandere-otome-game"
bw = "https://bookwalker.jp/series/150472"

[chapters]
released = 45
read = 45

[lists]
recommend = "B"
+++

Protag reincarnates into the world of an otome game where all of the heros are yanderes, and the villainess she reincarnated is insane and also kills the heroine in various routes. However since the protag is just a pure-hearted office worker from japan she doesn't want anything to do with this insanity and wants to avoid all of these weird events as much as possible. She largely does this by simply being there for them and caring for them while being a normal person.

<!--more-->

Once the game starts for real it becomes trying to figure out what the game's heroine is trying to do, and trying to deal with the fact that her fiance/husbando might be stolen from her in the worst case scenario. The work also kind of blasts through the main plot of the game, probably faster than it really should, as if it was a simply reading a summary of an otome game with tons of timeskips. There's still more content after the end of the game, like trying to figure out the intent of why she was reincarnated in the first place, if there was some god who wanted to see events play out in a certain way, etc. It also has some weird insane plot threads tacked on as well.

This work has a decent amount of similarities to hametsu flag so if you're already familiar with that work it might be interesting to cross reference and see how this work does certain events differently. However the ending of this work is kind of :pepemeds: insane, although ti does still end satisfactorily enough.
