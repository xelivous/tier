+++
title = "ひとりぼっちの異世界攻略"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-07
tags = [ 

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152610"
raw = "https://comic-gardo.com/episode/10834108156661709864"
md = "https://mangadex.org/title/34387/lonely-attack-on-the-different-world"
bw = "https://bookwalker.jp/series/219183/"

[chapters]
released = 186
read = 181

[lists]
recommend = "C"
+++

Protag is a "loner" who reads about isekais in light novels in his spare time and doesn't want to interact with his High Spec classmates, when all of a sudden a magical summoning circle appears in his classroom. As fun as it would be to get summoned to another world he'd rather not because it seems like it would be a pain in the ass, so he quickly absconds only to still get wrapped up it in anyways. However because he tried to escape he gets separated from everybody else and ends up in a pseudo-state, and for some reason the skills are first-come-first-serve and the protag only gets the weird leftover skills that everybody else neglected. And because god felt bad for him he was given all of the turbo trash skills instead of only being able to choose some of them. Included in those trash skills is something that forces that protag to be a loner hiki-neet so he ends up having to be a loser in this world as well, oh no!!

<!--more-->

ひとりぼっち my ass. Protag quickly gets a harem of girls and tons of friends/followers, and he also powers up excessively quickly obtaining skills 100x faster than everybody else, and he also just constantly gets broken items left and right which allows him to do anything. The work also focuses on weird tsukkomi comedy in just about every page/panel to the detriment of just about everything else.

ch142 is bad. it's basically just an excuse to have the girls dress up in waitress outfits. ch169 is also pretty brainlet doge tier.

Ultimately this is an incredibly shouneny-shounen where the loner protag who doesn't fit in wins with the power of friendship.
