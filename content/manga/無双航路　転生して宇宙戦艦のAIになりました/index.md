+++
title = "無双航路　転生して宇宙戦艦のAIになりました"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2022-04-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172126"
raw = "https://seiga.nicovideo.jp/comic/48011"
md = "https://mangadex.org/title/e92214c5-7ec2-4e29-91c7-c73ced18251d/unparalleled-path-reincarnated-as-the-ai-for-a-space-battleship"
bw = "https://bookwalker.jp/series/258158/"

[chapters]
released = 15
read = 9

[lists]
recommend = "C"
+++

Protag gets reincarnated as a space battleship's AI, crew members start to freak out about rogue Ai but the loli captain decides to keep him alive regardless, and then the protag dabs on all of the other ships with his advanced human AI thinking. If you want to see some space battles it's probably pretty good for that at least even though explosions in space are meme.

<!--more-->
