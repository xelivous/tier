+++
title = "転生した悪役令嬢は復讐を望まない"
title_en = ""
categories = []
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=174856"
raw = "https://magcomi.com/episode/13933686331761434251"
md = "https://mangadex.org/title/4c3fc172-fab6-4477-9dfc-57dd95bad19c/the-reincarnated-villainess-doesn-t-want-revenge"
bw = "https://bookwalker.jp/series/298457/"

[chapters]
released = 28
read = 9

[lists]
recommend = "C"
+++

I feel jebaited with the protag having twinbraids for most of first chapter and then she never has it again. Protag kind of just gets endlessly roped along too. I appreciate the meme furigana sprinkled about occasionally.

<!--more-->
