+++
title = "くじ引き特賞：無双ハーレム権"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-08-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152564"
raw = "http://seiga.nicovideo.jp/comic/37985"
md = "https://mangadex.org/title/7898980b-1b23-4910-ac59-50f83c493b71/kujibiki-tokushou-musou-harem-ken"
bw = "https://bookwalker.jp/series/199003"

[chapters]
released = 49.2
read = 34

[lists]
recommend = "D"
+++

Protag wins the super ultra secret deluxe massive prize from a random shopstall lottery, and gets brought into a weird interdimensionary room where he gets offered to pull another meme lottery where you can get epic skills and then get transferred to an isekai, except since he's the super ultra prize winner he gets the change to roll the lottery as much as he wants until he gets a skill he wants. Infinite rerolls gacha resemara! As a result he gets his stats multiplied by 777. He then gets isekai'd, punches some bulls, saves a booba princess, makes lodes of emone, and immediately buys a cute slave.

<!--more-->

Excessive meme sex with girl-of-the-month series. Protagonist dabs on everybody with his 777 skill and then fucks a new member of his harem, on repeat. You get like one sex scene per girl, and then he moves onto the next; no repeats. When he runs out of (capable) girls the plot moves on to find more girls to add to his harem and have a single sex scene with.

