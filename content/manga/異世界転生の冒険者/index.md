+++
title = "異世界転生の冒険者"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150166"
raw = "https://magcomi.com/episode/10834108156766407698"
md = "https://mangadex.org/title/33616/isekai-tensei-no-boukensha"
bw = "https://bookwalker.jp/series/189726/"

[chapters]
released = 54
read = 35

[lists]
recommend = "F"
+++

Protagonist dies young and is haunting his funeral surrounded by his family crying over is death when a god from another world pops in and offers the protagonist to isekai with abilities/etc in order to save their planet, and if he acts now the god will even throw in a wish for free! The protagonist was skeptical of the god's offer but a free wish was just the thing to ease the protagonist's worries, thus immediately accepted the shady offer for his soul and asked for every one of his loved ones to slowly lose their memories of him so they aren't sad over his death anymore. He then gets blessed by every god in that isekai, along with obtaining all of the strongest abilities and class, because all of the gods just adore how caring and delightful he is. The protagonist then gets reincarnated as a baby abandoned in the woods and gets adopted by some random adventurer couple strolling on by in the forest, then immediately timeskips 10 years into the future where he's no longer a baby.

<!--more-->

The beginning of this work is turbo garbage. The art is extremely abstract and rough at times to the point that it becomes pretty hard to follow along, the pacing is turbo rushed and keeps skipping over events constantly, the protagonist is so broken OP that nothing really matters at all. Honestly I think this entire manga was quickly sketched out with like a black ballpoint pen and then a few bits of shading were added later, which I guess is Unique but not necessarily in a good way; it's like taking someone's draft and trying to polish it enough to publish without like, actually drawing proper lines or bothering to refine it at all. The backgrounds are also basically nonexistent or if they do exist they're just barely touched up 3D renders.

And then there's the story itself. It starts out introducing his family and a happy home life while quickly giving some basic backstory on how epic and cool his adventurer parents are and how the protagonist just wants to chill in his little village and enjoy being around his parents, but strange events keep happening where powerful monsters are showing up left and right when they shouldn't be. Only to culminate in the dumbest Tragedy Tag plotline where the protag gets literally deus ex machina'd to survive, and then it immediately timeskips afterwards to the future yet again where he's met 3 catgirls who want his dick for some reason out of nowhere. The protag even has a convenient magic bag he created that could house everybody and everything inside of it to protect everybody safely and uses it constantly after the Tragedy, but never actually used it when he needed it most to prevent the Tragedy. It's little more than an extremely terrible meme plot device. He doesn't even go back to the king that offered him a position at all and just chills around in a dinky little village being an adventurer.

\> Protag defeats a ton of bandits out on a quest  
\> Immediately gets questioned by big scary knight dudes who skeptical that a little boy and 3 (cat)girls could take down the big bad bandits when *they* couldn't  
\> Their captain is a little girl, so clearly they should know to not judge people by appearances, and even the captain is slightly skeptical the protag could take them down  

??? Is the author retarded? Protag even gets 2 slave girls out of nowhere for no reason randomly and the 3 catgirls just disappear.

At least(?) there's a cute twinbraid girl in ch23+ but they're drawn as poorly as the rest of the work that they barely even register as twinbraids.

Horrible work I hate it thanks.
