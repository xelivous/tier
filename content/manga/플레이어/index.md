+++
title = "플레이어"
title_en = "Player"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166965"
raw = "https://comic.naver.com/webtoon/list?titleId=745876"
md = "https://mangadex.org/title/6994b74a-2ecd-4a54-8d23-d2adff5e0b0c/player"

[chapters]
released = 142
read = 24

[lists]
recommend = "F"
+++

it's the most shouneny shounen about a weakling that's bullied but who has a pure soul who gets isekai'd and then goes up the tower with the power of friendship and asspulls. the art is good at least. It's possible it gets better later on but i'm not that interested in finding out if that's true or not.

<!--more-->
