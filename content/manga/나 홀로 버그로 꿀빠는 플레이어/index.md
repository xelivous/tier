+++
title = "나 홀로 버그로 꿀빠는 플레이어"
title_en = "Solo Glitch Player"
categories = ["isekai", "reincarnation", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-04
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165369"
raw = "https://page.kakao.com/home?seriesId=54748024"
md = "https://mangadex.org/title/737af1eb-a79c-48fa-8a00-70968e20de32/solo-bug-player"

[chapters]
released = 120
read = 41

[lists]
recommend = "F"
+++

Protag no-lifes a game until he dies, then reincarnates into a super obese noble dude with a super loyal maidwaifu and magical vegetables that taste like shit but rapidly reduce weight so that he can easily become a chad with a harem despite starting off as super fat. Since the protag no-life'd the game he knows everything there is to know about everything and easily breezes through without any struggles. Also the art in season 2 becomes shit so I stopped reading it there.

<!--more-->

