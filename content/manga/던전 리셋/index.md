+++
title = "던전 리셋"
title_en = "Dungeon Reset"
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = ["isekai"]
sources = []
languages = ["korean"]
schedules = []
lastmod = 2023-01-06
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=163555"
raw = "https://page.kakao.com/home?seriesId=54471153"
md = "https://mangadex.org/title/45903/dungeon-reset"

[chapters]
released = 141
read = 136

[lists]
recommend = "C"
+++

A whole bunch of people including the protag get abducted into a weird game-like dungeon, where they're told they have to try to survive against all odds and reach the end. Except the protag has a "useless" purify skill and no combat skills to speak of, so he gets bullied by a lot of the combat-oriented types for being "useless". He then ends up falling into a spike pit while they were running away, and somehow manages to narrowly avoid the instant death properties of the pit (due to plot armor), while his friends abandon him since they're still getting chased by monsters and the protag is assumed dead. His friends then end up fighting and defeating the final boss of the first stage and the floor administrator heals all survivors, which causes the protag to remain alive at the last moment, while his friends leave the stage and the dungeon starts to reset (the work's title haha lol). Because the dungeon reset he is now a glitch in the system who isn't affected by the normal rules of anything, and desperately tries to survive without any combat skills.

<!--more-->

A large portion of the work is the protag essentially sequence breaking and playing with the systems of the dungeon/game to survive. It's a somewhat entertaining work in that regard although him constantly getting random buffs from "achievements" almost exactly when he needs them is kind of lackluster. Could recommend to someone who wants something like a broken speedrun in manwha format. However it starts to focus too much on food/cooking that it starts to get annoying personally. It's decent enough to pass the time until you get bored of it eventually at least.
