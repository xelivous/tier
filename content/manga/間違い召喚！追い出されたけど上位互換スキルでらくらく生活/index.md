+++
title = "間違い召喚！追い出されたけど上位互換スキルでらくらく生活"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series/lw9lw8u"
raw = "https://www.alphapolis.co.jp/manga/official/860000437"
md = "https://mangadex.org/title/64537bd9-3ba8-4b0c-86fb-19059d3539a3/machigai-shoukan-oidasareta-kedo-joui-gokan-skill-de-rakuraku-seikatsu"
#bw = ""

[chapters]
released = 21
read = 8

[lists]
recommend = "D"
+++

"sorry your initial status is trash i wanted an OP protag"  
"but all of my skills are OP even if they look bad initially"  
"no"

<!--more-->

Nice to see this artist again at least.