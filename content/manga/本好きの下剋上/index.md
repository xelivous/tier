+++
title = "本好きの下剋上"
title_en = ""
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = ["none"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=79185"
raw = "http://seiga.nicovideo.jp/comic/18228"
md = "https://mangadex.org/title/18488/honzuki-no-gekokujou-shisho-ni-naru-tame-ni-wa-shudan-wo-erandeiraremasen-dai-1-bu-hon-ga-nai-nara-tsukureba-ii"
bw = "https://bookwalker.jp/series/78265/"

[chapters]
released = 48
read = 0

[lists]
recommend = "B"
+++

Having read the LN and watched the anime and also read this manga, I would recommend you just read the LN instead and only check this out if you still want to read more of the series. The way this series is being adapted from the LN means you will spend the next few decades reading this work if it doesn't get axed in the meantime, and they're simultaneously adapting most of the major parts so that you can't even really follow it linearly either until the preceding part finishes. This makes it extremely awkward to actually read through and it's almost exclusively designed to be read by fans of the LN.

<!--more-->

myne is kind of annoying as a protagonist, but the world building and all of the other characters are good.