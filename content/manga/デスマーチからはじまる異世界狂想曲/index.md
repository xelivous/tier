+++
title = "デスマーチからはじまる異世界狂想曲"
title_en = ""
categories = ["isekai"]
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-04-22
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=116685"
raw = "https://comic-walker.com/contents/detail/KDCW_FS02000014010000_68/"
md = "https://mangadex.org/title/14850/death-march-kara-hajimaru-isekai-kyousoukyoku"
bw = "https://bookwalker.jp/series/42937/"

[chapters]
released = 100
read = 95

[lists]
recommend = "F"
+++

Protag is a coder working at a black company who spends all day working on an mmo browser game, and a few other games as well since why not multitask on many projects at once? As a result of constant excessive overwork and regularly not sleeping after staying up for days at a time, the next time he went to sleep he instead died and woke up in the game he was developing! He immediately gets ambushed, but he has the hastily developed "beginner helper skills" that he developed right before he passed out which he manages to use to turn the tides around.

<!--more-->

it handwaves away way too fucking much, skips over tons of actual interesting content, and then you're mostly left with 'food of the week' and other banal shit. The protag is too excessively broken compared to everybody else to the point that it's not even interesting. It teased some large overarching plot relatively early on with some weird voodoo memories the protag had but then never touched upon it again for ages afterwards. The protag has a massive harem (of lolis) and despite them being super thirsty he doesn't want any of that, so it's just a constant annoying back and forth of 'please let me suck your dick protagkun', 'no', 'pien'. I do appreciate the other reincarnator along with him but at the same time she's kind of pointless. It takes a lot of effort to not just constantly frodopost while reading this.

One merit is that it doesn't typically oversexualize any of the harem members; the combat-oriented folks have proper armor that makes sense and isn't overly ecchi in any way. Only exception to this is the noble ojousama desuwa with breasts bigger than her head that the protag constantly stares at every time they flop around.
