+++
title = "二度目の人生を異世界で"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=137168"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000021010000_68/"
md = "https://mangadex.org/title/20163/nidome-no-jinsei-wo-isekai-de"
bw = "https://bookwalker.jp/series/90838/"

[chapters]
released = 49.3
read = 49.3

[lists]
recommend = "C"
+++

Protag gets reincarnated without his memories of his past life but he does have all of his skills/experiences intact for the most part, gets de-aged to about 18 years old, and as a result can just blast fools with his superior japanese blade skills. While deciding where to go when trying to leave his starting forest he hears a woman cry out in the distance, immediately runs to their rescue, and absolutely destroys some dudes. Unfortunately one of the character's name is literally "Femme Fatale", which is the most meme part about this work.

<!--more-->

Aside from that it's pretty good; protag is a psychopathic battle junkie who kind of just gets roped into dealing with tons of random shit. The work leans fairly ecchi overall but it doesn't overwhelm the action scenes which is the main highlight of this work. Everything in this work related to the elves is incredibly based.

Unfortunately the work kind of just ends out of nowhere and the ending isn't particularly satisfying; Definitely feels like it got axed.
