+++
title = "The Dungeon Seeker"
title_en = ""
statuses = ["completed"]
demographics = ["shounen"]
sources = []
furigana = []
categories = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=133843"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/445000143"
md = "https://mangadex.org/title/19141/dungeon-seeker"
bw = "https://bookwalker.jp/series/108464/"

[chapters]
released = 33
read = 33

[lists]
recommend = "C"
+++

Relatively short revenge manga that is nothing but edgy revenge.

<!--more-->

