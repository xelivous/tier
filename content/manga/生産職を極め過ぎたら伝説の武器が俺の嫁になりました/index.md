+++
title = "生産職を極め過ぎたら伝説の武器が俺の嫁になりました"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=168575"
raw = "https://seiga.nicovideo.jp/comic/47264"
md = "https://mangadex.org/title/fe840e8f-1267-4611-869b-9ab290dc205b/the-legendary-weapon-became-my-bride-when-i-overwhelmed-the-production-job"
bw = "https://bookwalker.jp/series/267462"

[chapters]
released = 57
read = 9

[lists]
recommend = "F"
+++

Dude's a game tester and implements shitty broken weapons on the test server that never get implement in game, then gets to live out his fantasy in his isekai world based on the game with his epic broken weapons and cheat skills yay.

<!--more-->
