+++
title = "僕の部屋がダンジョンの休憩所になってしまった件"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-18
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143855"
raw = "https://webcomicgamma.takeshobo.co.jp/manga/bokudun/"
md = "https://mangadex.org/title/6e018caf-86c6-4740-b1db-d5a040430d2b/boku-no-heya-ga-dungeon-no-kyuukeijo-ni-natteshimatta-ken"
bw = "https://bookwalker.jp/series/137721"

[chapters]
released = 40
read = 40

[lists]
recommend = "D"
+++

Protag is looking for an apartment and gets offered an insanely cheap one that apparently has slimes/goblins/etc appear in it but signs up anyways because yolo it's cheap. And apparently the front entrance is a doorway to another world if you exit out of it (but he can get back to japan if he just like, exits out of the floor-level window I guess)!! And the first thing he does is come across a cute female knight about to be violated by goblins after being made defenseless from paralyzation, who wets herself in terror over the protag's face/language, and of course he kidnaps her (brings her back to his room) and falls asleep on her thighs after she's wet herself, then imagines ecchi sex scenes when she wants to repay him for bringing her to a safe location. Truly a peak protag. 

<!--more-->

This series is largely just slice of life ecchi scenes with no real plot. Events happen with no real explanation, and a lot of the work is spent just drawing his slowly built up harem in ecchi scenes while introducing random modern facilities/clothing to them since the protag can just go out whenever to buy whatever he needs at the supermarket without really any danger. The booba elf is good but you're prob best off just looking at booba elfs on exhentai. There isn't even romance in this story so you can't see cute romance scenes with the booba elf so what's the point.

There's a spinoff(?) alternate version of this work that is less ecchi and a little more focused with slightly different characters and focuses on romance a little more but it no longer has the booba elf prominently so it's shit, and is also only 12 chapters long...
