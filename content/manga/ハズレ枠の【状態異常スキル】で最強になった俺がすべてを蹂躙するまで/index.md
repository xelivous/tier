+++
title = "ハズレ枠の【状態異常スキル】で最強になった俺がすべてを蹂躙するまで"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-08-13
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155369"
raw = "https://comic-gardo.com/episode/10834108156661738245"
md = "https://mangadex.org/title/39571/hazure-waku-no-joutai-ijou-skill-de-saikyou-ni-natta-ore-ga-subete-wo-juurin-suru-made"
bw = "https://bookwalker.jp/series/230059/"

[chapters]
released = 52.2
read = 52.1

[lists]
recommend = "C"
+++

Protag gets summoned along with with his entire class, but the protag has a bottom-tier rank so the goddess just slaps him aside and casts him out, and all of his classmates also cast him out since he's a loser who doesn't fit in anyways (except the moe class prez). As a result he gets teleported to the endgame dungeon to die, because reasons. The only skills the protag has are status effects, except they're apparently 100% effect chance and extremely potent and people can't remove them so he actually has a super broken rank!!! Also the protag was abused by his parents when he was a child!!

<!--more-->

It's like someone took all of the bad parts of arifureta and didn't understand why people liked it. Though I do actually like the plot involving the pinnacle of humanity, the chaddest of chads. A large portion of the work is spent having the protag slowly surround himself with good-natured people who have also gotten abused/tricked by deceitful people, while being something of an anti-hero and feigning a persona of being Evil while seeking his own justice. The actual revenge plot and turbo-suffering at the beginning of the work is a little too outlandishly meme and ultimately detracts from the rest of the work which is moderately enjoyable. Kind of an elf-wife-core work, but not reading for jus that alone.
