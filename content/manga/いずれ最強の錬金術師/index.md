+++
title = "いずれ最強の錬金術師"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=167634"
raw = "https://www.alphapolis.co.jp/manga/official/443000228"
md = "https://mangadex.org/title/17b4a134-65a5-4ba9-851e-9cc824755bf5/someday-will-i-be-the-greatest-alchemist"
bw = "https://bookwalker.jp/series/197173"

[chapters]
released = 52
read = 41

[lists]
recommend = "D"
+++

Middle-aged protag gets accidentally isekai'd alongside 3 other random japanese kids but the goddess intervenes before he's actually summoned, but also can't send him back, so she gives him OP skills and a young body along with tons of other things as an "apology" then sends him off to the world anyways. As a result he's just a completely broken OP bishounen.

<!--more-->

It has just about every trope of a japanese isekai in it, including the obligatory Hand Pump meme, toilets, baths being only for nobility but the protag goes out of his way to make one, the adventurer's guild that exists solely to give the protag an ID for entry into a city, an evil church, slaves, etc. The fact that there's a type of slaves that can just be brought in by bandits without the person doing anything wrong and they're stuck being a slave indefinitely without ever having any recourse of being freed is absolutely dumb as hell.

He heals the elf, meets up with the slaver later on and she doesn't even comment on the helf being healed...

Oh my god he becomes wishy-washy about killing a dude, opts to try and kill a dude anyways, then gets literally banned by the goddess from being able to kill dudes all in the span of a chapter to solidify his weakwilled isekaijin-ness.

Feel like the highlights of this work are the elf-slave being the ikemen bf type (despite being a woman) and thus the male protag ends up being essentially the princess/damsel as a set piece with her, along with the other human slave just shipping this reverse ship constantly. I also like a majority of the human slave's outfits he cooked the designs. Actually the real highlight is the wolfgirl slave in ch40+; if she had twinbraids she would be 11/10 easily (although the fact that she's a debt slave, when those were originally excluded from the initial slave buy for Plot Reasons is kind of weird).
