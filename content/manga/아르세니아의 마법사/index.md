+++
title = "아르세니아의 마법사"
title_en = "Wizard of Arsenia"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=164666"
raw = "https://comic.naver.com/webtoon/list.nhn?titleId=741458"
md = "https://mangadex.org/title/b632dd7f-f166-45f5-b141-71f0d8b14efe/wizard-of-arsenia"

[chapters]
released = 88
read = 88

[lists]
recommend = "D"
+++

Dude is fucking bored in school, wants to get isekai'd, and then he gets isekai'd. He gets ultra powered up by the dragons he got isekai'd by on a whim, and gets told to just power up with the macguffins scattered around the world so he can eventually return home after powering up a ton. Along the way he meets tons of babes and makes them swoon for him while blasting everybody to pieces with his super OP dragon training.

<!--more-->

The work went on seemingly indefinite hiatus after season 1 and i don't really care to read it whenever season 2 might come out. Also the art style has that annoying rudolph tumblr nose shit.
