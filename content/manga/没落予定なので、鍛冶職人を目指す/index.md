+++
title = "没落予定なので、鍛冶職人を目指す"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation", "game"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-12

[links]
mu = "https://www.mangaupdates.com/series.html?id=141701"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01000044010000_68/"
md = "https://mangadex.org/title/21325/botsuraku-yotei-nanode-kajishokunin-wo-mezasu"
bw = "https://bookwalker.jp/series/121234/"

[chapters]
released = 76
read = 34

[lists]
recommend = "B"
+++

Protagonist gets run over by a truck and ends up reincarnating into the world of an otomege he played and he gets reincarnated into the fat pig who ends up marrying the villainess in the bad end when they live as destitute farmers together. As a result he tries his best to become fit, capable, and skilled enough to avoid any bad endings that might befall him (although getting married to the hot villainess isn't really a bad ending tbh).

<!--more-->

It's a comedy that is also strangely serious at times and also not a comedy. It's a strange work that can't be constrained by any meme labelling beyond being an isekai/reincarnation work. All things considered it's a fairly enjoyable work.
