+++
title = "捨てられた転生賢者"
title_en = ""
statuses = ["completed"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169083"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666633320"
md = "https://mangadex.org/title/52749/suterareta-tensei-kenja"
bw = "https://bookwalker.jp/series/280580/"

[chapters]
released = 81
read = 81

[lists]
recommend = "D"
+++

Protag develops tons of magic and eventually dies of old age, but before he dies cast reincarnation magic on himself so that he can continue pursuing the heights of magic. However when he actually reincarnates he's analyzed to have no talents at all and is abandoned by his parents as a failure as a newborn. Except instead of getting eaten by monsters, a goblin mother finds him and raises him as her own.

<!--more-->

This is largely a city building genre work where the protag just handles everything, everybody wants to join him without basically any issues since he's so super strong and knowledgable, and teaches all of the monsters to basically become psuedo-humans and get along. Clearly the monsters and more virtuous and human than actual humans. Would rec this if you want a city-building work that is less on the city-building and more on battles instead.
