+++
title = "랭킹 1위를 영혼까지 털어버림"
title_en = "I Stole the First Ranker's Soul"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-04-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/cjyeklh/i-stole-the-first-ranker-s-soul"
raw = "https://page.kakao.com/content/60182731"
md = "https://mangadex.org/title/7bd45a18-27d4-43ba-b3b9-2293f9a957d0/i-stole-the-first-ranker-s-soul"
wt = "https://www.webtoons.com/en/fantasy/i-stole-the-first-rankers-soul/list?title_no=5551"

[chapters]
released = 73
read = 38

[lists]
recommend = "B"
+++

The protagonist is an average worker at a black corporation after society has been upturned by the sudden arrival of dungeon portals and hunters, when all of a sudden one day she gets sucked into a dungeon while she's asleep with no way to fend for herself. However after picking a flower in the dungeon she manages to Awaken as a hunter, except it's a gathering class with no combat skills and the dungeon monsters are starting to close in on her. It turns out that everything she gathers is top-tier, so she tries gathering everything she can in the immediate vicinity, consumes it raw for temporary stat boosts, and manages to just barely defeat a creature in order to try and make it out of the dungeon using a method she recalled learning about.

<!--more-->

This is a really well-written variation of the standard "dungeon portal" subgenre. It's still mostly a blatant power fantasy with the protagonist just getting broken powerups nearly constantly and her power scaling is almost limitless as soon as she solves her only downside of not having an innate combat skill. However the plot progression is still fairly believable and the protagonist is both unconventional for this subgenre and also pleasant to read about. The male lead is also kind of cute :rempeace:.
