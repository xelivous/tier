+++
title = "公爵令嬢の嗜み"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-02-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=131748"
raw = "https://web-ace.jp/youngaceup/contents/1000012/"
md = "https://mangadex.org/title/17720/koushaku-reijou-no-tashinami"
bw = "https://bookwalker.jp/series/82250/"

[chapters]
released = 65
read = 59.3

[lists]
recommend = "C"
+++

<!--more-->

~~Hasn't been updated since 2022-09-02 so it's probably axed?~~ It became unaxed after over a year of hiatus I guess for a single chapter and hasn't been updated in a few months now, so it's kind of bizarre and limping along but might exist?
