+++
title = "리턴 투 플레이어"
title_en = "Return to Player"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-01-01
tags = [
]

[links]
mu = "https://www.mangaupdates.com/series/ewm8j5v/return-to-player"
raw = "https://series.naver.com/comic/detail.series?productNo=5410454"
md = "https://mangadex.org/title/cac1a74c-0b5f-498a-8472-05f0636e1b4d/return-to-player"
wt = "https://www.webtoons.com/en/action/return-to-player/list?title_no=2574"

[chapters]
released = 155
read = 140

[lists]
recommend = "B"
+++

The entire world gets wrapped up in a game played by the gods where they can choose avatars out of various humans to live vicariously through and observe the game state of the entire world. The protagonist ends up as the last survivor of his world but it's a doomed timeline where he's the only person left alive at the end of everything, and he can't even escape the planet since he was chosen as an avatar at some point. However he is given the option of resetting time back to the day this all started and he sets out to try and bring about a better timeline where he's not an epic loner this time.

<!--more-->

The work is pretty good up until the like ch80s or so and I really enjoyed it a lot but then the power levels just kept rising exponentially and the work became kind of mediocre. The murim world especially is a snoozefest.
