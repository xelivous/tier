+++
title = "結婚指輪物語"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=108009"
md = "https://mangadex.org/title/11521/kekkon-yubiwa-monogatari"
bw = "https://bookwalker.jp/series/21540/"

[chapters]
released = 63
read = 0

[lists]
recommend = ""
+++


<!--more-->
