+++
title = "ライセカミカ"
title_en = ""
statuses = ["completed"]
demographics = ["shounen"]
furigana = ["full"]
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=139119"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01000056010000_68/"
md = "https://mangadex.org/title/20624/raise-kamika"
bw = "https://bookwalker.jp/series/131854/"

[chapters]
released = 29
read = 29

[lists]
recommend = "D"
+++


<!--more-->
