+++
title = "最強陰陽師の異世界転生記"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=167354"
raw = "https://futabanet.jp/list/monster/work/5dd4fcf177656139da050000"
md = "https://mangadex.org/title/abd0bc20-063a-45a0-91b2-8b7049701d4a/the-reincarnation-of-the-strongest-onmyoji-these-monsters-are-too-weak-compared-to-my-youkai"
bw = "https://bookwalker.jp/series/250418"

[chapters]
released = 28.3
read = 25

[lists]
recommend = "C"
+++

Protagonist was a super strong onmyouji in his previous life who got killed by his subordinate but managed to cast a spell to reincarnate himself just before he died, and ends up reincarnating into a western-fantasy setting with magic/demons/heroes/etc. However the protagonist is odd since he has no magic of his own despite being born to a family of magicians, but can still get by with his spiritual arts from his previous life.

<!--more-->

Basically the protag manipulates everybody around him by playing 4d chess and using weird powers that nobody else knows about to obliterate everything in his way. I'm not entirely sure who I would recommend this to; the protag is emotionless and he's slowly build a harem of mostly platonicly friendly girls around him. The main issue is that the protag never really struggles or improves at all. There's a basic subplot of the protag "becoming more human" and gaining emotions again but it's super subtle and i'm not sure it's actually even happening.

Honestly the art is good. Mabel is cute. The art in the anime might be slightly better in some aspects but the art in the manga is also pretty good in its own way. Storywise they're basically identical so choose either/both if you want.
