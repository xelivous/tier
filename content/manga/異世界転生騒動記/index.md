+++
title = "異世界転生騒動記"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=122360"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/552000104"
md = "https://mangadex.org/title/15359/isekai-tensei-soudouki"
bw = "https://bookwalker.jp/series/68106/"

[chapters]
released = 89
read = 70

[lists]
recommend = "A"
+++

Inside you there are two wolves. One is a warring states general. The other is a chuuni otaku highschooler. Good luck.

<!--more-->

It's a fairly fun series where the protag occasionally gets possessed by one of his inner personalities to kind of asspull through some scenarios, and is generally influenced by both of them throughout his entire life while still being an "indepdent" third personality. The work does some fun stuff with gender roles as well and there's a lot of competent women throughout the work which is pretty refreshing when you're inundated with nothing but endless sex slaves in most isekai.

The art is pretty good and the faces in particularly are really well done and unique, but the backgrounds leave much to be desired with tons of empty panels constantly.
