+++
title = "私の推しは悪役令嬢"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=168833"
raw = "https://piccoma.com/web/product/50327"
md = "https://mangadex.org/title/22d6d048-9f9f-4c3a-95c2-3b145f110e20/i-favor-the-villainess"
bw = "https://bookwalker.jp/series/282008"

[chapters]
released = 34
read = 33

[lists]
recommend = "A"
+++

Protag reincarnates as the protagonist of an otome game but she's always been head over heels of the villainess instead of any of the heros, and wants to be stepped on and abused by the villainess frfr. As a result of being able to live out her fantasy of being together with her oshi she singlemindedly pursues the villainess in hopes of living happily ever after together.

<!--more-->

pretty solidly worl/moogy-shit and i feel like if you fall into the target demographic of this work you would enjoy it a lot. Even if you're not particularly interested in yuri or even otomege this work is still fairly interesting to read and pretty easily recommendable to just about anybody.