+++
title = "もふもふと異世界でスローライフを目指します!"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154772"
raw = "https://www.alphapolis.co.jp/manga/official/846000276"
md = "https://mangadex.org/title/ed3131fd-0d45-4e6d-a9e1-a1947b24e3d9/mofumofu-to-isekai-slow-life-o-mezashimasu"
bw = "https://bookwalker.jp/series/234855"

[chapters]
released = 47
read = 30

[lists]
recommend = "B"
+++

pretty cool work despite mostly being isekai tourism. Largely a wholesome series about not taking your Family for granted and remembering to cherish the present, etc. World has a decent amount of thought put into it as well. Just like every 'slow life' work it tends to focus a little too much on the food though.

<!--more-->

