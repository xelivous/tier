+++
title = "ウォルテニア戦記"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-03-05
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=140719"
raw = "https://firecross.jp/ebook/series/232"
md = "https://mangadex.org/title/21130/wortenia-senki"
bw = "https://bookwalker.jp/series/112158/"

[chapters]
released = 47
read = 47

[lists]
recommend = "A"
+++

Absolute chad who studied the blade gets summoned out of nowhere and absoutely destroys his summoners before they have a chance to do anything. However he comes across some slaves and saves them and then they basically become his slaves because slaves are just a thing that always happens in isekai (and they're also powerful haha lol). It's kind of odd that the slaves are described as having slightly darker skin but look fully white, and are basically wearing a bikini at all times but what can you do.

<!--more-->

One of the downsides of this adaptation is that it kind of just glosses over or speedruns certain developments and kind of just skips forward bits at a time; it doesn't bother setting up scenes and just drops you directly in the middle of them and expects you to piece everything together yourself through context. The scale continues to increase as time goes on and it starts to dabble in large-scale military strategy with callbacks to the warring period of china (like dynasty warriors lul) etc.

Overall it's pretty decent if you want an action manga with a focus on military strategy.
