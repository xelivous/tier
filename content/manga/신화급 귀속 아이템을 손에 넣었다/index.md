+++
title = "신화급 귀속 아이템을 손에 넣었다"
title_en = "Mythic Item Obtained"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2023-12-21
tags = [
    "male-protagonist",
    "human-protagonist",
    "rpg-game-system",
    "fighter-protagonist",
    "mage-protagonist",
    "harem",
    "power-fantasy",
    "story-starts-as-adult",
    "reincarnated-into-past",
    "reincarnated-as-teenager",
    "gods-on-planet",
    "quest-to-kill-god",
    "norse-mythology",
    "monsters-invade-earth",
    "dungeons-spawn-on-earth",
]

[links]
mu = "https://www.mangaupdates.com/series/itm1ceu/mythic-item-obtained"
raw = "https://comic.naver.com/webtoon/list?titleId=795297"
md = "https://mangadex.org/title/a08f45d1-788a-4b7c-b1c4-b87b083e1b59/i-obtained-a-mythic-item"
wt = "https://www.webtoons.com/en/fantasy/mythic-item-obtained/list?title_no=4582"

[chapters]
released = 83
read = 73

[lists]
recommend = "C"
+++

Protag is an absolute dumbass who chooses to be a physical fighter on the frontlines when he has basically no aptitude for it while he instead has the literal best aptitude for magic in the entire world. "But magic users are looked down upon and rn fighters make the big bucks", but then shortly afterwards mages actually pop off and become the best and fighters become useless, causing the protag to essentially be relegated to a completely worthless bumpkin with nothing to his name since apparently you can no longer increase your stats/etc once you're in your 20s (lol). Events happen and eventually when he's out on a dungeon as frontline fodder, an assassination is carried out on the team he's frontlining for and he ends up getting killed as well. But the gods have other plans for him, overwrite his system, and reincarnate him back to the moment in time before he solidified his choice to become a fighter.

<!--more-->

The author really wanted to make the protag a hybrid fighter/mage, but couldn't think of a good way to actually go about doing that, so what you end up with is the absolutely terrible beginning chapters where the protag seems like he's the biggest dumbass to ever exist. However if you can get past that it's a fairly entertaining series, albeit a very standard wish fulfillment power fantasy filled with all of the typical shounen korean tropes. The work is extremely heavy on norse mythology, or at the very least a large majority of the primary characters are related to characters in norse mythology, with the end goal of the protag being to kill Odin.

There's definitely far worse power-fantasy shounens in the korean space to read at least.
