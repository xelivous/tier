+++
title = "史上最強の大魔王、村人Aに転生する"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-09-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155613"
#raw = ""
md = "https://mangadex.org/title/14677fdb-7b5f-4178-9775-dc3e11752f0f/shijou-saikyou-no-daimaou-murabito-a-ni-tensei-suru"
bw = "https://bookwalker.jp/series/214648/"

[chapters]
released = 36
read = 29

[lists]
recommend = "D"
+++

The protagonist is the strongest demon lord feared by everybody despite protecting and cherishing all of demon kind/etc, but desparately just wants to hang out and chill with people and be friends yet because he's so scary and respected that's impossible. So he gets the brilliant idea to kill/reincarnate himself. However life isn't that easy, and despite being reincarnated as a random villager 12 years pass by and he's still just an autistic outcast because he doesn't know how to communicate from being isolated for so long. As a result he comes across as extremely weird whenever he tries to talk to anybody since he's an adult in a child's body and also because he's borderline autistic. However one day when strolling through the forest he saves a cute elf girl and she swoons over his epic magic abilities and becomes his first friend. They eventually end up enrolling in magic school, but there's nothing really to teach, and also they're both the descendants of the Heroes so the protagonist's desire to just chill out with friends and not be special isn't happening!!

<!--more-->

Extremely hot twinbraid booba elf in 6.2... it should've been me, not him... ch11 is based.

lmao. ch16/17. lmao. literal actual tentacle rape. this is the most graphic h-scene i've ever seen in a manga that isn't outright normally an h-doujin. Absolutely out of nowhere.

The most based part about this series is that both of his parents are gay/lesbian but somehow ended up shacking up together to get the autistic protagonist born.

Can i consider this isekai if people from japan isekai'd into this world and were subordinates of the protagonist at some point in time in the vague past?

Don't know what to make of this work honestly. Protag is an autistic chad who has women fauning over him while he constantly worries about how his subordinates from his past life (from 3000 years ago, and are still alive) think about him and how he "ran away from his duties" by killing himself yet spends ages tiptoeing around trying to hide that he in fact reincarnated despite them finding out more and more since he's so much more powerful than anybody else in this age and has all of the same mannerisms. It's extremely ecchi but doesn't bother to have nipples or go full into h-scenes or anything so it's in a weird limbo zone. The heroines are simultaneously airheaded bimbos and also somewhat complex and the artist actually draws them with neutral/pensive faces pretty regularly which is somewhat uncommon for this subgenre where normally they'd just have full on happy/angry faces all of the time instead, which is at least one notable thing about this work I guess? I don't understand what this work is trying to be and i'm not sure the author does either.
