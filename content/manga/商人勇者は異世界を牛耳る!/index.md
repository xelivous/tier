+++
title = "商人勇者は異世界を牛耳る!"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-29
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153768"
raw = "https://magcomi.com/episode/10834108156766291346"
md = "https://mangadex.org/title/37877/shounin-yuusha-wa-isekai-wo-gyuujiru"
bw = "https://bookwalker.jp/series/230538/"

[chapters]
released = 41
read = 24.5

[lists]
recommend = "C"
+++

Protag works at a black company slowly building up a savings fund so that one day he can own a shop of his own, but the moment he amasses enough funds he gets summoned into an isekai, and unfortunately the skill he got when he came over is useless in battle since it's just a farming-related skill! However instead of killing him or sending himn back, the king spares him and allows him to live in a shitty shack in a corner somewhere with a field and is tasked with providing food for the kingdom.

<!--more-->

the main heroine is moe. main highlight of this work is her. protag can infinitely print money but he's still just trying to sell his shit smh my head. Honestly this is kind of a negative IQ manga but the heroine is moe so that's fine. Weirdest part about this work is that the protag just kind of runs away from the kingdom he was summoned in and there isn't like a plotline to get him back at all.
