+++
title = "異世界で土地を買って農場を作ろう"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155770"
raw = "https://comic-boost.com/series/122"
md = "https://mangadex.org/title/45ed7ed8-23b3-45bf-9b9c-da6e9e88f747/isekai-de-tochi-o-katte-noujou-o-tsukurou"
bw = "https://bookwalker.jp/series/213381/"

[chapters]
released = 45
read = 31

[lists]
recommend = "C"
+++

Protagonist gets summoned to another world along with a ton of other random people from earth of all ages and backgrounds and are tasked with defeating the demons/demon lord. All of the people who were summoned have unique and rare skills, however the protagonist's special skill is nothing, or at least that's how it appears to the people of this world; In actually he has the skill to be the master of everything. Additionally they can't send anybody back to earth and he seemingly won't be of help defeating the demon lord "since he lacks a skill". So the protag ryan-maxxes, asks for a plot of land, and begins farming. And basically first thing he does is fish out a mermaid waifu from the sea.

<!--more-->

The mermaid is hot so this is definitely a ryan-core manga, or at least it would be if it focused more on the farming and didn't end up being a weird cooking manga with tons of high powered people surrounding the protag.
