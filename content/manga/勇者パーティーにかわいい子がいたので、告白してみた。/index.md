+++
title = "勇者パーティーにかわいい子がいたので、告白してみた。"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=178528"
raw = "https://gaugau.futabanet.jp/list/work/5dd64c1477656154fb010000"
md = "https://mangadex.org/title/024af7a5-f8ad-4b67-b950-b1708f52b018/yuusha-party-ni-kawaii-ko-ga-ita-no-de-kokuhaku-shite-mita"
bw = "https://bookwalker.jp/series/301735/"

[chapters]
released = 29.2
read = 28

[lists]
recommend = "B"

+++

Protag reincarnates as a demon and lives as a shut-in inside of the demon's lord castle as the hero's party continues to bash their heads against the unstoppable wall that is the protag in order to try and reach the demon lord, but continues to fail over and over. Over time the protag starts to get fond of the cleric that's along with the party and he eventually confesses to her. The demon lord dies and the protag decides to live as a human to get closer to the cleric. 

<!--more-->

It's mostly a comedy with a focus on romance. There's a large amount of varied relationships of all ages/personalities, although the most prominent ship (the protag's) is probably the least satisfactory. Loli x Gargoyle is a good ship. It's a cute series overall.
