+++
title = "転生侯爵令嬢奮闘記"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=175736"
raw = "https://www.alphapolis.co.jp/manga/official/141000353"
md = "https://mangadex.org/title/2632dd4b-4005-44f9-8a5f-5e1b752e3e42/the-struggle-of-being-reincarnated-as-the-marquess-s-daughter-i-ll-deal-with-what-s-coming-to-me"
bw = "https://bookwalker.jp/series/301303/"

[chapters]
released = 26
read = 13

[lists]
recommend = "C"
+++

4 chapters were released in 2023 so it's kind of like barely limping along with a weird sporadic update cycle.

<!--more-->
