+++
title = "神達に拾われた男"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=145806"
raw = "http://www.ganganonline.com/contents/kami/"
md = "https://mangadex.org/title/4187856d-65a9-4fb4-98ec-84c0bf16631f/kamitachi-ni-hirowareta-otoko"
bw = "https://bookwalker.jp/series/164622"

[chapters]
released = 53.1
read = 32

[lists]
recommend = "C"
+++

It's a feelgood healing manga and nothing more.

<!--more-->
