+++
title = "転生したらスライムだった件"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-10-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=119910"
md = "https://mangadex.org/title/15553/tensei-shitara-slime-datta-ken"
bw = "https://bookwalker.jp/series/56105/"
raw = "https://pocket.shonenmagazine.com/episode/10834108156631339284"

[chapters]
released = 122
read = 117

[lists]
recommend = "B"
+++

Protagonist is your average japanese salaryman virgin at the ripe age of 37, when one day he gets invited out to celebrate his kouhai's engagement to the popular girl in the office. However while on this little date a thief (with a knife) ends up almost barreling straight into the kouhai to which the protagonist pushes him out of the way and ends up getting stabbed instead as a result. In his dying breaths he starts rambling off a ton of thoughts and regrets about his life, and ends up hearing random "system messages" respond to him vowing to give him what he desires. As a result of all of his random thoughts he ends up reincarnating as a Slime in an isekai with some unique skills based on his thoughts. Not only does he have the ability to consume anything to gain their skills, but he also has an AI companion supercomputer in his brain that tells him everything he would ever need to know!

<!--more-->

The beginning of this work is enjoyable and does a good job with the protagonist slowly building up a civilization of monsters, culminating in the climax of the "first arc" with Megiddo, although megiddo is about 10x better in the anime than it is in the manga imo. The author does a decent enough job of constantly layering on "mysteries" that slowly get solved/thwarted by the protagonist just enjoying his life with his monster pals. Despite this being a "civilization-building" it doesn't go into any details about it at all and instead all of the focus is on various people working in the shadows or political machinations of large nations trying to cope with the protagonist being an epic all powerful force. If anything I would prefer a work that goes all into the slice of life city building instead at times, unlike this mainline series that is almost entirely a battle shounen, but there's a ton of spinoff works that go all into that instead (some of which even got anime adaptations).

I feel like the biggest problem with this work is that a lot of it feels aimless. There's a grand desire for the protagonist to make a world where humans and monsters can live together in peace and harmony but for all intents and purposes this has already come about by the resolution of the first arc and we're simply in a very long drawn out victory lap at this point where the protagonist implements more and more japanese culture to the world. There is no real antagonist or large conflict for the protagonist to pursue, he just introduces more and more manga/arts/food to the world to appease all of the random japanese people that get summoned to this world against their will.

It's still fairly competently done for a shounen series and I find aspects of it to be fairly enjoyable. I just wish there was more depth to it.
