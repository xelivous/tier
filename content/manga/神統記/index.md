+++
title = "神統記"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/yhstxcy"
raw = "https://pash-up.jp/content/00000099"
md = "https://mangadex.org/title/d0f57b4a-8a11-4fc4-9d25-de9b0527eab8/shintouki-teogonia"
bw = "https://bookwalker.jp/series/155380"

[chapters]
released = 116
read = 0

[lists]
recommend = ""
+++



<!--more-->

