+++
title = "막내 황녀님"
title_en = "The Beloved Little Princess"
categories = ["reincarnation"]
demographics = ["shoujo"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=164573"
raw = "https://page.kakao.com/home?seriesId=54680399"
md = "https://mangadex.org/title/412ab8fe-eca9-47c9-8f5c-f6deb75622e9/the-youngest-princess"

[chapters]
released = 134
read = 54

[lists]
recommend = "F"
+++

This is a weird work about how the entire universe bows down to this newborn baby since it's been prophesized for centuries that she would be born and lead the empire to prosperity, and on top of it the protag is said baby and is also the super powerful archmage from her previous life.  As a result a majority of the work is just everybody fawning over her and giving her presents and doting on her and etc. There's also some incredibly creepy romance. Honestly the fact that so many like literal adults want the protag's prepubescent body makes this impossible to read since it's just endless. Maybe the work could be relatively tolerable if she ever aged up, but I don't care to read even more to find out how worse it can make it whenever that happens.

<!--more-->

