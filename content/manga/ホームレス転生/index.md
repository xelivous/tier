+++
title = "ホームレス転生"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-12-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153598"
raw = "https://seiga.nicovideo.jp/comic/37369"
md = "https://mangadex.org/title/607f0bbc-e36d-46af-9efd-b74a40cbbe9d/homeless-tensei-isekai-de-jiyuu-sugiru-jikyuu-jisoku-seikatsu"
bw = "https://bookwalker.jp/series/196408"

[chapters]
released = 29
read = 11

[lists]
recommend = "F"
+++

Endless deus ex machina storytelling filled with oretueee. It's おっさん wish fulfillment with cute elf wife and a replacement son for the son he lost, and the home he lost, and the everything else he lost. You might be able to consider this to just be a story about his version of Heaven in all honesty if you wanted to but even then it's not particularly interesting as a story.

<!--more-->
