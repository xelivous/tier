+++
title = "おたくの隣りはエルフですか"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=155182"
raw = "https://comic-days.com/episode/10834108156668058247"
md = "https://mangadex.org/title/b1acb2cf-8f39-4bd7-926c-06377e485a9a/otaku-no-tonari-wa-erufu-desuka"
bw = "https://bookwalker.jp/series/232664"

[chapters]
released = 37
read = 0

[lists]
recommend = ""
+++



<!--more-->

