+++
title = "乙女ゲーのモブですらないんだが"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-21
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165864"
raw = "https://magazine.jp.square-enix.com/mangaup/original/otomegame_mob/"
md = "https://mangadex.org/title/e279d973-e1dd-4cfc-b643-fda857b6f4a0/otome-game-no-mob-desura-naindaga"
bw = "https://bookwalker.jp/series/263569"

[chapters]
released = 0
read = 21

[lists]
recommend = "B"
+++

Protag is the gardener's son of a potential villainess/rival in an otome game when the heroine goes on a certain route. As a result he's the nothingburger of nothingburgerness. However one of the first things he does when meeting this girl is destroy her with facts and logic.

<!--more-->

Fuck this society that doesn't understand the beauty of freckles tbh.

Pretty good moe romance work that doesn't beat around the bush and doesn't have any excessively dumb drama where people are dying left and right. It also eventually has multiple perspectives/protags which is nice.
