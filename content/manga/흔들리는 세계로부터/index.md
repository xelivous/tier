+++
title = "흔들리는 세계로부터"
title_en = "Wavering Worlds"
categories = ["isekai"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-12-19
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/1q10n0r/wavering-worlds"
raw = "https://comic.naver.com/webtoon/list?titleId=793662"
md = "https://mangadex.org/title/d0117cfd-ceea-4c1a-9959-1bd81818f825/from-the-shaking-world"
wt = "https://www.webtoons.com/en/romance/wavering-worlds/list?title_no=5135"

[chapters]
released = 33
read = 33

[lists]
recommend = "C"
+++

One day, the protagonist regularly see a parallel universe of herself in her dreams every night who has a "perfect" life where she's the top student, really pretty, coveted by everybody, and they share stories about how different their worlds are to each other. Then one day the "perfect" version of her offers to trade places for a week to which the protagonist accepts, however after living in that world for 2 weeks she still hasn't seen her parallel copy in her dreams again, and it's now time to head back to school since it's finally the end of winter break. How will she manage to live in this new parallel world where things are just slightly different than what she is used to, and why did her perfect copy want to swap in the first place?

<!--more-->

This is a strange mystery series that looks like it should be a romance based off of all of the marketing material but instead is a pure mystery with no romance to be found, despite having a fair amount of highschool angst/drama and some "date" scenes. The author even jokes about this in the epilogue that there was never a romance tag on (the published site) for the series to begin with. However for some reason the LINE webtoons (english) seems to have placed it under the romance category when it's not in that category on the korean website, weird.

This is apparently the author's first work and unfortunately it kind of shows; they had a basic idea on how they wanted the overall plot to pace out but somewhat failed to properly flesh out the intermediary events enough for anyone to reasonably piece out the "mystery" themselves. Mystery is fairly hard to write well so i'm not particularly surprised, and I do appreciate the attempt since it's overall a kind of cute series that has a concrete ending without getting stretched off into 100s of chapters just to drag it out for serialization.

I don't really know who I would recommend this to, but maybe if you just really love "mystery" as a genre it could be a fun quick read.
