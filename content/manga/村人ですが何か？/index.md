+++
title = "村人ですが何か？"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143062"
raw = "http://comic-walker.com/contents/detail/KDCW_FS01000040010000_68/"
md = "https://mangadex.org/title/21143/murabito-desu-ga-nani-ka"
bw = "https://bookwalker.jp/series/117776/"

[chapters]
released = 70
read = 53

[lists]
recommend = "A"
+++

Protagonist dies from a truck in japan and gets reincarnated as a generic villager without any real abilities, but he's also apparently in the Starting Town along with the Hero and Sage, and the hero has fallen in love with him. You could say it's pretty similar to all of the other stories where the protag falls in love with the villager girl but has to separate to go on his grand adventure, except gender swapped. However something unfortunate happens before she even leaves the village.

<!--more-->

If you're fine with some relatively light skill autism it's pretty good. ch28 has some good pages. actually a little 神. The work is largely about a ton of reincarnators all trying to one-up each other in a game of 5d chess while the world eventually leads towards a path of destruction. 