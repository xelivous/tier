+++
title = "訳あり悪役令嬢は、婚約破棄後の人生を自由に生きる"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-13
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149946"
raw = "https://www.alphapolis.co.jp/manga/official/302000223"
md = "https://mangadex.org/title/2643be00-cd89-447a-93ea-faf98de16390/for-certain-reasons-the-villainess-noble-lady-will-live-her-post-engagement-annulment-life-freely?tab=chapters"
bw = "https://bookwalker.jp/series/206266"

[chapters]
released = 26
read = 26

[lists]
recommend = "B"
+++

A nurse reincarnates as the villianess and just wants to cure everybody of the diseases that are afflicting other people, but the psychopathic heroines keep wanting to fuck their oshis and keep getting in the way of the noble act of healthcare smh my head. It feels relatively complete and probably did everything it needed to without skipping over too much, and I kind of appreciate that the protag never really played the game she was dropped into and is just trying to live her life without it being poisoned by future knowledge. A solid entry in the "villianess is good-natured and heroines are psychos" cinematic universe.  The ending is Kind Of just a little bit rushed but otherwise solid enough to at least try experiencing.

<!--more-->
