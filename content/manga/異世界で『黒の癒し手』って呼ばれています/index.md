+++
title = "異世界で『黒の癒し手』って呼ばれています"
title_en = ""
statuses = ["completed"]
demographics = ["shoujo"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=118731"
raw = "https://www.alphapolis.co.jp/manga/official/393000081"
md = "https://mangadex.org/title/16801/in-another-world-i-m-called-the-black-healer"
bw = "https://bookwalker.jp/series/93028/"

[chapters]
released = 49
read = 0

[lists]
recommend = "D"
+++

Honestly I can only rec this to someone if they reallly want a shoujo romance, as there is little else in the story beyond that. I wasn't really a fan of the ending and/or last arc either.

<!--more-->
