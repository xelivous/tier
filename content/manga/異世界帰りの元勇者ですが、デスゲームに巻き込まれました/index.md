+++
title = "異世界帰りの元勇者ですが、デスゲームに巻き込まれました"
title_en = ""
categories = ["post-isekai"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-03
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=186180"
raw = "https://comic-days.com/episode/316190246950987414"
md = "https://mangadex.org/title/720466f8-2eee-43a5-893b-67f9190c04ae/isekai-kaeri-no-moto-yuusha-desuga-death-game-ni-makikomare-mashita"
bw = "https://bookwalker.jp/series/336017/"

[chapters]
released = 19.2
read = 12

[lists]
recommend = "C"
+++

Protag goes to an isekai and then returns after an extremely long time there without any time having advanced on earth, and as a result he's nostalgic for all of the mundane school events that most people are already sick of. He just wants to take his time and enjoy the simple peace without having to worry about fighting a demon king or whatever. However while out on a school trip with his classmates they end up getting shoved into a death game with a bunch of psychos, and also the protag still retains all of his OP isekai magic/skills so he just dabs all over them.

<!--more-->

Weird take on the death game genre. seems like weird revenge porn against death game creators than anything. better watch out and not set up a death game or the isekai protag boogeyman will come and fuck your shit up. Protag's name is hilarious too. This is mostly a comedy manga but it's just kind of weird throughout.
