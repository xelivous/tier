+++
title = "転生魔女は滅びを告げる"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=152252"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00200707010000_68/"
md = "https://mangadex.org/title/4ad4b480-aacc-4835-b413-0dbb1cdf436e/the-reborn-witch-foretells-destruction"
bw = "https://bookwalker.jp/series/194425"

[chapters]
released = 29
read = 0

[lists]
recommend = ""
+++



<!--more-->

