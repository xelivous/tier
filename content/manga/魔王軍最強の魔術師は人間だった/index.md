+++
title = "魔王軍最強の魔術師は人間だった"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-10-18
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155231"
raw = "https://futabanet.jp/list/monster/work/5dceaa7a7765618ea9090000"
md = "https://mangadex.org/title/39384/maou-gun-saikyou-no-majutsushi-wa-ningen-datta"
bw = "https://bookwalker.jp/series/214322/"

[chapters]
released = 47.1
read = 46.2

[lists]
recommend = "F"
+++

Protag is your average japanese highschooler who randomly reincarnates as a human baby for some reason stuck in the middle of demon territory and raised by an eccentric demon as his own child. He makes a great effort to conceal the fact that he's a human living amongst demons whom are fighting against humans, but still tries to save humans to the best of his ability when he encounters them. He even works himself up to a realtively high position in the demon lord's army as a strategist and magician. However after standing out he is one day summoned by the latest demon lord who turns out to seemingly be a reincarnated japanese person similar to himself, and ends up getting promoted to the leader of a front line demon city and is ordered to protect it. His secret is also found out by a random maid by coincidence, so he makes her his personal maid because she's cute!!

<!--more-->

The author doesn't draw backgrounds 99% of the time, so you're just always looking at basic characters talking in a white (or black!) void. Except sometimes they'll throw in 3d renders cities and interiors I guess; the characters are probably traced from 3d models as well. The fucking spread of a 3d dragon rendered onto a white background and nothing else in the dumbest pose possible, man.

Honestly I don't buy that the protagonist hasn't been found out for being a human after all of this time. The work starts and he's immediately found out to be a human by so many people because he's a completely idealistic brainlet. Funniest part about this story is that the demon lord is actually oda nobunaga reincarnated as a hot busty demon :rempeace:

Honestly most unrealistic part about this work is that despite the protagonist being just your average lazy japanese highschool student he's for some reason a super ultra competent strategist who can never fail and just conveniently succeeds in everything he does because everybody else is so incompetent. The work is too naively idealistic and it's like reading a 8 year old's idea of how to solve the world's issues. Like fundamentally the demons love chaos and war and killing humans, but the protag just waltzes in and says "play nice together now all of you" and then they play nice and everybody lived happily ever after! 

Chapter 46 is unbelievably bad, i'm dropping this manga and never returning this is horrible.

I'm not sure I can recommend this to anybody; At best if you're reaching for the bottom of the barrel idealistic "humans and demons can surely live together uwu" story then maybe you could read this but there's at least like 20 others you should read instead before this.
