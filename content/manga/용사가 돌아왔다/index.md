+++
title = "용사가 돌아왔다"
title_en = "The Warrior Returns"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=183215"
raw = "https://comic.naver.com/webtoon/list?titleId=773459"
md = "https://mangadex.org/title/718d9146-8c9e-4ff3-9dc4-e4736a1239bb/the-warrior-returns"
wt = "https://www.webtoons.com/en/action/the-warrior-returns/list?title_no=3265"

[chapters]
released = 84
read = 0

[lists]
recommend = "C"
+++

lol. this is a work about the premise of "what if after you save the world in your isekai and come back, the only thing left waiting for you is excessive suffering?", presented in the edgiest way possible. This is like the manwha form of "i must kill chaos". There's a lot of sameface, especially for women. lmao at the epic twist in ch22/23 btw. ch38 is good though. would rec this if you want to see extremely meme edgy revenge series.

<!--more-->
