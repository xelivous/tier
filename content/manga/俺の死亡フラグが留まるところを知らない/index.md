+++
title = "俺の死亡フラグが留まるところを知らない"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation", "game"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-19
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=168441"
raw = "https://www.mangabox.me/reader/127460/episodes/"
md = "https://mangadex.org/title/50011/my-death-flags-show-no-sign-of-ending"
az = "https://www.amazon.co.jp/dp/B08JGD36DQ"

[chapters]
released = 70
read = 68

[lists]
recommend = "B"
+++

Protag gets reincarnated into one of his favorite console RPGs, except he was reincarnated into one of the least liked assholes of a character. To make matters worse he's incapable of deviating too far from his defined setting, and all of his thoughts get filtered through the antagonistic asshole that is his character. Because he will eventually die an early death due to all of the enemies he inevitably would make by following the path of the story, the protag needs to figure out a way to deviate from the story while staying within the confines of his unlikable character and without making too many enemies. I do enjoy that the protag has an outright hostile personality to just about everybody even if he doesn't want to be hostile, it adds a nice flavor to the story.

<!--more-->
