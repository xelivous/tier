+++
title = "クラスが異世界召喚されたなか俺だけ残ったんですが"
title_en = ""
categories = ["isekai", "reverse-isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-03-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=145960"
raw = "https://comic-boost.com/series/89"
md = "https://mangadex.org/title/2e3d0203-e1d2-4142-b6ab-69a70643d7a1/class-ga-isekai-shoukan-sareta-naka-ore-dake-nokotta-n-desu-ga"
bw = "https://bookwalker.jp/series/165917"

[chapters]
released = 32
read = 30

[lists]
recommend = "D"
+++

Protag's entire class gets isekai'd all at once, except since he's in the Fabled Protag Seat in the back-window spot he's just outside of the range of the transporation circle so he doesn't get isekai'd along with them. Although he still has his full isekai status/skills and can see the status of other people in his normal world. However some terrorists pull up to his school and start shooting up the place, and he has to step in and save his classmates with his broken stats/skills/level. He also has a cute imouto who wants to fuck him.

<!--more-->

The story occasionally flips over to the actual isekai'd classmates as well, going over their adventures and otherwise. Additionally later on the god who accidentally didn't isekai the protag in the first place starts trying to aggressively send him over to the isekai over time. I'm not particularly fond of the plot points it touches on, and the balance between the two worlds isn't particularly well-done either. It just kind of meanders along until it eventually gets axed. The chapters also get a lot shorter over time.
