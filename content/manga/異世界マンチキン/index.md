+++
title = "異世界マンチキン"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-02
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154522"
raw = "http://seiga.nicovideo.jp/comic/39410"
md = "https://mangadex.org/title/39256/otherworldly-munchkin-let-s-speedrun-the-dungeon-with-only-1-hp"
bw = "https://bookwalker.jp/series/218876/"

[chapters]
released = 93
read = 93

[lists]
recommend = "A"
+++

Protagonist is a hardcore gamer who likes doing challenge runs in games and has a super cute imouto who encourages him despite him being a bullied loner at school and is practically just a hikkineet. However one day when they're having super cute dinner time the sky becomes pitch black and a monster blasts into their house killing both of them. However shortly afterwards the protagonist gets told he can reincarnate into another world that is similar to DnD complete with rolling dice and character profile creation. The protagonist manages to find his sister in the GM book despite her supposedly being saved and still being alive, throws a tantrum and breaks the spacetime continuum, and takes the GM book with him down to the isekai.

<!--more-->

Love when a work starts out with a plot point like 20 chapters in and then skips back to the actual start... Ultimately this is an extremely DnD-core work filled with DnD concepts. It's actually fairly entertaining and relatively unique in the isekai genre; only thing remotely similar is like goblin slayer (but that's not isekai) and even then it focuses more on an actual party and plot instead of just endless goblin killing.

https://www.youtube.com/watch?v=SaKVoPTw7ic