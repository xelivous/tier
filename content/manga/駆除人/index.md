+++
title = "駆除人"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-31
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143822"
#raw = ""
md = "https://mangadex.org/title/ee0df4ab-1e8d-49b9-9404-da9dcb11a32a/kujonin"
bw = "https://bookwalker.jp/series/140735"

[chapters]
released = 25
read = 25

[lists]
recommend = "D"
+++

Protagonist is an ossan pest exterminator dude who died in japan and awoke in an isekai all of a sudden, but he didn't have any special sword/magic skills or even any skills at all really; all he has is his knowledge he obtained while working as an exterminator. Thus he starts working for the adventurer's guild doing basic cleaning and extermination tasks, like getting rid of rats using rat poison, etc. However he's kind of bugged and ends up levelling up massively by killing 1000s of rats indirectly through the rat poison. As a result he gets a ton of skill points and dumps them all into exterminator-related skills, since he doesn't want to do anything dangerous like fighting monsters or going on adventures.

<!--more-->

The work constantly does "here's an event that happens in the future, now let me tell you about how this event came to be" over and over fucking constantly it's terrible. Dude gets slaves out of nowhere, and the girl immediately wants his dick and wants to live with him forever despite knowing him for like 5 seconds. Then he beats up the guild promotion examiner wearing literal bikini armor in 1 hit since his levels skyrocketed from extermination, and she basically falls in love with him and wants to join him on his journey as well because she loves to battle and wants to be with someone strong uwu. The work also semi regularly throws in ecchi scenes I guess.

The work gets axed without really ever accomplishing anything and is all around just kind of lackluster without any real focus. The niche of focusing on an exterminator is nice but overall feels hard to recommend as a result of the issues with the lack of any real plot and no ending.
