+++
title = "勇者の嫁になりたくて"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=162183"
raw = "https://online.ichijinsha.co.jp/zerosum/comic/yusya"
md = "https://mangadex.org/title/4d40b2e2-a3b3-47ad-bed3-4822a6e5033f/i-want-to-become-the-hero-s-bride"
bw = "https://bookwalker.jp/series/265741"

[chapters]
released = 10
read = 0

[lists]
recommend = ""
+++



<!--more-->

