+++
title = "軍オタが魔法世界に転生したら、現代兵器で軍隊ハーレムを作っちゃいました？！"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-08-05
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=123074"
md = "https://mangadex.org/title/15946/gun-ota-ga-mahou-sekai-ni-tensei-shitara-gendai-heiki-de-guntai-harem-wo-tsukucchaimashita"
bw = "https://bookwalker.jp/series/61527/"
#raw = ""

[chapters]
released = 87
read = 53

[lists]
recommend = "F"
+++

Protagonist was a gun-otaku shut-in who cut himself off from society after getting bullied, and after his otaku friend ended up committing suicide after being bullied alongside him. Eventually he tries to re-integrate into society by getting a factory/manufacturing job but one of his highschool bullies ends up finding him after work one day in a dark alleyway and ends up knifing him to death because reasons. He then ends up reincarnating as a baby and is found by an orphanage owner alongside a wolfgirl, and slowly grows up in this environment.

<!--more-->

The beginning of this work is terrible with the timeline jumping all over the place and a horrible bang start.

The art is kind of terrible with extremely awkward poses and faces as if the artist has never seen another human before. THe fight scenes are atrociously bad but the only way to make them not last 5 seconds when the protagonist has a literal gun that pierces through all defenses is to make everybody a brainlet.

However the work takes a somewhat unexpected turn with the protagonist being so dumb that he ends up getting sold into slavery himself. Literally negative IQ. It skips over most of the plot that involves him getting into that situation, resolves multiple arcs in-between, and then finally does a flashback to it many chapters later for some godforsaken reason.

Chapter 51 is abysmally terrible.
