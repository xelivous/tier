+++
title = "異世界チート魔術師"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-08-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143657"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01000054010000_68/"
md = "https://mangadex.org/title/babc3f06-f618-487b-be06-c213ea75f489/isekai-cheat-magician"
bw = "https://bookwalker.jp/series/116621"

[chapters]
released = 72
read = 30

[lists]
recommend = "D"
+++

I'm not sure why this got an anime but i guess there's worse works.

<!--more-->
