+++
title = "ウイルス転生から始まる異世界感染物語"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=173324"
raw = "https://pocket.shonenmagazine.com/episode/316112896885179715"
md = "https://mangadex.org/title/29b7cf38-367e-4785-a7a0-3e92e57b09ef/virus-tensei-kara-hajimaru-isekai-kansen-monogatari"
bw = "https://bookwalker.jp/series/295919/"

[chapters]
released = 18.2
read = 18.2

[lists]
recommend = "D"
+++

Protag reincarnates as a virus and the first thing he infects is a rat. Every time he infects something he gets a portion of those skills, he rapidly duplicates in his host, and can take over the host. However despite duplicating he's the only one with a "will" and others just follow his every command. He's basically broken OP and unkillable in a world that doesn't really understand what viruses are, and he can transfer his conciousness to any other of his duplicates at any time.

<!--more-->

A major draw of this work is the cute heroine which is why she's front and center of all of the volume covers.

The work was axed and didn't even have a relatively satisfying conclusion, maybe something happened to the artist i'm not sure, i can't find anything on twitter or the official pages about why this work hasn't updated in 2+ years and the artist doesn't have any social media presence at all. The state the work is currently in especially with how it ends makes it near impossible to rec.
