+++
title = "転生したら宿屋の息子でした"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=176790"
raw = "https://magazine.jp.square-enix.com/mangaup/original/tensei_yadoya/"
md = "https://mangadex.org/title/18ce2342-f970-4fc4-bb2e-398577200879/tensei-shitara-yadoya-no-musuko-deshita-inakagai-de-nonbiri-slow-life-o-okurou"
bw = "https://bookwalker.jp/series/303286/"

[chapters]
released = 21
read = 9

[lists]
recommend = "D"
+++

Dude gets overworked at black company and then reincarnates and then decides to be as lazy/sleepy as possible to make up for his lifetime of barely getting any sleep in his previous life, but gets reluctantly forced to do things constantly by his family because obviously being a freeloader is bad. the end.

<!--more-->
