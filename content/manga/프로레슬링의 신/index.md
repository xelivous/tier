+++
title = "프로레슬링의 신"
title_en = "The God of Pro Wrestling"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=180343"
raw = "https://page.kakao.com/home?seriesId=56311337"
md = "https://mangadex.org/title/631bd05c-92df-48e6-bee3-72a3cab1a685/the-god-of-pro-wrestling"

[chapters]
released = 88
read = 67

[lists]
recommend = "C"
+++

This work is largely the author doing a critique on late 1990s early 2000s american media and how asian americans are always the badguys in media, they'll never get leading roles, and there's regular prejudice against them. It follows a single korean-american's career through the GWF and then WWF who regularly has to play the role of someone who loses, up until he eventually quits and works as a construction worker, up until he eventually dies and gets reincarnated back to right before he originally joined the GWF in the first place. He then sets out to change his future and make asian americans accepted in wrestling using whatever means possible.

<!--more-->

The work regularly uses parodies of actual wrestlers in that time period so an actual wrestling fan would likely get more out of this work than others, but it goes into pretty good detail about wrestling culture and the day-to-day life of wrestling along with all of the work that goes into a wrestling show as well so it's not a requirement to know too much about wrestling I feel. It's a shounen sports manga in the end.

Only translation available goes to shit after ch42