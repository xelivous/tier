+++
title = "검술명가 막내아들"
title_en = "Swordmaster’s Youngest Son"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2024-07-21
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=200168"
raw = "https://page.kakao.com/home?seriesId=59264548"
md = "https://mangadex.org/title/85a8ebda-9959-4244-ad03-a2d9f6a746a2/swordmaster-s-youngest-son"

[chapters]
released = 121
read = 100

[lists]
recommend = "C"
+++

Protag grows up in a bizarro family that fetishizes swords and for some reason is forbidden to use magic due to some weird dumb contract, and the protag sucks at using swords but is really good at using magic! He eventually dies and gets to redo his shit life, except this time the thing holding him back from being good at swords is no longer there and he can now be the very best and not only swords and magic, but also a 3rd divine power that all synergize together and makes him super OP!!!

<!--more-->

You know what actually this work is kind of cool. Most of the time the protag just gets saved by cool hot older women whenever he's in trouble, at least when he's not dabbing on everybody with his epic protagonist powers. Biggest problem with the work is that even though the protagonist keeps powering up, he just coincidentially encounters people who are barely over his power level each time to make every fight "epic" but he wins anyways with the power of bullshit despite being "weaker" (or from his hot mommies saving him). It's a nice shounen popcorn read and not much else.
