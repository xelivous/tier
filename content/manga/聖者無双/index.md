+++
title = "聖者無双"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2023-09-23
tags = [
    "male-protagonist",
    "death-by-gunshot",
    "reincarnated-as-adult",
    "status-windows",
    "letter-rank-adventurers-guild",
    "healer-protagonist",
    "beastperson-discrimination",
    "priest-protagonist",
    "multiple-reincarnators",
    "slavery",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155059"
raw = "https://seiga.nicovideo.jp/comic/25523"
md = "https://mangadex.org/title/062f098a-88bb-48f4-b022-3b7afa87bc47/the-great-cleric"
bw = "https://bookwalker.jp/series/117862/"

[chapters]
released = 74
read = 67

[lists]
recommend = "A"
+++

Protag is your average japanese salaryman working himself to death for the benefit of the company when he gets randomly shot some day (in the peaceful japan that doesn't really have guns in the first place) and gets forcefully reincarnated in an isekai due to an agreement between gods. He then decides to allocate his skill points into being a healer/cleric, walks his way into town, and as expected healers are given special benefits so everything kind of just works out for him. He then grinds his healing skills while regularly being around hot isekai oneesans. He then sigma grindsets for months slowly building up his skills under the adventurer's guild instructor to be the very best, meeting all kinds of beastpeople who are apparently discriminated against despite there being tons of them in the guild.

<!--more-->

It's a fairly entertaining work that is actually somewhat similar to [治癒魔法の間違った使い方](../治癒魔法の間違った使い方) in a lot of ways while also being fairly different. The protagonist in both series is a close-combat healer who focuses on training his body for the sole sake of survival, while grinding his stats enough to become overpowered. However this work basically has the protagonist consume a miasma chalice over and over through sheer will which causes him to do a lvl1 challenge run in an rpg unknowingly, and also improving all of his resistances as if he was playing rune factory and chugging poison constantly.

Romance is teased from time to time but despite being around tons of hot isekai oneesans who like him constantly, the romance never progresses and instead it's just the protag and his bros going around fighting injustices in the world, and him grinding away and getting better skills without devolving into epic skillspam like a lot of other works. The protagonist has a large supporting cast of mentors and teachers that are way stronger than him (due to him literally never levelling up for ages) but he still manages to persevere and make large accomplishments despite being level 1.

Also A few twinbraids appear at certain times and there's a full page twinbraid in ch49 which is blessed and she regularly appears after that point with tons of good panels thank you manga.
