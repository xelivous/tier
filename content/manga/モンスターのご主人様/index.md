+++
title = "モンスターのご主人様"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146606"
raw = "https://gaugau.futabanet.jp/list/work/5dce954877656110b1000000"
md = "https://mangadex.org/title/1aa09c5e-d6cd-4662-93f2-8c64331b6234/monster-no-goshujin-sama"
bw = "https://bookwalker.jp/series/152446"

[chapters]
released = 56.3
read = 51

[lists]
recommend = "F"
+++

Protag gets isekai'd with his entire class but he's in the large group of people who didn't receive any cheat abilities and is simply a normal human; only a select small group of his classmates received OP abilities. One day a small group of the OP ability users decide to slaughter all of the normies out of nowhere for some reason and the protag manages to crawl away from the carnage, and manages to almost get eaten by a slime. Except he in fact tames the slime and it heals him since that is apparently his ability.

<!--more-->

tldr protag is a misanthrope, has his slime gf consume the dead body of his cheat classmates, and then has sex with her. Despite hating humans he still decides to go with his classmates into a humanoid fort to learn more about lore dumps etc, because reasons.

the art/faces are kind of bad. it's honestly kind of a dumb series overall.
