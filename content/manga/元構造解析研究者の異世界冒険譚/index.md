+++
title = "元構造解析研究者の異世界冒険譚"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-09-01
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166520"
raw = "https://www.alphapolis.co.jp/manga/official/92000221"
md = "https://mangadex.org/title/e7c88847-9a61-4856-94db-fefae51a1d28/the-former-structural-analyst-s-otherworldly-adventure-story"
bw = "https://bookwalker.jp/series/201569"

[chapters]
released = 42
read = 31

[lists]
recommend = "D"
+++

The protagonist is a scientist working in a lab when herself and her coworkers get wrapped up in a devastating earthquake that ends up with her getting squashed by some large science equipment. She then dies and meets the Goddess of japan who offers to reincarnate her and even gets offered to reincarnate back on Earth, except the protagonist immediately states she wants to go to a world with Magic, and then goes on an autistic rant about the skills she wants which basically involve peeking/editing at the structure of the universe. The goddess then warns her that she's not allowed to start training magic until she's older than 3 years old, since a lot of reincarnated brainlets kept doing as babies and getting themselves killed from excessive mana.

<!--more-->

Protag literally has an ability to edit anything in the world, statuses included, as a result it's like using cheat engine in an RPG and just hacking the values whenever you want by looking at them. Because she has literal cheat engine she ends up doing tons of things that have "never happened before" introducing tons of obscure lost technology that her parents have to deal with, meanwhile the "saint" is out fucking everything and everybody up.

Story kind of goes all over the place, doesn't really resolve anything well, spends a ton of time on a weird arc and then just kind of ends. The side arc with the maid(?) and the researcher researching teleportation magic is more interesting than anything the protag is up to at any moment so I suppose it's nice that it's about half of the work, but i'm not sure what to think about it overall. Even with the protag having cheat engine she barely really uses it at all and instead just kind of meanders around the world trying to figure out how to go back home, so it's kind of wasted potential.
