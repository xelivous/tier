+++
title = "悲劇の元凶となる最強外道ラスボス女王は民の為に尽くします。"
title_en = ""
categories = ["reincarnation", "game", "isekai"]
demographics = ["shoujo"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=164290"
raw = "http://online.ichijinsha.co.jp/zerosum/comic/rasutame"
md = "https://mangadex.org/title/2c58fb86-7fb9-4db6-9c5f-9696934ea2cb/the-most-heretical-last-boss-queen-who-will-become-the-source-of-tragedy-will-devote-herself-for-the"
bw = "https://bookwalker.jp/series/269575/"

[chapters]
released = 18.5
read = 18.5

[lists]
recommend = "B"
+++

Protag is a normal highschool girl who gets truck'd, and ends up reincarnating as the first princess of a country in an otome game that she liked, who is also the Last Boss in that game. She also comes from a line of royalty that has varying levels of precognition, which helps her handwave knowing future game events.

<!--more-->

The art is great and the protagonist is very cute tbh. This series is mostly about showing the reader horrible despicable events and then the protagonist vowing to not be that horrible of a human and instead doing everything she can to cherish/protect those around her in some kind of weird mood whiplash. I did enjoy what little there is of this adapted though. Ch14 is kino would rec reading this solely for that chapter standalone; the rest of the work is good too but ch14 is like double the normal length and you can tell more effort than normal was put into that chapter specifically.

I have to wonder why so many villianess reincarnations believe that fate is unchangeable to the point that they will blindly end up dead from a bad end even while they're actively changing fate. Is there a villianess series where the protagonist actually has to watch on in horror while all of their attempts fail and they're unable to change fate, slowly dreading the day until they die in like some final destination type shit? If so i'd like to read it at least.

However the manga adaptation got axed due to the artist having health issues, but there's now an anime adaptation of this and the LN is still going p sure. Still a tragedy because I actually like this one a lot.
