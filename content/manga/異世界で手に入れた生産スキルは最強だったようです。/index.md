+++
title = "異世界で手に入れた生産スキルは最強だったようです。"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=163161"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000280010000_68/"
md = "https://mangadex.org/title/d68ceffd-ac56-45db-9129-3413dd0d7063/isekai-de-te-ni-ireta-seisan-skill-wa-saikyou-datta-you-desu"
bw = "https://bookwalker.jp/series/255881"

[chapters]
released = 32
read = 18

[lists]
recommend = "D"
+++

Protag loved firefighters as a kid and wanted to grow up to be a hero just like them, but ended up becoming a typical corporate slave at a black company who did nothing but work overtime every day. However one day on his way home from work he ends up waking up in a pitch black expanse and a game window pops up in front of him asking him what route he would like to be on, however he doesn't like any of the choices presented to him and ends up choosing "nothing" and gets teleproted to the middle of a random forest in an isekai.

<!--more-->

Theoretically the lack of a route should give him lackluster abilities but he's still broken OP, has no struggles, and just cheats his way forward without thinking. Despite being given the option to be a hero in the route selection, he chooses to not be a hero, but the first time he sees someone in danger he still sets out to be a hero regardless. He's also incredibly terrible at telling lies. Basically because he has the skill to create anything and everything, and because he chose to be on the hidden non-route, he never struggles with anything and he just creates anything from nothing. Even the other chars are like "that's just your luck my dude, don't question it or think about it too hard just enjoy being broken op my dude".
