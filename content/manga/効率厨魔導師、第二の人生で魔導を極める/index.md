+++
title = "効率厨魔導師、第二の人生で魔導を極める"
title_en = ""
statuses = ["completed"]
demographics = ["seinen"]
furigana = ["partial"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=128683"
raw = "https://www.alphapolis.co.jp/manga/official/69000123"
md = "https://mangadex.org/title/17812/kouritsu-chuu-madoushi-daini-no-jinsei-de-madou-wo-kiwameru"
bw = "https://bookwalker.jp/series/88398/"

[chapters]
released = 60
read = 60

[lists]
recommend = "F"
+++

Protag reached the pinnacle of his power and the best in the world at his specialization, but they invented a way to see the potential/levelcaps of people and apparently the specialization the protag was in was his worst stat and was capped fairly lowly; as a result they ousted him from his top position and wanted to give it to someone with a more promising level cap instead (even if nobody was currently better than him). So the protag uses his ultimate magic he recently developed to turn back time and prove everybody wrong and level up every magic stat to become the very best.

<!--more-->

A very large portion of the speech bubbles in this work are transparent with terrible borders around the font which makes it annoying to read. Additionally it's mostly just the characters slapping out random spells willy nilly fighting random monsters constantly while they slowly grow. The protagonist goes off and does things on his own to get stronger while leaving his teammates in the dark constantly and keeps going further and further ahead of them instead of helping them along or giving suggestions on what they should work on in their spare time.

Additionally a lot of the plot beats are kind of asspulls of the protag making a mistake or rushing, and then random shit happens that makes everything work out okay and everybody just chills happily and all of his harem (that he doesn't care about because he's internally like 90 years old) wants his D. The final boss of this work is real pepega shit too. The ending of this work is extremely meme. Feel like it's worth reading this just to experience the absolute trainwreck that is the last 10 chapters or so.

This author's later work is way better than this work so i guess he learned from the mistakes in this one?
