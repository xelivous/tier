+++
title = "異世界黙示録マイノグーラ"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-09-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=170140"
raw = "https://comic-walker.com/contents/detail/KDCW_AM00000023010000_68/"
md = "https://mangadex.org/title/6e156d65-cf65-4d5b-9d04-52f2f8100fbf/isekai-apocalypse-mynoghra-the-conquest-of-the-world-starts-with-the-civilisation-of-ruin"
bw = "https://bookwalker.jp/series/264824"

[chapters]
released = 28.2
read = 28

[lists]
recommend = "A"
+++

Protag regularly played a Civ-like game in his previous life since a majority of his life was spent in a hospital bed afflicted by various illnesses unable to go out, up until he died at the young age of 18; He even managed to be the top ranking player before he died. However after dying he's reincarnated into another world, alongside his favorite civ's Hero (a cute anime girl uwu) and she even remembers everything he did in his past life while controlling the civ and worships him for being a god gamer; in fact he's now literally the god of their civ.

<!--more-->

The art is great at times, and the rest of the time it's just an empty white void with no backgrounds which is kind of disappointing. The most interesting aspect of this work is that the protagonist isn't the only person that was isekai'd, and even on top of that every isekai'd person has their own unique mechanics based on the game they played the most, not just strategy games.

ch21 is powerful.

It's a fairly simple work overall but it's executed in a fairly satisfying way with the only negative being the severe lack of backgrounds.
