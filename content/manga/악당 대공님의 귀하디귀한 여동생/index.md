+++
title = "악당 대공님의 귀하디귀한 여동생"
title_en = "Villain Duke's Precious One"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-14
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=177520"
raw = "http://page.kakao.com/link/56430899"
md = "https://mangadex.org/title/8848b6bd-dfae-4fec-8bc4-8a130394c9a1/the-precious-sister-of-the-villainous-grand-duke"

[chapters]
released = 85
read = 58

[lists]
recommend = "D"
+++

Don't know what to really think about this one. The protag is just too pink for starters. The story itself has meme tier suffering and child abuse that the protag just magically farts away after some time. It spends quite a lot of time slowly increasing the ages of the characters but I feel like it spends a little too long in the young ages to really be interesting. It's mostly a story about a cute sibling relationship but it's not particularly compelling or well thought out. I mostly just enjoy the protag's twinbraids.

<!--more-->
