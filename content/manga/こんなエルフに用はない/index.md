+++
title = "こんなエルフに用はない"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=86170"
#raw = ""
md = "https://mangadex.org/title/403e7b5f-8650-4ff1-9ed5-dbab0bbd3cfa/konna-elf-ni-you-wa-nai"
bw = "https://bookwalker.jp/de02828a69-9503-4b91-89ef-5ab2e1123087"

[chapters]
released = 7
read = 0

[lists]
recommend = ""
+++



<!--more-->

