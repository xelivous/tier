+++
title = "世界最強の後衛"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-19
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148262"
raw = "http://seiga.nicovideo.jp/comic/33376"
md = "https://mangadex.org/title/b347d551-52eb-40cb-a11a-bfb9959d6495/sekai-saikyou-no-kouei-meikyuukoku-no-shinjin-tansakusha"
bw = "https://bookwalker.jp/series/173879"

[chapters]
released = 28
read = 27

[lists]
recommend = "D"
+++

Basically when you die (not of old age?) you get transported to somewhere else and are basically expected to dungeon dive, and unfortunately the protag and most of his coworkers die when heading to a company outing, including the protag's boss who is a hot (younger) girlboss. When choosing his class he ends up choosing the most generic nondescript class imaginable and becomes a corrupted class capable of doing basically anything I guess. As a result the protag is a salaryman ossan who gets a harem of cute girls to fight on the frontlines for him while he sits in the back really epic. Also his healing skill basically brainwashes the girls to want his dick. He gets like 1 new harem member for every 3 chapters; soon he'll have an entire army of haremettes.

<!--more-->

Ch17.5 is nice though. Misaki is good.

Honestly the harem is too big and the characters are paper thin as a result; instead of fleshing out the decent plot it opts to sexualize the harem with weird fanservice constantly which drags the work down. The overall plot is decent enough to continue reading it regardless of that but it'll take many years before it gets anywhere.
