+++
title = "치트라"
title_en = "Chitra"
categories = ["reincarnation", "isekai"]
demographics = ["shoujo"]
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153229"
raw = "https://page.kakao.com/home?seriesId=52044069"
md = "https://mangadex.org/title/c8093355-1ea5-4fa6-a452-cc4da82704cf/chitra"

[chapters]
released = 174
read = 54

[lists]
recommend = "D"
+++

it's a brainlet comedy for fujoshi who love gacha games, and the on-rails "tutorial quest" system just basically tells her what she needs to do to manage her territory. I'm deeply not in the target demographic and I feel like only the target demographic could ever like this.

<!--more-->
