+++
title = "賢者の孫"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=133460"
raw = "https://web-ace.jp/youngaceup/contents/1000015/"
md = "https://mangadex.org/title/18964/kenja-no-mago"
bw = "https://bookwalker.jp/series/90530/"

[chapters]
released = 79.3
read = 62

[lists]
recommend = "C"
+++

Bonus points for actually seriously progressing the protag's relationship, other characters having their own relationships, and not being a harem. Still meme though.

<!--more-->
