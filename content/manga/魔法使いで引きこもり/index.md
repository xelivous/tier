+++
title = "魔法使いで引きこもり"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=150211"
#raw = ""
md = "https://mangadex.org/title/401a7769-588f-4ea1-ae70-d83aea7fd965/mahou-tsukai-de-hikikomori"
bw = "https://bookwalker.jp/193479"

[chapters]
released = 31
read = 0

[lists]
recommend = ""
+++



<!--more-->

