+++
title = "お嬢様はもうおしまい"
title_en = ""
categories = []
demographics = []
statuses = []
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2024-02-28
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/cci42o0"
raw = "https://jp.piccoma.com/web/product/145755"
md = "https://mangadex.org/title/7b1aea97-5885-45b6-99ed-11cfdde3aea8/not-a-lady-anymore"
#bw = ""

[chapters]
released = 0
read = 0

[lists]
recommend = ""
+++



<!--more-->

