+++
title = "元最強の剣士は、異世界魔法に憧れる"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["reincarnation", "isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-19
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147514"
raw = "http://comicride.jp/motosaikyou/"
md = "https://mangadex.org/title/23709/moto-saikyou-no-kenshi-wa-isekai-mahou-ni-akogareru"
bw = "https://bookwalker.jp/series/184401/"

[chapters]
released = 54
read = 43

[lists]
recommend = "D"
+++

Protag becomes the peak swordsman who slayed a god with his epic sword skills, and the (dragon) god grants him his last latent wish to be reincarnated into a world that actually has magic in it, since there was no magic in his previous world. However the protag doesn't have any aptitude and will never be able to learn magic in this world as well! Or so it seems, but he will still persevere and try to learn magic anyways with the hard work and talent he cultivated reaching the peak of swordsmanship in his past life.

<!--more-->

this is like the new game plus of skill autism, mixed with the 'you can't break these cuffs' meme but with a dude cutting everything. If it exists it can be cut. If it doesn't exist it can also be cut. If it may or may not exist it can also certainly be cut. Also pretty much all of the characters have sameface and it's really hard to tell people apart outside of general hairstyle. Cute twinbraid gets introduced in the ch30 range thank you manga, although she immediately leaves the story and never really returns, thanks manga...

"Oh you can't use magic."  
"I can't use magic, you say. That's fine, i'll just cut the very concept of you beliving that i can't use magic to become able to use magic"  
「っ！！！」  

Honestly the story kind of speedruns certain plot points to the point that it feels like entire pages/chapters get left out at times, which to be fair probably happened since it's adapting from a novel and they're just blazing through. Overall outside of the meme factor of a dude who solves every problem by cutting something, it's not all that good.
