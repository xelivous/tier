+++
title = "The Primal Hunter"
title_en = ""
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["english"]
schedules = ["weekly"]
lastmod = 2024-04-18
tags = [
    
]

[links]
#mu = ""
#raw = ""
md = "https://mangadex.org/title/e19faf72-2235-4126-9a77-46f9275de222/primal-hunter"
wt = "https://www.webtoons.com/en/action/the-primal-hunter/list?title_no=5970"

[chapters]
released = 25
read = 20

[lists]
recommend = "D"
+++

Protagonist is your average introverted office worker who doesn't really fit in at all and doesn't really like talking, and feels like he's just not suited for modern society. His only friends are the few coworkers that go out of the way to include him in events, like his boss. One day when going to a mixer with his boss they start to take the elevator downwards when it starts to freefall and a system message pops up stating they're now a part of a tutorial, only to meet a weird alien that gives them a basic rundown of what will happen, and then get teleported to a forest after picking their classes.

<!--more-->

The art is fairly bad, the protagonist is a sociopath/psychopath, protag's coworkers are brainlets and are only brainlets to make the protagonist seem like an epic badass who's epically smart in comparison, etc.

This is apparently an adaption of some litrpg english web novel.
