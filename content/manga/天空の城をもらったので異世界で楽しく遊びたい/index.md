+++
title = "天空の城をもらったので異世界で楽しく遊びたい"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-07-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154759"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04201259010000_68/"
md = "https://mangadex.org/title/38472/tenkuu-no-shiro-o-moratta-no-de-isekai-de-tanoshiku-asobitai"
bw = "https://bookwalker.jp/series/230900/"

[chapters]
released = 44.1
read = 17

[lists]
recommend = "D"
+++

The cover art is the best thing about this series.

<!--more-->
