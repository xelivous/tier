+++
title = "最強の黒騎士♂、戦闘メイド♀に転職しました"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
categories = ["reincarnation"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146959"
raw = "https://comic-boost.com/series/107"
md = "https://mangadex.org/title/29119/saikyou-no-kurokishi-sentou-maid-ni-tenshoku-shimashita"
bw = "https://bookwalker.jp/series/149086/"

[chapters]
released = 54
read = 49

[lists]
recommend = "S"
+++

Protag was one of the strongest knights (male) of the kingdom but ended up dying due to various reasons in war, and ends up reincarnating as an orphan girl in the same country. He eventually gets picked up by nobility to work as a combat maid after showcasing his(her) combat skills to work as a bodyguard. The story starts out as a bang start with the protag already deep into her bodyguard work, and the first chapter exists to speedrun introductions into every major (current) character, whereas the 2nd chapter introduces every major (past) character through a flashback which becomes relevant later.

<!--more-->

Basically a large majority of this work is thge bromance between the protag's past life and his subordinates, and him becoming a girl and thinking himself as a girl naturally in his current life while coming into contact all of the people he left behind in his previous life, along with all of the complicated relationships that happen when your bro reincarnates as a young girl.

i love it. it's kino. please read it. I'm a fan of the author now and want to read all of their other works too; They almost exclusively write josei/shoujo except for this work.
