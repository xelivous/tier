+++
title = "멸망 이후의 세계"
title_en = "The World After the Fall"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-09-30
tags = [
    "male-protagonist",
    "status-windows",
    "apocalypse",
    "tragedy",
    "gore",
    "tragedy",
    "dead-significant-other",
    "eternal-youth",
    "rpg-game-system",
    "appraisal-skill",
    "hacking-the-matrix",
    "merciless-protagonist",
    "breaking-the-fourth-wall",
    "tournament-arc",
    "swordsman-protagonist",
    "battle-maniac-protagonist",
    "frequent-battles",
    "title-drop",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=195824"
raw = "https://comic.naver.com/webtoon/list?titleId=789979"
md = "https://mangadex.org/title/14b81959-13ed-46eb-a277-da66f672acb5/the-world-after-the-fall"
wt = "https://www.webtoons.com/en/action/the-world-after-the-fall/list?title_no=4011"

[chapters]
released = 86
read = 78

[lists]
recommend = "D"
+++

All of a sudden one day a random tower appears in the middle of just about every major city in the world and various people end up seeing a status window that asked them if they would like to participate in the tower, and upon accepting places them at the floor of the tower complete with newbie gear to get them started on their ascent. However at the same time monsters started to flow out of the towers killing a majority of humanity, and it is the tower challengers who will be given the tools/skills needed to fight back and become heroes for the world. However part way up the tower they ended up finding Regression Stones that stated they could send the user back in time to back when the tower first started along with their memories, similar to a roguelite where you could breeze through the tower/game now that you know more the 2nd time. Except they ended up in a separate timeline and essentially doomed their current timeline/planet to its fate; all except for the protagonist and a few other chads.

<!--more-->

I recommend skipping chapter 0.

Protag stabs and stabs until he becomes the absolute stabbing master. His special skill is stab. He transcends and becomes the stab. Have you heard of our lord and saviour, the stab? Has anyone really been far even as decided to stab even go want to do more like? The protag stabs so hard he shatters the fabric of reality and becomes one stab man, transcending spacetime and becoming the coolest stabber in the stab.

Honestly the entire arc in Chaos where the protag just kind of faffs about with dead people is incredibly boring and drags down the work, and that's taking into consideration that stabbing memes is about the only thing it had before that. FUnniest part about this series is the red-haired woman who is introduced, goes along with the protagonist for a while, and then eventually disappears never to be mentioned again. Is the strategy of modern webcomics to inundate you with absolutely terrible chapters for ages until it finally starts throwing in some decent ones so that you think, "wow maybe this work is pretty good (in comparison to the 60 shit chapters i just read)"? Because there are a few borderline decent chapters later on but I can't recommend a few chapters of "payoff" for sitting through the entire rest of the work that is primarily about a dude stabbing.
