+++
title = "ネクストライフ"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation", "game"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-05
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143159"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01000062010000_68/"
md = "https://mangadex.org/title/21604/next-life"
bw = "https://bookwalker.jp/series/137924/"

[chapters]
released = 55
read = 50

[lists]
recommend = "D"
+++

Protag his hiking with his bud up in the mountains when they get trapped by an avalanche/blizzard. Unfortunately the protag falls sleep (for good) while talking about his favorite immersive VRMMO but miraculously wakes up in the middle of a forest in a completely different body that is similar to the avatar he created. The inhabitants he comes across don't understand his language so the entire first chapter is just them trying to communicate without being able to understand each other which is nice. Except that goes away with a Convenient Timeskip directly aftwards in ch2 which makes it kind of meaningless in the first place.

<!--more-->

The main highlight of this work is the cute heroine on the cover. The antagonist at the beginning of the work is too comically evil. Ch29 is bad where it skips over all of the Fun parts and has them reminisce about all of the fun things they did in a flashback while sitting around and eating instead. Also the protag gets a succubus harem later on which just complicates the relationship with annette; arumu do be hot tho. when she's not wearing the dumb succubus outfits and is instead wearing cute outfits she's good.

Overall the plot just kind of meanders along while the protag buttblasts everybody with his broken OP magic, the relationship(s) never really advance, and it just all kind of feels lackluster. The main draw of the work is the cute girls and you might be better off just looking at pixiv/twitter art at that point if that's all you care about.
