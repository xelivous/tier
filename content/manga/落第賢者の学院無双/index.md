+++
title = "落第賢者の学院無双"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["light-novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156932"
raw = "https://magazine.jp.square-enix.com/mangaup/original/rakudai/"
md = "https://mangadex.org/title/41754/the-unsuccessful-yet-academically-unparalleled-sage-a-cheating-s-rank-sorcerer-s-post-rebirth-adventurer-log"
bw = "https://bookwalker.jp/series/233430/"

[chapters]
released = 18.2
read = 18.2

[lists]
recommend = "D"
+++

Protagonist is a part of the hero party that saves the world from the demon lord, but he's the oldest of the group, and ostensibly the weakest of the four. However even so he doesn't have too many troubles and carries on into old age with tons of disciples to take after him. However when he's nearing the end of his life they finally discover the means of telling someone's strongest attributes and the protagonist doesn't have any aptitude in magic at all yet still managed to reach the heights of nearly the very best through sheer effort alone. As a result he feels utterly hopeless in ever achieving a higher level of magic in his life so he dies and reincarnates (yet again) to try one more time.

<!--more-->

Kind of a weird series where the protag dabs on everybody with super powerful magic while nobody else can use it due to Reasons. His mom is kind of hot tho fr. Him getting a cute slave girl is cringe though sry. The girls in this series are cute but the story is pretty bad; maybe you can tolerate through for the cute girls but i doubt it. Once again another failure from the epic kenja universe.
