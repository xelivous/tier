+++
title = "地方騎士ハンズの受難"
title_en = ""
categories = []
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-28
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=120537"
raw = "https://www.alphapolis.co.jp/manga/official/13000100"
md = "https://mangadex.org/title/4fb1e3b5-2324-4550-a22f-3a1a9e314fae/chihou-kishi-hans-no-junan"
bw = "https://bookwalker.jp/series/90637"

[chapters]
released = 54
read = 0

[lists]
recommend = ""
+++



<!--more-->

