+++
title = "ギルドのチートな受付嬢"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-03-09
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146233"
raw = "https://futabanet.jp/list/monster/work/5dca1a9877656163ff000000"
md = "https://mangadex.org/title/7a3b7dfd-4ea6-4021-9e6a-130e6ecf3640/guild-no-cheat-na-uketsukejou"
bw = "https://bookwalker.jp/series/163661"

[chapters]
released = 42
read = 19

[lists]
recommend = "F"
+++

The art is terrible and the story is fairly boring. Basically none of the plot threads come together to form anything meaningful, and a large majority of the work is just the protag giving small bits of advice to people from their encyclopedic brain.

<!--more-->
