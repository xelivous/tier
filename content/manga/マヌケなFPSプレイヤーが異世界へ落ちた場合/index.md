+++
title = "マヌケなFPSプレイヤーが異世界へ落ちた場合"
title_en = ""
statuses = ["hiatus"]
demographics = ["seinen"]
furigana = ["full"]
categories = ["isekai", "game"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["sporadic"]
lastmod = 2023-10-01

[links]
mu = "https://www.mangaupdates.com/series.html?id=130969"
raw = "https://web-ace.jp/youngaceup/contents/1000014"
md = "https://mangadex.org/title/22017/manuke-na-fps-player-ga-isekai-e-ochita-baai"
bw = "https://bookwalker.jp/series/114681/"

[chapters]
released = 24.3
read = 22.3

[lists]
recommend = "D"
+++

Protag regularly plays a VRMMO FPS game every day after he gets home from work where you have power armor to jump around everywhere and shoot things in a 3d space. However one day when playing the game he basically falls out of the map somehow and ends up in an isekai instead but manages to keep his overpowered combat armor, inventory, status windows, and gear. He soon realizes that he is no longer in the game despite still looking like his avatar, and sets out to try and find a way on how to get back home.

<!--more-->

Did the girl captured by goblins really need to be literally ballgagged.... To be honest the protag being unable to communicate and the character just doing whatever the fuck they want with him is amusing, too bad it only lasts like 3 chapters like every other isekai haha lol. it has an uncanny sense of 'humor' that's like tactless deadpan edgy trauma. The power armor is badass at least.
