+++
title = "治癒魔法の間違った使い方"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-07-26
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144018"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04000032010000_68/"
md = "https://mangadex.org/title/21946/the-wrong-way-to-use-healing-magic"
bw = "https://bookwalker.jp/series/131858/"

[chapters]
released = 71
read = 71

[lists]
recommend = "C"
+++

Protag is an average gamer chilling out in highschool people watching since he forgot to bring his umbrella, when the two most popular people in school call out to him and gives him an umbrella and start walking home with him since they're just normal nice people. However on the way home all 3 of them end up getting summoned to an isekai together, and unfortunately the protagonist is just an extra and wasn't intended to be summoned along with the other two, and he can't return home. But he is blessed with the very rare healing element and is taken in with an eccentric squad that fights on the frontlines as battle healers.

<!--more-->

The characters are good. The character interactions are good. The comedy is good. The story is pretty good. In short it's good thx. Or at least the first couple of arcs in this work are very good; unfortunately the "necromancer arc" that's a good ways in (starts around ch28 and ends around ch39) is extremely terrible that i question the sanity of the author and how something so kino can become something so unimaginably garbage. what's the reason for the completely drastic change in quality? There's still some good parts mixed into it but it's just kind of disappointing. It becomes good again after that so I guess just stick with it through the absolutely abysmal arc because ultimately I like this work a lot and I want other people to read it.

Or at least that's what my original thoughts on the manga were, although on reread I think a little differently. I still enjoy the story up until the necromancer arc, but everything after that arc is actually just kind of bad or even mediocre at best which makes this hard to rec. I've now bumped the rating down since I can no longer agree with it being so high.
