+++
title = "不滅のあなたへ"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=137169"
raw = "https://pocket.shonenmagazine.com/episode/13932016480029113164"
md = "https://mangadex.org/title/20131/fumetsu-no-anata-e"
bw = "https://bookwalker.jp/series/96764/"

[chapters]
released = 148.2
read = 0

[lists]
recommend = ""
+++

<!--more-->
