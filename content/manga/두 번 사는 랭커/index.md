+++
title = "두 번 사는 랭커"
title_en = "Second Life Ranker"
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-23
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154346"
raw = "https://page.kakao.com/content/52697368"
md = "https://mangadex.org/title/1ffca916-3ad7-46d2-9591-a9b39e639971/second-life-ranker"

[chapters]
released = 138
read = 123

[lists]
recommend = "D"
+++

It's a story about a dude leveling up and gaining tons of skills mostly effortlessly while everybody around him praises and worships him for being ultra powerful and usurping everybody above him despite being a rookie. It's a standard edgy revenge series where the protag gets dank dark powers that incorporates chinese/greek mythology a lot on a superficial level. It's the most power of power fantasies to the point where it's like you're watching a dude just pay2winning a korean mmo.

<!--more-->

