+++
title = "食べるだけでレベルアップ"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-01-28
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150832"
raw = "https://seiga.nicovideo.jp/comic/34891"
md = "https://mangadex.org/title/e52367ac-5799-46d0-93db-67ef336de6dd/taberu-dake-de-level-up-damegami-to-issho-ni-isekai-musou"
bw = "https://bookwalker.jp/180179"

[chapters]
released = 25
read = 21

[lists]
recommend = "D"
+++

I originally thought this was going to be one of those terrible isekai tourism food manga but it wasn't that, so I was pleasantly surprised. Unfornately ch19 is terrible and everything after it is terrible and/or meme.

<!--more-->
