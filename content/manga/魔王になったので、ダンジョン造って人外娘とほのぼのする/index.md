+++
title = "魔王になったので、ダンジョン造って人外娘とほのぼのする"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-18
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149456"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200334010000_68/"
md = "https://mangadex.org/title/26297/maou-ni-natta-node-dungeon-tsukutte-jingai-musume-to-honobono-suru"
bw = "https://bookwalker.jp/series/187574/"

[chapters]
released = 63.2
read = 63.2

[lists]
recommend = "C"
+++

Protag wakes up as a demon lord next to a dungeon core and basically just has to protect it, but he kind of just wants to chill out and continue to be a regular old japanese dude slacking off in modern society. However unfortunately almost immediately upon stepping out of his cave he meets a super powerful dragon who is raring to obliterate him, yet he manages to basically tame her by giving her the illustrious Modern Chocolate / sweets and she just kind of starts living with him and teaching him stuff instead.

<!--more-->

Cute twinbraid maid :sobaaaa: Cute twinbraid maid :sobaaaa: Cute twinbraid maid :sobaaaa:

Honestly kind of a weird series. The closest comparison is Lazy Dungeon Master since it also has a protagonist who likes lolis and has a thigh/leg fetish but instead of going all in on like loli piss porn this work focuses more on happy home life with his waifu and all of the "children" they take care of together and it basically turns into a slice of life while they occasionally tour around a bit.
