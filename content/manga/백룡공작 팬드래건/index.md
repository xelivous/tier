+++
title = "백룡공작 팬드래건"
title_en = "Duke Pendragon"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=193351"
raw = "https://webtoon.kakao.com/content/백룡공작-팬드래건/2730"
md = "https://mangadex.org/title/1849fc45-8c4c-4d29-9242-5f35ea689023/duke-pendragon"

[chapters]
released = 65
read = 55

[lists]
recommend = "D"
+++

Decent work about a badass mercenary who gets betrayed multiple times and reincarnates/regresses back to being a useless heir with a macguffin of a dragon who wields absolute power. And because he's super chad everybody falls for him and thinks he is cool.

<!--more-->
