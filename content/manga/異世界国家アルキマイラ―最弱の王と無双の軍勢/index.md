+++
title = "異世界国家アルキマイラ―最弱の王と無双の軍勢"
title_en = ""
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-10-08
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/7carrgn"
raw = "https://www.ganganonline.com/title/1303"
md = "https://mangadex.org/title/32ee048e-c84a-4478-bfbb-af134ee99200/isekai-kokka-alchimaira-saijaku-no-ou-to-musou-no-gunzei"
bw = "https://bookwalker.jp/series/312572"

[chapters]
released = 12.1
read = 11.3

[lists]
recommend = "D"
+++

Protag plays a VRMMO strategy game for an excessively long time and builds up his city to be one of the most powerful in the world. He's soon to celebrate his 150th in-game year with a large party when all of a sudden his entire city and army get isekai'd together while he's in the game, all of the NPCs who could only use stock phrases are now able to think and talk freely, and he gets trapped inside of "the game" with no way to log out.

<!--more-->

This is overlord. I don't know if it will eventually end up having tons of other perspectives like overlord, but like the beginning is near identical to overlord. Protag roleplays as a king with a bunch of competent subordinates while spaghetti-ing on the inside trying to maintain his role despite being a weak japanese dude. While slowly taking over the world he finds himself in.
