+++
title = "三大陸英雄記"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=159431"
raw = "https://web-ace.jp/youngaceup/contents/1000136/"
md = "https://mangadex.org/title/69b96982-259f-46e9-a558-438d616b7996/heroic-chronicles-of-the-three-continents"
bw = "https://bookwalker.jp/series/253002/"

[chapters]
released = 19
read = 19

[lists]
recommend = "C"
+++

Protag gets knowledge from his past life as a salaryman in japan by going into a contract with some spirit or whatever, and ends up having to take the role of being the emperor of a dying country after being tricked(?) into it by a cute girl after he gives out random isekai knowledge to improve productivity. Protag is a pushover who will do anything for his cute childhood friend gf. This isn't "isekai" despite sharing a lot of similarities with isekai, although that's not explained until quite a fair bit into the story itself, and his past life isn't actually his past life. 

<!--more-->

The heroine is very cute, although having her wear armor without anything covering her abdomen is dumb, although i guess dariosu also had meme armor without the abdomen so at least there's some gender equality. It's basically a romance with the cute heroine with some kingdom-building and military tactics on the side. The main highlight is that the protag actually like goes out of his way to court the heroine and isn't a pushover wimp, but he sure is a simp. The longer the work goes on the more sexual it gets and it even outright has a sex scene, and on top of that the protag even gets raped.

Ch12 existing is kind of weird; I think it's meant to break up the pacing after the war arc or whatever but it's just a bad cooking chapter that doesn't flow at all in the story. Overall this is a military strategy work where the protag just breezes through with his epic otherworld knowledge while he fawns over his cute gf, but he never actually has sex with his gf and only has it with his succubus demon spirit which is unfortunate. The work also kind of gets rushed and ends while it is still getting started.
