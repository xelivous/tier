+++
title = "善人おっさん、生まれ変わったらSSSランク人生が確定した"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-29
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156775"
raw = "https://seiga.nicovideo.jp/comic/40678"
md = "https://mangadex.org/title/84a16155-afda-47d5-9aec-606e46307231/your-sss-rank-afterlife-is-confirmed-virtuous-old-man"
bw = "https://bookwalker.jp/series/218551"

[chapters]
released = 94
read = 73

[lists]
recommend = "F"
+++

Protag does so many good deeds he gets to have an SSS reincarnation when he dies, and is given the option to either become a god or reincarnate back as a human again, and chooses human because reasons. He also just nonchalantly walks into the reincarnation portal while everybody is still surprised that he's SSS rank so he gets to retain all of the memories of his previous life. He also has super broken magic powers now that he has reincarnated, and because he's so virtuous that he managed to get an SSS rank in his previous life he sets out to improve/fix/heal/benefit the entirety of humanity with his epic powers.

<!--more-->

So you're telling me the 16 year old empress with big booba is swooning over and wants the D of like a 7 year old who is half her height. How utterly dire are her romance prospects if she's digging that deep. The story is kind of all over the place with the protag solving everything magically with his infinite magic knowledge and infinite magic skills infinitely magicking all over everybody. The plot has no overarching longterm goals and it's simply whatever the author thought up at that exact moment and then resolves immediately afterwards with no trouble and everybody constantly praises him. It's to the extent that this is utterly painful to read and I feel it's legit impossible to read 70+ monthly chapters of this shit. I kneel in awe at anybody who is capable of doing so. It gets slightly easier to read when there's more cute girls in the series, enough that I managed to somehow read the rest and get relatively caught up, but it's still pepega.
