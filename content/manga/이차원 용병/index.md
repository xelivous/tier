+++
title = "이차원 용병"
title_en = "Dimensional Mercenary"
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2024-01-28
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154857"
raw = "https://page.kakao.com/home?seriesId=50465822"
md = "https://mangadex.org/title/47c46dcc-7266-4cd6-852b-d8619ca1dd6b/dimensional-mercenary"
#bw = ""

[chapters]
released = 270
read = 249

[lists]
recommend = "S"
+++

Protag is down on his luck, in debt, with his life in the shitter, when he comes across a random job site which teleports him into the middle of an insane battle as someone else. The main premise of this work is that the protag is a cross-dimensional mercenary who solves the requests/tasks of various inhabitants by taking on the role of a person in that world. Most of the time the protag is only given vague guidance on what he needs to do and he has to fill in the rest of the blanks himself. There isn't really a right answer either, since the requester only cares about the completion of the task but not fully about how it was done. There's a good air of mystery throughout the work and the author does a really good job of giving the bare minimum amount of information to make everything intriguing.

<!--more-->

The first season has some really jank artwork that is kind of endearing, which only lasts for 28 chapters, and then the artist changes permanently for season 2 onwards. Every once in a while it will flashback to season 1 again and it's amusing to see the contrast of artstyles later on. The protagonist slowly matures and changes after every dimension he goes through which changes his tactics and available options for every subsequent dimension based off of all of the experiences he had previously, which is just as you'd expect when the protagonist goes through harrowing life-threatening events nearly nonstop in alternate dimensions.

The first dimension the protagonist heads into is a good short work that introduces most of the concepts that will be used in every later dimension in the shortest time possible to give you a taste of the rest of the work. Probably the only successful "bang start" i've read in this genre. 

The second dimension is fairly forgettable in the grand scheme of things considering it's probably the most bog-standard and boring out of every single dimension the protag will come across for the entire rest of the work. In fact you could probably remove it entirely from the work and it wouldn't matter all that much, except for the fact that some things in dimension four would need to be changed. There is something to be said about pacing and something needs to happen before dimension three's events, but it's still relatively enjoyable to read through this dimension so it's "fine".

The third dimension is when the work starts going off the deep end and shoving the protagonist into more and more deeply uncomfortable situations, and it's really where the work starts to hit its stride. The main appeal of this work is that the reader can be dropped right into the middle of a plot, or even the ending of a plot, and you don't really need to spend time building up all of the prior "boring" events leading up to it. You can just think up a cool scenario, and then figure out a way for the protagonist to barrel through while keeping his own unique way of doing things in a way that satifies all parties (including the readers). The third dimension starts building this up but it's not until the fourth dimension that the payoff really starts to hit.

The fourth dimension is the longest arc in the work thus far and it is probably the one that affects the protagonist's emotions the most. This is probably my favorite arc in the work and I feel like dimensional mercenary is worth reading for this arc alone, although it wouldn't be as good without all of the other arcs before it as well. However I do feel like it has a few flaws in how some of the events play out but it's still entertaining to read through.

The fifth dimension goes completely off the rails thanks to the protagonist being completely warped by the fourth dimension, but as a result the arc drags out for a considerably long time, inevitably leading to the author seemingly axing the end of the dimension so they can go back to writing something else. The overall events were fairly entertaining but the constant angst between the protagonist and the Owner were tiring more than anything.

The sixth dimension is my weakness since I fundamentally dislike romance stories, and this is nothing but an incredibly drawn out cringe romance story between a commonor chef and a noble lady knight who curses the fact she was born a woman. This was probably the least satisfactory arc I read, just after the 2nd dimension, and was overall fairly painful to read through.

The seventh dimension is still ongoing so I don't have much to say about it at this time.
