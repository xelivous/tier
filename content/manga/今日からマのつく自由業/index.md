+++
title = "今日からマのつく自由業"
categories = []
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-01
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=214"
#raw = ""
md = "https://mangadex.org/title/1dac9183-5966-4588-beaf-47f944f8e9f7/kyou-kara-ma-no-tsuku-jiyuugyou"
#bw = ""

[chapters]
released = 133
read = 0

[lists]
recommend = ""
+++


<!--more-->
