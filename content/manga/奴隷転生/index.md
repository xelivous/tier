+++
title = "奴隷転生"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-07
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=173301"
raw = "https://pocket.shonenmagazine.com/episode/13933686331722280688"
md = "https://mangadex.org/title/56584/dorei-tensei-sono-dorei-saikyou-no-moto-ouji-ni-tsuki"
bw = "https://bookwalker.jp/series/295378/"

[chapters]
released = 116
read = 116

[lists]
recommend = "B"
+++

Protag is the first prince of a kingdom and a top tier adventurer setting out to defeat a dragon to stake his claim for rule, and intimidate other countries, so he blasts and blasts with his epic wizard powers, but he overexerts himself and ends up dying from using too much magic. However on his deathbed he attempts his unfinished experimental reincarnation magic he was working on, and as a result he wakes up in the body of a young boy inside of a dungeon, and to make matters worse he's a part of a community of slaves that will never be able to become anything other than slaves.

<!--more-->

The work is fairly engaging throughout and is decently well thought out with okay pacing. It kind of feels like the work was rushed/axed near the end but it also fully fulfills the main plotline it set out so it's not completely unsatisfying either. The slave aspect, which is like the entire main point of this work, is kind of bad to begin with and ultimately drags down the work by tying the protag down to a random princess. However it makes the most of the scenario and is overall still probably worth reading.
