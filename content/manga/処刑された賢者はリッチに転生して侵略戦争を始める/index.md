+++
title = "処刑された賢者はリッチに転生して侵略戦争を始める"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169478"
raw = "https://magazine.jp.square-enix.com/mangaup/original/shokei_lich/"
md = "https://mangadex.org/title/c466ced0-ec48-47c1-a8cf-d5ff1fe9d0d2/the-executed-sage-is-reincarnated-as-a-lich-and-starts-an-all-out-war"
bw = "https://bookwalker.jp/series/275434"

[chapters]
released = 16
read = 32

[lists]
recommend = "B"
+++

Protag saves the world by defeating the demon lord with his waifu the hero(ine), but then the king decides to brand them as traitors arbitrarily and kill them off (likely because their power could technically usurp his throne). As a result of the betrayal and fervent desire to live together with his waifu he ends up remaining as a lich long after their bodies have decomposed and eventually sets out to slaughter everybody in his path in an effort to bring about the hero's last wish of peace, in the most twisted way possible. He will become an undefeatable demon lord who will reign over the world with fear for all eternity so that humanity can remain united in their front against them instead of resorting to petty squabbles amongst themselves.

<!--more-->

The work is largely the "idealist vs realist" meme centered around the typical of story of everything being perfectly solved and happy once the Hero defeats the Demon Lord despite humans being full of shit and nothing ever working out so nicely like in the fairytales. It is also largely a battle shounen with more and more meme characters with increasingly strange powers showing up with backstories and names only to get slapped down by the protag. And unfortunately just about every single person in power is excessively evil even moreso than the demon lord, which is just about the only way you can justify someone like the protag going on a rampage everywhere and "totally being the better option".

The succubus do be hot tho.