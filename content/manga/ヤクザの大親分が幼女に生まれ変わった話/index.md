+++
title = "ヤクザの大親分が幼女に生まれ変わった話"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=153070"
raw = "https://comic.pixiv.net/works/5915"
md = "https://mangadex.org/title/76e17aaf-b32c-4a06-9709-74c1ae37868d/the-story-of-a-yakuza-boss-reborn-as-a-little-girl"
bw = "https://bookwalker.jp/series/214647"

[chapters]
released = 30
read = 0

[lists]
recommend = ""
+++



<!--more-->

