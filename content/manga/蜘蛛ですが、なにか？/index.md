+++
title = "蜘蛛ですが、なにか？"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=129680"
raw = "https://web-ace.jp/youngaceup/contents/1000013"
md = "https://mangadex.org/title/17709/kumo-desu-ga-nani-ka"
bw = "https://bookwalker.jp/series/75188/"

[chapters]
released = 65.2
read = 53.2

[lists]
recommend = "B"
+++

If you can read far enough to get out of the cave you'll probably be fine. If you enjoy the cave you'll definitely be fine.

<!--more-->
