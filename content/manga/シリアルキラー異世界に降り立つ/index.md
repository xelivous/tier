+++
title = "シリアルキラー異世界に降り立つ"
title_en = ""
categories = ["isekai","post-isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-25
tags = [
    "male-protagonist",
    "genderbend-to-female",
    "serial-killer-protagonist",
    "rape",
    "heroes-are-evil",
]

[links]
mu = "https://www.mangaupdates.com/series/zjch5fi"
#raw = ""
md = "https://mangadex.org/title/3eaabe03-d2bd-43f3-af71-bd1db4ab564a/serial-killer-isekai-ni-oritatsu"
bw = "https://bookwalker.jp/series/383944"

[chapters]
released = 0
read = 14

[lists]
recommend = "D"
+++

Protag kills tons of people and then eventually gets sentenced to death, dies, and ends up called by a goddess to be summoned to another world to kill more people, but he doesn't want to kill anybody and was happy to have finally died; the goddess doesn't care though and continues summoning him regardless. Ultimately the goal is to kill the other heroes that the goddess summoned into this world that are using their powers for evil purposes since they're drunk on power after the demon lord is defeated. 

<!--more-->

It is quite literally [世界最高の暗殺者、異世界貴族に転生する](/manga/世界最高の暗殺者異世界貴族に転生する) but with tons of heroes instead of just 1, and the protagonist actually immediately just starts killing them. Except the goddess is excessively more meme and annoying, and the characters are overall just meme charicatures.
