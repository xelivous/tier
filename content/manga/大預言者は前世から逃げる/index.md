+++
title = "大預言者は前世から逃げる"
title_en = ""
categories = ["reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158897"
raw = "https://comic-walker.com/contents/detail/KDCW_EB03201131010000_68/"
md = "https://mangadex.org/title/1d6f3dd6-f222-40d4-aefb-f1b5b949b635/the-great-prophet-is-running-from-her-previous-life"
bw = "https://bookwalker.jp/de08757484-09a2-44a1-8009-b2e97a88073b/"

[chapters]
released = 33
read = 11

[lists]
recommend = "D"
+++

The protag is the duke's daughter who is kind of a bitch to everybody, and on her way back home from a party she accidentally runs over someone with her carriage and remembers her past lives where she studies karate in japan and was a fortune teller in her current world after that. In her 2nd life she was killed by getting hit by a carriage driven by a blonde noble, just like what she did just now in her current life; and she additionally had a premonition of bad things happening to the blonde girl. Apparently she reincarnated as the daughter of that blonde girl she cursed for running her over.

<!--more-->

Technically this is an isekai, if you take the protagonist's first life into consideration, but considering that it's almost entirely glossed over and the 2nd life is more prominent to the story i've left the tag off. The first 3 chapters is basically the exact same information said over and over again. Also I legit thought the protag was a man in her first life since the artstyle is kind of weirdly ambiguous. The story is kind of really badly paced and i'm not sure I like the writing in general. I'm not sure who to really rec this to. I'd rec 最強の黒騎士 instead of this every time since it's fairly similar in some ways but that series is actually good.
