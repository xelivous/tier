+++
title = "변경백 서자는 황제였다"
title_en = "Ian the Illegitimate Son Was an Emperor"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2025-01-25
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/jamg1pn"
raw = "https://page.kakao.com/content/64257452"
md = "https://mangadex.org/title/bddba5ae-d308-4434-95fa-67dee73f5626/margrave-s-bastard-son-was-the-emperor"
#bw = ""

[chapters]
released = 60
read = 35

[lists]
recommend = "D"
+++

The protagonist is the empire with the uncommon ability to use magic as typically only commoners wield it and it's looked down upon (for some reason), and as a result of hiding it due to being tricked when he was younger he was eventually taken advantage of and killed by his trusted aide later on. However just before he gets killed another trusted aide with magic powers uses a forbidden spell to send the protagonist back in time, except it's 99 years earlier and he's in an entirely different body. Armed with knowledge of the future and in a shitty situation he'll need to cleverly manuevuer his new life to possibly become emperor once more.

<!--more-->

This is a political intrigue story where the protag just dabs on everybody else by being smarter and knowing future events.
