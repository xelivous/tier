+++
title = "転生先が残念王子だった件"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=175732"
raw = "https://gaugau.futabanet.jp/list/work/5e95b29a77656177aa040000"
md = "https://mangadex.org/title/7596b1b5-186e-4284-a89c-d2a083808e6e/tensei-saki-ga-zannen-ouji-datta-ken-ima-wa-fukkin-1-kai-mo-dekinai-kedo-yasete-isekai-sukuimasu"
bw = "https://bookwalker.jp/series/295046/"

[chapters]
released = 25.3
read = 7

[lists]
recommend = "C"
+++


<!--more-->
