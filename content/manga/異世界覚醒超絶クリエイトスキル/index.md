+++
title = "異世界覚醒超絶クリエイトスキル"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-02-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=183756"
#raw = ""
md = "https://mangadex.org/title/848e33e7-7721-4d77-9fbd-7b5c7bf04a11/isekai-kakusei-chouzetsu-create-skill-seisan-kakou-ni-mezameta-chou-yuunouna-boku-wo-sekai-wa"
bw = "https://bookwalker.jp/series/288536/"

[chapters]
released = 12
read = 8

[lists]
recommend = "F"
+++

The art is pretty bad, and the story is nonsense; for some reason the cover art is good tho.

<!--more-->
