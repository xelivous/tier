+++
title = "Legend"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-12-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=124977"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01000027010000_68/"
md = "https://mangadex.org/title/5f73422e-e47c-41a1-8b5d-96ba13498cfe/legend"
bw = "https://bookwalker.jp/series/75189/"

[chapters]
released = 78
read = 65

[lists]
recommend = "D"
+++

Protag dies and some powerful mage from another world plucks his soul because he's apparently the only one in many years who is compatible with the super OP ultra-epic magic that this mage developed and he wants to pass on all of his OP knowledge to the protag, because he's just that lucky. Not only that, but he gets eternal youth, tons of strength, tons of defence, and everything else! He also seemingly gets to inherit all of the OP skills from all of the mage's friends!!

<!--more-->

Standard adventurer's guild and ranking system; Don't really know why the protag bothered to sign up for it and why he cares with ranking up to a higher rank; he mentions something about needing to get to a higher rank to get better stones, but he can just like, go out into the world and kill anything on his own to get the stones without worrying about being a higher rank in the first place. It's not like the world is walled off to him if he isn't an S rank adventurer or some shit; just go out and slay the dragons my dude. Also there's a lot of unnecessary closeups/ecchi/upskirt shots of the guild staff to the point that it's just gross/weird. Honestly the adventurer guild systems in a lot of these works still bothers me.

Early chapters are kind of bad but it eventually kind of balances out after the first big subjugation quest to be relatively okay in the end although still not really recommendable at all.
