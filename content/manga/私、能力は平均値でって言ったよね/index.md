+++
title = "私、能力は平均値でって言ったよね"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=139985"
raw = "https://www.ganganonline.com/title/1318?fbclid=IwAR1rEZtTfAIFSQPc1m5_9tudtAtGY6ZjaxwQyztxQwf8nfWsh86hlzoicp8"
md = "https://mangadex.org/title/5f709f1b-e481-4e73-86d8-92369545d6f4/watashi-nouryoku-wa-heikinchi-de-tte-itta-yo-ne"
bw = "https://bookwalker.jp/series/108527"

[chapters]
released = 17.3
read = 0

[lists]
recommend = ""
+++



<!--more-->

