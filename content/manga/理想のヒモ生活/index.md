+++
title = "理想のヒモ生活"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=139230"
raw = "https://comic-walker.com/contents/detail/KDCW_KS02000048010000_68/"
md = "https://mangadex.org/title/fac7bdc7-c1f3-4595-82f5-b5bfb002b933/risou-no-himo-seikatsu"
bw = "https://bookwalker.jp/series/119694"

[chapters]
released = 71
read = 58

[lists]
recommend = "B"
+++

Protag is typical corporate slave toiling away his days until he eventually dies when all of a sudden he gets summoned into an isekai by a hot girlboss who then explains to him the situation on why he was summoned. In short, he exists to be a fuckbuddy himbo who has to have no aspirations of his own so as to not ursurp her power later on down the line and in return he gets to live a life of luxury without any real cares, and if he doesn't want that he can just return home.

<!--more-->

This work is primarily about a bunch of capable/competent women who are doing their best despite a patriarchal society belittling them and telling them to get back into the kitchen every step of the way. Until they end up meeting the protag who treats women as actual equals naturally and they fall head over heels for him and want to join his ever growin harem because he's just that cool. It's better than a lot of isekai that focus on politics and gender equality largely because it focuses on adults that actually act like adults, but it also has a fair bit of issues. Probably still okay to recommend to most people though.
