+++
title = "악녀의 딸로 태어났다"
title_en = "The Villainess's Daughter"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-10-02
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=190737"
raw = "https://page.kakao.com/home?seriesId=58038491"
md = "https://mangadex.org/title/6fd4e79c-b717-4f36-8226-5cd17d702547/born-as-the-villainess-daughter"

[chapters]
released = 60
read = 28

[lists]
recommend = "D"
+++

So the protag is trapped in a novel that's full of meme suffering for the sake of meme suffering since it's some extremely badly written turbo trash, and the protag is trying to just like, not die from the turbo trash novel filled with shit people. And then the work contrasts this by pampering the protag constantly in some kind of whiplash of cutesy healing in order to try and "heal" the protag's suffering since it's ultimately shoujo as fuck. I do appreciate that the protag constantly swaps out her ribbons and hairstyles and clothing in general at least v cute.

<!--more-->
