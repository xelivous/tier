+++
title = "くまクマ熊ベアー"
title_en = ""
categories = ["isekai", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-03-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147991"
raw = "https://pash-up.jp/content/00000001"
md = "https://mangadex.org/title/24244/kuma-kuma-kuma-bear"
bw = "https://bookwalker.jp/series/169130/"

[chapters]
released = 85
read = 85

[lists]
recommend = "B"
+++

Protag quits school to play the world's first VRMMO all day everyday. She was able to do this because she got lodes of emone from her parents and worked the stock market to never have to work a day in her life; also she has no friends and hates life haha lol. After a new update she gets a login present and has to choose one of out many boxes, and gets a bear clothing set. Also she gets a survey that basically forces her to wear the bear outfit and isekai's her with some of the game systems but she got returned back to level 1.

<!--more-->

The art is good, the protag is wholesome, and i really like the alternative perspective chapters that kind of recap the arc/volumes. it's a moe work where the protag slaps everybody with her cushy bear mittens while people stare at her unusual outfit.
