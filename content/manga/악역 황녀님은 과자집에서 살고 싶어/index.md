+++
title = "악역 황녀님은 과자집에서 살고 싶어"
title_en = "The Villainous Princess Wants to Live in a Gingerbread House"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-10-02
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=182352"
raw = "https://page.kakao.com/content/57110935"
md = "https://mangadex.org/title/784541d1-1dc4-4309-8a10-e312107f6423/the-villainous-princess-wants-to-live-in-a-cookie-house"

[chapters]
released = 98
read = 42

[lists]
recommend = "D"
+++

So basically the protag reincarnates into a villianess series complete with the standard plot of getting an OP summon thing that is more broken than everybody elses', but since she's a normal happy girl and not a villianess everything kind of just works out for her and everybody loves her while she goes around healing everybody's souls with her random pastries that her summon can recite all of the recipes to through the power of asspull. There's some good braids though fr.

<!--more-->
