+++
title = "魔導具師ダリヤはうつむかない ～Dahliya Wilts No More～"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=153377"
raw = "https://magcomi.com/episode/10834108156763618313"
md = "https://mangadex.org/title/8ca097bf-8680-4295-902d-69ebc36841b1/magic-artisan-dahliya-won-t-hang-her-head-dahliya-wilts-no-more"
bw = "https://bookwalker.jp/series/221252"

[chapters]
released = 34
read = 0

[lists]
recommend = ""
+++



<!--more-->

related to [魔導具師ダリヤはうつむかない　～今日から自由な職人ライフ～](/manga/魔導具師ダリヤはうつむかない　～今日から自由な職人ライフ～)
