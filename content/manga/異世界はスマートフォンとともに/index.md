+++
title = "異世界はスマートフォンとともに"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["every other week"]
lastmod = 2024-07-29
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143464"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04000031010000_68/"
md = "https://mangadex.org/title/21469/in-another-world-with-my-smartphone"
bw = "https://bookwalker.jp/series/116421/"

[chapters]
released = 91.1
read = 61

[lists]
recommend = "D"
+++

Protagonist gets accidentally killed by god and gets asked if he'd like anything in return for the mistake once he gets reincarnated in another world, and immediately asks to be reincarnated along with his smartphone, and since the protag is so polite and understanding god throws in ultra OP skills that basically make the protag invincible as well just because. Upon arriving he coincidentally meets a passing merchant who just so happens to love his highschooler clothing and pays a relatively small amount of money for them and gives protag native-looking clothing so he doesn't stand out, then heads to an inn to have a place to stay for the night. He then coincidentally saves two hot anime girls in the alleyway who tag along with him everywhere for the rest of the story.

<!--more-->

Dude can literally use any magic imaginable at full power and can teleport all over the place, and is basically invincible on top of it, and can get any information he needs out of his smartphone at any time. It's also extremely disney with the chars using the back end of their blades to knock out ruthless bandits that are basically attempted to kill/kidnap people from caravans. The art is extremely simple, and the backgrounds are basically nonexistent with almost exclusively white background panels.

Ultimately it's a harem series where the protag is literally a god but won't kill anybody and ends up being wholesome chungus with all of the kingdoms and eventually builds up a kingdom of his own with his massive harem of princesses.

You'd think this would be a parody work but no it's kind of just dumb.
