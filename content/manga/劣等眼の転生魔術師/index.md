+++
title = "劣等眼の転生魔術師"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["none"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153684"
raw = "http://seiga.nicovideo.jp/comic/37760"
md = "https://mangadex.org/title/36284/the-reincarnation-magician-of-the-inferior-eyes"
bw = "https://bookwalker.jp/series/194756/"

[chapters]
released = 102
read = 100

[lists]
recommend = "C"
+++

Protag lived in a world where all of the demons had a golden eye color, but at the same time some of the strongest magicians also had golden eyes (like the protag) and after beating the demon lord with his pals he ends up killing himself to reincarnate into the future where his amber eyes might no longer be miscontrued to be akin to demons. And the first thing he finds when he wakes up is the daughter of the demon king he spared there to greet him. All of the magicians in the world have regressed to basically baby tier but "science" has picked up in its place to automate everything, and amber eyes basically no longer exist. He also just straight up has sex with the demon lord's daugther after she waited for him for 200 years and got massive booba and prepared everything for him, after he helped her for like a few days and spared her life 200 years ago. All of the extra chapters are super ultra horny as well.

<!--more-->

One of the strangest parts about this series is that a large majority of the text is the protag's thoughts, but he rarely talks even if he's spoken to, just like a jrpg protag where NPCs talk at you and maybe there will be a dialog choice every once in a while. Additionally he doesn't really even try to hide his origin  of being a reincarnator, but he still hides his origin so all of the dialogue is just weird in general. There's also quite a few scenes that exist just for the protag to swoop in and save people from various predicaments without any effort, then stare off into the distance without an expression while thinking dumb thoughts and ignoring the massive harem he's building.

The story is boring as shit slice of life where the protag does everything perfectly every time while teaching/training everybody else while all of the girls want his dick, then 2bit villains come out of nowhere calling him a shitty golden eyes that can't do anything until he slaps them with his epic powers. However it does have some merits of making a modernized fantasy world and the resulting effects of a post-war society that has been at peace for decades mimicking the rapid development of technology of our world. There's a lot of parallels in the story surrounding computer development and mobile phones; how a lot of zoomers don't even understand filesystems or anything to do with computers, but you still need computers and people knowledgeable in the subject in order to continue making phones for the masses. As a result the average ability of society goes down the drain since there's no reason to actually put effort to learn when it's already so convenient to let someone else do all of the hard work for you. The work has a lot of flaws and isn't all that interesting to read but I can understand the author's inspiration and what they're trying to do with the story so it's still interesting enough that i've read 100 chapters of this.
