+++
title = "野球で戦争する異世界で超高校級エースが弱小国家を救うようです"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-16
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=175844"
raw = "https://seiga.nicovideo.jp/comic/48332"
md = "https://mangadex.org/title/1467be9c-017f-4ac9-996a-2653839bee82/in-another-world-where-baseball-is-war-a-high-school-ace-player-will-save-a-weak-nation"
bw = "https://bookwalker.jp/series/287974"

[chapters]
released = 36.2
read = 10

[lists]
recommend = "D"
+++

love japan's love for baseball. tons of ecchi/nudity randomly for no reason. the wolfgirl is hot tho. The premise is kind of flawed from the start since it's basically OP protagonist who already stands at the top of the baseball world on earth, fighting against physically superior isekai races who look down on weak humans and it's basically just a fight against brute force and Superior Earthly Techniques.

<!--more-->
