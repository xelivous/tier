+++
title = "カミヨメ"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=60577"
#raw = ""
md = "https://mangadex.org/title/aac739a3-3f71-420f-be13-5ac81ef333fb/kami-yome"
#bw = ""

[chapters]
released = 12
read = 0

[lists]
recommend = ""
+++



<!--more-->

