+++
title = "リオンクール戦記"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=156424"
raw = "https://webcomicgamma.takeshobo.co.jp/manga/saga_lioncourt/"
md = "https://mangadex.org/title/45458b4e-b851-44b7-a424-47691fb82c06/lion-coeur-senki"
bw = "https://bookwalker.jp/series/250169"

[chapters]
released = 35
read = 0

[lists]
recommend = ""
+++



<!--more-->

