+++
title = "アウトブレイク・カンパニー"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-01
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/7jdk4fv/outbreak-company-moeru-shinryakusha"
#raw = ""
md = "https://mangadex.org/title/5ccca4ae-c54a-4b20-aadb-c822f1fa2960/outbreak-company-moeru-shinryakusha"
bw = "https://bookwalker.jp/series/8958/"

[chapters]
released = 25
read = 0

[lists]
recommend = ""
+++

<!--more-->
