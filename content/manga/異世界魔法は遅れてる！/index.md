+++
title = "異世界魔法は遅れてる！"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=138265"
raw = "https://comic-gardo.com/episode/10834108156661711381"
md = "https://mangadex.org/title/20444/isekai-mahou-wa-okureteru"
bw = "https://bookwalker.jp/series/120431/"

[chapters]
released = 53
read = 24

[lists]
recommend = "D"
+++

Protagonist gets summoned along with 2 of his friends into an isekai, and while his best friend is the Hero the protag is just a straggler. To make matters worse he can't get sent back home and his friend has a death wish wanting to be the isekai ikemen hero and slay the demon lord.

<!--more-->

tldr the rational protagonist OBLITERATES fantasy world with FACTS and LOGIC. there's a few good panels here and there but the writing is kind of garbage and there's a little too much angst. it's like the chuuniest possible isekai and i've read arifureta. For a work that is so extensively about magic it doesn't do a good job of making the magic system seem interesting. For a work so extensively about battles, it doesn't make the battles particuarly interesting either. The girls are moderately cute but the backgrounds are basically non-existent so it's just yet another talking heads series with an excessive amount of battle scenes that are hard to follow. why is the "heroine" wearing a sailor seifuku later on tho....

I wouldn't outright recommend against this but it's hard to recommend.
