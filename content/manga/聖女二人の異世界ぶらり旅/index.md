+++
title = "聖女二人の異世界ぶらり旅"
title_en = ""
categories = ["isekai"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-01-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151453"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00200246010000_68/"
md = "https://mangadex.org/title/27ea199f-a993-4a0a-a571-cb254bed72af/two-saints-wander-off-into-a-different-world"
bw = "https://bookwalker.jp/series/176284"

[chapters]
released = 25
read = 10

[lists]
recommend = "B"
+++

Two office ladies isekai together and then have fun going on an isekai tour getting drunk the end.

<!--more-->
