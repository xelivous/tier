+++
title = "鍛冶屋ではじめる異世界スローライフ"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-16
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172903"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19201711010000_68/"
md = "https://mangadex.org/title/6b06fe1e-ff99-4c8d-be0b-5ad2a4df7603/kajiya-de-hajimeru-isekai-slow-life"
bw = "https://bookwalker.jp/series/288730"

[chapters]
released = 20.2
read = 20

[lists]
recommend = "C"
+++

Protag died while saving a cat from a truck and reincarnates into an isekai thanks to him saving the god of cats, and asks to be able to be a blacksmith, so he gets epic blacksmithing skillz as a result and becomes the very best with no effort. So he reincarnates slightly younger, gets a house with all of the blacksmithing items in the middle of nowhere, and saves a hot beastgirl almost immediately who then lives with him because reasons. Also everything he makes is super brokenly sharp/good because he's a cheat character with broken skills.

<!--more-->

In the early chapters it kind of just glosses over a lot of the blacksmithing portions and the work becomes a little weird as a result, and while the later chapters do go into it a bit more it's pretty obvious the author only has a passing knowledge on blacksmithing in general, and/or can't think of a way to actually make blacksmithing more interesting/varied with an ultra-OP cheat skill user. The beastgirl is also more on the furry side than the anime side as well while still being moe anime girl so if you're into that thing this work is a prime candidate. Beyond that the protag just gets more and more young girls in his workshop who clearly want the D but this isn't outright an ecchi work so nothing happens in that regard; he just gets more and more daughters.
