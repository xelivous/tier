+++
title = "異世界魔王と召喚少女の奴隷魔術"
title_en = ""
categories = ["isekai", "game"]
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
sources = ["light-novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-09-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=124600"
raw = "https://pocket.shonenmagazine.com/episode/10834108156640810986"
md = "https://mangadex.org/title/15950/isekai-maou-to-shoukan-shoujo-no-dorei-majutsu"
bw = "https://bookwalker.jp/series/61211/"

[chapters]
released = 101
read = 93

[lists]
recommend = "C"
+++

Protag is an epic gamer roleplaying as a demon lord in an mmo who is basically undefeated and revels in slaying normies with their healer GFs, when all of a sudden one day after going to sleep he wakes up in another world as a "demon lord" similar to the one he was roleplaying with all of his MMO equipment and the world he was transported to is similar to the MMO he was playing. He was summoned by an elf and a panthergirl to be their demon slave to get out of their unique situations, but due to the protagonist's epic MMO equipment he reverse-slaves them instead. However despite(?) roleplaying as a charismatic demon lord he's a permavirgin wimp who never really talked to a girl before, so he has to try and put up a front and roleplay as much as possible so he's not found out. However the magic and level of this world has severely degraded from his knowledge of the mmo and he's essentially a demigod since he was max level and has tons of knowledge about the game.

<!--more-->

This work is extremely horny to the extent that it's about as far as a shounen can go without becoming a straight up h-doujin, not quite toloveru but so where near. Although it substitutes h-scenes with like extremely meme equivalents at least; it's a very oldschool-core work back from when anime just had fanservice/pantyshots constantly. A majority of the work is the protagonist getting a larger and larger harem of (underage) demihuman girls who want his dick while he's roleplaying as a chad demon lord. Or at the very least I would say it doesn't have anything super explicit, but Volume 16 kicks up it to an excessive degree and has straight up tentacle rape, exhibitionism, and even extremely meme NTR fatdudes etc.

Feel like the biggest problem with this work is that the protagonist doesn't have any dreams, aspirations, or goals. He's simply dragged along for the ride trying to live his life as a neet but keeps getting pulled into wilder and wilder "pls save the world" stories. The beginning of the work is boring because it sets the premise that the protagonist is a god gamer that is like 8x the level of everybody else and at the level cap. Although it manages to work its way out of that situation later on by revealing that the world is the "completed" version of the MMO with tons of updates after the protagonist's death which increased the level cap and introduced a ton of unknown content to the protag to discover again, which allows the power levels to scale to a hilarious degree that makes the beginning of the work seem like a joke in comparison (akin to dragonball Z vs dragonball etc). I feel like the work is trying to be both a meme light slice of life and also a hardcore mmo gamer action series and it's not really achieving either of those and falls short of both.

emile-sama is the most based character in the work and is the highlight of every scene he is in, although he eventually just almost completely disappears from the story after a point which is unfortunate.
