+++
title = "実は俺、最強でした?"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-23
tags = [
    "male-protagonist",
    "protagonist-retains-memories",
    "reincarnated-as-baby",
    "immeasurable-power-level",
    "thrown-out-for-being-too-weak",
    "fighting-as-baby",
    "mage-protagonist",
    "monster-taming-through-naming",
    "nudity",
    "human-transformation",
    "timeskip-from-baby-age",
    "adopted-protagonist",
    "non-blood-related-sibling",
    "arbitrary-magic-system",
    "merciless-protagonist",
    "loved-by-imouto",
    "clones",
    "comedy-focus",
    "super-sentai",
    "school-arc",
    "delusions-are-actually-real",
    "adult-loli",
    "demon-lord-as-love-interest",
    "appraisal-skill",
    "shady-secret-organization",
    "mahou-shoujo",
    "hacking-the-matrix",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153516"
raw = "http://seiga.nicovideo.jp/comic/40386"
md = "https://mangadex.org/title/9484b1fd-0271-4c9b-b096-7e313823058e/jitsu-wa-ore-saikyou-deshita"
bw = "https://bookwalker.jp/series/222358"

[chapters]
released = 75
read = 66

[lists]
recommend = "A"
+++

Protagonist gets bullied in school and becomes a hikkineet until he randomly gets summoned to an isekai by a goddess without even dying, gets offered to reincarnate through random chance, and even gets a super powerful cheat skill to boot for no reason! As a result he wants to strive to have a life where he can just continue being a hikkineet and will do everything in his power to obtain that again. Thus he reincarnates as the baby of a king, but unfortunately his status is seemingly absolutely shit with a terrible level cap and only has the ability to use barrier magic; except his magic power is actually so high it breaks their shitty scales and was misinterpreted haha lol. He then fends for himself in the wilderness with his epic overpowered barrier magic, basically intimidates a wolf that tries to eat him so hard they become his familiar and then transforms into a cute girl to give him breast milk, but she doesn't have breastmilk and instead asks the protag to impregnate her so she can start producing some (big brain fr), but before that happens a relative of the king finds them and adopts the protagonist while providing the milk he needs.

<!--more-->

What even is barrier magic anyways? He can do literally anything and everything with it without any explanation or thought. Oh you want to create a clone of yourself? sure. You want to heal people's injuries? yep got it. Want to create a country-wide surveillance system as if they were security monitors? yep that definitely sounds like a barrier. connecting the barrier to japan and niconico so they can watch anime? well it's certainly breaking all kinds of barriers.

This work is an over-the-top comedy about a dude with a nonsense power that roleplays as a hero of justice super sentai after getting his younger sister addicted to japanese anime through his magic powers. The plot kind of chugs along as the characters get more and more wrapped up in various delusions about super sentai and then there's omake chapters just going all in on comedy. If anything it's probably closest to [陰の実力者になりたくて](/manga/陰の実力者になりたくて/) so if you hate that with a passion then maybe you will hate this as well? I enjoy them both a lot though and I feel like they would appeal to most people so idk.
