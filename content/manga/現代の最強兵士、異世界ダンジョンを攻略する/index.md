+++
title = "現代の最強兵士、異世界ダンジョンを攻略する"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-16
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/ebn5xt6"
raw = "https://web.hykecomic.com/landing_pages/48fbedf8b49e311ba9a4fd6174bc07ee/28.html"
md = "https://mangadex.org/title/9b404699-0aa7-4910-b455-c6a91dda8ae6/gendai-no-saikyou-heishi-isekai-dungeon-o-kouryaku-suru"
#bw = ""

[chapters]
released = 0
read = 43

[lists]
recommend = "D"
+++

Protag is an epic military chad fighting people in a war with his subordinates when all of a sudden he gets isekai'd into a world with magic and status windows like every nerd's dream, but since he's never played any games or read any isekai before he just kind of barrels through with military logic instead. He also learns the isekai language in like 5 seconds upon meeting somebody since he's just so cool.

<!--more-->

Basically the protag has to kill 10 gods and then he'll finally be able to go home, but it took 30 chapters to kill the first god so this thing is going to last for a very long time before it's finally done unless he ultra speedruns through the bosses, which could be a good thing if you're vibing with the series and want to read something for a long time but honestly it might just get super boring before it finishes and get axed.

ch21/22 is omegacringe with the "heroine" basically getting tentacle monster damsel in distress'd.

The way the protag fights is to just use coins dropped from enemies to summon whatever random earth gun/weapon he needs for the exact battle. Need to fight a dragon that is as strong as a tank? Use an anti-tank rifle. Need to mow down some branches? Spawn in a flamethrower. Honestly unless you really want to read a story about an epic military dude dabbing on isekai monsters with superior modern technology this is really hard to recommend.

