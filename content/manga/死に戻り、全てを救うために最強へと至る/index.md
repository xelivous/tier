+++
title = "死に戻り、全てを救うために最強へと至る"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156931"
raw = "https://urasunday.com/title/874"
md = "https://mangadex.org/title/41744/shi-ni-modori-subete-wo-sukuu-tame-ni-saikyou-he-to-itaru"
bw = "https://bookwalker.jp/series/237038/"

[chapters]
released = 65
read = 51

[lists]
recommend = "C"
+++

Protag loses his entire family/village in a tragedy and then devotes himself to killing everybody as a mercenary until one day he comes across a girl who comforts him and thus he simps hard for her and lives for her from then on. However during one battle the girl gets killed in action and then the protag also dies shortly after discovering her body, finally submits himself to death so that he can see his loved ones in heaven again, yet for some reason he reincarnates back to before any misfortune happened as a baby once more. He then resolves himself to change all of the misfortune in his past life and become strong enough to protect everybody.

<!--more-->

Unfortunately the protag is a dumbass, changes large portions of the future without actually changing a lot of the things that mattered and mostly just focuses on making himself as strong as he can be despite him only being able to do so much as a single person in the end. Beyond that the work starts to pick up a fair bit after the first arc and the protag is no longer relying on his future memories too much. However it doesn't really do anything particularly well, the protagonist just kind of gets by from training when he was younger and knowing a few future events. Viviana is hot tbh.

