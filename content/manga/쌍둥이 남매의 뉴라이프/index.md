+++
title = "쌍둥이 남매의 뉴라이프"
title_en = "The Twins' New Life"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["axed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2024-07-25
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=167906"
raw = "https://page.kakao.com/home?seriesId=55025067"
md = "https://mangadex.org/title/e4d4eef1-3d4f-4d52-9044-31e46016dc77/the-twin-siblings-new-life"

[chapters]
released = 159
read = 138

[lists]
recommend = "F"
+++

The protagonists are twins that died, and got reborn as the bastard children between the emperor and a commoner because Plot Reasons, who were mostly abandoned and left to fend for themselves while getting abused by their alcoholic commoner mother and maids for not catching the emperor's eyes. In this world only royalty has blonde hair, and the twins inherited the trademark blonde hair at least, along with the ability to telepathically communicate to each other. However since they've retained all of the memories of their previous life they're able to kind of deal with it and make by until one day their abusive mother dies in the dumbest way possible and they end up getting brought back to the castle.

<!--more-->

It's okay at best. Too many comically evil nobles looking down on the protags because they have "impure" blood in them (furi: not enough incest). It's mostly a "cute" story about familial love except constantly interrupted by dumb babytier drama. It's somewhat cool to have reincarnated twins but I feel it doesn't add that much in the end, and 99% of the PoV is from the girl anyways with the guy just kind of existing. Also the sizes of the children vary between "literally 10 months old" and "maybe 4 years old at best" which is a result of the art just being wonky and the artist over exxagerating their sizes (or not knowing how large children are). Doesn't help that they're even smaller after they get their small growth spurt. And because of meme "lore" reasons, even when they're 12 they're still the size of toddlers.

The work goes into a school arc for the sole purpose of introducing a character, and then forgets it ever had a school arc almost immediately afterwards. It also sets up an extremely cringe age-gap romance with the guardian and the female protagonist. The biggest problem of the work is that there's only a single guardian for the two protagonits, with it set up so the boy is more in tune with the guardian while the girl is more in tune with their spirit/mana, but over time she ends up basically stealing the boy's primary attribute simply because (she's the real protagonist of this story). The romance and constant plot switching/pacing makes basically no sense in this story and actively drags down the tail end of this work even further than the rest of it. The work even forgets that the twins can use telepathy later on in the series and just makes things really weird and akward later on. It's all around a disappointing work that becomes more and more aggravating to read, with the finale being absolutely atrocious.
