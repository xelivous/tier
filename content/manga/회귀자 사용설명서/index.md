+++
title = "회귀자 사용설명서"
title_en = "Regressor Instruction Manual"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-22
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=190936"
raw = "https://page.kakao.com/home?seriesId=58095657"
md = "https://mangadex.org/title/bd1bf883-c7d4-4f4d-857c-29e2470cb693/regressor-instruction-manual"

[chapters]
released = 76
read = 73

[lists]
recommend = "C"
+++

The protag is an extreme sociopath who has the ability to view other people's statuses (and nothing else), and comes across someone with a weird status that states he went back in time. So of course our sociopath protag does everything in his power to hitch a ride with this dude who already knows future events and help him do whatever he needs to bring about the ideal future. There's a lot of "sasuga ainz-sama" moments where the protag tries to figure out what the reincarnator is planning. There's actually a lot of moments where I was tempted to frodopost the events that were happening as well. 

<!--more-->

The work is enjoyable up until the chapters in the 40s which is when it starts shoving tons of memes instead of actually fleshing out conversations properly or even telling a story at all which greatly sours the work and makes me unwilling to continue the work much longer; they end the season shortly afterwards so maybe the author just ran out of ideas but didn't want to stop publishing shit to figure it out? I originally dropped this at the end of s1 just because it was so unbearable, but i've since gone back to read some of S2 and while it still occasionally has some of the cringe references it's not as excessively bad. It actually has decent humor when it's not relying on cheap references too which is a shame whenever it stoops that low!

I feel like one of the highlights of this work is that it focuses quite a fair bit on an Actual yandere which is kind of rare in modern works. Aside from the protag just constantly bullshitting as much as he possibly can in every situation.
