+++
title = "最強剣聖の魔法修行"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=181498"
raw = "https://www.ganganonline.com/title/1144"
md = "https://mangadex.org/title/c0e3b5a0-e3f6-4f61-ade2-829737e8bf78/saikyou-kensei-no-mahou-shugyou-level-99-no-status-wo-tamotta-mama-level-1-kara-yarinaosu"
bw = "https://bookwalker.jp/series/287481"

[chapters]
released = 15
read = 0

[lists]
recommend = ""
+++



<!--more-->

