+++
title = "巻き込まれて異世界転移する奴は、大抵チート"
title_en = ""
categories = []
demographics = ["seinen"]
statuses = []
furigana = []
sources = []
languages = []
schedules = ["monthly"]
lastmod = 2023-10-27
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=144768"
raw = "http://konomanga.jp/manga/taitei-cheat"
md = "https://mangadex.org/title/f58c6ee0-42f7-4e4a-901a-22cdb03a4e38/makikomarete-isekai-teni-suru-yatsu-wa-taitei-cheat"
bw = "https://bookwalker.jp/series/260233"

[chapters]
released = 54
read = 0

[lists]
recommend = ""
+++



<!--more-->

This is an h-manga with horrible art and horrible story and horrible characters. Please just read any of the 100s of high quality h-magazines that exist out there instead.
