+++
title = "魔石グルメ"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-21
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154663"
raw = "https://comic-walker.com/contents/detail/KDCW_FS00000019010000_68/"
md = "https://mangadex.org/title/38291/maseki-gurume-mamono-no-chikara-o-tabeta-ore-wa-saikyou"
bw = "https://bookwalker.jp/series/225253/"

[chapters]
released = 50
read = 49

[lists]
recommend = "C"
+++

Protag dies and gets offered to reincarnate somewhere else and gets to pull the epic reincarnation gacha to choose whawt skill he gets, and ends up getting basically top tier poison resistance. Poison resistance/neutralization is pretty useful in general but it's not too useful to become the very best OP chad in an isekai so the protag sets out to improve himself slowly in whatever ways he can to not be shown up by his younger sibling who has an normal straightforward skill that seems more promising at first glance. However one day when accompanying his mom while she's doing some work he comes across a magic stone and decides to lick it on a whim since it looked delicious, and somehow ends up permanently increasing his status from absorbing the power of the stone which is unique to his skill, which sparks off the story of him become cool OP chad dude.

<!--more-->

When I first read this work I enjoyed it a fair amount; On my re-read after reading just about every other work imaginable I like it a little less unfortunately. The main highlight of this work is the cute romance between the protag and the heroine, but tons of other random epic plots get mixed in that take focus away from that romance. The protag's guard, Chris, is ultimately a pretty annoying character since the author keeps pushing her as some kind of like potential concubine who is clearly falling for the protag and the protag keeps calling her beautiful which also detracts from the romance with the heroine. Honestly the largest problem with this work is the comically evil antagonists, with chapter 36 being peak meme. There's a lot I like about this series and a lot I don't like.
