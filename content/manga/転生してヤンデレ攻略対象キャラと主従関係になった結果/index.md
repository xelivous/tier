+++
title = "転生してヤンデレ攻略対象キャラと主従関係になった結果"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 1970-01-01
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153477"
raw = "https://seiga.nicovideo.jp/comic/39863"
md = "https://mangadex.org/title/71d22175-9ec2-4e06-a937-cd3d07c72a1d/the-result-of-being-reincarnated-is-having-a-master-servant-relationship-with-the-yandere-love"
bw = "https://bookwalker.jp/series/228561"

[chapters]
released = 14
read = 14

[lists]
recommend = "D"
+++

it's kind of just insane. Nothing really makes sense, and it has enough Le Epic Twists that nothing seems to matter anymore. It's short enough that it's worth a read through regardless if you want to read something in the villianess cinematic multiverse but i'm not sure I could recommend it in a general sense. It's definitely a work that would only appeal to people who have read too many villianess stories and have run out of everything else to read

<!--more-->
