+++
title = "劣等人の魔剣使い"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-21
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169080"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666634061"
md = "https://mangadex.org/title/50727/the-reincarnated-inferior-magic-swordsman"
bw = "https://bookwalker.jp/series/280578/"

[chapters]
released = 90
read = 82

[lists]
recommend = "D"
+++

Protag gets abused at work day after day until one day while walking home from work he slips into the boundary between worlds and then gets offered to reincarnate into a different world. However he reincarnated into a village of insane people who sacrificed the protag's body's previous inhabitant due to superstitions (and possibly brainwashing) and was dropped into a cave with tons of monsters in it. Additionally since the god between worlds keeps sending people to this isekai there's lore and history about all of the reincarnated peoplem, who more often than not end up being incompetent and die early.

<!--more-->

The guild clerk is coom and the elf shopkeep is good too. Honestly the character designs in this series are good and the art is great. Honestly beyond that the story isn't anything special. You might want to skim through to see all of the good panels of the guild clerk though since she's a regular character.
