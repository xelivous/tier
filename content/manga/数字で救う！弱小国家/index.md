+++
title = "数字で救う！弱小国家"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155349"
raw = "https://comic-walker.com/contents/detail/KDCW_MF02201046010000_68/"
md = "https://mangadex.org/title/39819/suuji-de-sukuu-jyakushou-kokka"
bw = "https://bookwalker.jp/series/233851/"

[chapters]
released = 33.5
read = 2

[lists]
recommend = "F"
+++

Protag is the grandson of a famour mathematician and grows up to be an insufferable intj asshole who thinks he is smarter than everybody else, and equates everything in life to mathematics. However even though he's super smart and capable he's just working a deadend job as a mover when one day while on the job he gets isekai'd.

<!--more-->

This work is absolutely unbearable. How did this get so many chapters. I legitimately cannot finish this. I do not know who to rec this to or who would even enjoy this.
