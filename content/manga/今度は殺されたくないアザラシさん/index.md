+++
title = "今度は殺されたくないアザラシさん"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=177209"
raw = "https://comic.pixiv.net/works/7226"
md = "https://mangadex.org/title/4c9cf9cd-2d73-42a8-a620-cc5b1b05185b/kondo-wa-korosaretakunai-azarashi-san"
bw = "https://bookwalker.jp/series/309230"

[chapters]
released = 19
read = 0

[lists]
recommend = ""
+++



<!--more-->

