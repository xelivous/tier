+++
title = "極振り拒否して手探りスタート！"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "game"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-07
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=160994"
raw = "https://comic-walker.com/contents/detail/KDCW_FS04201112010000_68/"
md = "https://mangadex.org/title/44405/gokufuri-kyohi-shite-tesaguri-start-tokka-shinai-healer-nakama-to-wakarete-tabi-ni-deru"
bw = "https://bookwalker.jp/series/238542/"

[chapters]
released = 43
read = 31

[lists]
recommend = "B"
+++

Protagonist gets summoned to a blank white area along with 100s of other people simultaneously out of nowhere, and is given a little bit of time to talk with the people around him and figure out his starting stats/appearance before he will be forcibly teleported to the new world. It turns out that the people in the protag's general vicinity are all of his online guildmates in the MMO they regularly played (which is obviously based off of Lineage). However after a long discussion and a ton of back and forth the protag ends up going his separate way from his party and becomes an all-rounder instead of a specialist that works in a party.

<!--more-->

I do enjoy that the manga is somewhat relatively grounded; Character skills/levels and team comps are fairly low-level (aside from obligatory 'unconventional' OPness of the protag due to his race) and just about anything and everything is dangerous. It's a slow work where the protagonist gets very slightly stronger over time while meeting various new people while travelling the world. Honestly if you're interested in an isekai adaptation of oldschool MMOs like lineage/etc then it's probably worth trying out.
