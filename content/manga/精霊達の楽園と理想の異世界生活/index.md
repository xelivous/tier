+++
title = "精霊達の楽園と理想の異世界生活"
title_en = ""
categories = []
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149689"
raw = "https://comic-boost.com/series/101"
md = "https://mangadex.org/title/29595/seirei-tachi-no-rakuen-to-risou-no-isekai-seikatsu"
bw = "https://bookwalker.jp/series/175134/"

[chapters]
released = 56
read = 20

[lists]
recommend = "D"
+++

It's basically a town-building simulator but without the town, where the protag cheats his way to cultivating a barren wasteland. Actually thinking about it some more, it's Rune Factory.

<!--more-->
