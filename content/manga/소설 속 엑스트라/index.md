+++
title = "소설 속 엑스트라"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2025-01-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/dktahwe/the-novel-s-extra-2022"
raw = "https://page.kakao.com/content/59365151"
md = "https://mangadex.org/title/6f18c4ef-3eb6-40ee-98a4-507343f05855/the-novel-s-extra"
#bw = ""

[chapters]
released = 113
read = 99

[lists]
recommend = "C"
+++

The protagonist is an author who wrote about a world where gate outbreaks happen only for awakeners to appear and save everybody from the apocalypse, except he put the webnovel on hiatus since he had writer's block only to receive an email from a "random fan" asking if they could rewrite/remake the story like fanfiction. However for some reason he ended up somehow reincarnating in this fan's "remake" story as a random background mob character without a face instead of as the protagonist. He will now have to live in this novel for what he assumes to be at least 10 years up until he reaches the point where he had writer's block, since he doesn't immediately know that he is in the "remade" story at the start.

<!--more-->

The protagonist knows a majority of the future events and will spoil what is going to happen before it happens, with the only twist being that the co-author continues to rewrite certain events specifically to troll the protag's future knowledge and make the plot events harder to survive. On top of the protagonist being the author of this world he comes off as kind of a douche at times, and easily becomes the top of his class in terms of academics. Half of the work is the protag getting glazed, and the other half is weird harem antics where he makes every woman in his general vicinity fall for him. There's definitely a lot of "sasuga ainz-sama" moments in the work since he keeps trying to hide his power level which causes everybody else to vastly overestimate him, which will likely cause him to get into trouble later on. It's a very simple battle shounen where the world and magic system doesn't make too much sense, and the abilities are also incredibly simplistic with weird descriptions that don't feel like they'd even work (which could be a fault of the translation).

It's entertaining enough, but I'm not that much of a fan of it personally. It's a standard magic academy work except every plot point gets half-spoiled right before it happens.
