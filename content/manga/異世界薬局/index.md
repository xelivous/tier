+++
title = "異世界薬局"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-25
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=138217"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000031010000_68/"
md = "https://mangadex.org/title/20398/isekai-yakkyoku"
bw = "https://bookwalker.jp/series/107987/"

[chapters]
released = 56
read = 55

[lists]
recommend = "A"
+++

The very first page is the character reminiscing about his very cute twinbraid imouto. Unfortunately she dies at an early age due to a brain tumor, which then drives the protagonist to dive deep into the world of Big Pharma and develop as many wonder drugs as possible in order to hopefully prevent others from losing their cute twinbraid imoutos like he did. However despite managing to create a ton of wonderdrugs capable of curing millions and being the leading scientist in his field at a young age he overworks himself to death and dies of a heart attack, only to wake up in the body of a child in an isekai with the name "Pharma" (lol). Essentially the child died of a lightning strike, and god of pharmacy swapped the protagonist into this child's body with a few extra powers to essentially bolster and improve the pharmaceutical knowledge of this medieval world. However he also doesn't have a shadow, and if found out he might be ostracized as a possessed demon or otherwise to the medieval residents of this world who still believe in bizarre crackpot remedies.

<!--more-->

I like this work a lot. It does a really good job of introducing the basic and even intermediate aspects of hygiene, diseases, pandemics, and all manners of other subjects in a way that anybody can understand while weaving an interesting story with pretty good pacing. The artist also manages to draw very handsome/beautiful men and women and there's a good amount of work that goes into the backgrounds throughout the work, although there was a small misstep imo on the in-world artist dude's changed artstyle later on but wcyd. It's also a rare case of the church in a work being actually really kino and great, with the head priest dude being a really great character in general; I love some of his later scenes with Pharma-kun around the late 40s.
