+++
title = "水神の生贄"
title_en = ""
statuses = ["completed"]
demographics = ["shoujo"]
furigana = ["full"]
categories = ["isekai"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=119023"
md = "https://mangadex.org/title/15248/suijin-no-hanayome"
bw = "https://bookwalker.jp/series/127827/"

[chapters]
released = 44.5
read = 44.5

[lists]
recommend = "D"
+++

Protag is a young girl who gets abducted through a small pool in her backyard while playing with her family, and ends up in an isekai in the middle of a forest after popping out of a lake. She then meets two young children who came across her by chance, and due to being scared and alone in a forest she ends up following them in hopes of finding her family again. However since this is obviously an archaic and lowtech society that was obviously the wrong choice and misfortune soon befalls her.

<!--more-->

Basically a girl gets stockholm syndrome'd into liking an emotionless god who belittles humans because she's very young, impressionable, and wants nothing more than to just go home and see her parents again. It's a romance work where the protag makes everything worse around her just by existing and her emotional outbursts literally call forth rain, while she slowly gets brainwashed into liking an asshole and people keep trying to NTR her. It's the most shoujo thing possible, which isn't necessarily a bad thing, but you should at least be warned. If the god wasn't the most ikemen pretty boy imaginable and a literal god this would be an entirely different work.

I do enjoy a lot of the earlier chapters where the protag literally gets her voice taken from her so you essentially just have a ton of chapters of no dialogue, but if you even stop for a minute to think about her situation and how eventually she comes to love the god it's just cringe.
