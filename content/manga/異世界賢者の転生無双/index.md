+++
title = "異世界賢者の転生無双"
title_en = ""
categories = ["reincarnation", "isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=162833"
raw = "https://www.ganganonline.com/contents/isekaikenjya/"
md = "https://mangadex.org/title/3d3dda69-7387-4732-b114-e4bb3de67180/isekai-kenja-no-tensei-musou-geemu-no-chishiki-de-isekai-saikyou"
bw = "https://bookwalker.jp/series/236589/"

[chapters]
released = 32.3
read = 28

[lists]
recommend = "B"
+++

Protagonist is an unhealthy gamer who stays up for nights on end theorycrafting strats for defeating raid bosses in VRMMOs and one day after successfully completing a hard raid he gets a heart attack and dies due to existing complications. However her opens his eyes shortly afterwards and realizes he was reborn in an isekai; In fact he was born in an isekai that is extremely similar to the VRMMO that he dedicated his last moments to and died playing.

<!--more-->

I'm actually enjoying this a fair amount. A standard powerfantasy game reincarnation scenario where the protag dabs on the plebs with his superior mmo gamer knowledge.