+++
title = "メイデーア転生物語"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-05-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=161897"
raw = "https://magazine.jp.square-enix.com/gfantasy/story/maydare/"
md = "https://mangadex.org/title/9e2707b8-b723-41de-a608-4e4d9ce2f99c/tales-of-reincarnation-in-maydare"
bw = "https://bookwalker.jp/series/258922"

[chapters]
released = 25
read = 25

[lists]
recommend = "C"
+++

Protag has a crush on a dude but this hanger-on girl that is always around her tries to steal the guy in like a weird attempt to bully the protag, and when the totally not NTR event is happening they both get murdered by some random blonde dude. Additionally the protag gets murdered at the same time. As a result the real protag wakes up, realizes it's all a dream and condemns the girl in the dream for being an idiot who should've just been more confident. The protag is cute though fr.

<!--more-->

It's a little too drama-filled and a little too romance-oriented. But holy fuck just fucking say it. Say you fucking love him. I swear to god stop fucking postponing it you fucks. Ch21 is especially egregious and almost irredeemable. And then ch22 is just so completely meme that it's hard to take the series seriously.
