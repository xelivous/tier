+++
title = "乙女ゲー世界はモブに厳しい世界です"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
categories = ["game", "isekai"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151017"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200541010000_68/"
md = "https://mangadex.org/title/31032/the-world-of-otome-games-is-tough-for-mobs"
bw = "https://bookwalker.jp/series/207004/"

[chapters]
released = 53
read = 54

[lists]
recommend = "B"
+++

Protag is absolutely whipped by his imouto and ends up playing her otome game for her, even though she basically kept ruining the protag's life. The otome game in question is a bizarre elaborate mecha srpg(?) with in-game monetization basically necessary to beat the game, and where men are trash slaves who have to bow down to women. tldr it's a kusoge. However after staying up for 2 days straight to complete this game for her he ends up blacking out and dying, only to reincarnate into aforementioned kusoge.

<!--more-->

The main fundamental premise of this work is that it's flipping the "women are only valuable until they're like 20 and then they're no longer desireable" trope and instead applying it to men. However it only flips that portion, and not the fact that the men still end up doing 90% of the work including fighting in wars and making all of the money, while the women just kind of sit around at home. As a result the work kind of just feels weird since it's trying to make a "point" with the roles reversed by not actually reversing the roles properly. A work that properly fully reverses the roles and forces the protag to go fully outside of his comfort zone to life in a society where men are dependent on women are the breadwinners and he has to do his best to either appeal to them or set on out his own path could be fun to read, but this isn't it.

However despite all of that it's a story about a protag slowly working his way from the ground up in an unreasonable world with a little help from his past life's knowledge of where certain items are. The protag is just completely fucking done with everything and wants to chill but keeps getting roped into shit, and keeps roping himself into even deeper shit, essentially falling upwards. The work also does a decent job of showing all of the negative aspects of the protag essentially taking all of the glory for himself.

It has issues but i would still say it's enjoyable/good, at the very least it's very entertaining to read the absolute shitbag of a protagonist stumble his way through interpersonal relationships and the other reincarnator messing everything up.
