+++
title = "라임 오딧세이"
title_en = "Lime Odyssey: The Chronicles of ORTA"
categories = []
demographics = []
statuses = []
furigana = []
sources = []
languages = ["korean"]
schedules = []
lastmod = 2023-10-11
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=87630"
raw = "http://cartoon.media.daum.net/webtoon/view/lime"
md = "https://mangadex.org/title/2c49c479-791f-4cc8-b82a-eb5c25c4c508/lime-odyssey-the-chronicles-of-orta"
#bw = ""

[chapters]
released = 45
read = 0

[lists]
recommend = ""
+++

Protagonist has a dream where she basically drowns despite regularly avoiding water irl since she doesn't know how to swim, while a random dude tells her to find a lime, but manages to survive by grabbing onto a rope that is in the water that pulls her up. However when she wakes up from her dream she's holding onto a male classmate's tie and basically strangling him from pulling on it. After class is done she follows him a bit and basically just rips the tie off of him to go get it fixed/cleaned after basically ruining it in class, but ends up falling into a river, isekai-ing, and turning into a man. Apparently the protagonist essentially took over the body of some random dude in an isekai who had an entire life and friends before the protagonist took over.

<!--more-->

The first chapters are utterly bizarre. 

I researched this a bit and apparently this work is based off a F2P mmo in korea that released open beta in 2012, had tons of server issues with the launch and generally looked like shit for the era and was panned constantly. The developers then stated it would probably shut down relatively soon, but then the webtoon was made in late 2012 and the game relaunched as part of a weird promotion. Then later on in 2014 the game shut down for good. Ultimately this work is a bizarre advertisement for a long dead MMO.
