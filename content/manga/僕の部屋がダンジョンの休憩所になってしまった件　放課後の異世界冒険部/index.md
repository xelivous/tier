+++
title = "僕の部屋がダンジョンの休憩所になってしまった件　放課後の異世界冒険部"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["none"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2022-04-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=171602"
#raw = ""
md = "https://mangadex.org/title/86391a2b-c9e3-456d-bec2-3c9dbe799a83/houkago-no-isekai-boukenbu"
bw = "https://bookwalker.jp/series/235266"

[chapters]
released = 12
read = 12

[lists]
recommend = "D"
+++

Okay so this is like a fully independent spinoff version of 僕の部屋がダンジョンの休憩所になってしまった件 that is short and a little more streamlined. It has some cute romance scenes and almost entirely revolves around the romance with some dungeon crawling flair. I guess this is where all of the romance from the main story went, which is unfortunate since it doesn't have any top tier booba elf.

<!--more-->
