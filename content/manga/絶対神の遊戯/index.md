+++
title = "絶対神の遊戯"
title_en = ""
categories = []
demographics = ["seinen"]
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-01
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=194261"
raw = "https://piccoma.com/web/product/81985"
md = "https://mangadex.org/title/f789b79b-0712-4080-82e6-87888df95fa2/zettai-kami-no-yuugi"
#bw = ""

[chapters]
released = 35
read = 10

[lists]
recommend = ""
+++

webcomics suck tbh. i'd rather read just about any other "death game" series in existence than suffer through this.

<!--more-->
