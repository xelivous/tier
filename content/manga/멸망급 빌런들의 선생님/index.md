+++
title = "멸망급 빌런들의 선생님"
title_en = "Master of Villains"
categories = ["isekai", "reincarnation", "game"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-09-03
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/gxheag1/master-of-villains"
raw = "https://comic.naver.com/webtoon/list?titleId=784843"
md = "https://mangadex.org/title/4f3ea018-11c5-414e-9964-cc0ee710b76b/master-of-villains"
wt = "https://www.webtoons.com/en/action/master-of-villains/list?title_no=4468"

[chapters]
released = 150
read = 141

[lists]
recommend = "D"
+++

Protagonist wakes up in a scifi bunker with a headache, something akin to a prison outfit, and for some reason has a knife. Apparently he's a test subject in a lab and he's already slaughtered all of his fellow test subjects along with a majority of the scientists after there was a massive breakout. He only then realizes that he is in a singleplayer VR game he created in his past life and he's currently taking part in the background scenario for one of the main Villains of the game. However there's still a cute loli researcher alive and conveniently she was in charge of him originally, so he ends up trying to work with her to get out of here and figure out how to leave the game.

<!--more-->

This is horrible. The art sucks, the characters suck, the plot sucks. Shoutout to them implementing gacha mechanics in a single player RPG and also the protagonist getting 10-star (max) rarity constantly every time he pulls in order to deus ex machina his way out of everything with P2W power. Although I guess minor shoutout to the "subversion" in ch7. Eventually the work transforms into something oddly bizarre, and it's largely the protagonist teaching a bunch of kids (who would've become villains themselves) in a school setting, and then he's eventually forced to livestream his "gameplay" due to a shady contract on a streaming site made specifically for this VR game.

This work is extremely 不気味. I can see what their vision was but they just weren't skilled(?) enough to fully realize it both in plot and art so you end up with this almost borderline amatuerish work, but I also respect that they followed through on this plot for so long and actually improved their art as time went along. It's extremely sovl. Honestly this work cooked, and while it did burn a lot of food along the way, still managed to make something endearing and borderline passable. It's a shounen work about a dude training a bunch of kids that could've ended up as villains originally to instead use their powers for good and save the world; to slowly become re-integrated into society and become respected members for everybody else to look up to.

I feel like the author forgot that he originally made the redhead SMU commander a necromancer of sorts when he first introduced her, since after a while she just, stopped being a necromancer and was instead just a brute force scythe wielder instead or whatever. And then finally like 70 chapters later she uses the skelebros again wcyd.

The beginning of this work is like uniquely bad in that it's really hard to recommend since saying "it becomes tolerably okay later on" isn't necessarily a glowing endorsement of a work, but at the very least I managed to make it to the end which is more than I can say for certain korean webtoons. I respect that this work exists and that it managed to reach "completion". Not sure i'd recommend it, but it's cool that it exists.
