+++
title = "ナイツ＆マジック"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2023-09-22
tags = [
    "mecha",
    "death-by-truck",
    "reincarnated-as-baby",
    "knight-protagonist",
    "programming-magic-system",
    "school-arc",
    "commander-protagonist",
    "multiple-perspectives",
    "war",
    "frequent-battles",
    "battle-maniac-protagonist",
]

[links]
mu = "https://www.mangaupdates.com/series/24bxh2x/knights-magic"
raw = "https://magazine.jp.square-enix.com/yg/introduction/knightsandm/"
md = "https://mangadex.org/title/1e03d257-f014-4f96-bc42-d28ed874ff1b/knights-magic"
bw = "https://bookwalker.jp/series/83632"

[chapters]
released = 122
read = 122

[lists]
recommend = "C"
+++

Protag is a programmer at a software company who really loves building mecha model kits in his spare time and loves mecha in general, when one day after saving everybody with his epic coding skills and buying the latest model kit he gets run over by a car and dies. He then reincarnates into an isekai that actually has mecha that is controlled by magic as the main source of their military, and the protag just absolutely loses it.

<!--more-->

The early portion of this work is the protag hellbent on creating giant mecha, and then the work eventually transitions into weird politics and endless mecha battles after the protagonist successfully creates a mecha. The school arc is mostly glossed over with the protag just constantly swapping through multiple high level classes without actually following any set curriculum. You also rarely get the protagonist's perspective and rarely get to hear his thoughts, instead the work vaguely follows characters around him that commentate on how insane he is.

The biggest problem with this work when it starts getting heavily into wars is the overall sense of time. Protagonist's group blasts the enemy with their superior technology, then pulls back, enemy develops an entirely new thing to counter them, they fight again, protag develops an entirely new thing based on that technology and blasts them away again, repeat. Having a ton of time to fully develop new shit and manufacture enough of them doesn't really make any sense and the sole reason it exists is to just introduce a ton of more mecha designs and varied without changing arcs/enemies. If it did actually play out in an even remotely realistic manner the protag's faction would just obliterate the entire rest of the continent and nobody else would really be able to fight back and the work would be done almost immediately, until the protag starts leaking out his own designs everywhere constantly. Maybe that would even be more interesting than having completely contrived politics/wars break out constantly where the protag can't steamroll them immediately for the sake of plot. And damn does the war go on for fucking ages.

tldr: first part of the work is "unlike other mecha, this one is about the mecha", then the later portions are "unlike other mecha, this one is about the characters" which is every mecha really.

I could recommend it to anybody who really wants to read a mecha and something that doesn't have all of the normal tropes of isekai works, but that's about it.
