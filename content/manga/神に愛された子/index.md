+++
title = "神に愛された子"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-03-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=164513"
raw = "https://www.alphapolis.co.jp/manga/official/738000291"
md = "https://mangadex.org/title/c63c5f01-2cbd-43fc-8668-544f0a6b148e/the-child-loved-by-god"
bw = "https://bookwalker.jp/series/241782/"

[chapters]
released = 29
read = 14

[lists]
recommend = "F"
+++

This story is extremely ADHD and none of the plot progression makes any sense. One minute he's joining the adventurer guild and saving some random kid, the next minute he's in the middle of a largescale war nuking the bad people's souls and resolving everything by simply existing, and then after that he's going to magic school despite having the most broken OP magic to ever exist that transcends all knowledge.

<!--more-->
