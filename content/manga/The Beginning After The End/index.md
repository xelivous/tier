+++
title = "The Beginning After the End"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["english"]
schedules = ["weekly"]
lastmod = 2025-01-23
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/rwg23en/the-beginning-after-the-end"
raw = "https://tapas.io/series/tbate-comic/info"
md = "https://mangadex.org/title/4ada20eb-085a-491a-8c49-477ab42014d7/the-beginning-after-the-end"
#bw = ""

[chapters]
released = 204
read = 181

[lists]
recommend = "C"
+++

The protagonist is an epic king who is the bestest at everything, but inevitably succumbs to death due to Reasons, only to wake up in the body of an infant in an isekai. Despite being an incredibly strange child that doesn't cry at all his parents just overlook his quirks while he sets out to learn more about the world, and even tries to learn magic almost immediately after seeing his mother perform it.

<!--more-->

Someone read mushoku tensei and thought they could make something better, then they followed through with releasing this work. A large portion of the plot points feel like they are "inspired" by mushoku even if most of the individual plot points are fairly different; it's not to the extent of plaigarism but you can definitely feel the influence in a majority of the work. 

The early portions focus on him as an (incredibly creepy) child who acts like an old man in a child's body, immediately turbo speedruns becoming a mage, and doesn't even try to hide himself being a skilled swordsman/etc; his parents and their adventurer friends just kind of gloss over it and everything involving these early chapters are just kind of terrible as a result. He even has a childhood friend elf who he saves, trains with, then eventually leaves behind. Time skips forward a fair bit while the protag slowly builds up experience and skills, meeting a few other "heroine candidates" in the process, then eventually meet up again in a long and fairly pointless academy arc that serves solely to introduce a ton of supplementary characters that will be used in later plot developments (and to glaze the protag even harder). There's even more plot points I could write down that would make the parallels to mushoku even clearer, but I think you get the point. This work does a few things better than mushoku, some things worse, because it is ultimately an entirely different work. But so many of the plot points just make no sense at all, especially the aftermath of the academy invasion.

The art is pretty terrible at the start but slowly improves over time with each chapter released; but the artist did at least go through the effort of actually modelling a majority of the scenes and there's clearly a ton of effort that was put into it over all. By the time season 5 ended the art was actually well above average for manwha in general; the artist really improved over time. Unfortunately Kakao bought Tapas shortly after season 5 ended (this is a Tapas Original work), and when renegotiating the contract for the artist/studio behind the adaptation Kakao didn't want to pay a decent amount anymore and the studio producing this work unfortunately stopped [^fuyuki]. Kakao then spent a while making a ton of random side content with different artists, until it finally found someone to agree to producing Season 6 with the lower pay. And the new art, panelling, modelling, overall scene composition, all just suck so much more than all of the seasons before it, even the first season which was kind of abysmal. The drop in quality is insane and I almost can't recommend this work solely because of this blight.

I enjoyed S1-S5 and I could rec this if you want to read EOP mushoku. But you might have to read the web novel after S5.

[^fuyuki]: https://www.instagram.com/p/CtzAWMApvmr/