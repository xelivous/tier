+++
title = "THE KING OF FANTASY 八神庵の異世界無双 月を見るたび思い出せ！"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
sources = []
furigana = []
categories = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=158698"
raw = "https://comic-walker.com/contents/detail/KDCW_MF09000003010000_68/"
md = "https://mangadex.org/title/43337/the-king-of-fantasy"

[chapters]
released = 26
read = 0

[lists]
recommend = ""
+++

<!--more-->

