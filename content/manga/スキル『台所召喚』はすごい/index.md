+++
title = "スキル『台所召喚』はすごい"
title_en = ""
categories = ["isekai"]
demographics = ["shoujo"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2022-03-26
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153524"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00200531010000_68"
md = "https://mangadex.org/title/8d525be8-f9c6-4231-9f35-5c67787a1ad3/this-summon-kitchen-skill-is-amazing-amassing-points-by-cooking-in-another-world"
bw = "https://bookwalker.jp/series/194946"

[chapters]
released = 11.5
read = 11.5

[lists]
recommend = "D"
+++

This is a cooking manga, if you somehow couldn't tell by the title, and it's also a "saintess" series. A large majority of the work is some basic romance with her guard that protects her, while she makes various food that gets wished into existence with her skill. The LN might go into more interesting topics with regards to the other saintess that got summoned alongside the protagonist but it never adapts that far so it's pointless to speculate on.

<!--more-->

As far as i'm aware this series was only ever planned to have 2 volumes of manga adapated for it, so it's "completed" in that sense, but the story itself isn't completed and it's mostly just a teaser for the LN similar to how an anime is just an advertisement for the manga/LN  usually. It has a decent sense of humor and overall is a pretty decent series, I do actually like it a fair bit for what's there, it's just a shame it is so short and will never be continued.

Wildest part about this series is the popeye reference.
