+++
title = "異世界召喚は二度目です"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-03
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149154"
raw = "https://futabanet.jp/list/monster/work/5dce9a157765614651010000"
md = "https://mangadex.org/title/28735/isekai-shoukan-wa-nidome-desu"
bw = "https://bookwalker.jp/series/184431/"

[chapters]
released = 48.3
read = 21

[lists]
recommend = "D"
+++

Protagonist got summoned to a world alone, saved it, got forcibly returned back to japan, yet kept all of his powers after returning to japan. However after some time he gets summoned along with his entire class to the same isekai and vows to enjoy his time in this isekai as someone who is broken OP from the start. 

<!--more-->

Basically the first thing he does is take the class instructor who used to be his former slave and spanks her butt until she faints/comes from the pleasure. The work doesn't improve from here; the protag just kind of strolls around the world meeting the people he had to leave behind again while they lust after him, and he just slaps some fools with his overwhelming strength. There's basically only solid color backgrounds as well so the art is fairly lazily done. Biggest issue is that the protagonist refuses to kill anybody so you just get the villain of the of week constantly bothering the protag over and over again without anything changing.

Why did this get an anime.
