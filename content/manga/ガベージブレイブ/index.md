+++
title = "ガベージブレイブ"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156672"
raw = "https://magcomi.com/episode/10834108156766378470"
md = "https://mangadex.org/title/737c292b-5b97-41bb-8b04-51bcc945f7a7/garbage-brave"
bw = "https://bookwalker.jp/series/250985"

[chapters]
released = 46
read = 34

[lists]
recommend = "D"
+++

Protag is chilling out in his home ec class (he's a good cook apparently) and the popular girl starts praising him for his mad cooking skills, when all of a sudden his entire class gets isekai'd together. Immediately a bunch of nobles of that world start bidding on the newly summoned heroes after appraising their potential, essentially buying them from the summoner, to go off to their countries and vanquish their demon lords. Except the protag has a useless cook job and basically nothing else so he gets thrown out without even being bid on, and the summoner basically just pushes him through a portal into the middle of a monster infested forest for the protag to go off and die. 

<!--more-->

Kind of does a revenge isekai speedrun at the start, then devolves into a bizarre murder mystery where 90% of it is done offscreen, and then goes back to the revenge but he constantly gets sidetracked by something else. He also conveniently gets more skills whenever he eats monster meat!! The beastgirls are hot tho ngl, i think it's the way this artist draws eyes tbh, too bad he gets a normal human girl as a slave instead haha lol (although he does get a booba beastgirl later).
