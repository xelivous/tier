+++
title = "역대급 영지 설계사"
title_en = "The Greatest Estate Developer"
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-14
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=186405"
raw = "https://comic.naver.com/webtoon/list?titleId=777767"
md = "https://mangadex.org/title/d7f56ace-cd30-48b9-8b64-afeca0077fca/the-greatest-estate-developer"
wt = "https://www.webtoons.com/en/fantasy/the-greatest-estate-developer/list?title_no=3596"

[chapters]
released = 114
read = 109

[lists]
recommend = "S"
+++

Protag wakes up blacked out drunk in the middle of a road as the bastard country son of a noble from a novel the protag read in his past life. Everybody hates him. The protag has to then try to redeem himself whatever way he can with his previous life's knowledge of being a civil engineer and his experience in doing lots of hard labor after his entire family died. 

<!--more-->

The faces are great, and the bro banter between the two main leads is great, and the banter of the system messages is also good. It's still just a skill cheat spam series where the protag gets the exact thing he needs the exact moment he needs it, but it still feels satisfying to read. The protag is an absolute jackass and also a badass but also a good guy.

Apparently this is a sequel of some other work that I didn't know about until I checked mangaupdates just now, but this work is still basically completely standalone so it doesn't really matter I guess? It does reference the older work in a minor way in a later chapter but it's mostly a surface level reference. Additionally the adaptation seems to have taken liberties to make it fairly different from the webnovel while still keeping all of the major plot beats in tact in a way the original author is fine with, generally enhancing the story in a positive way, which is pretty cool.

Either way I really enjoy this work. Please read it.
