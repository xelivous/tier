+++
title = "悪役令嬢の取り巻きやめようと思います"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=153738"
#raw = ""
md = "https://mangadex.org/title/29c6fb22-8913-4dc6-aa80-85b9c181c5da/beating-the-heroine-at-her-own-game"
bw = "https://bookwalker.jp/series/184553"

[chapters]
released = 37
read = 0

[lists]
recommend = ""
+++



<!--more-->

