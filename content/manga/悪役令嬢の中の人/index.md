+++
title = "悪役令嬢の中の人"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-30
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=192088"
raw = "https://comic.pixiv.net/works/7926"
md = "https://mangadex.org/title/6df29d13-2dab-4ca6-a0b5-74070abf3e1d/the-one-within-the-villainess"
bw = "https://bookwalker.jp/series/354096"

[chapters]
released = 14
read = 0

[lists]
recommend = ""
+++



<!--more-->

