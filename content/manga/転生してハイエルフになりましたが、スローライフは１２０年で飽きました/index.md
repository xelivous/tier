+++
title = "転生してハイエルフになりましたが、スローライフは１２０年で飽きました"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-10-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=184171"
raw = "https://comic.pixiv.net/works/7610"
md = "https://mangadex.org/title/8280b28c-aeff-4f89-ac99-a4c783e14782/growing-tired-of-the-lazy-high-elf-life-after-120-years"
bw = "https://bookwalker.jp/series/336697/"

[chapters]
released = 34
read = 34

[lists]
recommend = "S"
+++

The protagonist reincarnates as an elf in an isekai after living to be 30 in his previous life, and spends 120 years just lazing about living the slow life without a care in the world as a forest hippie. However after doing so he starts to get incredibly bored and seeks out excitement elsewhere since he has prior experience as a human in modern civilization and yearns for more stimulation than elves typically have. However since he's a peculiar elf going against the grain he doesn't have any money or connections in human society so he has to figure out how to live in human society with his extremely warped knowledge.

<!--more-->

This is both a fast paced work and also a very slowburn work, with fairly good pacing that mixes both aspects together. The protagonist will regularly spend 5-10 years just chilling out grinding and enjoying his life in an area, only for the work to timeskip a bit and eventually move on with his life. In a way this work is somewhere inbetween kino no tabi and freiren. A slow, yet fast, beautiful work about the passage of time. A work about raising a child and watching them grow up faster than you'd hope, yet still happy for everything they do.

However later on in the work the pacing seems to go slightly out of wack, starting a ton of plotlines but never really delving into any of them and just immediately moving onto the next one. This is the complete opposite of the earlier chapters where the protagonist will just spend decades doing one singular task while learning more about the surroundings and slowly changing it for the better; he'll now just spend 3 days somewhere, make some abrupt change, and then move on. I am still optimistic the work can go back and regain some of its original magic but i'm also slightly disappointed in the direction it is heading.
