+++
title = "災悪のアヴァロン"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["weekly"]
lastmod = 2025-01-16
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series/7nvcblg"
raw = "https://tonarinoyj.jp/episode/4856001361045246855"
md = "https://mangadex.org/title/7e4e5e8a-f3f0-4612-b04d-257e2ac0b7eb/saiaku-no-avalon"
#bw = ""

[chapters]
released = 73
read = 64

[lists]
recommend = "B"
+++

Protag is a normal salaryman who goes to work every day and then leaves immediately on time to go play his favorite VRMMO all day long. He's quite frankly addicted to it. One day after completing a difficult beta quest he gets a notice from the staff that he kind of just breezes through halfassed and gets prompted to either have the system auto generate a character or to do a full character customization, and the protag just autochooses like a brainlet. He then gets whisked away into the world of the VRMMO as a chubby dude with bottom tier skills at the bottom of his entire school's ranking.

<!--more-->

There's a lot of focus on the cute girls and a lot of the mechanics are kind of explained with showing the cute girls instead which is kind of weird but I guess that's one of the main highlights of this work? It's a fairly bogstandard school-ranking plot that is almost identical to Classroom of the Elite in some regards. It also semi-regularly swaps perspectives to one of the heroines or other characters. I'm looking forward to seeing where the plot goes in the future since it's still in the early chapters at the moment.
