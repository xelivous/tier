+++
title = "異世界で上前はねて生きていく"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=168690"
raw = "https://gaugau.futabanet.jp/list/work/5de5ee3b77656147c9030000"
md = "https://mangadex.org/title/89bf0886-8596-4f49-a045-7415608661d9/isekai-de-uwamae-hanete-ikiteiku-saisei-mahotsukai-no-yuru-fuwa-jinzai-haken-seikatsu"
bw = "https://bookwalker.jp/series/246585/"

[chapters]
released = 33.2
read = 16

[lists]
recommend = "C"
+++

Protagonist dies from overwork in a back alley coming home from work and ends up reincarnating into another world with magic/fantasy elements as a commoner with extremely capable support/restoration magic but no offensive magic to speak of. However this world has gender roles where men are offensive attackers and women are healers, so he doesn't really fit in too much, and all of the random inventions that japanese isekai'ers try to bring over have already been invented. As a result he goes around buying slaves and curing them with his OP restoration magic and makes them indebted to him to get a basic amount of income.

<!--more-->

This is a very narration-centric manga with very little dialog and tons of internal thoughts/etc. This is literally just a lightnovel shoved into manga format. The longer the manga goes on the more slaves he gets to the point that he just has hundreds underneath him shortly after the manga begins and he continues getting more and more slaves to the point that you don't really learn any of their names, except for the occasional chapter that is from their point of view and you learn the names of the people around that person. They exist solely to be cheap and easy labor for the protag to make tons of businesses and get infinite profit from extremely cheap labor for the most part.

The most based part of this manga is the large age gap romance with the capable girlboss ex-military soldier. This manga is definitely unique in the isekai/reincarnation space and worth reading just from how different it feels, even if it's not particularly good. It's just a flavor of the week where literally anything and everything can happen at any point in time. One chapter he's basically making a perpetual motion machine, then a pool, then they're having a japanese-style sports festival, and then next thing you know you're reading a sob story about a slave with a terminal illness who will do everything in her power to repay back her awkward and weird master.
