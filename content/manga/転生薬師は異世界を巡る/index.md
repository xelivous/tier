+++
title = "転生薬師は異世界を巡る"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=162221"
raw = "https://www.alphapolis.co.jp/manga/official/936000318"
md = "https://mangadex.org/title/e036111e-3754-4088-bc3d-6751f2b27261/tensei-kusushi-wa-isekai-wo-meguru"
bw = "https://bookwalker.jp/series/256374/list/"

[chapters]
released = 22
read = 0

[lists]
recommend = ""
+++



<!--more-->

