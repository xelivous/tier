+++
title = "異世界で最強の杖に転生した俺が嫌がる少女をムリヤリ魔法少女にPする!"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2021-09-19
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=139866"
raw = "http://seiga.nicovideo.jp/comic/26651"
md = "https://mangadex.org/title/94c97d6c-6d96-4547-b32f-7a29885b37c6/isekai-de-saikyou-no-tsue-ni-tensei-shita-ore-ga-iyagaru-shoujo-wo-muriyari-mahou-shoujo-ni-p-suru"
bw = "https://bookwalker.jp/series/123503"

[chapters]
released = 26
read = 21

[lists]
recommend = "B"
+++

Extreme mahou shoujo fan (male) gets reincarnated as a magic wand by a crazy lady who bestows power to him for Reasons, but he doesn't want to do anything if it's not mahou shoujo related so he grows despondent. Then a girl who looks extremely like his favorite mahou shoujo stumbles upon him and then he convinces her to become a mahou shoujo.

<!--more-->

It's an ecchi series with nudity and light sex scenes, that goes all in on mahou shoujo uooohing, and has some nice action scenes like all good mahou shoujo.