+++
title = "さまよえる転生者たちのリライブルゲーム"
title_en = ""
statuses = ["axed"]
demographics = ["shounen"]
categories = ["isekai"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154444"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01201043010000_68/"
md = "https://mangadex.org/title/37415/samayoeru-tensei-sha-tachi-no-relive-game"
bw = "https://bookwalker.jp/series/213995/"

[chapters]
released = 26
read = 26

[lists]
recommend = "C"
+++

Protag bought into the meme "nice guy" shit and became a complete pushover, confessed to the love of his live, and got rejected for being a "nice guy". Shortly afterwards he encounters a random Black Knight that steps out of some weird fog who almost kills him before a cute girl saves him, and while running away he doubles back to save the girl (because he's a nice guy) and gets himself killed in the process. For some reason he reincarnates into a bizarre world as the girl he saved. And it turns out the world he reincarnates into is basically Bishoujo Dark Souls.

<!--more-->

This work has some interesting worldbuilding that kind of ends up rushed since it was likely axed but still kind of cool. Probably worth reading to experience it at least.
