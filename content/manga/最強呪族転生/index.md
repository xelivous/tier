+++
title = "最強呪族転生"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-10-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151456"
raw = "https://comic-earthstar.jp/detail/jyuzoku/"
md = "https://mangadex.org/title/381f7cdf-8eae-4b2c-a8ca-f4298407b6c2/saikyou-juzoku-tensei-cheat-majutsushi-no-slow-life"
bw = "https://bookwalker.jp/series/217981"

[chapters]
released = 22
read = 20

[lists]
recommend = "C"
+++

Protagonist grows up with a magician as a father, the kind that uses tricks and pulls doves out of a hat not the kind that casts actual magic, and desperately wants magic to be real despite it obviously not being so. Even in highschool he joined the occult/magic club to try and research more into the very slight possibilty that magic might exist. However one day when walking home from school with his not-gf she almost gets run over by a car but the protag saves her by pushing her out of the way and dies in her place. As a result he ends up reincarnating into an isekai that actually has magic in it.

<!--more-->

The strangest part about this series is how hardcore is pushes incest. I forgot how cute filo is but she's even better on a reread. I keep reading horn girl as if she has the voice/personality of atuy. It seems to have gone on indefinite hiatus and is probably axed at this point although when checking the site it says a release on august 10th is upcoming so... Holy shit the series actually updated on august 10th the prophecy was real.
