+++
title = "보스였음"
title_en = "I Was the Final Boss"
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-04-11
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/pif01fy/i-was-the-final-boss"
raw = "https://comic.naver.com/webtoon/list?titleId=802833"
md = "https://mangadex.org/title/017928fe-96c5-4865-9b7f-1f7bc1b22824/i-was-the-final-boss"
wt = "https://www.webtoons.com/en/fantasy/i-was-the-final-boss/list?title_no=5170"

[chapters]
released = 76
read = 65

[lists]
recommend = "A"
+++

The world is invaded by dungeon portals one day and humans are devastated by the sudden outbreak of all of the dungeons affecting the world. Various people awaken their powers during this event and many other times in the future, and seek to destroy these towers using their newfound powers to prevent losing anyone else. However we follow the point of view of a boss of one of these dungeons who simply sees humans as toys. While continously revelling in the fighting with his "toys" he starts to learn more about human society and the outside world, and he longs to be free from his tower and experience the things he've heard of and seen through a phone that one of the "toys" dropped. However one day a powerful hunter finally shows up to kill the protagonist, how manages to succeed, but just before the protagonist dies he kills the hunter as well in turn. As a result the System ends up deciding to reincarnate the protagonist as a human just like he wished, although he's lost all of his powers he once had.

<!--more-->

This work is actually really cool and good. It's a simple battle shonen with a battle-maniac protagonist, and it's still a part of the "dungeon outbreak" subgenre with all of those tropes, but it does everything really competently and from a different angle from almost every other one i've seen thus far. The work is somewhat similar to "hataraku maou-sama" in a way although it doesn't have the protagonist raising a celestial baby for tons of arcs (yet). This work is mostly about trying to integrate a former demon/boss into human society while slowly learning how to be more human, along with various subplots about how both demons and humans are victims of some other larger machinations in play.

I enjoyed my time reading it, the official translation had no issues (although 2 missing words) and seems to have localized the jokes well, and it's just all around a solid work that I can't think of ways to improve or find too much fault in. 
