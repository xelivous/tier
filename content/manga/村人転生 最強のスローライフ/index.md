+++
title = "村人転生 最強のスローライフ"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["none"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-06
tags = [

]

[links]
mui = "https://www.mangaupdates.com/series.html?id=146903"
raw = "http://seiga.nicovideo.jp/comic/34130"
md = "https://mangadex.org/title/27211/murabito-tensei-saikyou-no-slow-life"
bw = "https://bookwalker.jp/series/148904/"

[chapters]
released = 57
read = 35

[lists]
recommend = "C"
+++

Protag reincarnates into an isekai with a few cheat skills as a villager on some random outskirts after becoming middle-aged in his previous life, and he's just enjoying his sweet country life. However the broken cheat skills he obtained are basically hero/superman tier; he has super strength so he can lift anything and his body is basically impervious, he has the ability to use any and all magic, and he has a near undestructible barrier skill that can be used to envelop everything. To top it off he is even surrounded by tons of cute girls who want his dick but since he's like 50 at this point combining both his lives he just sees them as basically younger sisters/daughters and ignores all of their advances.

<!--more-->

Basically it's a slowlife manga that isn't slowlife. The protagonist solves every issue everywhere around him and comes into contact with tons of influential people constantly. Additionally he comes across a ton of other reincarnated people too who rely on him for help, although it's kind of odd that everybody from japan has blonde hair. Overall it's a cute series with an extremely large and varied cast of unique characters, as long as you're not actually looking for anything even remotely "slow life".
