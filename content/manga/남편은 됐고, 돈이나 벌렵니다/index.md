+++
title = "남편은 됐고, 돈이나 벌렵니다"
title_en = "Forget My Husband, I'll Go Make Money"
categories = ["reincarnation", "isekai"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2025-01-26
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/cz6edjp/forget-my-husband-i-ll-go-make-money"
raw = "https://page.kakao.com/content/59978716"
md = "https://mangadex.org/title/1b2a2d45-b9c5-4d53-9fcb-dda7b3cd5d2e/forget-my-husband-i-ll-go-make-money"
#bw = ""

[chapters]
released = 95
read = 36

[lists]
recommend = "F"
+++

Protag reincarnates as the daughter of the emperor of a massive nation (Silvanus) that is at war with Ilugo and is unable to defeat ilugo because Ilugo have the chaddest of all chads on their side. After losing they suggested a truce and ended up giving the protag(/princess) to ilugo as a peace offering since the father kept trying and failing to Awaken the princess to no avail and functionally tortured her growing up, so now she is being sent off. However along the way they attempt to comically torture her for some reason however she's able to overcome most of the attempts due to her ability to see the future. In fact her ability to see the future comes with the ability to see the past, which is why she knows of her past life and was able to stay sane while being locked up for 10+ years as a child in a literal dungeon by her father.

<!--more-->

Why is everyone in this work a massive bitch what the fuck? Why is everybody so fucking dumb? What the fuck is this work? This is just the most brainlet tier grade school drama i've ever had the misfortune to read.

The protagonist is really cute (personality-wise) but this is actually impossible to read with all of the other terrible characters guffawing about complete fucking nonsense. If you're attractive you're a good person, and if you're unattractive you're a bad person. The world is so easy to understand just be conventionally pretty with a good face and maybe a 20pack 4head. Why does she need money separate from tarcan's funds? There's no valid reasoning given for it and it's essentially just used as a source of dumb misunderstandings to force the plot to limp along. Because the plot is so utterly banal the artist filled tons of pages with dumb references to popular media instead of actually putting in any wit or effort of their own, and 90% of the work (and comments about the work) are just "ooh big hot daddy with bare chest awooga". All of the characters that are of moderately reasonable intelligence stay far away from the protagonist so we're just left with the protagonist talking with complete dinguses with sub70 IQ the entire time to make her look smarter.

The work is moderately acceptable when it's just the protag by herself but the moment you put in just about any other character it's insufferable. Literally the only way I could recommend this is if you want to see the "hot" male lead be half-naked 90% of the time, since there's no actual value to this work beyond that.
