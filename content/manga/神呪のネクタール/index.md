+++
title = "神呪のネクタール"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-28
tags = [
    "basically-porn",
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=137545"
raw = "https://www.akitashoten.co.jp/series/7201"
md = "https://mangadex.org/title/3da17cce-8094-4947-a215-e04340beaa5d/shinju-no-nectar"
bw = "https://bookwalker.jp/series/111716"

[chapters]
released = 78
read = 0

[lists]
recommend = ""
+++



<!--more-->

