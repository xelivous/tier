+++
title = "この世界がゲームだと俺だけが知っている"
title_en = ""
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-03-31
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=113491"
raw = "https://comic-walker.com/contents/detail/KDCW_EB00000006010000_68/"
md = "https://mangadex.org/title/10ac3ce4-de88-49c6-b9f9-001a5bf08351/kono-sekai-ga-game-dato-ore-dake-ga-shitte-iru"
bw = "https://bookwalker.jp/series/21375"

[chapters]
released = 49
read = 25

[lists]
recommend = "B"
+++

Protag is a university student who plays a shitty VR game all day and forgets to hang out with his cousin or whatever (who clearly wants the D), when she breaks the dragon balls, rubs a magic lamp, and writes a wish on a magical tanabata paper, and also the magic hammer that then sends the protag into his kusoge!! The premise is literally just that this is a super buggy nonsensical game and the protag has to abuse all of his highly specific knowledge from playing it all day to survive.

<!--more-->

I enjoy this a fair bit actually. It utilizes the game systems well and manage to tell a decently compact story.
