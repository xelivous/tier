+++
title = "転生領主の優良開拓"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-16
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=191926"
raw = "https://www.ganganonline.com/title/706"
md = "https://mangadex.org/title/1b9181b3-98d7-4f60-8b36-66abe86f4204/tensei-ryoushu-no-yuuryou-kaitaku-zensei-no-kioku-o-ikashite-white-ni-tsutometara-yuunou-na-jinzai"
bw = "https://bookwalker.jp/series/275441/"

[chapters]
released = 31.1
read = 16.1

[lists]
recommend = "D"
+++

Dude worked at a black company and then gets reincarnated as a lord of a region but his parents die in a demon attack so he ends up all alone so he sends out job recruitment flyers that seem too good to be true so tons of people start showing up. 

<!--more-->

It's a wholesome chungus manga where the protag lives in an absolute hellhole of a world where everybody except the protag and the people who join him are excessively evil/lazy up until they join him whereupon they see the error of their ways and become Good.
