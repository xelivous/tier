+++
title = "異世界建国記"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-03
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144815"
raw = "https://web-ace.jp/youngaceup/contents/1000046/"
md = "https://mangadex.org/title/22303/isekai-kenkokuki"
bw = "https://bookwalker.jp/series/158920/"

[chapters]
released = 61.2
read = 60

[lists]
recommend = "B"
+++

Protag gets run over by a bus and ends up reincarnated into a forest in an isekai, and immediately almost gets eaten by a massive bird that can talk. However he manages to talk his way out of it and ends up getting told to raise some kids that got abandoned by humans instead. As a result he ends up teaching them how to be self-sufficient in the middle of the forest and build up a city from scratch with just the children.

<!--more-->

Basically the protagonist invents more and more things for his tiny village of children and basically modernizes his village as much as possible, while slowly building up influence to become king of an entire country one day. If you're interested in reading a kingdom building series it's probably one of the better ones. One of the highlights of this work is that pretty much every panel has a proper background for it, which is more than I can say for a lot of isekai out there.
