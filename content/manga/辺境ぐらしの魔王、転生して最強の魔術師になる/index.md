+++
title = "辺境ぐらしの魔王、転生して最強の魔術師になる"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-21
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=171478"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00201820010000_68/"
md = "https://mangadex.org/title/032f800d-d632-4442-8830-d037d616ead4/henkyou-gurashi-no-maou-tensei-shite-saikyou-no-majutsushi-ni-naru"
bw = "https://bookwalker.jp/series/299810/"

[chapters]
released = 42
read = 42

[lists]
recommend = "C"
+++

The protagonist is a strange "monstrosity" in this world that absorbs all excess magic particles and thus is 100000x stronger than everybody else and gets ostracized/hunted because of it when he just wants to live in peace and harmony, until he finally finds a village that will accept an unknown entity like himself and lets him live in peace for years. However because this world is so shit he eventually gets targeted by the evil church anyways that makes him out to be an evil entity and the cause of all of their suffering, so he ends up allowing himself to be killed by one of his villagers for "Political Purposes". Yet for some reason he gets to reincarnate as a human once he dies, yet still keeps a basic portion of his skills from his previous life. However everything is not perfect in his new life and there's a comically evil noble dude shitting things up.

<!--more-->

Ultimately this is an incredibly shoujo plotline put into a male-protagonist seinen manga. Protagonist and some random cute village girl he saves who has a crush on him reincarnate together and live together happily ever after. Possibly ningen-core? 

The combat is pretty brainlet in general though, the magic system is dumb, and all of the conflicts are dumb. "Oh no this ogre has a magic shield that absorbs magic whatever will we do" says the adventurer guild, meanwhile the protag just flanks it because obviously it can't defend itself with a shield from every angle; no shit. Dumb ass adventurers that exist solely to make the protag seem Epic without putting in actual work.
