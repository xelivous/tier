+++
title = "双穹の支配者"
title_en = ""
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=163621"
raw = "https://seiga.nicovideo.jp/comic/48100"
md = "https://mangadex.org/title/49426/soukyuu-no-shihai-sha-isekai-oppai-musouden"
bw = "https://bookwalker.jp/series/275618/"

[chapters]
released = 14
read = 0

[lists]
recommend = ""
+++


<!--more-->
