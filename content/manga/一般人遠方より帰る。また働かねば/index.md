+++
title = "一般人遠方より帰る。また働かねば"
title_en = ""
categories = ["isekai", "reverse-isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-17
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169086"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666633587"
md = "https://mangadex.org/title/d577918a-076d-4db7-9ce6-24a8c275a03e/ippanjin-enpou-yori-kaeru-mata-hatara-kaneba"
bw = "https://bookwalker.jp/series/280652/"

[chapters]
released = 14
read = 14

[lists]
recommend = "C"
+++

Protag is just a random dude living in japan working part time when all of a sudden he gets summoned, along with a few other random highschoolers, into a random location with nobody around them. The protag then decides to adventure out with these highschoolers, except his abilities are kind of trash and their abilities are broken OP. And after multiple years of tagging along with them trying his very best, he ends up getting transported back to japan alone leaving the highschoolers behind. Except he's retained all of his skills/magic so now he's actually broken OP compared to everybody else since he's the only one who can use them.

<!--more-->

Went on hiatus before it ever really started, but I enjoyed what was there. I'm tempted to read the WN/LN of it at some point since I highly doubt this adaptation will ever receive another chapter. But I also don't think it would be that interesting to read in LN form to begin with...

On one hand it's worth reading this because it's so short, but also because it's so short it's not worth reading this since you will never get a conclusion and it's nothing more than promo.
