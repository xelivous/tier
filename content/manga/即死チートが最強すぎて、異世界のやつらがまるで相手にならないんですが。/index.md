+++
title = "即死チートが最強すぎて、異世界のやつらがまるで相手にならないんですが。"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148724"
raw = "http://comic-earthstar.jp/detail/sokushicheat/"
md = "https://mangadex.org/title/24281/sokushi-cheat-ga-saikyou-sugite-isekai-no-yatsura-ga-marude-aite-ni-naranai-n-desu-ga"
bw = "https://bookwalker.jp/series/169748/"

[chapters]
released = 44
read = 41

[lists]
recommend = "S"
+++

Protag and all of his classmates get whisked away to an isekai while heading to a fieldtrip on a schoolbus, and they immediately encounter an insane girl that starts obliterating the chaperones. He then gets left to die (along with a few others) after his classmates are given an ultimatum, and it ends up with only him and a cute girl left. The protag was left behind because he didn't have obtain any special abilities, just like the other classmates that were left behind, and that's because he already has a broken ability to kill anything he wants to.

<!--more-->

Honestly it's a pretty cool series about what is basically an SCP in human form that nobody can do anything about that gets isekai'd with his classmates. The most fascinating part about this work is that the author continues to introduce random ideas and mixes them into a story in a way that is entertaining to read, no matter how outlandish or insane, since the protag is already utterly broken to begin with everything else has to at least be so far out there in order to make the work interesting.

Ch32 is kino. The story even introduces a JK android dovahkiin that regularly has internal conversations with her multiple cores. There's even a cute twinbraid later on in the story (kind of).