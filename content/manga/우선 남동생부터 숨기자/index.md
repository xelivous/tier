+++
title = "우선 남동생부터 숨기자"
title_en = "Let's Hide My Little Brother"
categories = ["reincarnation", "isekai"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=186936"
raw = "https://page.kakao.com/home?seriesId=57492806"
md = "https://mangadex.org/title/24302640-2313-4f9c-8149-b572ec450907/let-s-hide-my-younger-brother-first"

[chapters]
released = 60
read = 40

[lists]
recommend = "C"
+++

The beginning of this work is kind of scuffed since it glosses over the "korean" life and just immediately jumpstarts into the reincarnated life without really touching on the old life, until it does actually touch on it a bit a little later on, only to kind of just never do anything with it or reference it again later despite making somewhat of a big deal out of it. Apparently this is based on a web novel so maybe it does more with it there, or maybe it's just as vague/dumb. It has a slight action focus but is primarily about some kind of weird romance involving a boy who was raised as a girl to hide his identity, so it's essentially pseudo-yuri in a BL work where the protag is a cool knight and the hero is a pink-haired heroine. It's like on the borderline of touching upon gender dysphoria / gender identity but doesn't really make any kind of a big deal out of it which is cool. I mean just look at that cover.

<!--more-->
