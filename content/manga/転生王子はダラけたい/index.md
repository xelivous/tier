+++
title = "転生王子はダラけたい"
title_en = ""
statuses = ["axed"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146624"
raw = "https://www.alphapolis.co.jp/manga/official/564000194"
md = "https://mangadex.org/title/25641/tensei-ouji-wa-daraketai"
bw = "https://bookwalker.jp/series/187288/"

[chapters]
released = 26
read = 26

[lists]
recommend = "C"
+++

A rather simple yet pleasant manga. It's not particularly outstanding but at least it doesn't have dumb skill spam. It's kind of unfortunate that it got 'axed', but the plot seemed to be on a downward trend either way. There's apparently a continuation now at least.

<!--more-->
