+++
title = "망나니 소교주로 환생했다"
title_en = "Reincarnated as an Unruly Heir"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-01-20
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/0o0aeiq/reincarnated-as-an-unruly-heir"
raw = "https://comic.naver.com/webtoon/list?titleId=773796"
md = "https://mangadex.org/title/d364a3e4-4aad-4f14-b0c4-aaf9ff09a017/reincarnated-as-an-unruly-heir"
wt = "https://www.webtoons.com/en/action/reincarnated-as-an-unruly-heir/list?title_no=3597"

[chapters]
released = 116
read = 89

[lists]
recommend = "D"
+++

Protag is some random dude in the virtuous sect who gets betrayed, and ends up on the side of the demonic cult many years into the future as basically the leader. He has to navigate the politics and tries to steer the demonic cult to become more virtuous etc.

<!--more-->

This work starts out fine and largely focuses on politics since the protag doesn't want to reveal his hand of learning the most powerful demonic art which makes it fairly entertaining to read the entire like 50+ chapters in the beginning. However the longer the work goes on the more it becomes an absolutely dreadfully boring slugfest of dumb murim shit with terrible writing to the point that i hated it more and more with each chapter that I read.

Would only rec if you want to read like 40 chapters of a story about a dude navigating politics then forgetting the rest of it exists.
