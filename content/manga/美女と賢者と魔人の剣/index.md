+++
title = "美女と賢者と魔人の剣"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146715"
raw = "http://seiga.nicovideo.jp/comic/36004"
md = "https://mangadex.org/title/e7e950c1-bbc3-4579-a659-ac9ffacc4ab2/bijo-to-kenja-to-majin-no-ken"
bw = "https://bookwalker.jp/series/214922"

[chapters]
released = 26
read = 15

[lists]
recommend = "D"
+++

This series would actually probably be good if it weren't for the extremely over the top pointless sexualization of all of the female characters that's prevalent across the entire work.

<!--more-->
