+++
title = "転生悪女の黒歴史"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=150891"
raw = "https://manga-park.com/title/2513"
md = "https://mangadex.org/title/2ea8f5bc-31a7-472e-9c60-705d586f0c83/the-reincarnated-villainess-dark-history"
bw = "https://bookwalker.jp/series/188061"

[chapters]
released = 40.3
read = 0

[lists]
recommend = ""
+++



<!--more-->

