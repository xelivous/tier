+++
title = "MÄR"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=10"
#raw = ""
md = "https://mangadex.org/title/84b2fad4-cd1a-4a37-831a-582e8137970f/m-r"
#bw = ""

[chapters]
released = 161
read = 0

[lists]
recommend = ""
+++



<!--more-->

