+++
title = "悪役令嬢レベル99"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-16
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=162806"
raw = "https://comic-walker.com/contents/detail/KDCW_EB03201445010000_68/"
md = "https://mangadex.org/title/878634d2-ea39-4001-a4bf-31458020d16a/villainess-level-99-i-may-be-the-hidden-boss-but-i-m-not-the-demon-lord"
bw = "https://bookwalker.jp/series/265215/"

[chapters]
released = 18.1
read =  18.1

[lists]
recommend = "B"
+++

Protag all of a sudden remembers her previous life's memories, but as a result realizes that everyone dislikes her due to her black hair/eyes which appear cursed, and she lives alone with attendants while her parents live separately from her. The protag played an otomege in her previous japanese life and apparently ended up reincarnating as a random side character who turned out to be the hidden boss in the game. She also cared more about the RPG contents of the game instead of the ikemen so now that she knows she's in a world of magic/fantasy she just goes all out on grinding levels to be max level just like her game counterpart.

<!--more-->

It's a cute series about a taciturn/expressionless Gamer who only cares about efficiency when leveling and the effects she has on a society that largely is barely above level 20 due to their focus on safety first and foremost. Obviously the protag is basically untouchable by just about everybody else so the focus is more on her trying to integrate into society when she's perceived as death incarnate / the demon lord.
