+++
title = "塔の管理をしてみよう"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148886"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000064010000_68/"
md = "https://mangadex.org/title/27005/tou-no-kanri-o-shite-miyou"
bw = "https://bookwalker.jp/series/167399/"

[chapters]
released = 65
read = 49

[lists]
recommend = "C"
+++

Protag gets hit by a truck and then reincarnates and gets two booba maid servants as a bonus. He also gets mystical super powers in his chuuni eye thanks to the goddess for no reason! He then obliterates everything in his path with the help of his two servants and slowly ranks up in the adventurers guild until he learns about a mystical tower and immediately sets out to conquer it on a whim, breezes through the entire thing in a few pages skipping all of the fights and lack of struggles, and then subsequently becomes the master of the tower. Protag does random development of the tower then has sex with his maid slaves every night.

<!--more-->

I could maybe rec this to people who want completely braindead "city simulation" style content that goes into 0 detail, and occasionally has a few pages of porn every other chapter. His harem just continues to grow with new girls added that he then has sex with some chapters later.

Although the series does kind of come into its own around the ch30s+ mark and starts actually fleshing out the story some more, and starts to focus on the city/world/politics more than endless sex scenes. You could say that once the protag has had enough sex he goes into kenja time fr and starts pondering the meaning of the world. This series is also unique in that every single chapter is still available on ComicWalker for some reason; normally they remove everything except the latest chapter so you have to buy the tankobon.
