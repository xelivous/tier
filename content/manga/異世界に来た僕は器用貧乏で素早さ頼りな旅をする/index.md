+++
title = "異世界に来た僕は器用貧乏で素早さ頼りな旅をする"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
furigana = []
statuses = ["ongoing"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=170532"
raw = "https://seiga.nicovideo.jp/comic/45085"
md = "https://mangadex.org/title/52170/i-came-to-another-world-as-a-jack-of-all-trades-and-a-master-of-none-to-journey-while-relying-on-quickness"
bw = "https://bookwalker.jp/series/253815/"

[chapters]
released = 77
read = 23

[lists]
recommend = "D"

+++

Protagonist gets stabbed to death by a burglar while being a wagie and ends up getting dropped in the middle of nowhere in an isekai with a skill that allows him to learn anything quickly but never be able to master anything.

<!--more-->

Early chapters have a decent amount of naked/ecchi, then later on that aspect just completely disappears. Doesn't really do anything noteworthy or well but it's not offensively bad.