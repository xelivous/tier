+++
title = "高度に発達した医学は魔法と区別がつかない"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    "male-protagonist",
    "human-protagonist",
    "doctor-protagonist",
    "humans-are-evil",
    "evil-church",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=193320"
raw = "https://comic-days.com/episode/3269754496654602094"
md = "https://mangadex.org/title/1aad07fb-3ec1-4acf-8c5b-d8f5f5929a5d/koudo-ni-hattatsu-shita-igaku-wa-mahou-to-kubetsu-ga-tsukanai"
bw = "https://bookwalker.jp/series/354970"

[chapters]
released = 20
read = 19

[lists]
recommend = "B"
+++

Protag is a doctor living in japan who is getting transferred to a small rural island that doesn't have any doctors on it. However upon arriving on the island the house he's sitting in gets struck by lightning, and the next thing he knows he's in an isekai, saving a catgirl slave that ran away from owners that abandoned her and she's now running away from a chimera. Long story short he ends up saving her, and she ends up working as his doctor apprentice to start healing people in this world that typically only had human magicians as healers.

<!--more-->

The art is amazing, although maybe too good since if you're at all squeamish about gore/insides it might not be pleasant to read since it goes into a lot of detail about surgical operations. It's also accompanied with an absolute fuckton of medical terminology text dumps explaining what each and every term means. Extremely hot goth spider girl though.

The main issue with this story his how comically evil the higher up humans are. The other issue is that the primary conflict of this work is that the humans are hoarding the knowledge that god gave them to heal others, and god's solution is to summon the protagonist to this world with their medical knowledge to basically make healing mostly not necessary; However the gods could've simply given all of the other species healing magic to solve the "issue", and having all of the other species saved by yet another "human" cheapens the work since it feels like humans have to do everything for anything to get done in the first place. Where's the dwarfs and elves independently working on medical knowledge from the ground up researching on their own?
