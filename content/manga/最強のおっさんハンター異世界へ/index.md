+++
title = "最強のおっさんハンター異世界へ"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-01
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155694"
#raw = ""
md = "https://mangadex.org/title/0f83e219-f445-4b9b-96d1-dde82d254f3c/saikyou-no-ossan-hunter-isekai-e-kondokoso-yukkuri-shizuka-ni-kurashitai"
bw = "https://bookwalker.jp/series/229858"

[chapters]
released = 30.3
read = 12

[lists]
recommend = "C"
+++

So basically a completionist from the monster hunter universe falls into a hole and gets isekai'd, meets a booba elf and gets brought to her village, slays a dragon, and he's able to go back and forth between the two worlds and is tasked with killing more monsters in the other world.

<!--more-->

Would rec if you want to read a monster hunter inspired isekai where the protag is already in endgame equipment fighting against trash mobs but honestly mostly just befriending the monsters instead of slaying them as if it was pokemon.
