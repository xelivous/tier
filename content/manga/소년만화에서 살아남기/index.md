+++
title = "소년만화에서 살아남기"
title_en = "Surviving a Fantasy Adventure Comic"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-30
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/8lhmlwo/surviving-a-fantasy-adventure-comic"
raw = "https://series.naver.com/comic/detail.series?productNo=8332265"
md = "https://mangadex.org/title/76a2c911-e3af-4852-b03d-f062f1c497f5/surviving-a-fantasy-adventure-comic"
wt = "https://www.webtoons.com/en/fantasy/surviving-a-fantasy-adventure-comic/list?title_no=5181"

[chapters]
released = 119
read = 53

[lists]
recommend = "C"
+++

The protagonist is an average reader of a manga who gets upset that his favorite (and the most popular) character dies in the work, and he's not satisfied with the author's attitude of "if the characters didnt' survive it's because they didn't try hard enough". As aw result the protag gets angry at the author online, dms the author telling him he's selling all of his physicals/etc, and never reading again. To which the author responds "would you like to try it out yourself?", and immediately the protagonist gets sucked into the world of the comic and has to survive as a minor extra character until the very end of the work.

<!--more-->

Basically the original work that the protagonist was reading was extremely turbo-garbage shounen manga, and he has to set out to try and insert himself into the story by following the events closely but still changing them enough that he becomes one of the main focuses. All while battling a petulant child of an author who is upset that his story is getting changed from the garbage it once was to something very slightly better. I'd say the main annoying part is that while the protagonist is an absolute Expert on the series itself, we the readers don't really know anything about it (other than it's a cliche garbage story) so it's just the dude saying "oh yea, this will happen so i need to do this" and then it happens and he does that.

If you want to read a meta comic about an author writing about a reader fighting against an author's vision for a comic, then this is a good choice. However if you dislike battle shounen you will hate this work, since it's little more than a terrible battle shounen with meta elements.
