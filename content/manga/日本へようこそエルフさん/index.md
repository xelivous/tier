+++
title = "日本へようこそエルフさん"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
categories = ["isekai", "reverse-isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-23
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153002"
raw = "https://firecross.jp/ebook/series/326"
md = "https://mangadex.org/title/32912/welcome-to-japan-elf-san"
bw = "https://bookwalker.jp/series/206472/"

[chapters]
released = 47
read = 40

[lists]
recommend = "A"
+++

Protag is your average everyday salaryman except when he goes to sleep he wakes up an isekai where time runs slower and thus he is younger. In this isekai there's elves, magic, and everything you'd expect to normally see. Once he sleeps or dies in his dream (isekai) he'll end up back in japan again, and one day when he's out exploring with his cute elf waifu he ends up dying and somehow or another she ends up transporting to japan along with him.

<!--more-->

This is a romance nonbiri isekai tourism manga except it's a double feature where both of them tour both worlds back and forth repeatedly. A large majority of the insert images for chapter starts/etc has her with twinbraids and it's amazing thank you artist-sama. pls god give me cute elf gf. Would rec this manga if you want to see a cute blonde-adjacent elf make cute faces and wear cute clothes with cute reactions.
