+++
title = "死んでください！勇者でしょ？"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["none"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149701"
raw = "https://comic-walker.com/contents/detail/KDCW_AM06200502010000_68/"
md = "https://mangadex.org/title/ccb82067-e8e6-412e-83f4-2539b121bfc4/shinde-kudasai-yuusha-desho"
bw = "https://bookwalker.jp/series/196790/list"

[chapters]
released = 15
read = 15

[lists]
recommend = "C"
+++

Protag buys like every light novel imaginable and does weird rituals with them until one of them reacts and he's able to isekai himself into a light novel world, however he's still just a wimpy otaku with a shit body and no cheat skills since he just kind of isekai'd himself so he can't really do anything and is having trouble just living. He desperately wants the cute harem lifestyle but he's just so useless just like everybody else that wishes for the cute harem lifestyle in an isekai. However one day a bunch of cute girls invite him to join their party with a super shady contract and immediately after he signs it he gets stabbed to death by them.

<!--more-->

It's a short absurdist comedy manga that parodies isekai. You may or may not have seen a panel or two from this going around since this series was popular/memeable back when it was new. The longer the work goes on the more horny/yabai it gets to the point that it's kind of hard to recommend since the first few chapters are relatively benign and focusing on the comedy instead of absolutely horny, and then wraps back around to blash througt whatever is left of the story in an attempt to "finish" it while being like axed or something. I feel like it has some merit of recommendability just from being super short that there isn't much lost if you end up not liking it.
