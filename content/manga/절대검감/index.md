+++
title = "절대검감"
title_en = "Absolute Sword Sense"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-01-21
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/6lafgnt/absolute-sword-sense"
raw = "https://comic.naver.com/webtoon/list?titleId=796075"
md = "https://mangadex.org/title/825eb50f-7331-4455-bb0e-c0d58fa9976a/absolute-sword-sense"
wt = "https://www.webtoons.com/en/action/absolute-sword-sense/list?title_no=5100"

[chapters]
released = 68
read = 52

[lists]
recommend = "C"
+++

Protag is a low-level spy who has worked for the blood cult for a decade after being kidnapped by them, only to eventually get betrayed after a job. However he ends up consuming a high level scroll in the process, ends up reincarnating back in time to the day he got kidnapped, and can now hear the voice of blades. He will then use this newly acquired power and his knowledge of the future to try and steer his life into a better direction than it was in his past life.

<!--more-->

Same old murim story where the protag cultivates his qi until he becomes the coolest epic badass while slowly building up a harem of cute girls since he's the only relatively attractive person around. It's probably a decent choice for reading general murim stuff if you want that but there's also 8 billion other murim entries you can also read and this one doesn't stand out too much from any of those in a notable way.
