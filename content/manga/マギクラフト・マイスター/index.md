+++
title = "マギクラフト・マイスター"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144433"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000049010000_68/"
md = "https://mangadex.org/title/fc2a2247-1a29-48e9-a1c5-b45ed7166c9a/magi-craft-meister"
bw = "https://bookwalker.jp/series/146381"

[chapters]
released = 54
read = 52

[lists]
recommend = "C"
+++

imagine if you were an engineer but you could make anything you can think of without really trying as long as you understood the basics of the laws of nature. that's this manga. also the japanese language is the peak of kino where kanji  is mega broken OP because you can fit concepts into less amount of letters therefore it's better obvs. Also one of the very first things he creates is the venerable isekai water pump!!!

<!--more-->

There is a cute twinbraid villager girl who shows the protagonist around though which is nice, and she's actually a major aspect of the early chapters so you get tons of cute panels of her.

Most of the work is isekai tourism where the protag teleports around with his robot daughter helping people by making random stuff they need and discovering more about the world. With great power comes great responsibility; what will the protag use his power for, and how will various people misuse the power that has been given to them?
