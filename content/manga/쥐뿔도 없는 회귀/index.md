+++
title = "쥐뿔도 없는 회귀"
title_en = "Returning With Absolutely Nothing"
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2024-06-26
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=198658"
raw = "https://webtoon.kakao.com/content/쥐뿔도-없는-회귀/2799"
md = "https://mangadex.org/title/5bad4a36-31a3-491d-a63c-2640f08f0cba/worthless-regression"

[chapters]
released = 62
read = 62

[lists]
recommend = "B"
+++

Every day at noon a new person gets transferred to this world called Eria in the same spot, which is a fairly standard medieval-like isekai with its own native inhabitants. As a result of this people learned to scam the newbie transfers immediately upon their first arrival since they're typically easy pickings, and the protag was the same. He spent his entire life as basically a nobody unable to really accomplish anything until he eventually dies. However he manages to reincarnate due to a random stone he obtained in a dungeon and is able to at least try to improve his life this next time around.

<!--more-->

This work is fairly kino actually if you're fine with like a straightforward battle shounen with some murim influences. Dude gets isekai'd with no abilities and no job, then gets shit on for 17 years of his life since he's a class-less noob without any merits that can never match up to the Geniuses who start with pre-existing knowledge and abilities. Then he gets a chance to redo his life from the start of his isekai after coming across a mcguffin and slowly builds up his strength to try and overcome his previous weakness and show that Hard Work (and future knowledge and luck) can beat Talent.

Read through this work again and I actually think it's more kino now actually; just all around a really decent work that goes hard on narrative prose and the internal struggle of the protag. It's all around well done with the only caveat being that it's turbo murim-core.
