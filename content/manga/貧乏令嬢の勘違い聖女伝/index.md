+++
title = "貧乏令嬢の勘違い聖女伝"
title_en = ""
categories = ["reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=176561"
raw = "https://zerosumonline.com/detail/bimboreijo"
md = "https://mangadex.org/title/c7a10d9f-6628-4d58-b27a-5081ec3db27b/binbou-reijou-no-kanchigai-seijo-den"
bw = "https://bookwalker.jp/series/302587/"

[chapters]
released = 27
read = 10

[lists]
recommend = "C"
+++

Standard reincarnation loop where they get to re-do their life over from scratch and learn from their mistakes.

<!--more-->
