+++
title = "佐々木とピーちゃん"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=176864"
raw = "https://comic-walker.com/contents/detail/KDCW_KS13202040010000_68/"
md = "https://mangadex.org/title/54202943-b55d-45a1-830f-44c9a8a691f0/sasaki-to-pii-chan-isekai-de-slow-life-wo-tanishimou-to-shitara-gendai-de-inou-battle-ni-makikomareta-ken-mahou-shoujo-ga-up-wo-hajimeta-you-desu"
bw = "https://bookwalker.jp/series/304686"

[chapters]
released = 14.3
read = 0

[lists]
recommend = ""
+++



<!--more-->

