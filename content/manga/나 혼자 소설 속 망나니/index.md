+++
title = "나 혼자 소설 속 망나니"
title_en = "I Woke Up as the Villain"
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2024-07-23
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172147"
raw = "https://page.kakao.com/home?seriesId=55452034"
md = "https://mangadex.org/title/bd757d3a-694e-49c3-b6da-d033265fe843/trapped-in-a-webnovel-as-a-good-for-nothing"

[chapters]
released = 175
read = 175

[lists]
recommend = "D"
+++

Protagonist is an average game developer at not-nexon when he gets reincarnated into the lightnovel he was reading, as a relatively minor character in the work who gets killed off almost immediately, and to make matters worse the timeline starts after this character has already met the original protagonist marking his life to be doomed. However not all things are bad since this character is the spoiled son of the owner of one of the largest conglomerates in korea, so all he has to do is try to turn his life around with near unlimited money and access to every opportunity imaginable. This is also set in a world with dungeon outbreaks, gods talking through system prompts, awakenings, towers, and any other korean reincarnation/isekai trope imaginable.

<!--more-->

The protagonist pay2wins his way to success while stealing a bunch of the artifacts/events from the original work to prop himself up enough for the sake of "survival" while slowly becoming more OP than the broken original protagonist, while they have totally not-gay bromance angst constantly. It's an entertaining enough work for a majority of it, focusing on "what it takes to be the leader of a cutthroat large conglomerate" and the sibling wars that result of a psychopathic father heading this business. However it's also a skillspam work where the protag just grinds dungeon outbreaks and gets just strong enough to survive everything that happens without any real losses.

However the work also apparently badly adapts a lot of the original content making questionable pacing choices (according to comments of people that read the webnovel/etc), focusing on things that don't matter at all while neglecting to adapt important scenes featuring the original protagonist. This becomes especially egregious near the point where it gets axed in the later 100s, and i'd say the entire latter half of the work isn't as interesting to read through as the earlier half. The art also leaves a fair bit to be desired at times, since it seems to be primarily focused on making the two protagonist look like fashionable kpop stars than actually drawing the world/fights well. All of these factors probably helped contribute to it eventualy getting axed, making it pretty hard to rec.
