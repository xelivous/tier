+++
title = "이그레트"
title_en = "Egret: The Great Sage"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-11
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=186076"
raw = "https://page.kakao.com/home?seriesId=57162799"
md = "https://mangadex.org/title/56d45142-7e9e-4f46-9b40-0f0e11f0b3cd/egret-the-great-sage"

[chapters]
released = 85
read = 70

[lists]
recommend = "C"
+++

The protag bought into the mantra that with great power comes great responsibility, and thus lived out his life paralyzed in fear in making any choices that utilized his power after growing disgusted with humans that wanted to use his power for their own gain, only to regret his decision in the final moments before his death. He then miraculously reincarnates into a frail body that was seemingly getting bullied/harrased/etc, the body of a prince who is essentially an outcast, with his memories intact. He then sets out to find out the mystery of his reincarnation, and to act upon his regrets from his previous life.

<!--more-->

This series has the highest concentration of good twinbraids i've yet to see. So many good panels and it's probably the main reason why I enjoyed reading this. Christina is hot tbh.
