+++
title = "骸骨騎士様、只今異世界へお出掛け中"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=140194"
raw = "https://comic-gardo.com/episode/10834108156661711035"
md = "https://mangadex.org/title/20741/gaikotsu-kishi-sama-tadaima-isekai-e-odekake-chuu"
bw = "https://bookwalker.jp/series/124080/"

[chapters]
released = 59
read = 44

[lists]
recommend = "C"
+++

Really good art with a rather constant focus on general factions and overall worldbuilding, but tends to focus a little too much on ""high stakes"" battles that are either won immediately or where the protag swoops in to win immediately causing some random princess to be in his debt.

<!--more-->
