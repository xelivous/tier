+++
title = "金色の文字使い"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-16
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=116497"
raw = "https://comic-walker.com/contents/detail/KDCW_FS02000013010000_68/"
md = "https://mangadex.org/title/e3d88382-a2c4-4958-8311-f8bd9a3f1e21/konjiki-no-word-master-yuusha-yonin-ni-makikomareta-unique-cheat"
bw = "https://bookwalker.jp/series/24936"

[chapters]
released = 99
read = 0

[lists]
recommend = ""
+++

<!--more-->
