+++
title = "明日もまた勇者のとなり"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/i1sm89e"
raw = "https://comic.webnewtype.com/contents/matayusha/"
md = "https://mangadex.org/title/b7978238-e912-4ccd-ad04-ce9582ab5ded/ashita-mo-mata-yuusha-no-tonari"
bw = "https://bookwalker.jp/series/429406"

[chapters]
released = 11
read = 0

[lists]
recommend = ""
+++



<!--more-->

