+++
title = "八男って、それはないでしょう!"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-10-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=121839"
raw = "http://comic-walker.com/contents/detail/KDCW_MF00000009010000_68/"
md = "https://mangadex.org/title/17916/hachinan-tte-sore-wa-nai-deshou"
bw = "https://bookwalker.jp/series/53040/"

[chapters]
released = 85
read = 61

[lists]
recommend = "D"
+++

<!--more-->

Honestly i don't know why this got an anime.
