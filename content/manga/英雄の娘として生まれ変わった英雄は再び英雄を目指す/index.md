+++
title = "英雄の娘として生まれ変わった英雄は再び英雄を目指す"
title_en = ""
statuses = ["axed"]
demographics = ["seinen"]
furigana = ["partial"]
categories = ["isekai", "reincarnation"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147335"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000063010000_68/"
md = "https://mangadex.org/title/f39ab2d5-9156-4cfd-a0cd-4f89d74cb096/eiyuu-no-musume-toshite-umarekawatta-eiyuu-wa-futatabi-eiyuu-o-mezasu"
bw = "https://bookwalker.jp/series/197294/"

[chapters]
released = 24.5
read = 24.5

[lists]
recommend = "D"
+++

The protagonist is a former hero (chuuni, male) who dies and gets reincarnated as the daughter of his former partymembers.

<!--more-->
