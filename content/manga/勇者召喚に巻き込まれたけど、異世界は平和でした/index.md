+++
title = "勇者召喚に巻き込まれたけど、異世界は平和でした"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150176"
raw = "https://seiga.nicovideo.jp/comic/39510"
md = "https://mangadex.org/title/dfdd79ad-eae8-489f-8843-bfae7e08e74b/yuusha-shoukan-ni-makikomareta-kedo-isekai-wa-heiwa-deshita"
bw = "https://bookwalker.jp/series/201305"

[chapters]
released = 46
read = 43

[lists]
recommend = "S"
+++

Protag gets summoned to another world by accident by walking behind a chad with 2 gfs who is the real hero, and as a result has to spend the next year in an isekai where everything is at peace and there's no demon lord or conflicts to solve. Along the way he ends up meeting a large variety of powerful people and slowly befriends them. the power of friendship prevails. It's a fairly slow work about a dude who simply tries to understand the hearts of others using his unique magic and befriends them.

<!--more-->
