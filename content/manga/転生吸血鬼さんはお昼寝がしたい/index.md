+++
title = "転生吸血鬼さんはお昼寝がしたい"
title_en = ""
categories = []
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143814"
raw = "https://www.comic-earthstar.jp/detail/tenseiohirune/"
md = "https://mangadex.org/title/5eb1e122-f4bc-4b69-8ab1-906cdfb245a0/tensei-kyuuketsuki-san-wa-ohirune-ga-shitai"
bw = "https://bookwalker.jp/series/137658/"

[chapters]
released = 65
read = 31

[lists]
recommend = "B"
+++

Tends to be a comedy that regularly dips into charlie-brown-style art for additional comedic effect. somewhere between moogy-core and wareya-core. There's times i want to raise it up to an A, and then i reconsider and drop it down to a B. The author made a big deal about the protag needing blood desperately in the beginning and eating constantly, then forgets and the protag doesn't eat for like ever after that until he finally remembers which is a little annoying. There's constant minor nitpicks here and there where it feels like the author is just winging it, while also mixing in actually decent content somehow inbetween.

<!--more-->
