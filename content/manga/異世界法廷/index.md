+++
title = "異世界法廷"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=134789"
#raw = ""
md = "https://mangadex.org/title/383b86b1-fbe8-418f-967c-7caaeab87c73/isekai-houtei-rebuttal-barrister"
bw = "https://bookwalker.jp/series/94941"

[chapters]
released = 17
read = 0

[lists]
recommend = ""
+++



<!--more-->

