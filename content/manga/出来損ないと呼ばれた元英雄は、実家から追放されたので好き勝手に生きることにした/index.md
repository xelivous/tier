+++
title = "出来損ないと呼ばれた元英雄は、実家から追放されたので好き勝手に生きることにした"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156497"
raw = "http://seiga.nicovideo.jp/comic/38765"
md = "https://mangadex.org/title/39854/dekisokonai-to-yobareta-moto-eiyuu-wa-jikka-kara-tsuihousareta-no-de-suki-katte-ni-ikiru-koto-ni-shita"
bw = "https://bookwalker.jp/series/207639/"

[chapters]
released = 34.1
read = 14.2

[lists]
recommend = "F"
+++

Protag used to be a hero in his past life but when he reincarnated he wanted to just chill and do nothing so he wouldn't have to deal with all of that pressure. As a result he gets kicked out of his house since his power is too great for the appraisal dude to measure his power level and as a result he's deemed as an incompetent with no skills despite retaining his strength from his previous life (haha lol).

<!--more-->

The protag then finds his royal ex-fiance randomly in the middle of some field almost dying to some random monstrosity, saves her, randomly strolls along with her, and gets wrapped up in tons of random events. He also showcases all of his powers that he hid for his entire life despite not wanting to stick out at any time before simply showing 1% of his power would've allowed him to stay in his household instead of getting kicked out.

The artist also draws all eyes as if the characters haven't slept in over a week. There was a cute twinbraid at least with a really good design. The twinbraid panels are good. Shoutout to 21.1 :reansamaikneel: Honestly the story is kind of all over the place and the only reason why I kept up with it was the occasional twinbraid panel and can't recommend it outside of that, and nobody is as mentally ill as I am about that aspect so in general it's hard to rec to anybody else. The main problem is that there's a lot of series that do everything this work does but better.
