+++
title = "召喚された賢者は異世界を往く"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["partial"]
categories = ["isekai", "reincarnation", "game"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-28
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156799"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000140010000_68/"
md = "https://mangadex.org/title/52444/shoukan-sareta-kenja-wa-isekai-wo-yuku"
bw = "https://bookwalker.jp/series/219552/"

[chapters]
released = 37.1
read = 35

[lists]
recommend = "F"
+++

Protag is a hardcore VRMMO gamer who primarily plays a game with shittier EXP rates than launch maplestory where there's a really high max level and it takes years to get anywhere so that nobody is anywhere near level cap, but also has PVP which means the only people who are able to do anything in the game are no-lifer neets. Despite being one of the highest levels in the game he also has an alt character that primarily focuses on magic where he puts all of the magic drops he receives on but he never levels up. However one day when he's transferring items to his alt account and logged in as that account, he gets isekai'd as that character and has to start all over again from scratch as a level one (although with all of his useless items from his main). However because he's on his alt account at that time he doesn't have any special statuses and essentially gets abandoned.

<!--more-->

There's a nice twinbraid loli but the art in this work is kind of jank; There's a decent amount of effort put into the art and a lot of the toning/shading is done well but I think it's the character designs that feel weird if anything. It might be the overall contrast instead though idk. Eventually the artist slows down enough that they do half of a chapter every month, so a full chapter every other month, for an absolutely glacial pace. Honestly story-wise and art-wise this work is pretty mediocre at best. Protag skillspams and powerlevels and has so much meta knowledge that nobody else can even touch him and he just goes around getting larger and larger harems while things happen around him. Once again a work with 賢者 in the title sucks.
