+++
title = "ゲート 自衛隊彼の地にて、斯く戦えり"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["none"]
categories = ["isekai", "reverse-isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-03-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=72210"
raw = "https://www.alphapolis.co.jp/manga/official/138000030"
md = "https://mangadex.org/title/5353/gate-jieitai-kanochi-nite-kaku-tatakaeri"
bw = "https://bookwalker.jp/series/58696/"

[chapters]
released = 121
read = 121

[lists]
recommend = "D"
+++

All of a sudden one day a Gate opens up in the middle of japan and tons of isekai monsters and knights pour out massacre-ing japanese people. Meanwhile the protag is out and about heading to summer comiket when he notices dragons flying about, and since he's a part of the JSDF he helps evacuate as many citizens as possible while heading off to the front lines to help command the troops against the invasion of this foreign army, at which point they obliterate the invading forces because obviously nothing compares to modern military might and guns haha lol. As a result of his actions he ends up getting promoted and becomes a leader of one of the teams heading to the new world to try and build relations.

<!--more-->

There's nudity and sex scenes, along with slavery and rape, gore, along with just about every other content warning you could think of. The bunny queen is probably the most interesting character in this work. Honestly a large portion of the early parts of this manga is way too horny for its own good, although it's probably fine on some level since it's pretty obvious that tons of people would want to see cute catgirls/etc. However the harem centered around the protag along with rory's horniness is just a little too excessive compared to the rest of the work. It mostly gets toned down around the 70s/80s when stuff starts getting Real Deal, and after that point it restricts the nudity/horniness to only places where it would actually make sense.

The work doesn't really start hitting its strides until the 60+ range, which is a very large ask to read that long for a monthly series. Can't just go "Oh just read 5 years worth of content until it gets good haha lol". Even when it does start to get good you still need to suspend your disbelief since a lot of the conflicts are fairly avoidable. In short, the art is good, the characters are pretty good, the story is bad until it's eventually enjoyable, and it's too horny until it's eventually not horny. Also it goes a little too "japan #1", similar to how it's annoying how a lot of american media worships the american army etc and portrays them as epic badasses all of the time.
