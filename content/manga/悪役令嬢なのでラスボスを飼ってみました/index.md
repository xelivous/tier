+++
title = "悪役令嬢なのでラスボスを飼ってみました"
title_en = ""
statuses = ["completed"]
demographics = ["shoujo"]
furigana = ["full"]
categories = ["isekai", "reincarnation", "game"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151722"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04200444010000_68/"
md = "https://mangadex.org/title/35064/i-m-a-villainous-daughter-so-i-m-going-to-keep-the-last-boss"
bw = "https://bookwalker.jp/series/182093/"

[chapters]
released = 13.5
read = 13.5

[lists]
recommend = "A"
+++

Protag reincarnates as a villainess after living in a hospital her entire previous life, and she doesn't get her memories back until just after being thrown away by her fiance after the commoner heroine usurps her spot and is about to get exiled/executed. She somehow manages to delay her execution and immediately sets off to where the demon king lives instead.

<!--more-->

Relatively short yet competent "villainess" manga. I feel like with its length it's worth trying out regardless just to get a feel for the genre, and even if you're familiar with the genre it's worth it.
