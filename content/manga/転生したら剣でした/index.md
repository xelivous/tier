+++
title = "転生したら剣でした"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=138015"
raw = "https://comic-boost.com/content/00290001"
md = "https://mangadex.org/title/20338/tensei-shitara-ken-deshita"
bw = "https://bookwalker.jp/series/112059/"

[chapters]
released = 70
read = 48

[lists]
recommend = "B"
+++


<!--more-->
