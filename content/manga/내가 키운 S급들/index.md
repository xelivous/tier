+++
title = "내가 키운 S급들"
title_en = "My S-Class Hunters"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2023-12-26
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/lp9q2vx/my-s-class-hunters"
raw = "https://series.naver.com/comic/detail.series?productNo=6763216"
md = "https://mangadex.org/title/3f53fb25-b1b7-4fa6-92f8-2fc438a1ae65/my-s-class-hunters"
wt = "https://www.webtoons.com/en/action/my-s-class-hunters/list?title_no=3963"

[chapters]
released = 117
read = 93

[lists]
recommend = "C"
+++

The protag has been taking care of his younger brother since they were children after they were abandoned by their parents, doing everything he can to be essentially a parental figure for the brother. However one day the world goes to shit and monsters spew out of portals like every other manwha in existence causing everybody to awaken their latent chuuni powers to fight back. While the brother managed to get the top-tier SSSSSSS rank skills and a latent overall potential of rank S, the protagonist is the most bottom of bottom tier F ranks with nothing of value and essentially gets abandoned by the the brother he raised. Various events happen, his brother ends up dying protecting the protag, and he ends up getting a one-time use item to roll back time to hopefully mend his relationship and make the world a better place for everybody (including himself).

<!--more-->

The author tries to think of every way possible to make it seem somewhat plausible to have a super weak F-ranker constantly interact with the most powerful people on the planet and essentially take care of them. This works well for most of the chapters but the longer the work goes on the more deus ex machina bullshit needs to happen, and once you get into the upper 80s in chapters it just becomes nonstop epic battles fighting with the powers of imagination which is a far cry from where it started out, and is kind of exhausting to read nonstop.

Over the course of the work the protag essentially brainwashes various people to make them consider him to be their most esteemed parent figure, basically overlapping their grandma's visage onto him whenever they see him in order to trust him. He basically has an EX-rank charm skill after reincarnating and continues to get more and more epic deux ex machina bullshit as time goes on.

I'd say the main highlights of the work are the mystery around how and why the protagonist even got a chance to reincarnate in the first place, why the system spawned alongside all of the monsters, etc. Not sure it's worth recommending in a general sense but if you're fine with extreme system autism it's probably one of the better korean system autism works.
