+++
title = "スピリット・マイグレーション"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2022-01-30
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=118825"
raw = "https://www.alphapolis.co.jp/manga/viewOpening/128000095"
md = "https://mangadex.org/title/34144247-0c21-43d9-a62f-f4143adface0/spirit-migration"
bw = "https://bookwalker.jp/series/104335"

[chapters]
released = 27
read = 27

[lists]
recommend = "B"

+++

A story about the protag waking up in an unknown world as an unknown entity with no existing knowledge, trying to interact with everything he meets in whatever way he can to try and find out his purpose. It's kind of fun since the protagonist hops between multiple hosts regularly and sees tons of different perspectives, but it still centers around a core cast of characters. The manga adaptation seems to have been killed off but the WN/LN seem to continue past that so at the very least it could serve to be a good advertisement for those.

<!--more-->

The longer the series goes on the more ecchi it becomes, although it does start off at least slightly ecchi with the female warrior making weird poses occasionally, but the beast(?)girl archer being the most egregious example; there's so many panels that just emphasize her massive breasts while she makes weird poses which doesn't really add much to the overarching storyline. 

I do enjoy it a decent amount, it's the perfect amount of jank campiness that is entertaining to read. Virus tensei is almost an exact copy of this work but done slightly worse.
