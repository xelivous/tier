+++
title = "元ホームセンター店員の異世界生活"
title_en = ""
categories = ["isekai"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-20
tags = [
    "female-protagonist",
    "status-windows",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=178683"
raw = "https://comic-walker.com/contents/detail/KDCW_EB03202073010000_68/"
md = "https://mangadex.org/title/a7ad8806-57c1-4fc6-9822-8de8edf956e9/home-centre-sales-clerk-s-life-in-another-world"
bw = "https://bookwalker.jp/series/320265"

[chapters]
released = 16.2
read = 12.1

[lists]
recommend = "C"
+++

Protagonist wakes up in the middle of a field in an isekai after falling asleep after coming home from work, and the world has status windows and rpg skills/magic etc. There's two wolfboys sitting right next to her when she wakes up as well who offer her shelter as a terrible storm is quickly approaching. They are apparently orphans living in a worn-down house, and the protag kind of wishes up random metal components using magic to fix the house up to thank them for giving her a place to sleep for the night, although the rest of the villagers are super wary of humans and don't trust her.

<!--more-->

Honestly kind of a cute series about a mom raising two wolfboys while living in a village, and trying to reverse the discrimination against beastpeople in this world in general. The main quirk that sets this apart from other isekais is that the protagonist constantly references quotes from Kamen Rider I guess? It's mostly a city-building series with some merchant stuff on the side and doesn't even dip into the nonsense adventurer's guild stuff.

Though Ch12.1 sucks ass and I might just stop reading this now because of it. Dumb princess just starts blabbing about her sad sobstory of being a whore's child while choking out the protagonist and then stops choking the protagonist for some reason, then gets bodied by the protagonist after she has an epic monologue in return. Actually the dumbest fight scene I have ever seen in an isekai. Maybe someone else can overlook it since otherwise it's relatively fun enough to read but i'm out I guess.
