+++
title = "便利屋斎藤さん、異世界に行く"
title_en = ""
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151039"
raw = "https://comic-walker.com/contents/detail/KDCW_EB00000027010000_68/"
md = "https://mangadex.org/title/31078/handyman-saitou-in-another-world"
bw = "https://bookwalker.jp/series/211520/"

[chapters]
released = 239
read = 0

[lists]
recommend = "F"
+++

It's basically a 4koma but not in 4koma format; little more than someone's shower thoughts.

<!--more-->
