+++
title = "神無き世界のカミサマ活動"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-04-13
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=162480"
raw = "https://viewer.heros-web.com/episode/10834108156652407792"
md = "https://mangadex.org/title/d5ec700b-c61b-4ab9-97fe-ed2d57fd0d5c/kaminaki-sekai-no-kamisama-katsudou"
bw = "https://bookwalker.jp/series/236719/"

[chapters]
released = 39
read = 5

[lists]
recommend = "C"
+++

Protag has an absolutely batshit father who is basically in a cult and forces the protag to go through all kinds of traumatic shit growing up, until eventually his dad goes too far and kills the protag in a ritual. As a result the protag wishes to be reincarnated into a world without any gods/etc in it because he's completely done with that shit. And he finally gets to live out his dream of having fun with friends his age and drink beer and laugh merrily.

<!--more-->

Except the world he reincarnated into is slightly crazy, the villagers he was happily frolicking with were social pariahs, and this world is batshit where they will just outright execute people for Reasons.

The series doesn't personally click with me but I do feel that it's decent enough overall.
