+++
title = "元・世界１位のサブキャラ育成日記"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation", "game"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-18
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151825"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01200682010000_68/"
md = "https://mangadex.org/title/34196/moto-sekai-ichi-i-subchara-ikusei-nikki-hai-player-isekai-wo-kouryakuchuu"
bw = "https://bookwalker.jp/series/220038/"

[chapters]
released = 51
read = 45

[lists]
recommend = "C"
+++

Protag neglects everything else in life to nolife and reach the top rankings of a VRMMO to become well-known by everyone like zezima in runescape. Except his main character he spent all of his time on got hacked and deleted, and none of his data or any of the other affected top-ranking users were able to get their data restored. Thus, the protag kills himself. He then wakes up in the MMO as his alt character with zero experience that exists solely to hold extra items from his main. Due to his overwhelming joy at being reincarnated into the kamige he devoted his entire life to, he spends the first few hours sobbing uncontrollably in front of the starting store and gets arrested.

<!--more-->

The author clearly understands the meta and mechanics of MMOs that uses a skill system similar to Mabinogi's (although doesn't have a reincarnation system so it's not 1:1 on mabinogi), even if the work essentially becomes a skill/stat dump fiesta as a result. The work is largely about a dude who autistically studied every single mechanic about a game while being told throughout his entire life that there's no meaning in being the top ranker in a videogame and he should instead devote all of his effort to something "in real life", finally getting a chance to put all of his knowledge to good use "in real life" after reincarnation, while also regretting the "pointless" effort towards being the top online player in his previous life was.

The drill oujousama is good. Masochistic fox mommy is a meme.
