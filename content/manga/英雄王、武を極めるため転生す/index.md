+++
title = "英雄王、武を極めるため転生す"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=159310"
raw = "https://firecross.jp/ebook/series/366"
md = "https://mangadex.org/title/c7421641-dc50-4a3c-80a5-5cdcb2cae890/eiyuu-ou-bu-wo-kiwameru-tame-tenseisu"
bw = "https://bookwalker.jp/series/263076"

[chapters]
released = 25
read = 0

[lists]
recommend = ""
+++



<!--more-->

slowest update pace imaginable. manga has been going on for like 5 years and it still hasn't adapted the content the anime did in 12 eps.
