+++
title = "最低キャラに転生した俺は生き残りたい"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-30
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/79n9b2m"
raw = "https://comic-walker.com/contents/detail/KDCW_CW01204008010000_68/"
md = "https://mangadex.org/title/94210b9d-93fc-430d-9a1d-b9a8cfcb2461/saitei-chara-ni-tensei-shita-ore-wa-ikinokoritai"
#bw = ""

[chapters]
released = 6.2
read = 0

[lists]
recommend = ""
+++



<!--more-->

