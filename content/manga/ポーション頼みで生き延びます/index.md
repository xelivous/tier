+++
title = "ポーション頼みで生き延びます"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-10-19
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/l2l65ox/potion-danomi-de-ikinobimasu"
raw = "https://pocket.shonenmagazine.com/episode/10834108156705672961"
md = "https://mangadex.org/title/8fe5cbe9-c968-4e89-bc5b-b16a4420c70c/potion-danomi-de-ikinobimasu"
bw = "https://bookwalker.jp/series/136268/"

[chapters]
released = 75 # 49+26
read = 49

[lists]
recommend = "C"
+++

Protag is a normal office lady when all of a sudden she gets whisked into the space in-between worlds due to a weird glitch and gets offered to reincarnate into an isekai with magic/etc. She immediately starts negotiating terms to get epic cheat powers, linguistic abilities, and everything else she would need to live peacefully in the other world. She also negotiates to inform her family and friends that she's migrating to another world when they're all sleeping. tldr she eventually gets her epic cheat power and chooses to be able to make any kind of potion. tldr Protag schemes and lies her way through society while helping people with her magical potions that can do anything and quick wits. And the main issue is that the world she isekai'd into doesn't actually really have anybody with magic in it and it's just kind of a normal medieval society, and her broken cheat magicky powers are super out of place and causes tons of issues for her.

<!--more-->

Funniest part about this work is that she can make literally anything she wants for the most part thanks to her "potion-container making skill" by just attaching a tiny bit of a potion container to whatever she is making. Want to make an epic chainsaw? Just make a chainsaw and then have a tiny potion container attached like a charm or something who cares it's magic haha lol.

Kind of a weird series where the goddess is a brainlet, the protag is a smart brainlet, and the protag just does whatever she wants while constantly getting into weird shenanigans while saving everybody with her broken OP powers while occasionally using the "ditzy wrath of the goddess" and other religious schemes to handle anything that comes her way. Since she keeps basically outing herself wherever she goes it's more or less isekai tourism where she basically sees a new location every volume along with tons of new people and new problems.

Okay so like the manga volume just kind of gets cut off in vol9, says it will continue in volume 10, but volume 10 never released and the story doesn't have an ending at all? It just teased a vol10 back at the beginning of 2022 and then never did anything more I guess? Ah I see, after volume 9 they got a new artist and continued it in the [続 continuation version](https://pocket.shonenmagazine.com/episode/316112896909127232) I guess? Not sure i'm up to reading the story with the complete change in artist at this point... Not going to separate out the two works since they're the same thing in the end and i'll keep this as ongoing I guess?
