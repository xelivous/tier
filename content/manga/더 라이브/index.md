+++
title = "더 라이브"
title_en = "The Live"
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-04
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165135"
raw = "https://page.kakao.com/content/54728055"
md = "https://mangadex.org/title/0f13d05a-dacf-43ff-93ca-165523e55a46/the-live"

[chapters]
released = 133
read = 104

[lists]
recommend = "D"
+++

Protagonist is a normal corporate slave who is still coping over the death of his family that got murdered by a serial killer, when all of a sudden his diary becomes a Future Diary (lol) from his future self wishing to change the future somewhat, and give hope to his past self that he could somehow reincarnate his dead wife/daughter. However the protag is kind of a brainlet and just kind of brute forces his way through while getting cheat items. The revenge/suffering at the beginning is kind of cringe/amateurish. The work also likes doing more and more timeskips near the start which makes the passage of time seem really odd.

<!--more-->

I do appreciate how much focus is put on bro friendships at least! Would recommend if you want an action fantasy that focuses a lot on two bros just beating up random monsters. However the story becomes very boring to read around ch80+ (basically where s2 starts) where its just the most boring Epic Fights back to back to back, with ch95 being excessively dumb. Long drawn out fight scenes wouldn't be bad if it weren't for the fact that these are probably the worst written fight scenes imaginable. And it just continues to tank in quality the longer the series goes on so i don't really want to read this at all anymore...