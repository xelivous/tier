+++
title = "復讐を希う最強勇者は、闇の力で殲滅無双する"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["reincarnation", "post-isekai"]
sources = ["light-novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152659"
raw = "https://seiga.nicovideo.jp/comic/39335"
md = "https://mangadex.org/title/34404/fukushuu-o-koinegau-saikyou-yuusha-wa-yami-no-chikara-de-senmetsu-musou-suru"
bw = "https://bookwalker.jp/series/218554/"

[chapters]
released = 77
read = 29

[lists]
recommend = "F"
+++

Protag defeats the demon lord but instead of given a parade and happiness he's instead met with crucifixion and despair, and vows to destroy everybody involved in his betrayal.

<!--more-->

This story is what an edgy 12 year old would think is cool, even more so than arifureta. All of the characters are extremely meme and the plot is stupid. The author just writes whatever depraved shit he can come up with and tries to tie it together in whatever meme plot he can think up at the moment, and as a result this series mostly just exists for torture porn. Also a cute twinbraid girl appears some ways into the story and then leaves the story never to return again; Absolutely tragic. Read the first few chapters and if you think the story seems cool continue with it but i'll call you a chuuni edgelord in return. It's probably a fine rec if you just want tons of gore/porn/rape since this series goes all out on that.
