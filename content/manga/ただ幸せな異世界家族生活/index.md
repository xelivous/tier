+++
title = "ただ幸せな異世界家族生活"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=177549"
raw = "https://www.ganganonline.com/title/976"
md = "https://mangadex.org/title/54016980-802d-4780-b964-23ee03891b6d/tada-shiawasena-isekai-kazoku-seikatsu"
bw = "https://bookwalker.jp/series/266895/"

[chapters]
released = 28
read = 18

[lists]
recommend = "C"
+++

The start of the manga and the first like 3 chapters or so are cool, but i'm not sure if it's the manga's fault or the original source but all of the characters just feel like props for the protag to move at his whims since he's basically god and nobody will ever question him despite being literally a baby. I want to like it but unless it changes in later chapters (however long it will take to reach there) i'm reluctant to raise it higher than this.

<!--more-->

Also the chapter where the protag literally introduces the extremely mindblowing concept of *the wheel* to the dumb as hell peasants of this world is mind numbing.

If you just want to follow a literal 2 year old (and his harem of 2 year old girls) introducing tons of high tech inventions like the Wheel to medieval peasants while learning about the joy of being together with family then this might be a pleasant read.
