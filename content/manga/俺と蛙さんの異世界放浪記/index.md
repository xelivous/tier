+++
title = "俺と蛙さんの異世界放浪記"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-01-31
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=105582"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/641000052/"
md = "https://mangadex.org/title/fe0ca912-db5a-4073-a8b6-e303a1d30459/ore-to-kawazu-san-no-isekai-hourouki"
bw = "https://bookwalker.jp/series/111746"

[chapters]
released = 40
read = 40

[lists]
recommend = "C"
+++

I remember reading this back when it was first releasing, then I got caught up on it, and then forgot about it for years. It's basically a comedy isekai that borrows stories from basic fairytales while introducing random modern aspects to isekai people, like computers, blogs, and swimsuits.

<!--more-->
