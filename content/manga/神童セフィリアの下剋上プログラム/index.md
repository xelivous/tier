+++
title = "神童セフィリアの下剋上プログラム"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["none"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154834"
raw = "https://gammaplus.takeshobo.co.jp/manga/sephiria/"
md = "https://mangadex.org/title/37871/shindou-sefiria-no-gekokujou-program"
bw = "https://bookwalker.jp/series/191516/"

[chapters]
released = 35.1
read = 23

[lists]
recommend = "D"
+++

Protagonist reincarnates into a world with magic in it after overworking themselves to death and managed to keep their memories intact, and decides to live a life of leisure in this world so that she won't have to die of overwork. Also the protag was an epic coder in her past life who coded up a mad storm. Also she ends up standing out in peculiar ways since she can't wait to just be gungho and be the very best there ever was even before she's grown up so she's just walking and talking as a 6 month old fighting off bears and shit, since fuck taking things leisurely let's overwork ourselves to death before we're 2 years old.

<!--more-->

End of chapter 3 has peak armor design; nothing will ever top it. coders rise up. Unfortunately the series is kind of all over the place and ridiculous. There's like constant ecchi breastfeeding scenes too. Nothing really makes sense and having the protag be literally like 1 year old for most of the work is a baffling choice outside of maybe the author/artist has an extreme breastfeeding fetish. Honestly this work is just baffling overall; ch23 is where I have to throw in the towel good luck if you get past that.
