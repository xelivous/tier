+++
title = "チートなタブレットを持って快適異世界生活"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-06
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/ej9njmy"
raw = "https://www.alphapolis.co.jp/manga/official/990000359"
md = "https://mangadex.org/title/9bc0e0a9-a871-480e-a512-a28a5cb364e0/cheat-na-tablet-wo-motte-kaiteki-isekai-seikatsu"
bw = "https://bookwalker.jp/series/310305/"

[chapters]
released = 24
read = 22

[lists]
recommend = "C"
+++

Protag wakes up in an isekai and the only thing he has with him is a tablet that requies money to unlock the features of, and has the appearance of a normal book to residents of this world. Since his only isekai benefit is having a tablet he is just a normal human otherwise and can't really go out to fight monsters so he does menial tasks in the backline to support the combatants instead. However not every appreciates what the backline can do and small petty drama builds up and the protag has to figure out how to live in this world with only a tablet as his special ability.

<!--more-->

Kind of just a chill manga where the protag is basically a househusband doing all of the chores/cleaning for the adventurers, since all of his abilities come from the tablet and he himself is no different than any other normal human in a world with monsters/magic. Kind of a wholesome series about how you shouldn't take someone who is good at housework for granted even if they don't directly assist in combat I guess? It's also kind of a cooking manga except it's not a particularly satisfying one when it skips over everything and the protag just whips out tons of dishes with little explanation.

All in all the highlight of this manga is the few panels with the cute twinbraid catgirl.
