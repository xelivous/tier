+++
title = "サキュバス＆ヒットマン"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-05
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=161654"
raw = "https://mangacross.jp/comics/succubus"
md = "https://mangadex.org/title/a90b8d56-4900-41fb-bdbe-49f812fe2807/succubus-hitman"
bw = "https://bookwalker.jp/series/263643"

[chapters]
released = 32
read = 18

[lists]
recommend = "B"
+++

Protag dies, then sells his soul to a devil to get reincarnated into some other dude's body so he can take revenge on the people who killed him, and as a result he has to regularly kill people to feed the devil that hangs around him.

<!--more-->

Very meme grimdark revenge manga, that is actually fairly enjoyable to read. The comedy lands fairly well and it's paced decently enough to not be boring. Of all of the meme revenge titles of this ilk it's probably the best one. It kind of dips into ecchi content a little too often though with somewhat regular nudity, and tons of gore/etc. It's a basically shounen-tier writing that throws in as many "adult" topics as possible so it comes off as weird.

The hacker girl is kwi; she even has programmer socks so you know she's realdeal.