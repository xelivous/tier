+++
title = "限界超えの天賦は、転生者にしか扱えない"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=175962"
raw = "https://comic-walker.com/contents/detail/KDCW_AM05201991010000_68/"
md = "https://mangadex.org/title/e57ac6cc-c475-4ef1-a970-b2e261d2ca59/genkai-koe-no-tenpu-wa-tensei-sha-ni-shika-atsukaenai-overlimit-skill-holders"
bw = "https://bookwalker.jp/series/304684/"

[chapters]
released = 9
read = 25

[lists]
recommend = "B"
+++

Protagonist lives as a miner/slave in a world where skills are everything after being sold by his parents, and you can find skills in the mines so it's their duty to mine them out. He's in a team with a girl that is slightly older than him and their goal is to make it out of there together alive, however she managed to find a super rare skill and is going to be set free before him. Except schenanigans happen and their slave owner gets killed in an accident freeing them from their subservient brainwashing allowing them to escape during the massive chaos in the mines.

<!--more-->

The main highlight of this work is the stellar artwork, and the overall worldbuilding is pretty cool as well. The forward timeskip to reverse timeskip in chapter 15 is actually brainlet tier though and should never have existed. Overall a fun series to experience even if the protag's skill is bullshit, and the entire plotline with his "older sister" is bizarre. There's tons of minor complaints I can make about this series but there's a lot that it does well too so wcyd.
