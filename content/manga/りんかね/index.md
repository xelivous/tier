+++
title = "りんかね"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-13
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=185995"
raw = "https://comicbushi-web.com/episode/3269632237268235971"
md = "https://mangadex.org/title/58adad3f-1770-4fbb-8645-935ef830a8db/reincarne?tab=chapters"
bw = "https://bookwalker.jp/dea4a7cb62-8d6c-4eed-9b7e-ccb02a9f3755/"

[chapters]
released = 16
read = 16

[lists]
recommend = "C"
+++

Protag and his buds are like samurai who defeated the Big Bad in their past lives, but also died together shortly afterwards from their wounds, pledging to have a more fulfilling peaceful life if they reincarnate. As a result protag reincarnates as a random japanese dude who regularly has dreams about his past life, and one of his pals from his past life becomes a transfer student to his school, except he's now a girl, and doesn't remember his past life. Also apparently his other friend was also always in this school and also a cute girl now.

<!--more-->

Tldr it's a weird romance where the protag tries to date his ex-bf who is now a gf who doesn't know memories, but the protag constantly overlaps the prev dude with the new appearance constantly, and then also making a harem with both of his ex-bf's who are now gf's. "it's not gay if he's a girl, even if it's an 俺っ子". It's a wetnoodle protag wish fulfillment harem series with tons of cute girls and tons of ecchi scenes where the protag gets tons of girls fawning over him simply because they existed as people in his previous life.

feel like this is very ryancore. ch9 cover image is great. Unfortunately the ending is kind of :peepoweird:.
