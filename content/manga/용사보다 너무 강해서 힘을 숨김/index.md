+++
title = "용사보다 너무 강해서 힘을 숨김"
title_en = "Secretly More Powerful than the Hero"
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-28
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/u7ah0n3/secretly-more-powerful-than-the-hero"
raw = "https://series.naver.com/comic/detail.series?productNo=10074273"
md = "https://mangadex.org/title/a3591ae9-16cf-469a-854c-4be0b6386cba/secretly-more-powerful-than-the-hero"
wt = "https://www.webtoons.com/en/fantasy/secretly-more-powerful-than-the-hero/list?title_no=5845"

[chapters]
released = 37
read = 24

[lists]
recommend = "D"
+++

Protagonist looks up to the "secretly badass" side characters in manga and strives to live his life just like them, only to eventually end up getting trucked and isekai'd by a goddess who notices how saintly and cool he is. However when asking to reincarnate he wanted to be an ultra strong background character and to enforce this the goddess made it so if his powers are even found out he'll immediately die! The protagonist now has no choice to stay in the "background" solving all issues without ever being found out, but still being stronger than anything and everything else in existence.

<!--more-->

The premise is omega dumb and the execution of it isn't much better. Shoutout to the "heroine" with bright red hair trying to pretend to be the protagonist with his white hair and thinking nobody would notice the difference between them. Also the protagonist is just a complete masochist who wants to be looked down on and belittled by people constantly which is kind of cringe. The very first arc is even more cringe somehow. The protagonist also isn't particularly good at hiding his powers and despite being found out and basically constantly shown to be uniquely strong he still hasn't died how odd!! Funniest part about this series is that the dude friend tagging along is actually the entire "secretly OP side char" the protoag wanted to be all along.

Ultimately I don't know who to rec this to.
