+++
title = "落ちぶれ王女と異世界勇者の建国史"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=159325"
raw = "https://comic-fuz.com/manga/2330"
md = "https://mangadex.org/title/16a240ab-3936-4421-be33-b46ac0518411/a-ruined-princess-and-alternate-world-hero-make-a-great-country"
bw = "https://bookwalker.jp/series/261542"

[chapters]
released = 26
read = 0

[lists]
recommend = ""
+++



<!--more-->

