+++
title = "악당들에게 키워지는 중입니다"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2025-01-19
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/60hrhq7/being-raised-by-villains"
raw = "https://page.kakao.com/content/61822163"
md = "https://mangadex.org/title/49d64db2-5841-4ccc-aee8-7b3a0970f6df/i-m-being-raised-by-villains"
#bw = ""

[chapters]
released = 85
read = 61

[lists]
recommend = "D"
+++

The protagonist is unloved and unwanted by her family and desperately wishes to be reincarnated into her favorite novel about a heroine that is loved by everybody. She miraculously has her wish come true but instead reincarnates as a "random" pink-haired extra that is banished from the family soon, so she sets out to try and come up with a backup plan almost immediately afterwards once she's banished. However things don't go as planned and she might end up being a part of the family after all since she caught the eye of the eccentric heir to the family, as he vows to be her father and adopt her.

<!--more-->

The beginning of this work is fairly decent and it's a nice "child-doting father-daughter" manwha, until it full shits the bed and becomes some strange yandere harem simulator instead. The entire reason why the protagonist is banished from the family is because she's some "deplorable" lizard-shapeshifter hybrid and shapeshifters are all subhumans. But oh yes, in this family with dragon blood mixed in it that champions around all of the heirs with thick dragon blood, there's the protag that shapeshifts into a lizard; there's absolutely no way this lizard girl could ever be a dragon, right? No, she's just a subhuman lizard nothing to see here. This is a major plot point that nobody in the work, including the protag, realizes for 60+ chapters because they're all brainlets, for absolutely no reason. This is the level of writing in this work. Contrived dumb bullshit happens while the protag meets all of the male leads (that all wear earrings for some reason) and they're all clinically insane yanderes who want to immediately marry the 5 year old protag.

It is not a great read for those happy "child-raising" series, it's not a great read for drama/intrigue, it's not even that great of a read for a harem series since none of the relationships feel even great when they're all literally like 5-10 years old and they're all brainlet yanderes to boot. I appreciate the like single panel of her hair being done up in twinbraids later on but that's about it.

The art is above average; not outstanding but they did at least put a lot of effort into their 3DCG backgrounds, and they have good paneling and good "camera angles". If anything the art is probably the best part about this work. If anything i'd be more interested in checking out other works this artist has done (if any) as long as it's not by this author.
