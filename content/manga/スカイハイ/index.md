+++
title = "スカイハイ"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=5058"
#raw = ""
md = "https://mangadex.org/title/59b0f38e-20bc-47d3-811d-64daea3f1e09/skyhigh"
bw = "https://bookwalker.jp/deb53384d6-2ddd-4a68-9446-09f875066f22"

[chapters]
released = 10
read = 0

[lists]
recommend = ""
+++



<!--more-->

