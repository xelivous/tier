+++
title = "装備製作系チートで異世界を自由に生きていきます"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-13
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=160278"
raw = "https://www.alphapolis.co.jp/manga/official/240000293"
md = "https://mangadex.org/title/42835/i-will-live-freely-in-another-world-with-equipment-manufacturing-cheat"
bw = "https://bookwalker.jp/series/253169/"

[chapters]
released = 24.2
read = 24.2

[lists]
recommend = "D"
+++

Protag is a generic japanese 29year old part-timer who gets wrapped up in a summon circle meant for a highschool chad and his harem by accident, and just kind of gets kicked out of the castle for being a useless old dude. So protag gets like all of the support/manufacturing skills, and instead of like focusing on using those skills to the fullest he instead just, like, becomes an adventurer since that's what mushoku tensei did right, and that's what you do in isekai you rank up in the adventurer's guild. 

<!--more-->

At best you have to completely shut your brain off because the protag just keeps whipping up random ass shit constantly for ""0 cost"" as the ultimate crafter with broken summons.
