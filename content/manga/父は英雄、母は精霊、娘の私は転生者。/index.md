+++
title = "父は英雄、母は精霊、娘の私は転生者。"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["partial"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-09-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150727"
raw = "https://comic.pixiv.net/works/5551"
md = "https://mangadex.org/title/36246/dad-is-a-hero-mom-is-a-spirit-i-m-a-reincarnator"
bw = "https://bookwalker.jp/series/203951/"

[chapters]
released = 63
read = 63

[lists]
recommend = "D"
+++

Protagonist is the daugher of the Hero and his spirit queen waifu, resulting in her being a half-spirit, and she's even a reincarnated scientist from japan and resporn as a spirit specifically with the power over control of all base elements (because science!). She then uses all of her knowledge to make a right mess of everything in this world and everything gets progressively more and more fucked up!

<!--more-->

Does it really have to make the ex-fiance an extremely meme fat caricature to make her even more hated? It's extremely excessive when she's already depicted as having a bad personality, the artist didn't have to make her be literally jaba the hutt as well. 

The writing in this story is horrible. Every time you think it might not be as bad as you think it becomes even worse. Which is unfortunate since the art is fairly good with actually decent backgrounds most of the time as well. I'm not sure I can really recommend this to anybody but it's not to the point of being offensively irredeemably bad to be F tier I suppose.
