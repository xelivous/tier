+++
title = "Live Dungeon!"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["game", "isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-10-07
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150480"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200320010000_68/"
md = "https://mangadex.org/title/24659/live-dungeon"
bw = "https://bookwalker.jp/series/171559/"

[chapters]
released = 78.2
read = 73

[lists]
recommend = "A"
+++

Protag nolifes an MMO so hard he multiboxes and plays all 5 characters in a party at once, along with his main of being a healer, because he was disatisfied with how other people were playing their characters. Upon reaching the end of the endgame dungeon "solo" he gets isekai'd into the MMO he played, and immediately dies then gets ressurected. The weird part about the MMO he gets ressurected into is that apparently healers are used as like pheonix downs that only exist to ressurect and then immediately get killed by the boss afterwards due to aggro pull. Essentially everybody in this world thinks it is like a "Modern Action MMO" where everybody is a DPS and healers are basically useless (vindictus/warframe/etc) instead of a traditional MMO with the usual trinity (dps/healer/tank); buffers/debuffers also exist but they're not as prominent until later and much like other MMOs that focus on "The Trinity" they're somewhat glossed over.

<!--more-->
