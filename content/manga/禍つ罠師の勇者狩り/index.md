+++
title = "禍つ罠師の勇者狩り"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["none"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158652"
raw = "https://seiga.nicovideo.jp/comic/46357"
md = "https://mangadex.org/title/f3ddc6af-998a-4475-9691-3ca83534f349/wicked-trapper-hunter-of-heroes"
bw = "https://bookwalker.jp/series/244419"

[chapters]
released = 20
read = 20

[lists]
recommend = "D"
+++

Protag is a game developer who loves making meme trap kusoge that are super difficult and near impossible to clear, but casuals don't like that and it's poor for sales so he ends up quitting his game development job. However he gets summoned by a demon to be the very best dungeon master with tons of over the top killer traps that nobody will ever be able to clear.

<!--more-->

Once again humans are meme-tier evil and it's actually the demons that are wholesome chungus who should be loved and repsected instead. I appreciate the meme factor of some of the villains just being over the top sex-related, ch8 in particular is just peak memes, but this work just is near nonstop gore/sex and little else. I wonder if ch4/5 is mellycore... If you want gratuitous meme gore and sex scenes this is the work for you.

