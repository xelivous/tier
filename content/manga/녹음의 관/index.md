+++
title = "녹음의 관"
title_en = "The Viridescent Tiara"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-04
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=173378"
raw = "https://page.kakao.com/home?seriesId=55889891"
md = "https://mangadex.org/title/161e8bde-914f-4464-a009-071ad6d981ac/the-viridescent-tiara"

[chapters]
released = 83
read = 71

[lists]
recommend = "D"
+++

Protag gets reincarnated into a novel she wrote as the protag's not-blood-related-sister (yay legal incest), and tries to steer it along a happier path than the one that was originally presented in the novel. A few unfortunate events couldn't be prevented but she still manages to somewhat change the flow of events, and gets closer and closer to the protag. It's basically just a typical romance in a fantasy setting except the protag knows tons of weird knowledge that she uses to her benefit and for some reason most people don't really question her too deeply beyond a surface level of "wow that's weird. anyways", which kind of takes you out of the work constantly. Could this also be considered the epitome of "self-inserting"? ch54 is good though. 

<!--more-->

