+++
title = "異世界ウォーキング"
title_en = ""
categories = []
demographics = []
statuses = []
furigana = []
sources = []
languages = []
schedules = []
lastmod = 2024-08-26
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/wd5i930"
raw = "https://pocket.shonenmagazine.com/episode/316190246913879211"
md = "https://mangadex.org/title/72331318-7f5e-44b3-8510-12133149fde3/isekai-walking"
bw = "https://bookwalker.jp/series/402738"

[chapters]
released = 80
read = 0

[lists]
recommend = ""
+++

Protag is your average japanese highschool guy heading to school on the city bus when all of a sudden he and 6 other random people on the bus get transported to an isekai together and get told to slay the demon lord. However while the 6 other people get epic cool skills and massively OP statuses, the protagonist is "jobless" with the lowest possible stats and only has a single skill: Walking. As a result of the protagonist being so useless he gets cast out of the "hero party", thrown away by the kingdom, and left to fend for himself while the rest of the 6 train to defeat the demon king. However he gets a single experience point with every step he takes, and upon leveling up can choose any skill he wants since he's "jobless". And also his walking skill allows him to basically carry an infinite amount of items on his back since it reduces the weight of anything he carries while he's walking. Also the very first person he meets is a super nice cook who will give him just about everything for basically free because he pities the protag I guess!!

<!--more-->

