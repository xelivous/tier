+++
title = "没落予定の貴族だけど、暇だったから魔法を極めてみた"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2025-01-13
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/faiaz8t/botsuraku-yotei-no-kizoku-dakedo-hima-datta-kara-mahou-wo-kiwamete-mita"
raw = "https://to-corona-ex.com/comics/20000000045909"
md = "https://mangadex.org/title/2ffebd60-0b19-4719-bb79-c47b82ac2234/botsuraku-yotei-no-kizoku-dakedo-hima-datta-kara-mahou-wo-kiwamete-mita"
bw = "https://bookwalker.jp/series/260145/"

[chapters]
released = 47.2
read = 18

[lists]
recommend = "F"
+++

Protag's a random commoner who gets reincarnated into the body of the 5th sone of a count for some reason, and his family is desperately seeking Achievements so that they can remain as nobles after their father passes the title onto them otherwise they'll become commoners. However the protag doesn't really care about succession and ends up finding out about magic, and apparently he's super ultra compatible with magic somehow and basically the god emperor of magic that is better than everybody else, and just manages to do everything flawlessly because of his epic unconventional commoner wisdom, along with some help from some random dude passing by that is conveniently one of the best mages in existence. The protag then saves everybody while they glaze the fuck out of him and bow down in worship in his presence.

<!--more-->

Protag joins the adventurer guild and immediately enslaves the cute girl he parties with, then the girl introduces her to a milf adventurer and he enslaves her too. On top of that anybody he enslaves adapts to the "perfect form" which in the case of the 40+ year old woman means she becomes a hot teenager again or whatever lmao. He then goes around enslaving just about everything else and becoming dragonborn and basically marrying the princess while blasting off epic magic, and inventing dried ramen because that's surely a thing a typical medieval commoner would think of doing.

The art is fairly poor, the story sucks, the plot makes no sense at all, and it ultimately just becomes an incredibly terrible "city builder" series where everybody is enslaved to the protag. It's terrible. Why was this adapted? Why is this still ongoing? Why did this get an anime? I'm utterly baffled.
