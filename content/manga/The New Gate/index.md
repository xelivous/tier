+++
title = "The New Gate"
title_en = ""
categories = ["isekai", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-01-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=116976"
raw = "https://www.alphapolis.co.jp/manga/official/428000082"
md = "https://mangadex.org/title/b41bef1e-7df9-4255-bd82-ecf570fec566/the-new-gate"
bw = "https://bookwalker.jp/series/83899"

[chapters]
released = 87
read = 72

[lists]
recommend = "C"
+++

Starts off with a bang start of the protag obliterating the final boss of a deathgame VRMMO as if this was the ending of SAO. He waits there in the final boss room until the last people left alive in the game slowly log off, when all of a sudden after everybody else has logged out a random doorway pops into existence that sucks the protag into it, and he wakes up in the middle of the field but his logout button is missing (again). He basically gets sucked into the "real" world after the events of the game far into the future. Except he got some super broken titles from beating the last boss which makes him nigh untouchable and broken OP.

<!--more-->

It's a kind of interesting retirement home vrmmo series where the protag just kind of reminisces about the good old times he had with his buds while traveling the world now that its freed from the confines of being a game. Kind of like the after-credits portion of a JRPG where the protag just walks around and meets everybody again and heals his soul after the horrible shit he had to go through to reach/defeat the final boss. Kind of like a mix between SAO and Overlord except slightly less meme.

I feel like a lot of the LN designs are a little better than the manga designs, but overall the art is still good. It's kind of interesting that the protag has larger/cuter eyes than most women in the series as well. Tsubaki's eyes are the best though fr.

Ch33/34 are peak.

Overall a solid entry in the VRMMO category.
