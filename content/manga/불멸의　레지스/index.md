+++
title = "불멸의　레지스"
title_en = "Immortal Regis"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=16177"
md = "https://mangadex.org/title/480391fc-15d2-4622-9965-90f5e7021028/immortal-regis"

[chapters]
released = 44
read = 44

[lists]
recommend = "F"
+++

This is kind of a bizarre series. It's like shounen chuuni with oldschool designs and light ecchi since it was made in 2006. It starts building up a world and then just rapidly accelerates the story into some Epic with tons of fights that don't really make any sense, and it feels like it was rushed/axed and it's basically nonsense after a certain point. Except this series continues in another series called Cavalier of the Abyss that has like 3x as many chapters before it finishes. Events regularly just happen without any reasoning. it also pushes an akward romance between the protag and the demon who converted him into an undead despite them basically never interacting at all in any capacity; why the fuck does she like him despite there being nothing to drive that??

<!--more-->
