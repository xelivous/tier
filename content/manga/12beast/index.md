+++
title = "12beast"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-01
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/grzkqzc/12-beast"
raw = "https://comic-walker.com/contents/detail/KDCW_FS02000004010000_68/"
md = "https://mangadex.org/title/b0e6e187-bc12-4add-891f-9e8011e9c5d4/12-beast"
bw = "https://bookwalker.jp/series/9797/list"

[chapters]
released = 36
read = 0

[lists]
recommend = ""
+++

<!--more-->

I'm not particularly surprised that this got cancelled.
