+++
title = "そのおっさん、異世界で二周目プレイを満喫中"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-04-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154898"
raw = "http://seiga.nicovideo.jp/comic/42845"
md = "https://mangadex.org/title/4585777a-485f-4f03-93be-313b336f0c2e/sono-ossan-isekai-de-nishuume-play-wo-mankitsuchuu"
bw = "https://bookwalker.jp/series/214315"

[chapters]
released = 17
read = 8

[lists]
recommend = "D"
+++

Protag reaches max level and gets given the chance to reset his level back to 1 and start all over again, which he accepts since he got the absolute worst possible luck on level-up stat RNG giving himself the lowest roll for every possible level up. He also comes across a foxgirl trapped in ice while resetting his level and brings her back to his house, for reasons. He then later picks up an elf girl and they go grinding on various monsters. 

<!--more-->

The faces are pretty jank at times. The protag just gets more and more blonde lolis that want his dick over time. I don't really understand what part about this series is "isekai" even though it has isekai in the title; it's not even reincarnation either other than him resetting his levels. The synopsis on mangaupdates refers to his past life but it's never actually shown in the manga at all so it's kind of baffling. Considering how quickly this got axed, I don't really think this is worth starting/reading. It just kind of ends out of nowhere with no further updates, and the last 2 chapters aren't even available in a tankobon. There's other better works if you want to read foxgirl lolis fauning over a milquetoast protag.
