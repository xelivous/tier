+++
title = "転生したらパーティが男の子だらけだったけど断じて俺はショタコンじゃない"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=156895"
raw = "https://www.pixiv.net/user/2258616/series/38203"
md = "https://mangadex.org/title/968b8311-b754-48a1-9762-9583f97f12c5/after-reincarnation-my-party-was-full-of-boys-but-i-m-not-a-shotacon"
bw = "https://bookwalker.jp/series/197723"

[chapters]
released = 23
read = 0

[lists]
recommend = ""
+++

<!--more-->

The chapters are incredibly short and it gets updated maybe 3 times a year; it's little more than a low quality pixiv manga.
