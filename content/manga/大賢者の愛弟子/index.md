+++
title = "大賢者の愛弟子"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=163435"
raw = "https://seiga.nicovideo.jp/comic/46807"
md = "https://mangadex.org/title/d506f85f-f318-42bc-84fa-a651a3bae4e4/great-wise-man-s-beloved-pupil"
bw = "https://bookwalker.jp/series/260144/"

[chapters]
released = 21.1
read = 10.2

[lists]
recommend = "B"
+++

Protag only has attribute-less defensive magic which is largely looked down upon in this world since just about everybody has an attribute of some kind, but he still strives to be the very best in the world. However by the time he becomes a young adult he's a delinquent who is flipping skirts of young noble ladies instead. However when he's fighting the goons of one of the noble lady's skirts he flipped his childhood friend who can use all elements and is a gigachad swoops in and save the day; Also the gigachad childhood friend is obviously a reincarnated japanese person.

<!--more-->

It's an reincarnation/isekai but the protagonist is not the one who reincarnated, the protagonist is his rival instead, which is interesting enough in its own right. It's about a badass using magic in unconventional ways hoping people can learn about how absolutely awesome this style of magic is, while people look down on him for using what is normally considered to be beginner's magic. It dips into ecchi stuff relatively frequently as well I guess. I enjoy it a fair amount personally.
