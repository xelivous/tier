+++
title = "비선실세 레이디"
title_en = "The Little Lady Behind the Scenes"
categories = ["reincarnation", "isekai"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=191062"
raw = "https://page.kakao.com/content/58094906"
md = "https://mangadex.org/title/a972aebe-50fb-4dfe-8f61-0150f3659467/an-unseemly-lady"

[chapters]
released = 52
read = 33

[lists]
recommend = "D"
+++

Gets reincarnated as the protag's younger sister in a harem romance in noble society, and the original protag is an extremely competent girlboss that's top of her class so she has tons of high ranking nobility wanting to get married to her. Meanwhile the protag of this story (the sister) is fairly incompetent and just sits around eating desert all day while cockblocking everyone who tries to go after the original protag, and then eventually gets her own mini-harem involving literally the 2nd prince as well because of course. There's some good bits with the relationship involving the two protags and their sisterly love but beyond that it's not particularly interesting to me. I do appreciate that twinbraids become more and more common as the story goes along though.

<!--more-->
