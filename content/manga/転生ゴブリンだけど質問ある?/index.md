+++
title = "転生ゴブリンだけど質問ある?"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = ["every other week"]
lastmod = 2024-09-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=163155"
raw = "https://tonarinoyj.jp/episode/10834108156765668108"
md = "https://mangadex.org/title/782211b2-44ca-44ae-89f2-8a1160221b96/tensei-goblin-dakedo-shitsumon-aru"
bw = "https://bookwalker.jp/series/250018"

[chapters]
released = 106
read = 45

[lists]
recommend = "C"
+++

The protagonist is your average japanese office worker who is looked up to by his kouhai, and even selfless enough to throw himself in front of a truck when he spots a child running out in the middle of the street like a dingus. However while he's dying his final thoughts end up being entered into a spiritual search bar where he ends up asking for a long life and the ability to pass skills onto others, and ends up reincarnating into an isekai as a goblin (who fell from the sky). However goblins are a race that live for a very short amount of time, but are allowed to give their skills to another goblin upon death (but only the skill they were born with); yet the protagonist's skill is the ability to live for how long an average human could live for (since he wished for it), and as a result he ends up amassing a wide assortment of skills from all of the dying goblins as they continue to explode in population and die off immediately afterwards. He even manages to come across the dastardly nogooder evil bossdude who terrorized the world yet for some reason is just chilling there dying in a goblin forest conveniently for the protagonist to come across, and the protag even gets his skill which allows him to control anybody he's defeated before!!!!

<!--more-->

This work is kind of about evolution, similar to re:monster in a lot of ways except with a righteous dogooder protag instead of a rapey meme evil protag. Wouldn't be surprised if someone read re:monster and thought to themselves "this is interesting but what if it was more wholesome?" and then set out to make this. Despite the protagonist being fairly overpowered as a result of his circumstances it manages to balance it somewhat properly through the existence of even more powerful skills owned by various other people so that he isn't just a complete god slaughtering everybody. A relatively simple work that is somewhere in-between Slime and re:monster I feel.
