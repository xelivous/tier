+++
title = "異世界帰りの勇者が現代最強！"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["post-isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-03
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153497"
raw = "https://www.ganganonline.com/contents/isekai/"
md = "https://mangadex.org/title/35794/the-hero-who-returned-remains-the-strongest-in-the-modern-world"
bw = "https://bookwalker.jp/series/219018/"

[chapters]
released = 21.4
read = 21.6

[lists]
recommend = "C"
+++

Protagonist is a wimp who is getting bullied in school when he gets summoned to an isekai out of the blue, and eventually after many long years he ends up successfully vanquishing the demon lord and returns back hom to earth. However for some reason he retains all of his OP skills/magic even on earth. For some reason he has a loli mom as well haha!

<!--more-->

This work is largely the protagonist bruteforcing his way through a strange underworld society of supernatural beings on earth who all have powers of their own, but since he's some broken ex-isekai protag he's just kind of immune to everything and blasts through everything without care. Additionally he ends up dragging some other people into his game-like isekai skill system as well later on.
