+++
title = "대위님! 이번 전쟁터는 이곳인가요?"
title_en = "Reporting for Duty, Duchess!"
categories = ["reincarnation", "isekai"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-04
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/izo08g8/reporting-for-duty-duchess"
raw = "https://series.naver.com/comic/detail.series?originalProductId=489894"
md = "https://mangadex.org/title/79a9cce8-26f8-457b-935d-437149682855/reporting-for-duty-duchess"
wt = "https://www.webtoons.com/en/romance/reporting-for-duty-duchess/list?title_no=5384"

[chapters]
released = 67
read = 41

[lists]
recommend = "C"
+++

Protagonist is a high ranking officer in the korean(?) military who ends up dying in action from getting bombed suddenly, however instead of heading to the afterlife she wakes up in the body of a frail girl in noble society. Apparently she reincarnated as one of the friend-characters in her friend's novel from her previous life that she greatly disliked for being too naive, and she didn't like the main male protagonist of this romance novel either.

<!--more-->

If you want an emotionally stunted girlboss protagonist not taking shit from anybody while all of the male leads fawn over her this is an okay villainess work to read through. The romance is extremely slow to develop and she's essentially building up a harem of ikemen who love her for being a competent girlboss.
