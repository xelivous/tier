+++
title = "밑 빠진 용병대에 돈 붓기"
title_en = "Saving a Mercenary Unit from Bankruptcy"
categories = ["isekai"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/9wm5ffp/saving-a-mercenary-unit-from-bankruptcy"
raw = "https://comic.naver.com/webtoon/list?titleId=784823"
md = "https://mangadex.org/title/456b642e-4b6a-4ec0-86fa-d0ff5cb99695/pouring-money-into-the-underdog-mercenaries"
wt = "https://www.webtoons.com/en/fantasy/saving-a-mercenary-unit-from-bankruptcy/list?title_no=4686"

[chapters]
released = 80
read = 66

[lists]
recommend = "C"
+++

Protagonist is an average office lady in a weird version of korea where people regularly get whisked away to isekais apparently and the isekai she went to regularly has enough isekaijin that they have actual terminology for it. Additionally all isekaijin have the ability to understand every language flawlessly as if it were magic!! She is picked up by a band of mercenaries after getting isekai'd into the middle of snowy wilderness, and basically one of the first things out of her mouth is whether or not they need her management consulting services...

<!--more-->

The work tries pretty hard to make accounting seem interesting and fun and a compelling career path for women so it's pretty cool in that regard at least, even if the concepts it introduces are all just super basic surface level things that the "medieval neanderthals" get exited about. The work also does some cool things with parallel universes and multiple isekaijin intermingling throughout varying spacetime and technology levels, randomly popping into and out of existence in this isekai (and theoretically varous other isekai).

Unfortunately the battles, tactics, sense of scale, sense of time, and overall plot/writing leaves a lot to be desired. I also started to bounce off of it a lot more the longer it went on and kind of felt bored reading it.

Apparently the work is completed but the official english translation isn't there yet so i'll have to check back in a month+ until it's done.
