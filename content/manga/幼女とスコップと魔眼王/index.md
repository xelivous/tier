+++
title = "幼女とスコップと魔眼王"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165523"
raw = "https://seiga.nicovideo.jp/comic/45717"
md = "https://mangadex.org/title/57b8b608-1026-4cab-ad6a-28d796eca663/the-girl-the-shovel-and-the-evil-eye"
bw = "https://bookwalker.jp/series/283911/"

[chapters]
released = 24.5
read = 24.5

[lists]
recommend = "B"
+++

Protag gives his seat up to an asshole on a bus since he's a pushover and ends up dying in a carcrash because of it, while the asshole looks on in glee and records him on his phone. However the protag shortly wakes up in a cave afterwards without any of the wounds he had just moments before. There's a society of miners who collect ore from the entrance of the dungeon for poverty-tier money, adventurers who clear out monsters for a lot more money to make it safe for the miners, among a few other residents; since the protag is brand new to this world and needs to figure out how to survive he starts out as a miner and gets saved / taught the ropes by a loli sheepgirl.

<!--more-->

This series has the most powerful "that's bullshit, but I believe it" energy. The characters are fun and engaging, and the pacing is nice if you don't take into considering that it's monthly. It gets a little too skill spammy at times and has meme skill window nonsense but otherwise is still fun. The combat scenes are also decently well done. The series has been on hiatus for a few months now with no planned continuation so hopefully it continues at some point? The Wn/LN have been on hiatus as well so the prospects seem grim unfortunately...
