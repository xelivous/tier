+++
title = "두 번 사는 프로듀서"
title_en = "The Producer's Second Life"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-12-18
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/rlj6bo7/the-producer-s-second-life"
raw = "https://comic.naver.com/webtoon/list?titleId=780253"
md = "https://mangadex.org/title/d15c5412-dfb3-4e4e-bfcf-0f62ef7bc6c9/second-life-producer"
wt = "https://www.webtoons.com/en/drama/the-producers-second-life/list?title_no=5046"

[chapters]
released = 168
read = 168

[lists]
recommend = "B"
+++

The protagonist is a producer(director) in korea working on various shows but lives primarily in obscurity for most of his life due to constantly getting "bad luck" with terrible superiors that take advantage of him. One day he cries himself to sleep hoping he could do everything all over again, and wouldn't you know it for some reason he travels back in time to the first day he started on the job and gets to do everything all over again but better! He'll then use all of his knowledge and experience of the future to navigate the interpersonal relationships and harsh reality of showbiz to make his unfulfilled dreams come true.

<!--more-->

It's a fairly competent work that goes all in on the interpersonal aspects of the production/directing side of showbusiness, where the work is primarily wholesome chungus where more or less everything just kind of works out for the protagonist without any kind of setbacks. The work handles most of this fairly well and it's relatively entertaining to read when it's simply about the works being created, having to meet deadlines, trying to make ends meet within a production budget, etc. However the work kind of really goes to shit later on when it starts to get further and further into a meme gotoubun-esque romance aspect of "who's the mystery heroine he's going to end up with when they all want his dick???". It's kind of like oshi no ko in that respect where you get an overview of the more meta aspects of showbiz and how it affects all of the people involved, along with a side of the dumbest harem/romance drama on the side.
