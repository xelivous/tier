+++
title = "戦隊レッド 異世界で冒険者になる"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=173028"
raw = "https://www.ganganonline.com/title/1267"
md = "https://mangadex.org/title/d7f927c8-c1f5-474e-bb3d-57da04be7662/the-red-ranger-becomes-an-adventurer-in-another-world"
bw = "https://bookwalker.jp/series/295786"

[chapters]
released = 24.2
read = 0

[lists]
recommend = ""
+++



<!--more-->

