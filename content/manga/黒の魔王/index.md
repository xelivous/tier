+++
title = "黒の魔王"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-21
tags = [
    "evil-church",
    "summoned-protagonist",
    "enslaved-protagonist",
    "revenge",
    "torture",
    "gore",
    "mage-protagonist",
    "nudity",
    "number-rank-adventurer-guild",
    "adventurer-guild",
    "arbitrary-magic-system",
    "male-protagonist",
    "frequent-battles",
    "guns",
    "multiple-perspectives",
    "bunny-outfit",
    "yandere-heroine",
    "tragedy",
    "merciless-protagonist",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150723"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000087010000_68/"
md = "https://mangadex.org/title/30728/kuro-no-maou"
bw = "https://bookwalker.jp/series/193348/"

[chapters]
released = 30
read = 30

[lists]
recommend = "D"
+++

Protag is a tall japanese dude who looks like a thug but he's really just a hetare brainlet in the literature club, with a cute waifu who even has a crush on him but when she finally confesses to him he gets forcibly summoned to another world. However his summoners are an evil organization (the church) who basically kidnap humans from other worlds and experiment on them, and immediately upon arriving the protagonist gets a slavery halo placed upon him and he's unable to go against his captures. He then gets tortured for months with various experiments and has to fight against tons of monsters (and humans) in the process. However one day through an accident the slavery halo gets removed and seeks to break out of captivation.

<!--more-->

It's kind of a weird work that is painfully seinen with tons of edgy/ecchi content inserted in seemingly for shock value, and a large majority of the work is just the protagonist fighting against the Evil Church in large epic battles. There's a few moments of Good mixed in but it's hard to recommend unless you want a super edgy battle shounen with tons of gore/nudity. There's a cute twinbraid in a few panels at least.
