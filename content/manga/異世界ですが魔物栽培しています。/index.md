+++
title = "異世界ですが魔物栽培しています。"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-10-15
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144880"
raw = "http://comic-walker.com/contents/detail/KDCW_MF00000040010000_68"
md = "https://mangadex.org/title/1ed075e6-432a-4e21-a4f6-d63e4c5ae485/isekai-desu-ga-mamono-saibai-shiteimasu"
bw = "https://bookwalker.jp/series/135229"

[chapters]
released = 35
read = 35

[lists]
recommend = "D"
+++

Protag is your average horny teenager living on his own but is still kind of mooching off of his parents, when one day he opens up the door to his apartment and gets transported to an isekai. There's dangerous creatures about so surely the protagonist should've gotten some cheat skill to handle them, but nope he didn't he's as weak as anybody else and almost immediately dies. He eventually limps his way to a nearby town and tries registering to the adventurer's guild in hopes that he might be able to figure out if he has any cheat skills, but it costs money to view your status and he's broke. He then tries to head to a restaurant to eat some food but they throw him out for being broke (and they won't give him a job), so he ends up eating leftover trash that the restaurant throws out instead, wherein he finds a random seed in the trash and gets the brilliant idea to cultivate his own food outside of town near an abandoned house instead. The seed somehow almost immediately grows into a plant but after nuturing it for multiple days it turns out to be a carnivorous monster plant that tries to eat him; but he's saved by a cute tsundere knight who tells him that all seeds only grow monsters in this world and he's a dumbass. Yet that isn't going to stop our dumbass of a protag, he's going to cultivate his monster farm to sell their rare/exclusive fruit and sell them with the help of this cute knight.

<!--more-->

Protag shoves stuff in ground. Stuff comes out of ground. You can't explain that. I really appreciate the double page in ch27 though