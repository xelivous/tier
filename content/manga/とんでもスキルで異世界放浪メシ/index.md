+++
title = "とんでもスキルで異世界放浪メシ"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-25
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=140468"
raw = "https://comic-gardo.com/episode/10834108156661710941"
md = "https://mangadex.org/title/21055/tondemo-skill-de-isekai-hourou-meshi"
bw = "https://bookwalker.jp/series/142109/"

[chapters]
released = 55.2
read = 55.1

[lists]
recommend = "D"
+++

Protag gets mistakenly summoned along with the heroes as an accident, but he doesn't have any noticeable combat skills and the kingdom that summoned him seems a little sus so he decides to just fuck off on his own after receiving some money. The only real skill is the ability to order off of isekai amazon using this world's currency, and as a result he can just buy any extremely high quality goods for dirt cheap and then sell them for a massive markup which turns itself into an infinite money cheat. Also any food he makes boosts the stats of whoever eats it (like a game) which is out of the norm for this world so he's doubly cheaty.

<!--more-->

This is largely a cooking manga where the protag just gets shuttled around by his legendary monster companion while he bumbles about breaking every convention possible. It kind of makes a joke with the guild system by glossing over it and making the guild rep not give a shit about the explanation of anything, only to expedite the protag through the ranks solely to give his familiars missions since like obviously his familiar can just obliterate anything and everything anyways so the ranking system doesn't make any sense.
