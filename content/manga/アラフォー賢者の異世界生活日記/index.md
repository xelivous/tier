+++
title = "アラフォー賢者の異世界生活日記"
title_en = ""
categories = ["isekai", "game"]
demographics = ["seinen"]
statuses = ["hiatus"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-02-23
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149663"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000071010000_68/"
md = "https://mangadex.org/title/adce0aea-3f6d-4fd2-9902-1107f59f94da/arafoo-kenja-no-isekai-seikatsu-nikki"
bw = "https://bookwalker.jp/series/181859"

[chapters]
released = 39
read = 30

[lists]
recommend = "C"
+++

Middle-aged programmer dude that got laid off is a part of some hardcore VRMMORPG gamer group doing top-level raids, and after finishing a raid for the "final boss" all of his party mates log off, the boss comes back and does weird shit, and then the power for the entirety of japan goes out stranding the players inside of the game (including the protag). The protag then gets super maxed out stats/etc after getting reincarnated by the goddess due to reasons since he was the one who helped beat the "final boss" haha lol. Also he almost gets raped by the apes living in the forest he got isekai'd into constantly. Also the magic system in this is programming-based, which suits the protag because he's a programmer. Coders rise up.

<!--more-->

It's okay I guess. Protag just dabs on everybody with his superior stats and programmer knowledge while doing whatever he feels like, occasionally advancing the magic knowledge of the world by leaps and bounds and upheaving society as we know it with everything he does. It's nice that he's not the only person to get stuck in this VRMMO world but very little actually comes about from the additional people being here since the protag is just so broken OP anyways.
