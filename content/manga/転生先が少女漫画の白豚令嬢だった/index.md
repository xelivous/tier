+++
title = "転生先が少女漫画の白豚令嬢だった"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=148040"
raw = "http://seiga.nicovideo.jp/comic/33841"
md = "https://mangadex.org/title/fd4f1147-c090-4a28-923e-eb4918f94d5a/i-reincarnated-as-a-white-pig-noble-s-daughter-from-a-shoujo-manga"
bw = "https://bookwalker.jp/series/173006"

[chapters]
released = 10
read = 0

[lists]
recommend = ""
+++


<!--more-->
