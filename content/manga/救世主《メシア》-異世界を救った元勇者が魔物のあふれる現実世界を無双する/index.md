+++
title = "救世主《メシア》 ~異世界を救った元勇者が魔物のあふれる現実世界を無双する~"
title_en = ""
categories = ["isekai", "post-isekai", "reverse-isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["original"]
languages = ["japanese"]
schedules = ["every other week"]
lastmod = 2024-12-04
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/pmpit16/messiah-isekai-o-sukutta-moto-yuusha-ga-mamono-no-afureru-genjitsu-sekai-o-musou-suru"
raw = "https://tonarinoyj.jp/episode/316112896809499276"
md = "https://mangadex.org/title/3b999f4a-0b7f-4aef-a849-cd86e31c4b57/kyuuseishu-messiah-isekai-o-sukutta-motoyuusha-ga-mamono-no-afureru-genjitsu-sekai-o-musou-suru"
bw = "https://bookwalker.jp/series/393651/"

[chapters]
released = 48
read = 48

[lists]
recommend = "A"
+++

Protag is a loser that got bullied in his own world and deeply desired to be whisked away to an isekai while trying to run away from his problems, then actually got isekai'd in his sleep to a place harrassed by a demon lord and told to save it as a hero. Except once he had completed his task he became powerful enough to threaten the kings so they betrayed him, villified him to the masses, and executed him. Except after his death he wakes up in a hospital bed, after apparently being in a coma for years after falling off the roof of a building after "playing with his friends", except this is someone else's body entirely and he has actually no idea what is going on. On top of all that he seems to have been flung into the far future where there's monster outbreaks all around the world and Hunters awoke to combat them as if this were a korean manwha. Unfortunately on his way home his (new) family gets caught up in a monster portal outbreak and while he manages to avert the initial crisis he still has trauma from standing out too much and getting killed in his previous life, so has a dillemma over saving the people around him or trying to hold back his powers as much as possible. But will he????

<!--more-->

I enjoy this work a lot. All of the characters are great, the girls actually get good action scenes and can fight, the protag may be kind of a bulldozer of infinite power but he doesn't really bulldoze his way to victory and a lot of the work is about exploring and expanding upon his emotional state and figuring out how he ended up in his new body. The work touches on a lot of genres and is pretty clearly inspired by korean dungeon outbreak manwha, and it's also manga original which is why it feels so fresh/novel compared to all of the narou-core works.
