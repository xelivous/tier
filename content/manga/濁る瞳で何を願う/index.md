+++
title = "濁る瞳で何を願う"
title_en = ""
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=195079"
raw = "https://pocket.shonenmagazine.com/episode/3270375685457628827"
md = "https://mangadex.org/title/418be4a0-a376-4a7e-aa34-51576b2e6fa7/nigoru-hitomi-de-nani-wo-negau-highserk-senki"
bw = "https://bookwalker.jp/series/379715"

[chapters]
released = 16
read = 0

[lists]
recommend = ""
+++



<!--more-->

