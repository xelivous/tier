+++
title = "グッバイ！異世界転生"
title_en = ""
categories = ["reverse-isekai", "game"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-03-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146135"
raw = "https://pocket.shonenmagazine.com/episode/10834108156643409833"
md = "https://mangadex.org/title/ca200b90-2fb5-44f9-90de-8fda4058f734/goodbye-isekai-tensei"
bw = "https://bookwalker.jp/series/163978/"

[chapters]
released = 18
read = 18

[lists]
recommend = "D"
+++

The ecchi content greatly distracts from the rest of the plot and feels extremely out of place every time it is brought up, which is far too frequently; It adds nothing to the story and feels like cheap filler in an already short story. The rest of the work is kind of an absurdist comedy series that does some parodies about other isekai/games and just goes all in on a weird storyline that gets interrupted by the bad ecchi scenes constantly. The ending is nonsense as well, and a lot of the characters feel kind of pointless to the story aside from their one off chapters that don't really lead anywhere. I don't know who I would rec it to, but it's short enough that it's probably worth experiencing regardless.

<!--more-->

