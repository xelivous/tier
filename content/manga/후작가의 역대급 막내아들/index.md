+++
title = "후작가의 역대급 막내아들"
title_en = "The Marquess's Youngest Son"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2024-07-27
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=197725"
raw = "https://page.kakao.com/home?seriesId=56760327"
md = "https://mangadex.org/title/43547c04-8fa2-49ce-84b4-b0424dd98bd6/legendary-youngest-son-of-the-marquis-house"

[chapters]
released = 111
read = 104

[lists]
recommend = "F"
+++

Protag obliterates everybody in his last life then dies because he's bored after getting his revenge, but gets reincarnated back to when he was young again and sets out to make this life different so that he wouldn't need to get revenge in the first place. He openly tells people that he's from the future and knows future events because he doesn't care about hiding anything since he can still just obliterate anybody even with his frail original body for some reason. 

<!--more-->

It has some okay worldbuilding but nothing beyond that. The plot holes are so enormous that quof would literally become frodo personified and it's little more than extreme power fantasy wish fulfillment even outside of when the story isn't ruining itself. The official translation has an ultra meme title of "Jack Be Invicible" which is just silly and bad. To make matters worse the official translation also uses excessive punctuation constantly like 5 !!!!! or ????? after bubbles regularly which is super annoying!!!!! Also it constantly misgenders characters that aren't even ambiguous as if the translator keeps changing and they don't have any translation notes at all (and/or it's just MTL). The longer the series goes on the more 3D is used (in bad ways); from characters to outright terrible 3d dragons, and 3d backgrounds, etc; the art only gets more and more terrible.
