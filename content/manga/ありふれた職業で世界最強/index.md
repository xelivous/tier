+++
title = "ありふれた職業で世界最強"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-02-26
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=128108"
raw = "https://comic-gardo.com/episode/10834108156661711704"
md = "https://mangadex.org/title/18091/arifureta-shokugyou-de-sekai-saikyou"
bw = "https://bookwalker.jp/series/99891/"

[chapters]
released = 67
read = 53

[lists]
recommend = "C"
+++

So this work starts off with the entire class getting isekai'd all at once, and they can't be returned back to their home unless the gods will it. Unfortunately the protag is a loser otaku who gets shit stats and a shit class and is destined to be completely useless, but he's the epic badass protag so he'll try his best regardless. Unfortunately he gets backstabbed and falls down into the depths of the dungeon, and that's when the protag goes on an epic training montage to be the very best like no one ever was thanks to the power of demonization.

<!--more-->

Good comedy manga. Edgy McEdgerton kills everyone with his edge. The art is pretty great, but it skips a little too much that both the LN/Anime go over in detail. Useful as a supplement to one of the other mediums, but you might need to handwave some knowledge if you're reading it as your first exposure. Overall I do enjoy it as popcorn material but it goes a little too hard on haremy antics strays too far away from the epic edginess it had near the beginning.
