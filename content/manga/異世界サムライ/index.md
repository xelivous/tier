+++
title = "異世界サムライ"
title_en = ""
categories = []
demographics = []
statuses = []
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-08
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/7bn8es4"
raw = "https://comic-walker.com/contents/detail/KDCW_CW01203832010000_68"
md = "https://mangadex.org/title/13d2856e-9163-43a1-86f9-cd66d41db09b/isekai-samurai"
bw = "https://bookwalker.jp/series/421614"

[chapters]
released = 0
read = 0

[lists]
recommend = ""
+++



<!--more-->

