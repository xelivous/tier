+++
title = "復讐を誓った白猫は竜王の膝の上で惰眠をむさぼる"
title_en = ""
categories = ["isekai"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144277"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00000004010000_68/"
md = "https://mangadex.org/title/8bd50e0c-f9ce-4f84-bc9a-95c597a7aed4/the-white-cat-s-revenge-as-plotted-from-the-dragon-king-s-lap"
bw = "https://bookwalker.jp/series/156164"

[chapters]
released = 22
read = 19

[lists]
recommend = "C"
+++

The protag is a disney princess surrounded by the fairies from jintai. The protag's "friend" is a psychopath and a lot of the plot is either "doing nothing" or "dealing with the consequences of the protag's "friend" being a psychopath" so it's in a weird spot of healing and aggravating. Holy shit she's aggravating. Would rec if you want to be annoyed and/or want to read a story with jintai fairies in it, or where the protag transforms into an actual cat for a ton of chapters.

<!--more-->
