+++
title = "せっかくチートを貰って異世界に転移したんだから、好きなように生きてみたい"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147560"
raw = "http://comicride.jp/sekkache/"
md = "https://mangadex.org/title/d36b802a-7d1e-4153-80ee-b6ee509555ff/sekkaku-cheat"
bw = "https://bookwalker.jp/series/184399/"

[chapters]
released = 57.2
read = 34

[lists]
recommend = "B"
+++

Protag gets isekai'd with broken cheat-like healing abilities, and decides to spend almost all of his time in a brothel/etc having sex with prostitutes. The sex scenes are also vividly go into like vivid outlandish roleplay to get around having to draw actual sex scenes with tons of euphemisms and parallels. As a result the work doesn't feel particularly erotic and is more meant to be a tasteful(?) look on prostitution in another world that leans on comedy a fair bit.

<!--more-->

It's like ishuzoku reviewers except with an isekai'd dude that has healing powers, which eventually turns into a mecha series with sex on the side? ch15 is based btw. It's honestly an incredibly ridiculous series that goes over the top of sexual performances in a comedic fashion that is worth reading.
