+++
title = "転生したらSSS級ゴブリンになりました"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-16
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/qdqampq"
#raw = ""
md = "https://mangadex.org/title/caf82174-0460-4995-879f-1c20f5783703/i-reincarnated-as-an-sss-ranked-goblin"
bw = "https://mechacomic.jp/books/182331"

[chapters]
released = 23
read = 5

[lists]
recommend = "F"
+++

Protag plays a VRMMO regularly and then ends up getting reincarnated into it as a very exceedingly human-like "goblin" complete with a cute blind imouto and both of them are basically indentured servants to a local lord. However one day while getting bullied by literally everybody imaginable for being a despicable goblin, the humans attack his settlement and he gets subjugated shortly after reincarnating. However after dying (again) the goddess just sighs, gives him all of the super broken cheat skills that are actually epic edgy shadowy dark magic skills, and he gets reincarnated again for reasons. He'll then become the coolest dude imaginable and save the world as an anti-hero because he's so edgy.

<!--more-->

This is painful. Basically goblins got killed so hard that their collective deaths prestiged to create the god emperor goblin that is the protagonist who can just buttblast everybody. He exists solely to dab on everybody for slaughtering goblins and treating them as fodder, like a bizarre corrective action in the world. However despite being the most broken existence in the world that exists slowly to dab on everybody, his first plan of action is to join the academy and have a happy school life so that he can meet with the king once he becomes renowned enough at the school. What? Why?

I feel like this plot was chatgpt-generated.