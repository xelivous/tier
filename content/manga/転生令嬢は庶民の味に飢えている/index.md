+++
title = "転生令嬢は庶民の味に飢えている"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=156871"
raw = "https://www.alphapolis.co.jp/manga/official/30000278"
md = "https://mangadex.org/title/d9e60571-5473-4e0e-b491-d2ddb42a4675/tensei-reijou-wa-shomin-no-aji-ni-uete-iru"
bw = "https://bookwalker.jp/series/245473"

[chapters]
released = 29
read = 0

[lists]
recommend = ""
+++



<!--more-->

