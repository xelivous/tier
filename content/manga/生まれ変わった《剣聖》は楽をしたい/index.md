+++
title = "生まれ変わった《剣聖》は楽をしたい"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166748"
raw = "https://comic-gardo.com/episode/13933686331636232737"
md = "https://mangadex.org/title/c491f3c5-5fea-4229-a098-52440e0b67c7/the-reincarnated-sword-saint-wants-to-take-it-easy"
bw = "https://bookwalker.jp/series/264427/"

[chapters]
released = 20
read = 20

[lists]
recommend = "D"
+++

Protagonist travels around everywhere honing his sword skills to be the very best there ever was and then eventually dies of old age only to reincarnate into the same world many years later, manages to keep his memories for some reason, and vows to use his skills to just have fun and do whatever he wants. One day he gets assigned to be a bodyguard for a noble girl who is also fairly strong at sword skills herself and refuses to take any bodyguards, so he has to basically make her submit and be protected by him.

<!--more-->

I appreciate the focus on teaching/mentoring but the work doesn't really have any meaning or struggles it's just the protag dabbing on people and then it ends. Probalby an okay minor romp if you enjoy battle shounen?
