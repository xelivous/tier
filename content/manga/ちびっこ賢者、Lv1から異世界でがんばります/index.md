+++
title = "ちびっこ賢者、Lv.1から異世界でがんばります"
title_en = ""
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-18
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153413"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19200714010000_68/"
md = "https://mangadex.org/title/35573/the-small-sage-will-try-her-best-in-the-different-world-from-lv-1"
bw = "https://bookwalker.jp/series/210400/"

[chapters]
released = 30
read = 30

[lists]
recommend = "D"
+++

Protag has been playing this fulldive VRMMO for 3 years, and finally reached the level required to change into the class/job she always wanted which apparently also resets people's levels back to 1 every time (kind of weird but okay), and also for some reason teleports you to the Real isekai that the game is based on. She quickly gets discovered by some local knights doing their patrols, gets mistaken for a lost girl, and subsequently taken in for questioning. Since she made her avatar a cute loli she's actually 8 years old in this world now and essentially gets adopted by the knights while trying to figure out how to get back home.

<!--more-->

I feel like the unique part about this work is that there's a good amount of focus on non-bishounen male characters; there's decent enough representation.

Unfortunately I don't really think the series is worth reading. The plot is all over the place and nothing of note really happens; the protag levels up and then the people are confused about levelling up and all of the game-specific knowledge the protag keeps dropping, while occasionally roaming around and defeating the macguffin bosses to get the key to get to the tower to get home. However the issue is that the actual character interactions aren't particularly deep, and this is largely a character-focused work.
