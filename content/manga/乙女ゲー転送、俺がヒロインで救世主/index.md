+++
title = "乙女ゲー転送、俺がヒロインで救世主"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156669"
raw = "https://seiga.nicovideo.jp/comic/46917"
md = "https://mangadex.org/title/3ef126d1-5e41-4594-924b-cb6e5ca99bdb/isekai-tensei-ore-ga-otome-ge-de-kyuuseishu"
bw = "https://bookwalker.jp/de6b9adc23-2539-41c9-b9bf-4f94ad05a6e7"

[chapters]
released = 27
read = 26

[lists]
recommend = "S"
+++

Protag's sister ends up accidentally isekai-ing him when she was experimenting with various rituals instead of herself, and he ends up trapped in the otomege that the sister wanted to visit. As a result the protag needs to try his best to reach the lovey-dovey ending with a guy in order to get the macguffin to return home including doing all of the usual events that a saintess would normally do in an otomege, except he's not gay. Thankfully(?) he can still hear his sister communicate with him so she can help him understand some parts of the plot.

<!--more-->

it's a cool series that mixes the otomege/villianess genres with general male-protag isekai with a focus on comedy and it's kind of a really cool blend of josei and shounen.
