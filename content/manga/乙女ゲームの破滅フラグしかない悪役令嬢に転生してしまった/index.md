+++
title = "乙女ゲームの破滅フラグしかない悪役令嬢に転生してしまった"
title_en = ""
statuses = ["ongoing"]
demographics = ["josei"]
categories = ["game", "isekai"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-21
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=142479"
raw = "http://online.ichijinsha.co.jp/zerosum/comic/hametu"
md = "https://mangadex.org/title/c6bbbeca-f4fd-4595-9d4d-4ba31370d08c"
bw = "https://bookwalker.jp/series/249713/"

[chapters]
released = 56
read = 54

[lists]
recommend = "B"
+++

Protag stays up all night playing otome games then get's killed/isekai'd/reincarnated from not paying attention, only to wake up as the villainess in the otome game she was playing that night. Which is unfortunate since in just about every route in the game only bad things happen to her character! However the protag is a complete dumbass and isn't suited to be a villainess so she fumbles her way around doing the dumbest things imaginable to try and not die, and in the process ends up endearing everybody around her. The work focuses on lighthearted comedy focused with tons of weird heavier topics intermixed, with the most focus being on the protag getting a massive harem of both genders through her thickheaded nonsense. 

<!--more-->

This adaptation is a decent alternative to watching the anime although won't really add anything if you've already watched it. However the anime has already adapted far far ahead of where the manga has adapted, and it will likely take many more years before they're even at the same point.
