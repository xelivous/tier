+++
title = "おもいでエマノン"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=29891"
raw = "http://www.comic-ryu.jp/_emanon/"
md = "https://mangadex.org/title/636f5140-2561-4520-984f-6e10a8cc5922/memories-of-emanon"
bw = "https://bookwalker.jp/de9e83e41b-bd9c-4d2a-9aa9-7285209fb872"

[chapters]
released = 9.5
read = 0

[lists]
recommend = ""
+++

Also includes さすらいエマノン.

<!--more-->

