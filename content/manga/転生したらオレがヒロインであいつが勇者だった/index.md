+++
title = "転生したらオレがヒロインであいつが勇者だった"
title_en = ""
statuses = ["completed"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=164960"
raw = "https://comic-gardo.com/episode/13933686331617741525"
md = "https://mangadex.org/title/49634/when-i-was-reincarnated-in-another-world-i-was-a-heroine-and-he-was-a-hero"
bw = "https://bookwalker.jp/series/260055/"

[chapters]
released = 45
read = 0

[lists]
recommend = "C"
+++

it's not love but so where near. honestly if you removed the context that the protag is a "guy in a female body" the story would be mostly identical so it's pointless?

<!--more-->
