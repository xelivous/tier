+++
title = "이제 곧 죽습니다"
title_en = "Death's Game"
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-08-26
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/pqeqhng/death-s-game"
raw = "https://comic.naver.com/webtoon/list?titleId=727189"
md = "https://mangadex.org/title/42ff706e-5f8d-4c6a-aa85-1da68fdbda50/i-will-die-soon"
wt = "https://www.webtoons.com/en/drama/deaths-game/list?title_no=1265"

[chapters]
released = 66
read = 66

[lists]
recommend = "B"
+++

Protagonist is a 31 year old neet who graduated college but was still unable to get employed anywhere for 5+ years, invested most of his money into scam cryptocoins and lost all of his savings, and can't pay his rent. And since he was a dejected loser neet living at home his pretty and successful girlfriend eventually left him since he only became more and more of a loser after his life started to fall apart. Thus he decides to kill himself. However he wakes up in a plane next to a person who claims to be (and is) the personification of death, and is set on punishing the protagonist for making light of Death, and his punishment is that he'll die and reincarnte as 13 different people who are soon to die, however if he manages to somehow survive he's allowed to live out that life.

<!--more-->

This is a work about suicide, depression, family, and revenge, told through what is essentially a collection of short stories featuring all sorts of different protagonists, all colored by an insufferable dumbass of a protagonist that succeeded in killing himself. The protagonist is truly just horrible to read and not because they're badly written, the opposite really, the author really knows how to write someone who likely would've actually killed themselves from thinking their life was over. The protagonist is as petulant as you could ever really imagine somebody being in his situation, stuck reincarnating only to repeatedly die a horrible death over and over.

However I feel like the work is a little too heavy-handed with its overall message of "pls don't kill yourself u have so much to live 4"; it never feels like it goes further than the surface level on any of the topics/lives the protagonist suffers through. The ending of the work feels extremely rushed as if it got axed, although it still managed to "complete" what it started out to do in the end in a relatively satisfying manner so I marked it as "completed".

Ultimately I ended up liking the work and feel like it's worth experiencing overall, although if you struggle with depression or suicidal thoughts I would maybe recommend staying away from this work since the work almost constantly tries to blackpill the reader even if it eventually kind of tries to redeem itself a little near the end.
