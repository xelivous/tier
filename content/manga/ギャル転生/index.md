+++
title = "ギャル転生"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-03-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148135"
raw = "https://web-ace.jp/youngaceup/contents/1000063/"
md = "https://mangadex.org/title/d63aa51b-6472-4fcd-a098-575e5c7a39bb/gyaru-tensei-isekai-seikatsu-majidarui"
bw = "https://bookwalker.jp/series/164981/"

[chapters]
released = 17
read = 17

[lists]
recommend = "F"
+++

I feel like someone could power through if they really loved gyaru but.........

<!--more-->
