+++
title = "던전 마제스티"
title_en = "Dungeon Majesty"
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150147"
raw = "https://page.kakao.com/home?seriesId=56385442"
md = "https://mangadex.org/title/85ce2a5a-5d32-4d63-a776-6e4d8a3db32c/dungeon-majesty"

[chapters]
released = 142
read = 111

[lists]
recommend = "D"
+++

The start of the work is interesting enough with the concept of being able to swap bodies if your inherent soul is strong enough to inhabit the magical properties of that body, but I don't feel like it utilizes that mechanic in any meaningful way outside of as a vehicle to increase/decrease the cast's power levels arbitrarily to make the story "work". It also starts to meander a bit after a while in odd directions. It's barely hinted at why "booba heroine" isekai'd the protag, why she's living in his body, and why she keeps supporting him, outside of the tiny hints its dropped that in his previous life he was like her master or whatever and was probably the harem master of every other girl in the series since why not. Every single girl in the series starts craving him after a small amount of time of being around him anyways.

<!--more-->

