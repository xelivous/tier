+++
title = "그 기사가 레이디로 사는 법"
title_en = "From a Knight to a Lady"
categories = ["reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-10-02
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=178896"
raw = "https://comic.naver.com/webtoon/list.nhn?titleId=758675&week=fri"
md = "https://mangadex.org/title/25ce6f0e-51ae-467e-8519-fe90060586dc/from-a-knight-to-a-lady"
wt = "https://www.webtoons.com/en/fantasy/from-a-knight-to-a-lady/list?title_no=3164"

[chapters]
released = 104
read = 64

[lists]
recommend = "D"
+++

Protag slowly works her way up as a knight in ye olde society that believes women can't do anything, and as a commoner, only to get betrayed by her closest "friend"/vice-commander when they were losing a war in order to save himself even if it meant betraying his losing country. Except the protag reincarnates as a "very pretty" noble in the empire that destroyed her old country who basically committed suicide, and has to work out how to live in her new body as a noble; she's also engaged to a duke because why not. She originally tries to get revenge and then kind of just stops trying and eventually just tries to learn how to fit into noble society and does some romance with her fiance; it's all just kind of unfocused and the plot is all over the place with mixed signals on what kind of story it wants to tell.

<!--more-->

