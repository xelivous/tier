+++
title = "迷宮ブラックカンパニー"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=137717"
raw = "https://magcomi.com/episode/10834108156766291310"
md = "https://mangadex.org/title/4c86f38a-cead-4575-a9c3-acf7261a58ff/meikyuu-black-company"
#bw = ""

[chapters]
released = 50.2
read = 0

[lists]
recommend = ""
+++

<!--more-->
