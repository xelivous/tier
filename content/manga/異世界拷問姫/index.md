+++
title = "異世界拷問姫"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["partial"]
sources = ["light-novel"]
languages = ["japanese"]
schedules = []
lastmod = 2022-03-28
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=142343"
#raw = ""
md = "https://mangadex.org/title/c3e37161-3c45-4e81-89cc-7b2353617ca9/isekai-goumon-hime"
bw = "https://bookwalker.jp/series/134750"

[chapters]
released = 15
read = 15

[lists]
recommend = "D"
+++

This is basically an edgy grotesque "horror" manga. The author alternates between wanting to get stepped on by a dominatrix and wanting to draw generic gore scenes, then it goes back and forth between talking head exposition and then action scenes with no dialogue that are extremely hard to follow since there's so much blood and guts in the panels. The work is a hallucinatory drugtrip inside the mind of a sociopath after being pushed to the brink of breaking through horrifying events and has lost all of their inhibitions.

<!--more-->

There's moments of this that are good but it's so laced in extremely edgy gore that it's hard to take seriously, and I could only recommend it to someone who actually enjoys the aforementioned contents in excessive amounts. The work is "completed" in that it resolves most of the plot threads by rushing through the ending but doesn't resolve everything, and there's no way it was a full complete adaptation considering how many LN volumes were out so i'm marking it as axed. It's possible that the LN is more bearable since it wouldn't have constant guro on every page and would probably delve more into their fucked psychology but I'm not going to bother reading it to find out.