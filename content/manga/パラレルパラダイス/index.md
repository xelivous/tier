+++
title = "パラレルパラダイス"
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["partial"]
categories = ["isekai"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=139763"
raw = "https://comic-days.com/episode/13932016480029400361"
md = "https://mangadex.org/title/21040/parallel-paradise"
bw = "https://bookwalker.jp/series/121327/"

[chapters]
released = 237
read = 121

[lists]
recommend = "C"
+++

Endless gratuitous sex scenes with meme plot.

<!--more-->
