+++
title = "전투종족으로 환생한 나의 공무원 생활기"
title_en = "My Civil Servant Life Reborn in the Strange World"
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-12
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=197126"
raw = "https://page.kakao.com/content/58870225"
md = "https://mangadex.org/title/c0d16b08-8bd3-4292-8958-370619e48996/my-civil-servant-life-reborn-in-the-strange-world"

[chapters]
released = 45
read = 40

[lists]
recommend = "D"
+++

extremely OP dude lives in extremely OP village after reincarnating from his boring dull life and he just wants to honor his previous life's mother's last dying wish to be a basic boring civil servant, so he sets out from his insane OP village to try and fit in a mundane lifestyle with the plebs. Except he ends up just doing tons of random shit like pretending to be lupin the 3rd instead while his family tries to track him down. The story is basically nonsense with no direction, the action scenes suck ass, the characters have like a weird glow on them. idk.

<!--more-->
