+++
title = "懲役339年"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=110523"
#raw = ""
md = "https://mangadex.org/title/1712d8ff-8d10-4c6b-8b38-ea7e8c311833/339-years-of-penal-servitude"
bw = "https://bookwalker.jp/series/129354"

[chapters]
released = 43
read = 0

[lists]
recommend = ""
+++



<!--more-->

