+++
title = "悪役令嬢の執事様"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172105"
raw = "https://www.ganganonline.com/contents/shitsujisama"
md = "https://mangadex.org/title/7722cc2c-cc19-4ca6-ac22-81bce771afe8/akuyaku-reijou-no-shitsuji-sama"
bw = "https://bookwalker.jp/series/283901"

[chapters]
released = 33.2
read = 31.2

[lists]
recommend = "B"
+++

Protag reincarnates as the butler of his favorite otomege heroine which he only played since his sister kept shoving the games at him, except it's not the heroine it's the villainess who is getting harassed by her maid and is on the path to destruction. The protag sets out to prop up his waifu to be the very best there ever was and make sure she doesn't get executed.

<!--more-->

Protag essentially grooms his waifu he shares a death flag with while they politically maneouver themselves through High Society while he offers her up to her fiance regularly because he's a betacuck. I do appreciate that the main heroine isn't just a figurehead who is incapable of doing anything and actually is able to play a role in the story without her Totally Amazing Butler handling everything for her.

I'm rather fond of the characters, the art is nice enough, and the main heroine is pretty. However it's kind of annoying that the story is constantly saying "but they're only 12!!!"; legit feel like the author is a lolicon or something; they're obviously a young age to showcase how unnaturally competent the protag/villainess is but it just feels annoying to keep that up for the entire story.
