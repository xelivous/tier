+++
title = "빌런투킬퓨핀"
title_en = "Villain to Kill"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/uikn3xv/villain-to-kill"
raw = "https://comic.naver.com/webtoon/list.nhn?titleId=765156&weekday=tue"
md = "https://mangadex.org/title/94e6f5a4-a215-4f5b-ae82-febc560be66d/villain-to-kill"
wt = "https://www.webtoons.com/en/action/villain-to-kill/list?title_no=2857"

[chapters]
released = 76
read = 54

[lists]
recommend = "D"
+++

So this is like a superhero battle shounen where the (shit) hero dies and reincarnates as a villain as if he was in a zombie game in an FPS, but still tries to be a hero even after being a villain. He was originally prejudiced when he was a hero thinking that all villain were scum, but eventually comes across some villains that aren't because actually the world is not black and white and heroes aren't always justice. The battle scenes are braindead and kind of are bad which is unfortunate since that's like what 90% of this work is composed of.

<!--more-->
