+++
title = "外道転移者のハーレムダンジョン製作記"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-07-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=183318"
raw = "https://gammaplus.takeshobo.co.jp/manga/gedodan/"
md = "https://mangadex.org/title/94da9ae3-67dd-460f-8516-b5d4623d5282/records-of-the-creation-of-the-demon-otherworlder-s-harem-dungeon"
#bw = ""

[chapters]
released = 12.2
read = 2

[lists]
recommend = "F"
+++

In the preview chapter the protag owns a dungeon, and the first thing he does is capture a woman trying to find her fiance in a forest, drugs her, cuffs her to a bed, rapes her, then brainwashes her into being his slave. That's what it chose to preview the series on, and as a result you can reasonably expect that is all that this series has to offer. And if you try to read on even further it just becomes even more trash and tries to justify the protag's actions by pretending he's Saving her, since she would've gotten raped by the evil villagemen instead, and it's so much better that he raped her instead.

<!--more-->

There's far better dungeon management works out there. Please read those instead. I suppose if you want to read about some deplorable dude who only exists as a vehicle for the author to write whatever rape fantasies he wants then go ahead.
