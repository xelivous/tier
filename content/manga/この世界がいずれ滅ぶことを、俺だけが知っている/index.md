+++
title = "この世界がいずれ滅ぶことを、俺だけが知っている"
title_en = ""
categories = ["reincarnation", "game", "reverse-isekai"]
demographics = []
statuses = []
furigana = []
sources = ["web novel"]
languages = ["japanese"]
schedules = ["weekly"]
lastmod = 2023-10-07
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/8rszvsg"
raw = "https://pocket.shonenmagazine.com/episode/316112896826245581"
md = "https://mangadex.org/title/0aa2520a-a18f-406d-8908-32cec22b88ac/kono-sekai-ga-izure-horobu-koto-wo-ore-dake-ga-shitte-iru"
bw = "https://bookwalker.jp/series/387782"

[chapters]
released = 48
read = 0

[lists]
recommend = ""
+++

Protag dies from a random minotaur in the middle of the city somehow (unexplained) and then wakes up at his job where he gets scolded by his hot boss for sleeping on the job and assume he just had a nightmare. He then experiences deja vu on his way home from work and meets the minotaur again, pretty much dies almost immediately again like his dream, and a status window pops up giving him a quest to defeat the minotaur. However once again he returns back to waking up at his job to get scolded by his boss.

<!--more-->

