+++
title = "歴史に残る悪女になるぞ"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-10-06
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166672"
raw = "https://comic-walker.com/contents/detail/KDCW_EB03201656010000_68/"
md = "https://mangadex.org/title/bc20fa98-b707-43cc-b195-bcee8e518ed9/rekishi-ni-nokoru-akujo-ni-naru-zo"
bw = "https://bookwalker.jp/series/281761"

[chapters]
released = 21.4
read = 12

[lists]
recommend = "B"
+++

Protagonist is a noble's daughter and remembers her past life, where she played a certain otomege and just really loved villainesses and wanted to become a villainess; luckily for her she reincarnated as the villainess of her favorite otomege! Thus she sets out to become the very best villainess there ever was, despite being a normal japanese person on the inside and can't help being like a wholesome chungus instead. However the main difference is that unlike the heroine who is naive and idealistic the villainess is a realist and more practical which is why the protagonist liked the villainess in the first place, and why it's okay that both of the characters are wholesome chungus on the inside.

<!--more-->

This series is good. Realist girlboss villainess dabs on the idealistic naive heroine who thinks hopes and dreams can solve poverty and inequality. Later on she even starts wearing an eyepatch which is unique enough on its own in this subgenre. Ultimately just a fun series where you turn your brain off and enjoy the protag constantly poking holes in the brainlet-tier ideas the heroine has to Save The World.
