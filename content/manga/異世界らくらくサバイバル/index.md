+++
title = "異世界らくらくサバイバル"
title_en = ""
categories = []
demographics = []
statuses = []
furigana = ["partial"]
sources = []
languages = []
schedules = []
lastmod = 2024-12-21
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/5rx3rb1/isekai-rakuraku-survival-seizon-skill-kyousha-no-ore-ga-bishoujo-yonin-to-class-mujintou-seikatsu"
raw = "https://yanmaga.jp/comics/異世界らくらくサバイバル生存スキル強者の俺が美少女四人と暮らす無人島生活"
md = "https://mangadex.org/title/9b12ac90-9407-4fe8-9de8-c8f4fe65df6f/isekai-rakuraku-survival-seizon-skill-kyousha-no-ore-ga-bishoujo-yonin-to-class-mujintou-seikatsu"
bw = "https://bookwalker.jp/series/426970/"

[chapters]
released = 49.2
read = 0

[lists]
recommend = ""
+++



<!--more-->

