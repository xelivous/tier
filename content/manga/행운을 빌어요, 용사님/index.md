+++
title = "행운을 빌어요, 용사님"
title_en = "Good Luck, Hero!"
categories = ["isekai", "reincarnation"]
demographics = []
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/o1km338/good-luck-hero"
raw = "https://comic.naver.com/webtoon/list?titleId=775334"
md = "https://mangadex.org/title/4770ee67-cceb-4f16-b1a3-8262937118c5/good-luck-hero"
wt = "https://www.webtoons.com/en/fantasy/good-luck-hero/list?title_no=3346"

[chapters]
released = 64
read = 11

[lists]
recommend = "D"
+++

Protag defeats the demon lord in their previous life, then asks to retain their memories when they reincarnate as their wish to the angel. Except the memories of a grand adventure in an isekai where you defeat a demon lord isn't particularly useful in a modern earth world. Also they're not the only person to reincarnate with their memories.

<!--more-->

It's basically just Yet Another Korean Romance with some weird roleplay mixed in to spice up the relationship.
