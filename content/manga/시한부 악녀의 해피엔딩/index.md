+++
title = "시한부 악녀의 해피엔딩"
title_en = "The Villainess's Days are Numbered!"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=174607"
raw = "https://page.kakao.com/home?seriesId=56095545"
md = "https://mangadex.org/title/ad098069-94ba-4c1f-8954-69f832234aab/the-villainess-days-are-numbered"

[chapters]
released = 108
read = 23

[lists]
recommend = "D"
+++

Okay the health being represented by an actual health bar is extremely meme, even if it gets contextualized somewhat a little bit later to be somewhat understandable. It's a decent work about a romance when someone clearly doesn't have that much time left to live, and the thoughts that come about from trying to make the most of your time left and not leaving any regrets.

<!--more-->

The only fan translation available is kind of turbo-ass after the first few chapters though so good luck and either read the official translation or cry.

I'm starting to kind of dislike this series around ch24. I don't even really want to read further since the protag is just lying out her ass constantly for almost no reason. "No the fact that I was poisoned doesn't concern you despite me being you wife. No me being literally blind right now doesn't concern you" like please.
