+++
title = "私はご都合主義な解決担当の王女である"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158658"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00201307010000_68/"
md = "https://mangadex.org/title/cb945ce8-b7a8-4e84-bd06-bbe0d2b170f9/i-m-an-opportunistic-princess-in-charge-of-solving-things"
bw = "https://bookwalker.jp/series/235281"

[chapters]
released = 27.3
read = 10

[lists]
recommend = "B"
+++

Fairly entertaining manga about a girl that gets reincarnated into one of her favorite BL series and is absolutely surrounded by gay men who want nothing to do with women except for the rare man who does actually like women. How are children made with all of the gay/gay pairings going around? Well while there's one faction that is only pure gay and adopts children, there's one that has a main gay coupling and then a side wife solely to bear children, and then the rare man/woman pairing. Truly a based series.

<!--more-->

There's enough sideplots/happenings going on beyond just the Everybody Is Gay setting that the work is based on that makes it entertaining enough as well.