+++
title = "異世界を制御魔法で切り開け！"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2022-01-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=133671"
raw = "https://www.alphapolis.co.jp/manga/viewOpening/255000138"
md = "https://mangadex.org/title/ea3d37dc-e694-4535-a7cb-dfb43c71a897/isekai-wo-seigyo-mahou-de-kirihirake"
bw = "https://bookwalker.jp/series/101305"

[chapters]
released = 36
read = 36

[lists]
recommend = "D"
+++

Protag has knowledge of a past life where he was an epic programmer who coded up a storm, and reincarnates into a world where there's a programming-like magic system. However since his max MP was so low and he had the accursed black hair (despite both of his parents having not-black) he got basically exiled to a random cabin along with a cute animalgirl. However thanks to the epic programming dream he can now use his unconvential programming magic to become the very best there ever was in this world.

<!--more-->

The work constantly flashbacks to when they're children while they're in the middle of fighting for their lives which kind of ruins the pacing. The work also timeskips a lot. The manga adaptation becomes utterly butchered near the end where the last 10 chapters or so are just rushed nonsense. I enjoyed the beginning chapters to an extent but over time it just unfortunately became worse and worse until it got seemingly axed. Maybe the WN/LN is better?