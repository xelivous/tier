+++
title = "로판 빙의 만화"
title_en = "How to Survive a Romance Fantasy"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["axed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-12-25
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/i4dy14x/how-to-survive-a-romance-fantasy"
raw = "https://series.naver.com/comic/detail.series?productNo=6005849"
md = "https://mangadex.org/title/9e628476-79fe-494a-9de9-9abb1ce3eae0/how-to-survive-a-romance-fantasy"
wt = "https://www.webtoons.com/en/fantasy/how-to-survive-a-romance-fantasy/list?title_no=2855"

[chapters]
released = 61
read = 61

[lists]
recommend = "D"
+++

Three protagonists get reincarnated into the world of a romcom/fantasy isekai villainess novel, with one being the male lead, the other being the main heroine, and the other being the evil villainess. However none of them really want to deal with the plot of the novel and just kind of want to chill, especially when two of them died in their previous life just before entering the world, so they immediately skedaddle away from their responsibilities and all end up at the same house in another country by coincidence.

<!--more-->

This is primarily a comedy, but I don't really find any part of it to be funny. Drama/character-wise it's not particularly great either. Ultimately I really hated reading this, but a lot of women consider it to be a hilarious work for some reason so quite honestly it's possible I just don't get the humor and i'm not the target demographic. I don't think I would be ablef to recommend it to anybody since I completely bounced off of it and can't see any merit to it outside of the humor. There isn't really an ending to it either, nothing ever happened and everything is just kind of left up into the air as if it was axed since the author likely just ran out of ideas and didn't want to continue it?

There's a decent amount of work that was put into the backgrounds/etc, but the overall panelling of the work is atrociously bad with like 70% of each chapter being white space separating small rectangular panels of content that it almost feels reminiscent of the korean equivalent of 4koma. The addition of small subtext under various panels reinforces this feeling and ultimately cheapens the work to feeling like bad twitter comments on various art trying to explain the obvious events in the panel you just saw. Which I guess makes sense considering this work originated from a draft on twitter.
