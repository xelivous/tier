+++
title = "悪役令嬢転生おじさん"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=162983"
raw = "https://sp.handycomic.jp/product/index/title_id/20034254/"
md = "https://mangadex.org/title/61b0d21c-b703-4999-910d-ebffc44791bb/akuyaku-reijou-tensei-oji-san"
bw = "https://bookwalker.jp/series/272896"

[chapters]
released = 35
read = 15

[lists]
recommend = "B"
+++

Protag jumps out in front of a truck to save a kid, gets truck'd, and reincarnates into the villainess in an otomege. The protag is a hardcore oldschool otaku who raised his daughter up to be similar to him, and he occasionally caught glimpses of her playing this otomege when he got home from work. He now needs to navigate noble society as a woman and as the villainess no less. Thankfully just about all of his actions get automatically translated into proper female noble etiquette so whenever he does something too weird internally it will still come across as something relatively sane.

<!--more-->

It's actually a fairly decent story where an old dude calmly tries to rationalize everything and give out life advice that comes from living a full life, that doesn't seem too out of place for a duke's daughter to say. It's kind of wholesome chungus cringe.
