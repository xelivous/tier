+++
title = "断頭台に消えた伝説の悪女、二度目の人生ではガリ勉地味眼鏡になって平穏を望む"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-30
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/sm6axyg"
raw = "https://comic.pixiv.net/works/8591"
md = "https://mangadex.org/title/185b9534-dc03-4eff-b40b-66346541d005/dantoudai-ni-kieta-densetsu-no-akujo-nidome-no-jinsei-de-wa-gariben-jimi-megane-ni-natte-heion-wo"
bw = "https://bookwalker.jp/series/407384"

[chapters]
released = 5.2
read = 0

[lists]
recommend = ""
+++



<!--more-->

