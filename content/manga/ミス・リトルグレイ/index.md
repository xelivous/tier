+++
title = "ミス・リトルグレイ"
title_en = ""
categories = []
demographics = ["shoujo"]
statuses = ["axed"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/3vupf21"
raw = "https://shonenjumpplus.com/episode/3269754496381675593"
md = "https://mangadex.org/title/325be3b3-ef27-4b19-b059-eb17ffd406bf/miss-little-gray"
#bw = ""

[chapters]
released = 35
read = 35

[lists]
recommend = "D"
+++

Protag has an ikemen bff but she's just a forever alone incel, and when her bff finally gets married she gets stabbed by a crazy stalker of the bff by mistake. However she wakes up in an isekai in a noble household! And apparently she was born into a house that regularly has people with their past life's memories as like a genetic freak mutation.

<!--more-->

This work is baffling at best. The first half of the work is just completely filled with memes and references to other works constantly. Like every 3rd panel is a meme of some kind, and the work at this point is largely a weird romcom with a focus on the com and basically no rom. Ch4 is especially bad and they literally have fucking harry potter houses. It randomly shoves in color panels occasionally which is kind of cool I guess? And then ch9 just flashbacks to the previous life out of nowhere for no reason fucking over the entire pacing. The male lead is insufferable.

And then the entire manga just completely shifts 720 degrees after a certain point and just waxes philosophical about sociopaths for a bunch of chapters until it eventually gets axed. The first half of this work and the second half of the work are like two entirely different manga to the extent that it's just a baffling whiplash at best. The plot is completely nonsensical and I have no idea what they were going for when writing this story.
