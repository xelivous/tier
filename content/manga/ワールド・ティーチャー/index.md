+++
title = "ワールド・ティーチャー"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-16
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=137811"
raw = "https://comic-gardo.com/episode/3269754496561211927"
md = "https://mangadex.org/title/20288/world-teacher-other-world-style-education-agent"
bw = "https://bookwalker.jp/series/108796/"

[chapters]
released = 68
read = 48

[lists]
recommend = "D"
+++

Protag is some epic spy dude who is in the middle of training some kids to succeed him when the mission goes to shit and he ends up dying, only to end up reincarnating instead. For some reason his parents aren't around and instead lives in a mansion with only a small amount of servants instead.

<!--more-->

Feels rather uninspired; just a low-conflict story where some Nobles think they're hot shit and beastpeople are slightly discriminated against. The pacing is awkward as well, and there's quite a lot of timeskips. The first year of magic school passes by almost immediately without much happening and you're kind of just blindsided by "oh yea they're in the 2nd year now lol". It also calls back to various points in time that it skipped over "oh yea i met this elf long ago when i was younger" but it's like never mentioned previously except another flashback where it's alluded to out of nowhere. There's so many flashbacks to "oh yea this totally happened at some point in the past, totally". From other comments it seems that it just adapts the webnovel/ln super badly and skips over a lot of the content which is why you get this weird timeskipping adaptation.

The art is kind of weird at times and the faces are regularly off model although at least the artist properly draws breasts that don't have support I guess?
