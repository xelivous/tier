+++
title = "남주가 흑역사를 책임지라고 합니다"
title_en = "Hiding the Archduke's Humiliating History"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2025-01-28
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/urv6uwl/hiding-the-archduke-s-humiliating-history"
raw = "https://page.kakao.com/content/61715316"
md = "https://mangadex.org/title/862b0c32-0f12-46d9-ae7d-649c86cf6349/hiding-the-archduke-s-humiliating-history"
#bw = ""

[chapters]
released = 90
read = 30

[lists]
recommend = "F"
+++

Protag reincarnates as a girl in a noble family, but the family immediately becomes destitute from poor investing choices and being betrayed by their close friends. She doesn't remember which novel/work she was reincarnated into but she does know that she was at least, and she also retains a majority of her memories from her previous life.

<!--more-->

This is easily the worst first chapter in isekai. The protag is actually insufferable and constantly spews out words without actually saying anything of value while talking in riddles dreamed up by toddlers. It doesn't tell you anything about either of the characters involved and goes on for far longer than anybody should be subjected to. The author is clearly trying to write an "intelligent" mysterious conversation but is utterly failing at it likely because they're not smart enough to even understand most conversations to begin with. ChatGPT could make more engaging and intelligent nonsense than whatever I just had the misfortune of reading.

Who the fuck stores gunpowder in a duke's office bro, the author is trippin'. And then they continue to put down the esteemed duke's grandchild solely to prop up how epic and beautiful and cool the protagonist is by comparison. Cringing hard rn fr. Her scheme to make money is also extremely dumb why am I still reading this; yes sure just buy all of the grain ahead of time, where the fuck are you going to store it bro. Yea the more this goes along the more painful it is to read the author trying to write characters with even the bare minimum amount of intelligence.

The work doesn't improve very much past the first chapter so I can't really recommend this at all. There's far better works out there about a female protag in noble society that this has virtually no worth. Unless you specifically want to see a white-hair red-eye protag I guess? There's probably a better one out there for that regardless.
