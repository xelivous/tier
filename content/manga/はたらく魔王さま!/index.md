+++
title = "はたらく魔王さま!"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["light-novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=72199"
raw = "https://comic-walker.com/contents/detail/KDCW_AM05000008010000_68/"
md = "https://mangadex.org/title/6241/hataraku-maou-sama"
bw = "https://bookwalker.jp/series/4278/"

[chapters]
released = 112
read = 103

[lists]
recommend = "D"
+++


On one hand the story is about Devils coming to learn about the struggles of human society, and on another the story is a relatively standard shounen about how the cliche battle between devils/angels is contrived and that a god that dictates to slaughter a race arbitrarily is no god, and is no better than a human trying to cover up their injustices on the side by painting a target on someone else to distract the masses. That all is fine, and a decent enough premise for a work, but unfortunately the series is marred with banal non-commital "love comedy" present in every chapter while the villain of the week just pops up of the woodwork to get beat over the head with a sudden powerup, followed by sudden immediate loss in powerup so that they can repeat this same setup over and over again, barely changing.

<!--more-->

Crestia Bell is extremely moe and is the only thing keeping me reading this series at this point; if she ever leaves the series i'm also leaving.

Seriously just kill the old dude who just keeps instigating everything and then live the happy home life with more cute Crestia chapters.

{{% spoiler %}}lmao at the scifi angle near the ch100 mark{{% /spoiler %}}

The LN has apparently ended already and the manga seems to be somewhat wrapping up a bit so it looks like they might actually get around to do a full complete adaptation which is cool. I'm more surprised about the random anime s2 that's airing long after the end of the LN.