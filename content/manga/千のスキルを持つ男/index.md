+++
title = "千のスキルを持つ男"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-26
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152778"
raw = "https://polca.123hon.com/book_series/1000skills/"
md = "https://mangadex.org/title/61e5cfcb-aff4-4916-ac20-132f4542e451/sen-no-sukiru-o-motsu-otoko"
bw = "https://bookwalker.jp/series/211390"

[chapters]
released = 59
read = 59

[lists]
recommend = "D"
+++

Protag marries some girl and after 3 months she already is ghosting him and is likely cheating on him, so he escape out to the mountains to go hiking and free his mind. However while hiking he all of a sudden gets transported to some random dungeon and upon opening a door he's in another world, comes across a cute girl about to get devoured by a spider, pulls a pickaxe out from thin air, and just absolutely destroys it. He then repeatedly gets summoned back into that world, slowly amassing power since he gets a skill every time he does so.

<!--more-->

The protagonist can regularly go back and forth between his world and the isekai, and uses that functionality to bring all kinds of gadgets and knowledge to the isekai as a summon. It's a fairly realistic work, but because it's so realistic it's kind of boring since everything you think would be done is obviously done exactly when you're likely to expect it. It is fairly nice to have it mostly be just two otaku bros shootin' the shit in another world together slowly trying to improve their lives back in japan and elsewhere.

The artist also uses 3d models for all of the art including the characters so the faces can look a little jank at times, at least towards the beginning of the work; the longer the work goes on the less 3D seems to be used. The girl that gets introduced in the ch20s is like the most mellycore mellygirl to exist. That girl then has an extremely long arc dedicated to her later on so I could rec this if you want to read about a mellygirl.
