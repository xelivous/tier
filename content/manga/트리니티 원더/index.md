+++
title = "트리니티 원더"
title_en = "Trinity Wonder"
categories = ["reverse-isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=132239"
raw = "http://webtoon.daum.net/webtoon/view/TrinityWonder"
md = "https://mangadex.org/title/e1fe1f35-1144-4aac-910f-9e2ce92dccf4/trinity-wonder"

[chapters]
released = 102
read = 8

[lists]
recommend = "F"
+++

Only read up to ch8, it has 102 chapters out, maybe i'll come back and actually read it but i'm getting brain damage already. The art looks pretty good and high effort at least when it's not hypersexualizing the women but the story is extremely brainlet tier action with epic scifi nonsense that has little meaning since the prologue just continuously drags on without ever setting up the real plot or any kind of actual intrigue. You're just strung along with some random events that have very little connection to each other. I don't see this ever going above a D tier if i ever finish reading it unless it somehow becomes the most kino thing in the next 90 chapters but i'll never find out unless someone else wants to read  it for some reason.

<!--more-->
