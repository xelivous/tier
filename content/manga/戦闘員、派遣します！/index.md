+++
title = "戦闘員、派遣します！"
title_en = ""
categories = []
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-07-19
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148607"
raw = "https://comic-walker.com/contents/detail/KDCW_MF02200275010000_68"
md = "https://mangadex.org/title/24288/sentouin-hakenshimasu"
bw = "https://bookwalker.jp/series/170330/"

[chapters]
released = 58
read = 32

[lists]
recommend = "B"
+++


<!--more-->
