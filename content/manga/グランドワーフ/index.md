+++
title = "グランドワーフ"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["original"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2023-09-29
tags = [
    "dies-from-old-age",
    "male-protagonist",
    "status-windows",
    "reincarnated-as-teenager",
    "comically-evil-antagonist",
    "underdog-hero",
]

[links]
mu = "https://www.mangaupdates.com/series/pgk4yc8"
raw = "https://tonarinoyj.jp/episode/4855956445066295876"
md = "https://mangadex.org/title/a2e62666-3eac-4ea3-b0a2-1db36026103d/grand-dwarf"
bw = "https://bookwalker.jp/series/415634"

[chapters]
released = 16
read = 15

[lists]
recommend = "D"
+++

Protag is a dude who basically did nothing but work at a factory slowly honing his craft until he ends up old and alone with no money to his name, and eventually dies from neglecting his body for too long. He then wakes up much younger with his entire workshop/tools/etc, in an isekai with magic and magical materials/etc. He comes across a cleric with a missing hand and burn marks and her own sobstory, fixes up her prosthetic using Superior Japanese Manufacturing Skills, and makes a gun to let her be the hero of this world.

<!--more-->

This is painfully shounen; despite being "seinen" and lacking furigana. It's a very tiny and brief window into the world of metalworking that skips over a majority of the metalworking and just ends up with the protag making anything and everything out of magical materials that do weird things. It's like a promotion for kids to start picking up a trade and how they too can create cool stuff with epic machining skills, but it's somehow a seinen. The antagonists are either extremely evil demons, or just comically evil assholes who have no qualms about killing anything in sight if it furthers their agenda; there's no nuance in the story at all.
