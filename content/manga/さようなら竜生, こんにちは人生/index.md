+++
title = "さようなら竜生, こんにちは人生"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-09
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=129695"
raw = "https://www.alphapolis.co.jp/manga/official/333000127"
md = "https://mangadex.org/title/18991/sayounara-ryuusei-konnichiwa-jinsei"
bw = "https://bookwalker.jp/series/104425/"

[chapters]
released = 77
read = 74

[lists]
recommend = "B"
+++

Protag is an epic dragon who eventually dies and then get reincarnated as a human, and learns about the slow happy fulfilling life that a human leads. For some reason he retained most/all of his dragon powers though so he's never really in any danger, and he can still converse with people he knew in his past life in the Spirtual Realm etc.

<!--more-->

And since the protag has already learned about the joys of humanity, the manga instead quickly becomes some kind of weird battle shounen where the protag is fighting off against demons/etc, saving people from them and doing whatever else with the help of his lamia gf. The lamia has decent twinbraids but even that doesn't do much to help it. I think the biggest question is how do lamia get pregnant to begin with?

Protag also heads off to Magic School where dumb nobles try to belittle the protag since he's a filthy commoner!!

The art near the beginning of the story isn't particularly noteworthy but it greatly increases in quality as the story goes on, with sections of chapters that forego text entirely and tell story from the art alone which is nice.
