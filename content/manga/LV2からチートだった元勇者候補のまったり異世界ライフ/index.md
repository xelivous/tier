+++
title = "LV2からチートだった元勇者候補のまったり異世界ライフ"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-12-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152684"
raw = "https://comic-gardo.com/episode/10834108156661709946"
md = "https://mangadex.org/title/33797/lv2-kara-cheat-datta-moto-yuusha-kouho-no-mattari-isekai-life"
bw = "https://bookwalker.jp/series/211352/"

[chapters]
released = 40
read = 38

[lists]
recommend = "D"
+++

Protag is a merchant in a fantasy world who gets isekai'd into another fantasy world, but after they find out he has no special powers they just cast him aside and he ends up stuck in that isekai for Reasons and has to figure out how to live now. And despite his status upon reincarnating being trash, when he levels up after fighting a single slime his stats become super ultra broken and he gets every single skill as well!!! He then changes his appearance, heads back into town, escorts a loli foxgirl to the forest along with a harem of knights, and then the foxgirl loli loves him the end and becomes his wife. After he gets his foxgirl waifu it mostly becomes some kind of weird comedy slife of life, about the acceptance of other races and abolishing slavery. However the plot just kind of meanders without doing anything of real note, since obviously the protagonist is broken OP and literally nothing can stand in his way aside from his morals.

<!--more-->
