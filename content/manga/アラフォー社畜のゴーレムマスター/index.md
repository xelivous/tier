+++
title = "アラフォー社畜のゴーレムマスター"
title_en = ""
statuses = ["axed"]
demographics = ["seinen"]
furigana = ["none"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-02-23
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149557"
raw = "https://gaugau.futabanet.jp/list/work/5dce87c67765612448040000"
md = "https://mangadex.org/title/37481/arafoo-shachiku-no-golem-master"
bw = "https://bookwalker.jp/series/172341/"

[chapters]
released = 43
read = 17

[lists]
recommend = "D"
+++

Extremely ugly dude works himself to death in a shit japanese company and then gets reincarnated, and he has magical OP summoner/golem powers. First thing he does is summon a golem which removes soil from the ground which conveniently reveals a hidden dungeon/etc just below the ground that clearly nobody has ever known about before and has a super powerful epic thing in it that is perfect for him!! Then he comes across a wolfgirl and gives her his blood to save her because reasons and he becomes her master! The art is fairly bad and most of the time there isn't even a background for the characters; maybe 1 panel on the page has a background of any kind on it.

<!--more-->

It's not completely irredeemable trash but i'm not sure who I would really recommend it to.
