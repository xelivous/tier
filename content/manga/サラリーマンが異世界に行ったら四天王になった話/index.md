+++
title = "サラリーマンが異世界に行ったら四天王になった話"
title_en = ""
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
categories = ["isekai"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-04-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=160304"
raw = "https://comic-gardo.com/episode/10834108156731296296"
md = "https://mangadex.org/title/45945/salaryman-ga-isekai-ni-ittara-shitennou-ni-natta-hanashi"
bw = "https://bookwalker.jp/series/251534/"

[chapters]
released = 64
read = 29

[lists]
recommend = "D"
+++

Protag is an average salaryman working at a black company in japan when he dies in an accident and gets isekai'd by the Maou to be one of the Elite Four for his epic managerial skills. The protag believes he's nothing special and questions why he's reincarnated but the Maou just goes off on how epic and cool this salaryman is and he shouldn't listen to his superiors who clearly don't understand his worth.

<!--more-->

Pretty much everybody just praises the protag for absolutely basic shit. It's pandering for black company salarymen and not much more.
