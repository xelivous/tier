+++
title = "ご主人様とゆく異世界サバイバル！"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=167636"
raw = "http://comicride.jp/survival/"
md = "https://mangadex.org/title/83067b19-bdfb-4ba1-8178-7052e68b41bf/goshujin-sama-to-yuku-isekai-survival"
bw = "https://bookwalker.jp/series/263897"

[chapters]
released = 41.2
read = 24

[lists]
recommend = "D"
+++

So the protag is randomly dropped in a world one day, and his super magic isekai cheat power is that he's literally a minecraft character, with all of the meme minecraft "physics" in play along with some extra bonuses from meme mods. Think of pressing spacebar in your mind to jump and you jump really high; place down some blocks and then remove the lower ones to have blocks floating in the air; put down a 2x2 of water to have infinite water; the possibilities are endless! A large majority of the work is the protag interacting with the inhabitants of the world and them being shocked at the meme "game logic" of minecraft. Also the protag has sex with a hot elf like every 4 chapters.

<!--more-->

it's like reading someone's minecraft let's play fanfic with sex scenes. Does that sound interesting to you? Maybe it does, if so maybe you'd want to read this
