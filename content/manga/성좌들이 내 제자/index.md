+++
title = "성좌들이 내 제자"
title_en = "The Constellations Are My Disciples"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-03
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/4v7jbit/the-constellations-are-my-disciples"
raw = "https://series.naver.com/comic/detail.series?productNo=9463749"
md = "https://mangadex.org/title/33d43bb9-da74-43f7-b5a7-bb597c19406a/the-constellations-are-my-disciples"
wt = "https://www.webtoons.com/en/fantasy/the-constellations-are-my-disciples/list?title_no=5638"

[chapters]
released = 59
read = 28

[lists]
recommend = "D"
+++

Protagonist successfully prevents the end of the world but ends up dying in the process, only to wake up in the body of a random 17 year old 300 years into the future. He would normally be content to just live out his life in peace now that he's saved the world but instead he's given a system notification that the world-ending towers will appear in another 5 years (again) and that if he wants to get back all of his power he can find it at a certain location. He then reluctantly heads out to try and prepare for his grim fate yet again.

<!--more-->

Protagonist just absolutely dabs on everybody because he's a really cool epic badass that is so much cooler than everybody else. There is no tension or conflict in this work, it's just the protag steamrolling everybody.
