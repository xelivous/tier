+++
title = "孤児院テイマー"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156001"
raw = "https://firecross.jp/ebook/series/347"
md = "https://mangadex.org/title/05c2917a-e0a4-4c55-abac-8be0f8b213e4/kojiin-tamer"
bw = "https://bookwalker.jp/series/250142"

[chapters]
released = 38
read = 30

[lists]
recommend = "C"
+++

Protag dies of old age then gets to reincarnate into an isekai wtih cheat abilities with no strings attached because reasons, and the protag chooses to be a monster tamer. The work starts off where he's a baby with all of his memories, living in an orphange with good upkeep, among a bunch of other babies/kids who do not have their past lives' memories, slowly growing up with all of the kids in the orphanage and dabbing all over everything with his super powered magic abilities and previous knowledge while teaching the kids things.

<!--more-->

The work is kind of weird since the orphanage employees don't really question that the protag can just do anything, and they're mostly fine with him heading to dangerous areas constantly, despite everybody regularly panicking about the tiniest of monsters normally. It's relatively laidback nonbiri series where the protag slowly grows in age and level, and the monsters he kills/tame get progressively larger. There's a cute twinbraid(tribraid?) a little later on. The protag is still young after many years so i'm not sure if they'll ever actually become anything close to an adult in age unless there's a massive timeskip at some point.

Overall it's not offensively bad and if you want a very slow/relaxed series where the protag lives life with a large cast of orphans then it's probably okay. I don't personally like it all that much but I can see the appeal.
