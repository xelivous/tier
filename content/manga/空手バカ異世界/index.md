+++
title = "空手バカ異世界"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153863"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000170010000_68/"
md = "https://mangadex.org/title/250e6bcf-1f12-4cc7-83eb-5fdaf8111dcf/karate-baka-isekai"
bw = "https://bookwalker.jp/series/249854"

[chapters]
released = 27.1
read = 18.1

[lists]
recommend = "B"
+++

Karate dude rejects OP cheats as a matter of principle but he's already unhumanly OP naturally so it's kind of pointless. Normal action manga featuring the Power of Superior Martial Arts. Cute booba goddess and twinbraid elf princess tho.

<!--more-->
