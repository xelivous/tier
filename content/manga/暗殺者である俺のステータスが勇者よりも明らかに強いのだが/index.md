+++
title = "暗殺者である俺のステータスが勇者よりも明らかに強いのだが"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-21
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149362"
raw = "https://comic-gardo.com/episode/10834108156661710102"
md = "https://mangadex.org/title/28901/assassin-de-aru-ore-no-sutetasu-ga-yuusha-yori-mo-akiraka-ni-tsuyoi-nodaga"
bw = "https://bookwalker.jp/series/189259/"

[chapters]
released = 32.2
read = 28

[lists]
recommend = "C"
+++

Protag gets isekai'd along with all of his classmates to subjugate the demon lord and they're all given abilities based on what they did in their normal lives, and only one of them is the actual Hero. Not the protagonist though because he's the edgy assassin who immediately thinks everything is shifty and sus and immediately hides his presense so he can go incognito. And wouldn't you know it the royalty is actually sus and evil!

<!--more-->

The work focuses on meme skill fuckery nonsense near constantly while the protag tries to dig deep into the true meaning of the world along with his elf waifu who also has the byakugan to see into the depths of the world, while his angsty boyfriend goes out to possibly subjugate the demon lord who isn't actually all that evil because humans are the real evil fr fr.

ch15/16/17 is retarded. ch18 is a little Based but superficial and still kind of retarded.
