+++
title = "異世界で最強魔王の子供達10人のママになっちゃいました"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["original"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-10-13
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153791"
raw = "https://pocket.shonenmagazine.com/episode/10834108156652672518"
md = "https://mangadex.org/title/36503/i-became-the-mother-of-the-strongest-demon-lord-s-10-children-in-another-world"
bw = "https://bookwalker.jp/series/229419/"

[chapters]
released = 40
read = 40

[lists]
recommend = "C"
+++

The protagonist is your average JK who is close to her (single) mother and are totes besties, however her mom dies in a truck accident and ends up all alone. One year later she reminisces about the game her mom bought her and regrets never finding the time to complete it and dreams of becoming a great mother like her mom one day, when all of a sudden she gets summoned to an isekai (to become the mother of the demon lord's children). She got summoned for the sole purpose of birthing the demon lord's children through a semi-misunderstanding and is practically immediately forced down on a bed and told to spread her legs. However instead she just drinks a potion and immediately becomes pregnant instead, and almost immediately afterwards her belly swells up, then she pops out an egg somehow.

<!--more-->

The first few chapters are kind of...a thing... but it definitely gets a lot better as it goes along. Realdeal 'treasure your family' manga. Actually no this series is just wild throughout the entire thing. It's an extremely happy-go-lucky shoujo/shounen where everything is saved by the power of ~~friendship~~ family except it regularly goes back to ecchi preganancy jokes. The ending is a little lackluster but does finalize most of the plot threads it started so I can understand why it ended. This work seems to be largely the author writing down her thoughts about childbirth, as her child was born around the time this work started. 

Could rec if you want a weird ecchi shounen written by a female author who just recently went through childbirth making a weird isekai about childbirth and unwanted(?) teen pregnancy.
