+++
title = "悪の女王の軌跡"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = ["light-novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=138041"
raw = "https://www.alphapolis.co.jp/manga/official/561000155"
md = "https://mangadex.org/title/6577f3e3-82e3-457d-9ddc-ed55b1b4c558/aku-no-joou-no-kiseki"
#bw = ""

[chapters]
released = 16
read = 0

[lists]
recommend = ""
+++


<!--more-->
