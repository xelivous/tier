+++
title = "역대급 창기사의 회귀"
title_en = "Return of the Legendary Spear Knight"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=189528"
raw = "https://page.kakao.com/home?seriesId=57451201"
md = "https://mangadex.org/title/707a112b-55cb-41a5-afe1-601d3e52e9a8/return-of-the-legendary-spear-knight"

[chapters]
released = 80
read = 78

[lists]
recommend = "D"
+++

Protag gets betrayed by all of the people he helped make it to high positions because he was too powerful that they started to fear him, so he dies and gets se1nt back in time by the spear he kept by his side after finding it one day.

<!--more-->

These kinds of works would be better if the protag's weapon of choice wasn't seen as something trash in their universe and actually managed to rise to the top amongst people who are also good and also are striving to further develop the art of the weapon of choice. Unfortunately these works use the cheapest method instead and have to have the protagonist show the entire world how amazing this weapon is that is regularly used in wars but for some reason isn't used by like anybody of note.

Honestly there's too many comically evil dumbasses trying to kill the protag or whatever. There's a relatively cute twinbraid who regularly appears but she's also a villain.

The timeskips can make it slightly annoying the follow the story. It starts up tons of plotlines when he's younger, and then goes off on tons of tangents, skips some time, and then all of a sudden it's like 6 years later and things are still relatively unresolved.
