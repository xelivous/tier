+++
title = "ぼっち転生記"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["light-novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=157798"
raw = "https://gammaplus.takeshobo.co.jp/manga/bocchi/"
md = "https://mangadex.org/title/3587f6e7-a90d-4134-ba65-293ca9c6123e/bocchi-tenseiki"
bw = "https://bookwalker.jp/series/253725/"

[chapters]
released = 28.2
read = 11

[lists]
recommend = "F"
+++

Protag hates humans, gets reincarnated into a world a magic but he's incredibly incompetent at just about everything, and nobody likes him. His special power is that he can see spirits, but the spirits in this world are deemed to be evil existences and using them is a taboo, and nobody else can see the spirits. He comes across a slave girl getting abused by two dudes and almost raped, and then gets a massive powerup through the power of Bullshit by a spirit coming to help him out and he becomes superman and saves the day.

<!--more-->

tldr lonely dude isekais and gets slave GFs who wouldn't ever think of abandoning him while tons of ecchi/sex happens haha lol. also he's super broken OP and learns ultra tier spirit magic almost immediately haha lol.

It's honestly rather painful to read.
