+++
title = "魔剣師の魔剣による魔剣のためのハーレムライフ"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153730"
raw = "http://gammaplus.takeshobo.co.jp/manga/makenshi_harem/"
md = "https://mangadex.org/title/36197/makenshi-no-maken-niyoru-maken-no-tame-no-harem-life"
bw = "https://bookwalker.jp/series/229285/"

[chapters]
released = 25.2
read = 15

[lists]
recommend = "D"
+++

sociopathic protag booba. Only real value is the sex scenes although you're probably better off just reading actual hdoujins.

<!--more-->
