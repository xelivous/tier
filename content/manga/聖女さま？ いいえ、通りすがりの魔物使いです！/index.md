+++
title = "聖女さま？ いいえ、通りすがりの魔物使いです！"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=170027"
raw = "https://seiga.nicovideo.jp/comic/46001"
md = "https://mangadex.org/title/d4c40e73-251a-4bcb-a5a6-1edeec1e00e7/saint-no-just-a-passing-monster-tamer-the-completely-unparalleled-saint-travels-with-fluffies"
bw = "https://bookwalker.jp/series/260202/"

[chapters]
released = 20.1
read = 8

[lists]
recommend = "B"
+++

The premise is that a girl wants nothing more than to pet animals all day long but she's so fanatical about it, and she's so powerful, that most animals are afraid of her, so she keeps training and training to become even more OP and then chooses the ""worst"" job class of monster tamer in hopes she could tame some cute animals to pet. The protag is an extreme deviant who is excessively creepy when it comes to fluffing animals.

<!--more-->

There's tons of background(?) plots that happen while the protag is off daydreaming about fluffing animals that are large in scale and rather interesting so overall i'd say it's an enjoyable work as long as people don't get turned off by the protag.
