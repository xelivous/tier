+++
title = "사실은 내가 진짜였다"
title_en = "I Am the Real One"
categories = ["reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172423"
raw = "https://page.kakao.com/home?seriesId=55721630"
md = "https://mangadex.org/title/48d231af-d0cd-4f1e-a1e2-9d1a7aa91a6a/i-am-the-real-one"

[chapters]
released = 85
read = 44

[lists]
recommend = "B"
+++

Protagonist is a sheltered girl with daddy issues who gets her whole life flipturned upside down by a massive bitch, and since the protag is so mentally and emotionally stunted due to her lack of any real parenting she eventually gets the death penalty due to being ousted as a "fake daughter from adultery" because there's a prophecy that only 1 girl heir should have the powers of the spirits, etc. The work does an incredibly good job of making you hate the antagonist; Usually a lot of these works delve a little into noble politics and talk about killing siblings or whatever, but this is the first time i've actually thought "wow it would be better if she just got choked out right now frfr". 

<!--more-->

After the protag dies and regresses back to a little before the homewrecker comes into her life she starts to realize the mistakes of her first life and try to actually communicate with the people around her, which leads to her actually start to develop emotionally enough to prepare for the incoming shitstorm of the antagonist, who will inevitably come through and try to wreck anything the protag is doing, no matter how cute, precious, and fun to read it is. There's actually some depth into the reasons why she regressed, why the outcomes came about in the first timeline, etc, even if a little contrived. It's a well done cute work with an insufferable bitch in it.

Read to ch44. only available translation after that point is absolute shit tier aside from the paid/official translation. Once again tempted to learn korean.
