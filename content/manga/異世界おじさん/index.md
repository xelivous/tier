+++
title = "異世界おじさん"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151721"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000079010000_68/"
md = "https://mangadex.org/title/31488/isekai-ojisan"
bw = "https://bookwalker.jp/series/181865/"

[chapters]
released = 35
read = 34

[lists]
recommend = "S"
+++


<!--more-->
