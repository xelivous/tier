+++
title = "箱庭王国の創造主サマ"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=130595"
#raw = ""
md = "https://mangadex.org/title/e081b8a0-5e9d-44cb-af79-75f87b7ceeb4/hakoniwa-oukoku-no-craft-master"
bw = "https://bookwalker.jp/series/81891"

[chapters]
released = 33.5
read = 19

[lists]
recommend = "C"
+++

tfw you create your nekomimi harem kingdom and isekai into it but have to spend most of your time fighting off other random kingdoms of other people that also isekai'd

<!--more-->
