+++
title = "悪役令嬢は、庶民に嫁ぎたい！！"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["axed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-15
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153065"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00200243010000_68/"
md = "https://mangadex.org/title/787c0327-c98e-4b18-a3f8-d2988904fdb1/the-villainess-wants-to-marry-a-commoner"
bw = "https://bookwalker.jp/series/168434"

[chapters]
released = 16
read = 16

[lists]
recommend = "A"
+++

Protag plays an otomege soshage every day since she's infatuated with an NPC/background character who is on the gacha screen and thus spends literally all of her money on the gacha since her husbando congratulates her every time she pulls. However one day when they release official merch for the game and immediately rushes out only to miss the obvious point that the merch is only for the main love interests (not her husbando), she becomes dejected, falls down stairs, and dies. She then reincarnates into the villainess in the soshage she always played.

<!--more-->

It starts out as a cute romance between the cute protag and the cute hero, and then evolves ever further into skill fuckery nonsense that abuses the systems in dank ways. It also has a good sense of humor. The unfortunate part is that the series just kind of ends out of nowhere with the most lackluster ending from getting seemingly axed which is super unfortunate since I was actually pretty invested in the series at that point; I'd rate it higher if not for it getting axed honestly. Actually kind of tempted to read the LN now but I feel like the content might be unbearable in text form? 