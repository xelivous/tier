+++
title = "천마는 평범하게 살 수 없다"
title_en = "Descended from Divinity"
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-12-22
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/gjguta9/descended-from-divinity"
raw = "https://comic.naver.com/webtoon/list?titleId=774358"
#md = ""
wt = "https://www.webtoons.com/en/fantasy/descended-from-divinity/list?title_no=3450"

[chapters]
released = 131
read = 124

[lists]
recommend = "D"
+++

Supremo demon sect murim dude dies of old age and for some reason reincarnates into the body of a useless dimwit of a noble that killed himself due to the situation he found himself in, and as a result this badass martial artist needs to fix this broken life he's put into with all of the knowledge he had in his previous life. Because he was like an 80000 year old epic martial badass who reached the peak of martial arts nothing phases him, he can do all techniques, and takes no bullshit from anybody so he immediately sets out to slay some dudes. He succeeds of course since he's just that cool despite being in a body that clearly can't handle his martial arts.

<!--more-->

There's very little conflict, the protagonist is always correct, everybody loves and worships him because he's the coolest chad of all time, if someone doesn't join the protag they are complete idiots and if they do join the protag they are the smartest people ever because clearly joining the protag is what you should do at all times. This work has tons of political conversations where the solution to every problem is the protag beating the fuck out of the opposition with his overwhelming strength. He is the one man army, and everybody else should see his point of view lest they be destroyed. That's pretty much the entire work.
