+++
title = "ハズレポーションが醤油だったので料理することにしました"
title_en = ""
categories = ["isekai"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156685"
raw = "https://gaugau.futabanet.jp/list/work/5dce92a87765614c0a010000"
md = "https://mangadex.org/title/1fb6e5d7-35b8-4741-9fca-2426f6c37e31/hazure-potion-ga-shouyu-datta-no-de-ryouri-suru-koto-ni-shimashita"
bw = "https://bookwalker.jp/series/200754/"

[chapters]
released = 41
read = 10

[lists]
recommend = "D"
+++

Protag marries a dude who wants her to be a housewife, but they never have kids or sex or anything, then he has an affair with a coworker and tries to divorce her; tldr her husband is a massive asshole. So she heads out to find a job but instead gets whisked away to an isekai while entering hellowork???

<!--more-->

Also since she's been staying at home all day and doing nothing her status is absolutely shit, and she meets a super S ranked adventurer ikemen who helps her out and is also like 3x taller than her. Since she's basically mistaken as a kid and she's too passive to correct anybody she ends up basically working with children at a daycare beating up slimes to get a small chance of a potion to drop. However not every potion is a good potion since apparently tons of other random color potions without uses come out of the monsters. Except the failure potions are actually cooking ingredients that will revolutionize the world as we know it!!

Protag dabs on isekai people with superior japanese cuisine with excessively broken stat bonuses from the dishes. What were they eating before the superior nippon food? Stale bread. What savages they were, unlike the common japanese person who enjoys rice and soy sauce.
