+++
title = "勇者に全部奪われた俺は勇者の母親とパーティを組みました"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-30
tags = [
    "male-protagonist",
    "basically-porn",
    "nudity",
    "older-heroine",
    "harem",
    "engages-in-slavery",
    "adventurer-guild",
    "letter-rank-adventurer-guild",
]

[links]
mu = "https://www.mangaupdates.com/series/ibg136z"
raw = "https://comic-walker.com/contents/detail/KDCW_CW01203595010000_68/"
md = "https://mangadex.org/title/9e107c4a-ad46-4fcf-9ca1-98f21c6ec2a3/yuusha-ni-zenbu-ubawareta-ore-wa-yuusha-no-hahaoya-to-party-wo-kumimashita"
bw = "https://bookwalker.jp/series/416003"

[chapters]
released = 9
read = 9

[lists]
recommend = "D"
+++

Protag gets reincarnated into an isekai and ends up joining the hero's party, but eventually gets kicked out because the hero is a brainlet ntr male lead who exists solely to have sex with every young woman imaginable and the party was basically just the hero, the protag, and the hero's harem. However in this world women are worthless after they turn like 18 because (meme reasons), and because the protagonist is like internally 60 years old he can't get interested in any of the women in this world he ends up only being attracted to 30+ year old women who are already thrown away by everybody. 

<!--more-->

Long story short he ends up getting tons of cheap milf lovers (some of them slaves) nobody wants and fucks them nonstop while the hero party he got kicked out of falls into ruin without the epic competent protag keeping them afloat. It's kind of oneshota in terms of age gap but it's a normal guy and milfs instead, and basically nothing but the protag having sex with them, but it falls into a niche that if that's all you want to read this could be recommendable to you. However honestly just read h-manga instead probably?
