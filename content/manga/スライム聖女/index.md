+++
title = "スライム聖女"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["original"]
languages = ["japanese"]
schedules = ["every other week"]
lastmod = 2024-12-20
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/k2fbbvm"
raw = "https://tonarinoyj.jp/episode/14079602755535964377"
md = "https://mangadex.org/title/6b64ee46-605b-47ed-a208-4a727bf66b25/slime-seijo"
bw = "https://bookwalker.jp/series/473208"

[chapters]
released = 25
read = 24

[lists]
recommend = "D"
+++

A small lonely slime lives in fear of humans in the forest when one day a "human" approaches it and reveals that she is actually a slime just like it, and that it's possible for slimes to inhabit the corpses of humans that recently died to integrate into human society where they won't have to live in constant fear and will be able to get tons of tasty food as well. Conveniently a random girl seems to have been poisoned, pushed off of a cliff, and left for dead just nearby where the slime lives, which provides a great opportunity for the slime to enter human society. Except this was the body of a notorious villainous saint who abused her power, her servants, and everybody else to get whatever she wanted, making tons of enemies and was killed as a result. Now the slime needs to not only learn how to function in a foreign body, somehow prevent others from finding out she's a slime monster, but also mend her reputation to not be the absolute worst and also learn how to function as a saint on a day-to-day basis.

<!--more-->

Fairly nice healing/comedy series that has the same brainlet-tier energy as 300nen slime. The art is the main highlight since the story isn't particularly groundbreaking or novel unfortunately.
