+++
title = "初期スキルが便利すぎて異世界生活が楽しすぎる!"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2024-02-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172774"
raw = "https://www.alphapolis.co.jp/manga/official/469000311"
md = "https://mangadex.org/title/5127bc3e-6b87-4fa2-82eb-fd363cd4c10a/shoki-skill-ga-benri-sugite-isekai-seikatsu-ga-tanoshisugiru"
bw = "https://bookwalker.jp/series/261919"

[chapters]
released = 19
read = 15

[lists]
recommend = "C"
+++

Protag's life was filled with misfortune and he eventually dies with a truck because some random god got bored and toyed with his fate, so as an apology the protag gets to reincarnate into a world of fantasy/magic with all of his memories intact and also he can pick out whatever broken skills he wants. However because the appraisal system in this world is absolutely dumb he ends up getting expelled from his house and orphaned. However he regularly meets tons of really nice people who just wants to help him out and all of them are really powerful influential people so he never really struggles with anything. I feel like the main highlight of this work is that there's a cute male father figure in this work as well for the protag and their overall relationship is moe.

<!--more-->

The character designs in this story are pretty good though honestly; all of the girls are cute and all of the men are very varied in appearance. There's a really cute elf mage with cowtits and a cowbell too.

Overall could rec this to you if you want to read a cute story about a guy and his cute dad who gets along with all of the really nice friendly people around him in a wholesome work with basically no drama at all while skill spam happens left and right. ~~Except it's been on hiatus/axed since 2021 and didn't really end and will likely never have a continuation.~~ It stopped being on hiatus all of a sudden after like 2 years of hiatus idk man.
