+++
title = "魔王様、リトライ!"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-07-31
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146909"
raw = "https://futabanet.jp/list/monster/work/5dbfa19177656129f3010000"
md = "https://mangadex.org/title/ff2beee0-7ba8-4661-ad80-f5e85337b540/maou-sama-retry"
bw = "https://bookwalker.jp/series/157025"

[chapters]
released = 66 #25 + 41
read = 55 #25 + 30

[lists]
recommend = "A"
+++

Protag is a developer who created and was an admin of an MMO for 15 years, and regularly enjoyed playing as a "demon lord" character up until he finally decided to shut the servers down. However the next time he wakes up he's actually in the body of his favorite demon lord character, however while not all of the systems of the MMO are intact he at least has some of his admin powers to try and figure out how to survive in this new world as this demon lord character that he made.

<!--more-->

This is as comedy(?). Maybe? It has a very unique feel to it where I can't really place it as one particular genre, it's just "art". It introduces a ton of characters and tries to make everything be extremely chuuni while still being kind of wholesome. And if all of the characters in this world weren't enough, it semi-regularly flashes back to the protag's previous life and all of the characters in the real world as well which is pretty nice. I appreciate what they're slowly cooking up with all of the flashbacks.

I feel like you might be better off experiencing this work with the Anime or LN; the manga adaptation seems to zoom a little too much with the pacing being a little awkward. I've only experienced the Anime but I enjoyed it more for what was available of it; kind of tempted to read the LN as well. However apparently a S2 of the anime is finally releasing, shortly after I was compelled to re-read through this, so maybe it will do organ's arc justice?

The girls are cute though, and this is essentially a harem of sorts so there's a lot of cute panels of the girls. The names in this work sure are hilarious though fr.

This work continues in Maou-Sama Retry R (the "sequel") which is basically just a continuation of the original series, and I include both of them into this entry.
