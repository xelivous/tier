+++
title = "田中家、転生する。"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-10-02
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169216"
raw = "https://comic-walker.com/contents/detail/KDCW_AM01201802010000_68/"
md = "https://mangadex.org/title/6e9e4d13-1138-4631-9483-860787a9bbad/the-tanaka-family-reincarnates"
bw = "https://bookwalker.jp/series/306835/"

[chapters]
released = 34.2
read = 16

[lists]
recommend = "A"
+++


<!--more-->
