+++
title = "黒鉄のヴァルハリアン"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-21
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=184327"
#raw = ""
md = "https://mangadex.org/title/2759019e-5fb7-4f16-8632-a86ea1e4244c/kurogane-no-valhallian"
bw = "https://bookwalker.jp/series/322923/"

[chapters]
released = 55
read = 17

[lists]
recommend = "C"
+++

pretty standard 'historical figures battle it out' seinen manga; if you like the others you will probably like this one, although nothing particularly stands out for me in this one from what i've read so far.

<!--more-->
