+++
title = "史上最強の魔法剣士、Fランク冒険者に転生する"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = ["every other week"]
lastmod = 2024-02-25
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=157215"
raw = "http://seiga.nicovideo.jp/comic/43930"
md = "https://mangadex.org/title/41726/shijou-saikyou-no-mahou-kenshi-f-rank-boukensha-ni-tensei-suru"
bw = "https://bookwalker.jp/series/227957/"

[chapters]
released = 105
read = 47

[lists]
recommend = "F"
+++

Protagonist is a typical japanese black company stooge who falls into an isekai and gets so excited to learn magic that he becomes the foremost pinnacle of magic. He then gets reincarnated again into another isekai and tries out swordsmanship instead because it sounds fun, and also becomes the pinnacle of swordsmanship. He then gets disillusioned by humanity because he's such an epic powerful badass that other people can only see him as an enigmatic monster with power beyond their control despite helping them constantly so he remains isolated in both of his lives, until he eventually reincarnates yet again and tries to hide his power level this time.

<!--more-->

nooooo but he's f rankkkk how can he do these thingssss how is he so sugoiiiii. real deal F tier manga.