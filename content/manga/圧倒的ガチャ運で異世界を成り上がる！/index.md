+++
title = "圧倒的ガチャ運で異世界を成り上がる！"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-02
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=171546"
raw = "https://seiga.nicovideo.jp/comic/49316"
md = "https://mangadex.org/title/ac2088f9-624d-4e80-a244-95c1a33c90f5/attouteki-gacha-un-de-isekai-wo-nariagaru"
bw = "https://bookwalker.jp/defb0e5e9b-6633-44bb-9607-651f52ef37fe/"

[chapters]
released = 35
read = 35

[lists]
recommend = "D"
+++

Protag dies from overworking in a black company, and when told he is dead by the cute goddess he fiercly uooohs over the fact he no longer has to go to work anymore. He's able to go up to heaven, but instead he rejects that because he wants to get reincarnated into another world and build a harem of cute girls, gets rejected, then while trying to fondle the loli goddesses' breasts ends up getting isekai'd along with the goddess while she's struggling to get away from him. The protag also gets an utterly broken luck stat and a lot of functionality that only the goddess has, while the goddess is stripped of all of her powers.

<!--more-->

Whenever the protag needs the macguffin to solve a problem he goes pull a scam gacha that he wins 100% of the time because he has meme broken luck haha lol. It's a harem series with some ecchi and an extremely broken OP protag. The goddess is cute though fr. The character/clothing design is pretty good overall although the elf girl's outfit is extremely meme. The gacha clerk being a hot twinbraid also certain helps.

Also despite being an "reincarnation isekai" he doesn't actually reincarnate at all he keeps the same age/etc and just plops into the world so I haven't tagged this as reincarnation even though the protag ""dies"".
