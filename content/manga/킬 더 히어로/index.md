+++
title = "킬 더 히어로"
title_en = "Kill the Hero"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=164539"
raw = "https://page.kakao.com/home?seriesId=54527564"
md = "https://mangadex.org/title/6964942d-5920-4e79-bc45-1f798d62b2bd/kill-the-hero"

[chapters]
released = 125
read = 100

[lists]
recommend = "C"
+++

Somewhat enjoyable work where the protag goes back in time after getting betrayed and uses his OP future knowledge to do things right, obtain all of the OP items himself stealing them from who should've had them in the first place, with the eventual goal of getting his revenge and properly Saving The World. The weirdest part about this story is that the protag is using an alternate identity of a Russian dude for like 80% of it to hide his own identity and keep them separate, and to double dip on rewards simply to progress the story faster. 

<!--more-->
