+++
title = "고수, 후궁으로 깨어나다"
title_en = "A Martial Master Wakes Up as a Concubine"
categories = ["reincarnation"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-03
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/y64qopc/a-martial-master-wakes-up-as-a-concubine"
raw = "https://series.naver.com/comic/detail.series?productNo=9805698"
md = "https://mangadex.org/title/005b44a5-0da9-4cf4-80e4-b4ebc9a781e6/a-master-who-woke-up-as-a-concubine"
wt = "https://www.webtoons.com/en/fantasy/a-martial-master-wakes-up-as-a-concubine/list?title_no=5732"

[chapters]
released = 50
read = 21

[lists]
recommend = "C"
+++

The protagonist is one of the foremost martial arts masters of this time period when she gets poisoned by her lover(?) and eventually dies in a cave, only to wake up as a noble lady concubine in the royal palace that also got poisoned by the same poison. As a result she has to navigate the inner palace harem and all of its politics while being a demon sect martial art warrior that only knows how to solve things by brute force. She then eventually gets wrapped up in love quadrangles and tons of schenanigans just like any other korean romance/drama.

<!--more-->

The protagonist is a complete dumbass and this is ultimately a comedy of misunderstandings and wholesome chungus where the emperor slowly falls in love with an innocent brainlet.
