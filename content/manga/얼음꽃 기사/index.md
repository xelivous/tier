+++
title = "얼음꽃 기사"
title_en = "The Frost Flower Knight"
categories = ["reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-14
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=194470"
raw = "https://page.kakao.com/home?seriesId=58589333"
md = "https://mangadex.org/title/334e8f15-75e6-4638-9f12-536556561247/knight-of-the-frozen-flower"

[chapters]
released = 50
read = 40

[lists]
recommend = "F"
+++

lmao. So like the protag is "super duper ugly" and by ugly i mean she has slightly dark skin like her father and hentai protag face (no eyes) and is cursed to have somewhat bulky arms/physique since she's been training to be a swordmaster since that's apparently the only way to fix her "curse", except she never actually solved her curse in her first life. Then she magically reincarnates after being betrayed by the prince that lied to her for so many years, goes out and does random training on a mountain, and breaks free from her curse after doing the same training she always did. She now is "super beautiful" with "milky white" skin and long hair and big eyes and is "super cute" unlike the boorish appearance she used to have. And now everybody loves her since she's super beautiful.

<!--more-->
