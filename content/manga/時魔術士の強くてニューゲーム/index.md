+++
title = "時魔術士の強くてニューゲーム"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/no0yuvu"
raw = "https://comic-walker.com/contents/detail/KDCW_AM05203242010000_68"
md = "https://mangadex.org/title/1bd78711-2fe0-412d-8570-cea963c50a92/the-time-mage-s-strong-new-game-i-returned-to-the-past-to-rewrite-it-as-the-world-s-strongest"
bw = "https://bookwalker.jp/series/395122"

[chapters]
released = 13
read = 0

[lists]
recommend = ""
+++



<!--more-->

