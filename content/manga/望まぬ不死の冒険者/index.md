+++
title = "望まぬ不死の冒険者"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=145794"
raw = "https://comic-gardo.com/episode/10834108156661710518"
md = "https://mangadex.org/title/22775/nozomanu-fushi-no-boukensha"
bw = "https://bookwalker.jp/series/161597/"

[chapters]
released = 57
read = 54

[lists]
recommend = "B"
+++

Protagonist is an adventurer who never moves up from one of the bottom ranks of the guild even after 10 years since his upper limits is so low, but he's still a respected member of the guild since he looks out for all of the newbies and overall increases the likelihood they'll return safely. However one day when going through the dungeon as usual he comes across a completely unknown chamber in this well-explored newbie dungeon and comes across a super powerful dragon that immediately eats him and he dies. However he turns into a skeleton/monster and is able to retain all of his memories/personality so he sets out to try and evolve enough to integrate back into human society despite being a monster.

<!--more-->

This work has very good art to the extent that the artist/author will regularly put in pages without text for large spans of time simply to show off how capable they are which is cool, since every other section of the work is absolutely filled to the brim with endless text and exposition when it's not doing battle-shounen-tier fights. The work is primarily about the protag slowly building up his power that was basically stalled now that he has a new body, but a lot of the plot developments seem very weird. It's still relatively interesting to read about but you have to suspend your disbelief at least a few times and just enjoy the ride. Guild clerk has twinbraids even if they don't look like twinbraids from the front and only from the back which is kind of weird.
