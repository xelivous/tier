+++
title = "装備枠ゼロの最強剣士"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    "amnesia-protagonist",
    "male-protagonist",
    "lowest-rank-adventurer",
    "letter-rank-adventurers-guild",
    "comically-evil-antagonist",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158462"
raw = "https://www.ganganonline.com/contents/soubiwaku/"
md = "https://mangadex.org/title/01d483fe-cc41-4a35-ac15-e0a2d761f342/sobiwaku-zero-no-saikyou-kenshi-demo-noroi-no-soubi-kawaii-nara-9999-ko-tsuke-hodai"
bw = "https://bookwalker.jp/series/226590/"

[chapters]
released = 40
read = 34

[lists]
recommend = "D"
+++

Protag gets isekai'd into a bizarre world where everybody has a set amount of equipment slots, and he has 0, so he's considered to be trash and is basically bullied and slapped around by everybody. Except he doesn't have 0, he has 10000, when most people only have like 1-3, but he's been cursed because Reasons to only be able to equip cursed equipment, so he effectively has 0 until he can manage to find cursed equipment which typically has outrageously terrible effects like killing everybody indiscriminantly including it's wearer etc!! And tons of Super Helpful Coincendental events happen at the beginning of the work and instead of questioning it they just kind of roll with it and yolo because actually writing a compelling story is too hard!! The protagonist is also a complete pervert that gets off on the thought of touching equipment and wants to have sex with equipment!!

<!--more-->

The first chapter is terrible. In fact the first volume as a whole is pretty bad. However the beginning of vol2 is kind of funny and possibly worth the setup chapters if the writing was better. The protagonist basically having a hardcore fetish for equipment and having all of the humor revolve around idiotic things the doll or protagonist says is kind of annoying at best. The weirdest part about this work is that most of the cursed items are actually cute girls who transform between their original forms and their cute girl form.

Then there's volume3 which is very similar to the work the artist worked on before this, [異世界で最強の杖に転生した俺が嫌がる少女をムリヤリ魔法少女にPする](../異世界で最強の杖に転生した俺が嫌がる少女をムリヤリ魔法少女にpする/), minus all of the ecchi and mahoushoujo pandering, which honestly is 90% of that work, yet still gives me that feeling. Meanwhile Volume4 is basically persona5. Although volume3+4+5 are pretty decent overall if not simplistic. Then vol6 goes all in on idol culture.

I don't really know what to think about this work but I do feel like it's pretty hard to recommend in general.
