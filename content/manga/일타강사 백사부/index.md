+++
title = "일타강사 백사부"
title_en = "Best Teacher Baek"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-01-20
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/ed5yz0e/best-teacher-baek"
raw = "https://comic.naver.com/webtoon/list?titleId=779632&page=1&sort=DESC"
md = "https://mangadex.org/title/914c97e0-2623-4f06-9c0e-664393a15424/star-instructor-master-baek"
wt = "https://www.webtoons.com/en/action/best-teacher-baek/list?title_no=4153"

[chapters]
released = 99
read = 92

[lists]
recommend = "D"
+++

The protag is forced to learn all of the greatest techniques of some masters for the sole purpose of making a training manual out of them (but sabotages the things he writes to be imperfect) and then gets killed by an epic dude who is more powerful than all 5 of them combined. Then the protagonist miraculously gets reincarnated, but as some dude with a weak constitution out in the virtuous sect area. He then sets out on eventually getting revenge on the dude that killed him and forced him to learn the greatest techniques while occasionally giving out lessons to random people.

<!--more-->

This work is about teaching but it doesn't really have much to do with teaching at all. The protagonist occasionally says one-liners while his students randomly powerup and the protagonist just buttblasts some fools with his epic skills. There's better murim works imo.
