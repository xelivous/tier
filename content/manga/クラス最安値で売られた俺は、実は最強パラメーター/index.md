+++
title = "クラス最安値で売られた俺は、実は最強パラメーター"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-03-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=183656"
raw = "https://web-ace.jp/youngaceup/contents/1000168/"
md = "https://mangadex.org/title/9d4311a4-7ad0-4fce-bf80-6c19ba37fb47/class-saiyasune-de-urareta-ore-wa-jitsu-wa-saikyou-parameter"
bw = "https://bookwalker.jp/decc7c9d38-5856-4ed2-9e52-687e010fc959/?adpcnt=7qM_SasK"

[chapters]
released = 21.2
read = 13.2

[lists]
recommend = "C"
+++

Protag is planning on confessing to the hottest girl in the entire city that happens to be in his class with the help of his female childhood friend while they're on a their way to a fieldtrip, when all of a sudden their bus with all of their classmates gets isekai'd into a fantasy world with mecha. The native inhabitants summoned the bus to sell all of the riders to various other people as slaves as pilots for these mecha, since there's a higher chance that earthlings have higher compatibility with the mecha than the native inhabitants. All of the classmates then get their power level measured (lol) and the protag has basically the lowest possible value of 2!! Except we later find out it's not actually 2, but so high that it wrapped around their power level meter and did an integer overflow!! As a result he gets bought by an asshole for 2 nuts and forced to live locked in an abandoned house with small amounts of food along with the other pitiful slaves.

<!--more-->

One of the good parts about this work is that it actually has a bro in the main cast and isn't just a harem. Unfortunately the mecha action scenes are a little hard to make sense of and the politics and character motivations aren't particularly interesting. It feels fairly low-tier in terms of mecha series, but if you specifically want isekai-mecha there aren't too many options so it's probably a decent entry to read? You kind of have to turn your brain off though since a lot of the scenarios/plot points seem contrived at best.
