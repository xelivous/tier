+++
title = "転生しちゃったよ（いや、ごめん）"
title_en = ""
categories = ["isekai", "reincarnation"]
statuses = ["completed"]
demographics = ["shounen"]
furigana = ["none"]
sources = ["light novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-10-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=126023"
raw = "https://www.alphapolis.co.jp/manga/official/997000115"
md = "https://mangadex.org/title/18642/tensei-shichattayo-iya-gomen"
bw = "https://bookwalker.jp/series/88397/"

[chapters]
released = 89
read = 79

[lists]
recommend = "D"
+++

The protagonist is an ikemen japanese highschooler with no parents who thinks he's not good looking because when he was younger his mother told him his face reminded her of (his father) who she despised, and all of the girls his age are awkward around him because he's so ikemen that they cause him to misunderstand even further. However a flowerpot falls from a tall building and lands directly on his head, killing him, only for him to wake up in a strange expanse in a field of flowers. Then god does a flying dogeza right in front of the protag and apologizes for the protagonist dying due to a mistake, and offers to either reincarnate the protag or completely nuke him from existence, to which the protagonist obviously opts to reincarnate and asks simply to keep his memories and not have any extra special powers. However after reincarnating he starts getting into antics as a baby since he still retained all of his memories and after learning that magics exists in this world immediately starts to seek out how to do it, only to learn that the magic system in this world is literally just japanese kanji.

<!--more-->

I feel like the biggest problem with this work is that a 3/4 year old is just going around blasting fools with epic magic skills and solving college level math problems but nobody really gives a shit outside of thinking the protag is "impressive". The protagonist has already god-emperor power levels immediately out of the gate so the work has nowhere to go, nowhere to improve upon, the protagonist is just taking his victory lap while dabbing on everybody else. 

Even stranger is how his mother (in the isekai life) seems to just completely fade out of the story, and never wears anything other than a nightgown, never appears at any major events, etc. It's not like the protagonist hates women due to his previous mother basically neglected him the entire time growing up, and he had a relatively decent relationship with his mother early on, but the work just completely blows past all of that and writes her out of the story almost entirely.

I don't particularly enjoy the earlier arcs in this work, they feel kind of contrived and the resolutions to them aren't particularly satisfying; i don't know what messages they are trying to convey or really how they're setting up any future events for the protag. At best the end of the assassination arc is "my dad who obviously loves me does actually love me after all" which is a nothingburger. It starts to pick up a bit after that by delving into a few of the unexplained coincidences in this isekai world at least.

The cute twinbraid girl in the friend group is great though, but not enough to actually read this work. The noble kid is also pretty great since normally they would continue being stuck up and a semi-antagonist but he almost immediately turns around and becomes an almost best bro to the protag.