+++
title = "クロの戦記 異世界転移した僕が最強なのはベッドの上だけのようです"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=170024"
raw = "https://comic-walker.com/contents/detail/KDCW_KS13201759010000_68"
md = "https://mangadex.org/title/e6914956-e13b-42d4-aab6-6113daa352fa/kuro-no-senki-isekai-ten-i-shita-boku-ga-saikyou-na-no-wa-bed-no-ue-dake-no-you-desu"
#bw = ""

[chapters]
released = 11
read = 0

[lists]
recommend = ""
+++



<!--more-->

Series changes artist after chapter 11 and changes the serialization title to "2" and is still ongoing I guess.
