+++
title = "転生令嬢は冒険者を志す"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-24
tags = [
    "recall-past-life-as-child",
    "female-protagonist",
    "villainess-protagonist",
    "wholesome-villainess",
    "tamed-overpowered-creature",
    "purposefully-hiding-powerlevel",
    "problematic-age-gap-marriage-proposal",
    "over-protective-older-sibling",
    "dead-parent",
    "heir-to-great-house",
    "talks-with-animals",
    "letter-rank-adventurer-guild",
    "item-box",
    "arbitrary-magic-system",
    "multiple-reincarnator",
    "selfish-otomege-protagonist",
    "the-original-plots-fate-is-strong",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=156746"
raw = "http://seiga.nicovideo.jp/comic/40129"
md = "https://mangadex.org/title/e8b6d229-8feb-4994-ac77-9dd3af12aa6a/the-reincarnated-young-lady-aims-to-be-an-adventurer"
bw = "https://bookwalker.jp/series/235284"

[chapters]
released = 28
read = 14.5

[lists]
recommend = "A"
+++

Protagonist is a regular OL working at a company who never got married, and ends up reincarnating into a fantasy world as a noble girl, although she doesn't remember this fact until she comes across a tiger cub and suddenly remembers everything. This is actually the world of an otomege novel and she was in fact reincarnated as the villainess of that series. The villainess in this series is contracted to one of the 4 godbeasts, a white tiger that looks similar to the cub she came across in fact, and eventually sics the tiger on the heroine which spells her downfall in the future. However our protagonist is a goody twoshoes who doesn't want to meet a bad end and just wants to live in peace. She tries hiding her overwhelming magic powers and ends up with a result of having "no magic at all" in her plot to eventually just become an adventurer instead of having to go through the entire plot of this otome novel.

<!--more-->

It's a very simple and straightforward otome-isekai work that doesn't do anything particularly different than any of the others in this sub-genre but does it all well enough that it's easy to recommend to anybody. As long as you can get past the like 16 year old hitting on a 6 year old near the start which is extremely meme.
