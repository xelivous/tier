+++
title = "엔딩메이커"
title_en = "Ending Maker"
categories = ["isekai", "reincarnation", "game"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-14
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/o4y7mjk"
raw = "https://page.kakao.com/home?seriesId=57710234"
md = "https://mangadex.org/title/cb676e05-8e6e-4ec4-8ba0-d3cb4f033cfa/ending-maker"

[chapters]
released = 38
read = 19

[lists]
recommend = "D"
+++

it's basically a light romance series about 2 top-ranking mmo dorks trying to fight together to prevent the end of their game world. However it constantly references other (better) works (primarily japanese) in tons of panels as "jokes" where it is constantly like "did you get the reference??" which greatly cheapens a lot of scenes. It's overall pretty low effort in that regard. The girl protag is cute enough at least. Might've been a C tier if the girl protag wasn't an absolute brainlet and seems to be just going along with whatever the male protag does despite being literally the 2nd place ranker in the game.

<!--more-->
