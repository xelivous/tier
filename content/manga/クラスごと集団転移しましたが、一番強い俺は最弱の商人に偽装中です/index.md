+++
title = "クラスごと集団転移しましたが、一番強い俺は最弱の商人に偽装中です"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-03-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=171045"
raw = "https://seiga.nicovideo.jp/comic/39087"
md = "https://mangadex.org/title/2bd70bdf-8ccc-47e8-8436-9ee8d59aed7d/class-goto-shuudan-teni-shimashita-ga-ichiban-tsuyoi-ore-wa-saijaku-no-shounin-ni-gisouchuu"
bw = "https://bookwalker.jp/series/190807"

[chapters]
released = 42
read = 15

[lists]
recommend = "F"
+++

Dude keeps getting isekai'd, beats the demon lord 88888 times, and then eventually he isekais with his class but decides to hide his power level because he doesn't want the rest of his classmates to find out but ends up sealing most of his skills, but is still super ultra powerful anyways because why not. tldr he strolls around with his scantily clad harem while they crave for his dick the end. The art is kind of jank, and the character designs are kind of weird at best. It's a very ecchi series with some nudity though. It also has some okay humor at times.

<!--more-->
