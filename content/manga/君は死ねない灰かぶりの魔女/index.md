+++
title = "君は死ねない灰かぶりの魔女"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=172306"
raw = "https://web-ace.jp/youngaceup/contents/1000144/"
md = "https://mangadex.org/title/7c145eaf-1037-48cb-b6ba-f259103b05ea/kimi-wa-shinenai-hai-kaburi-no-majo"
bw = "https://bookwalker.jp/series/269242"

[chapters]
released = 7.2
read = 0

[lists]
recommend = ""
+++



<!--more-->

