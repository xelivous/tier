+++
title = "異世界でチート能力を手にした俺は、現実世界をも無双する"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-26
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=161686"
raw = "https://comic-walker.com/contents/detail/KDCW_AM19201336010000_68/"
md = "https://mangadex.org/title/37838287-3fe9-443d-9d22-eafc1089140e/isekai-de-cheat-skill-wo-te-ni-shita-ore-wa-genjitsu-sekai-wo-mo-musou-suru-level-up-wa-jinsei-wo-kaeta"
bw = "https://bookwalker.jp/series/260198"

[chapters]
released = 24
read = 23

[lists]
recommend = "F"
+++

Protag is an ugly fat dork who gets regularly encouraged by his grandpa in his previous life to continue to be a good person, but eventually his grandpa dies and he's left to fend for himself in a society that hates him. He continues to get regularly bullied at school for being ugly and hates his life as a result, and even his younger siblings (who look attractive) partake in the bullying. However even with everything in his life going shit for him he wants to uphold the values that his grandpa instilled on him and ends up standing up to some thugs when a girl is getting harrassed. He also randomly finds a door to another world that his grandpa hid in his house and ends up going through it.

<!--more-->

This would've been a lot better if he actually had to work to improve himself instead of just immediately becoming Chad in a single night. This is the most power fantasy of power fantasies. Ah, this is by the same author of shinka no mi, everything makes sense now.
