+++
title = "四畳半異世界交流記"
title_en = ""
categories = ["reverse-isekai"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["full"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-01
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146665"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200157010000_68"
md = "https://mangadex.org/title/30662/yojouhan-isekai-kouryuuki"
bw = "https://bookwalker.jp/series/168956/"

[chapters]
released = 16
read = 16

[lists]
recommend = "D"
+++

Protag finally acquires an apartment of his own and gets to live alone, except when he sets foot into his apartment there's 3 isekai'd elves there to greet(furi: threaten) him. Basically the protag is one of the few people in the world who is able to understand these otherworlders, his dad is a higher up in some kind of isekai cultural exchange project, and the protag was forced to take part in this since his dad considers him to be useless otherwise.

<!--more-->

This series is incredibly horny and multiple chapters are basically as close to a doujin or even a JAV in manga format as possible; It's an incredible horny meme of a series. Chapter 7 in particular is too based for reality. However the work kind of goes off the deep end and becomes pretty bad by the end of it. It's probably one of the only ways you can end an excessively horny series like this but it's just too far out there.
