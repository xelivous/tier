+++
title = "탐식의 재림"
title_en = "The Second Coming of Gluttony"
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=159674"
raw = "https://webtoon.kakao.com/content/탐식의-재림/2354"
md = "https://mangadex.org/title/45a1ac5c-8be7-4a5a-8610-ff6495a1e72c/the-second-coming-of-gluttony"
#bw = ""

[chapters]
released = 109
read = 109

[lists]
recommend = "D"
+++

it's a somewhat interesting death game that is something like an isekai and is also somewhat shouneny despite having a decent amount of gore. The artist change in season 2 is kind of jarring, and even the plot kind of skips over the interesting parts once transitioning to season 2. The protag doesn't feel ultra broken OP aside from having some kind of special sight that can give suggestions, which is used interestingly enough. Okay nevermind season2 is kind of bad.

<!--more-->

as far as I know this is a loose sequel of "memorize"