+++
title = "로그인 무림"
title_en = "Murim Login"
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-08-09
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166824"
raw = "https://page.kakao.com/home?seriesId=54872059"
md = "https://mangadex.org/title/246f2c0c-4c50-4ef4-b292-c17f0f68a17c/murim-login"

[chapters]
released = 190
read = 117

[lists]
recommend = "B"
+++

The world was overrun with dungeons/monsters from an outbreak and humanity awoke to mysterious powers where they became Hunters with the ability to go into the dungeons and fight back, and eventually humanity succeeded in slaying the demon king at the end of all of these dungeons already. Now humanity just uses the hunters to farm the dungeons for other-world materials and artifacts, but the story focuses on a dude who is the lowest F-rank hunter who is so trash he got kicked out of the guild and is soon to be homeless. However in desperation he went to the junkyard and brought back a random machine to his house which apparently was a state of the art VR training device that was completed shortly before the demon king was killed; however once you go inside you cannot log out, and if you die in the game you die in real life (just like sao haha lol). Things happen and the protag ends up getting unintentionally sucked into the VR game, but he's now stuck in a Murim world (in china, so wuxia?) as the delinquent youngest son who is a disgrace to his family.

<!--more-->

Pretty cool sao-like that's set in a chinese world where the protag gets trapped in a super realistic VR where if you die you die irl. The early chapters are kind of meme since it's kind of just a training arc but it's actually pretty good. ch32 is kino. It's still mostly just a power fantasy/oretuee but it does it in an interesting way and the protag isn't completely the most OP person in the world, he's surrounded by OP people, it's just that he's rapidly improving thanks to the Meta System helping him out. This work also kind of mixes it up a bit by having the protagonist semi-regularly swap between the murim world and his dungeon-break original work, usually after completing a major arc in either one of them so it just kind of trades back and forth.

Asura is the only translation available after the early 100s which is garbage so i haven't really read beyond that much unfortunately.
