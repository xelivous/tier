+++
title = "이번 생은 가주가 되겠습니다"
title_en = "I Shall Master this Family"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2024-07-26
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=178430"
raw = "https://page.kakao.com/home?seriesId=56566288"
md = "https://mangadex.org/title/f89ed57a-e4c0-48f5-b664-8ef88aa87fd9/i-shall-master-this-family"

[chapters]
released = 158
read = 152

[lists]
recommend = "B"
+++

So basically a girl gets reincarnated into another world, but shit goes wrong and eventually she dies again in a similar to way to her first death. But wouldn't you know it she gets sent back to when she was younger in her 2nd life and she's able to re-do everything that went wrong the first time until she can girlboss enough to be the head of the family when she gets older.

<!--more-->

It's actually a pretty good series. Solid B tier. There's a few spots where i've considered raising it up to A tier and then a few spots that make me bring it back down to B tier afterwards. The main issue with the work is that there's basically only two types of characters in the story: those that think the protag is so amazing and cool, and the other type which is brainlets who constantly yell and make bad decisions. There is basically nothing in-between. Political and merchant meetings take up a majority of this work, with the rest being scenes of the antagonist being douchebags, and the few sparse scenes of the male lead being "super cool and dreamy and stoic and badass". I feel like one of the major highlights of this work is the design of the protagonist since it's relatively rare to get red irish-like curly-ish hair in korean/japanese works at all.

Also, read the official translation otherwise you're stuck with the worst fantranslations imaginable.
