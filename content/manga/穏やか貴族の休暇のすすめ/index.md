+++
title = "穏やか貴族の休暇のすすめ"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-09
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=155024"
raw = "http://seiga.nicovideo.jp/comic/38686"
md = "https://mangadex.org/title/8b5c31a3-9ca8-4284-ac7f-6ed30cd3e504/a-mild-noble-s-vacation-suggestion"
bw = "https://bookwalker.jp/series/207669"

[chapters]
released = 42.1
read = 0

[lists]
recommend = ""
+++

<!--more-->
