+++
title = "人間牧場"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["none"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155278"
raw = "https://gammaplus.takeshobo.co.jp/manga/human_ranch/"
md = "https://mangadex.org/title/a31c3700-7a93-4b64-bfa4-9b09bf3a29dd/human-ranch"
bw = "https://bookwalker.jp/series/205197"

[chapters]
released = 27
read = 27

[lists]
recommend = "D"
+++

Isekai horror is kind of a weird niche tbh. Seems similar to Promised Neverland but it doesn't follow annoying children but also involves tons of random sex/gore scenes. I feel like if you want a gore-y horror it's still worth trying out but if you don't want that then it seems easy to pass on it. 

<!--more-->

The work kind of just ends and barely anything happens of note; this work is almost exclusively misery porn.