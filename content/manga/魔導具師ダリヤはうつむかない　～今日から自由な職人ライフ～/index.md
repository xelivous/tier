+++
title = "魔導具師ダリヤはうつむかない　～今日から自由な職人ライフ～"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=153826"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04200882010000_68/"
md = "https://mangadex.org/title/0346b0eb-d56a-4b69-af71-5cf283792372/magic-artisan-dahliya-won-t-hang-her-head-a-free-craftsman-life-from-now-on"
bw = "https://bookwalker.jp/series/220040"

[chapters]
released = 10
read = 0

[lists]
recommend = ""
+++



<!--more-->

