+++
title = "今際の国のアリス"
title_en = ""
statuses = ["completed"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
lastmod = 2021-09-04T00:00:00Z
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=59540"
md = "https://mangadex.org/title/2890/alice-in-borderland"
bw = "https://bookwalker.jp/series/124293/"

[chapters]
released = 65
read = 0

[lists]
recommend = ""
+++

<!--more-->
