+++
title = "魔法使いの猫"
title_en = ""
categories = []
demographics = ["josei"]
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-01
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=46544"
#raw = ""
md = "https://mangadex.org/title/baebe4db-9d17-4515-9fc1-798cb263171e/mahoutsukai-no-neko"
bw = "https://bookwalker.jp/series/13746/"

[chapters]
released = 33
read = 0

[lists]
recommend = ""
+++


<!--more-->
