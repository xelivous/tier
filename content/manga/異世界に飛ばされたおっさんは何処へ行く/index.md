+++
title = "異世界に飛ばされたおっさんは何処へ行く"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146038"
raw = "https://www.alphapolis.co.jp/manga/official/887000196"
md = "https://mangadex.org/title/0855e750-6ada-4c22-bfc5-fa63b1118bda/isekai-ni-tobasareta-ossan-wa-doko-e-iku"
bw = "https://bookwalker.jp/series/170958"

[chapters]
released = 65
read = 33

[lists]
recommend = "B"
+++

Protag get's blasted in the face by a wheel while riding his motorcycle and ends up waking up in an isekai. The first thing he does is save a dog that was drowning right in front of where he woke up, and gets a revelation to head to the nearest town and pray at the church to hear more details about how he was isekai'd. And once he reaches the church and gets to talk with the goddess, she ends up granting him the ability to basically use the internet, a smartphone/pc/etc, and also cigarrettes, and also an item box, and also infinite magic power, and also he's like super duper strong haha lol.  

<!--more-->

Despite the protag being strong he isn't omnipotent and his lack of resolve causes him to face quite a lot of hardship. The art in this work is also excessively meme, and backgrounds are exceedingly sparse so they may as well just be talking in a hyperbolic time chamber.
