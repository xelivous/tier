+++
title = "시한부 천재 암흑기사"
title_en = "Time-Limited Genius Dark Knight"
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-28
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/zeu3fx5/time-limited-genius-dark-knight"
raw = "https://series.naver.com/comic/detail.series?productNo=9768215"
md = "https://mangadex.org/title/10c548ad-f7ce-47c3-8db8-b8863440a07b/time-limited-genius-dark-knight"
wt = "https://www.webtoons.com/en/fantasy/time-limited-genius-dark-knight/list?title_no=5639"

[chapters]
released = 51
read = 29

[lists]
recommend = "D"
+++

The protagonist only has 2 years left to live, so he ends up spending the rest of his life playing and mastering an RPG and basically becomes a speedrunner of it. However when he goes to start another run with a meme build with some penaties on it that would cause the character to have a short lifespan (like himself) he ends up dying and even reincarnating as the villain of the RPG with those debuffs. Apparently that "character" is his reality, and the 2 years he spent in "korea" playing an RPG of this game was a fantasy to escape his imminent death, however it gave him foresight on events that will happen in the future as well, theoretically.

<!--more-->

The work restricts the protagonist by causing him to think about his timelimit, although also mostly removes the restrictions whenever it actually matters by just allowing him to increase the limit making it largely pointless. There could be a pretty cool story made about a guy who desperately tries to increase his lifespan using any means imaginable but this one isn't it; the protag is mostly just OP and the time-limited aspect is not particularly noteworthy. It somewhat regularly brings up stats near the beginning of the work but they don't really matter all that much and are seemingly very arbitrary. "4" is the average level of stamina for an adult male, while "luck" caps out at 10, and "charm" is regularly seen in the 20+ range.

The "heroine" in the early chapters being largely useless and basically needing to be protected by Chadman Protagonist in the forest is kind of cringe at best, even if she does help out a little bit near the end.

Main highlights of this work are that each chapter is fairly long despite being weekly, and the women in this work are cute even if they all mostly have sameface (if they all didn't have a distinct color they would be indistinquishable). It's a very simple work overall about a dude starting with a terrible weak body and quickly becoome super OP strong in a short amount of time while everybody swoons over how cool he is becoming.
