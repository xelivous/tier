+++
title = "힐러인데 먼치킨"
title_en = "Overpowered Healer"
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["korean"]
schedules = []
lastmod = 2023-10-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/vh9aaz5"
raw = "https://webtoon.kakao.com/content/힐러인데-먼치킨/3552?tab=episode"
md = "https://mangadex.org/title/a02b7d24-9e17-4c83-8ace-54dbdd7c8828/overpowered-healer"
#bw = ""

[chapters]
released = 32
read = 32

[lists]
recommend = "C"
+++

Protagonist gets summoned to an isekai as a spearman but gets forced to be a healer instead and work as a mule without any respect for some reason, but is slowly building up his points in order to return home to earth in the meantime. One day when he is 1 point away from returning his party gets ambushed, one of their party members almost dies and the protag has to use up all of his mana healing her, and since they can no longer get healed they decide to retreat but come across a random treasure chest and decide to gamble on opening it up because who knows maybe it has super epic loot? However it turns out it's just useless scraps of leather so the party throws it to the healer to carry around, yet when he touches it his head starts hurting like crazy and all of a sudden can hear a "game master" and another person basically playing a DnD game, rolling dice etc. Long story short the protagonist is trapped in a DnD game as a pawn of a player and can hear the voice of the Game Master and the game's systems.

<!--more-->

What if Arifureta was a DnD game where the protag suffers and then gets revenge and then fights the gods and kills them constantly while being edgy? There's no epic harem though and it's just skill spam central. 

As far as I can tell there's another adaptation of this work that already finished back in 2020 or so but isn't translated?
