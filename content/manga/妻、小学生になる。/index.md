+++
title = "妻、小学生になる。"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149706"
raw = "https://piccoma.com/web/product/6387?etype=episode"
md = "https://mangadex.org/title/2fc6e547-cd3d-4030-a733-defc244df2d5/if-my-wife-became-an-elementary-school-student"
bw = "https://bookwalker.jp/series/200600"

[chapters]
released = 111
read = 50

[lists]
recommend = "B"
+++

I was originally put off by the cover arts and the basic premise, but it turned out to actually be a pretty competent work. I feel like the work is largely about parenthood at whole, and somewhat getting over the loss of a loved one and/or divorce and carrying on with your life, but at the same time the core premise of having your loved one reincarnate to "fill the hole in your heart" and help the characters move on kind of cheapens the message overall. It's a competent work but i'm not sure what message they're actually aiming for beyond that. The art improves a lot over time at least.

<!--more-->
