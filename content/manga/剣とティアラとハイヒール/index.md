+++
title = "剣とティアラとハイヒール"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=176491"
raw = "https://seiga.nicovideo.jp/comic/51013"
md = "https://mangadex.org/title/3747dbbb-63ab-41e4-a389-0a66382840e0/sword-tiara-and-high-heels"
bw = "https://bookwalker.jp/series/310846/list/"

[chapters]
released = 20
read = 0

[lists]
recommend = ""
+++



<!--more-->

