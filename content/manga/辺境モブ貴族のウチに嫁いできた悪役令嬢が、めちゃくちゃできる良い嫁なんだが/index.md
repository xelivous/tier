+++
title = "辺境モブ貴族のウチに嫁いできた悪役令嬢が、めちゃくちゃできる良い嫁なんだが"
title_en = ""
categories = []
demographics = []
statuses = []
furigana = []
sources = []
languages = []
schedules = []
lastmod = 2025-01-16
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/tfbq2ok"
raw = "https://comic-walker.com/detail/KC_005954_S/episodes/KC_0059540000200011_E"
md = "https://mangadex.org/title/4bf066dc-3a6d-4c33-997b-6033208c4176/henkyou-mob-kizoku-no-uchi-ni-totsuidekita-akuyaku-reijou-ga-mechakucha-dekiru-yoi-yome-nanda-ga"
#bw = ""

[chapters]
released = 0
read = 0

[lists]
recommend = ""
+++



<!--more-->

