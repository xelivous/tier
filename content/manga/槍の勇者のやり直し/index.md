+++
title = "槍の勇者のやり直し"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=145187"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000048010000_68/"
md = "https://mangadex.org/title/5de63ed1-61ae-4f1a-8822-1b7e40e96e42/yari-no-yuusha-no-yarinaoshi"
bw = "https://bookwalker.jp/series/140731"

[chapters]
released = 55
read = 55

[lists]
recommend = "A"
+++

Okay so like this is the New Game+ of [shield hero](../盾の勇者の成り上がり) where you get another route to see a completely different timeline from another character's perspective, except the timeine is completely fucked up and bizarre. You will not really understand any part of the story or why anything matters without first reading shield hero LN up to volume 16, so normally this would be relegated as sequel status and not listed, but it's unique enough that I listed it anyways and I really like it a lot. Main target demographic of this manga is someone who managed to stick around with shield hero long enough for it to get good, and wanted some utterly bizarre story set in the same world with some of the same characters. {{< spoiler >}}Holy shit they actually killed the red hair douche from the start of shield hero in one of the loops.{{< /spoiler >}}. 

<!--more-->

Shoutout to the protag seeing almost all women as literally pigs who do nothing but oink all day so he can't even understand what they're saying. Probably the most misogynistic manga i've read fr, would love to see the hilarious shitstorm that happens on twitter if it ever got an anime. Also shoutout to raising tons of bird children and peeking on them while they're in the bath and raising them to enjoy being peeked on while in the bath.

This manga is realdeal quofbait though with some cool time loops. The weird inconsistencies that are just slightly off from the storyline that you already know is actually kind of nice. However the manga was unfortunately axed/discontinued for some reason partway through the story and doesn't go into any further loops leaving it off at a kind of weird spot. I really don't want to read the WN for this though and it's unlikely we'd get an anime adaptation of this...
