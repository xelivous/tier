+++
title = "サキュバスに転生したのでミルクをしぼります"
title_en = ""
statuses = ["completed"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146614"
raw = "http://seiga.nicovideo.jp/comic/34125"
md = "https://mangadex.org/title/22526/succubus-ni-tensei-shita-node-milk-wo-shiborimasu"
az = "https://www.amazon.co.jp/dp/B07BHHZFJ4"

[chapters]
released = 20.3
read = 20.3

[lists]
recommend = "C"
+++

(Male) Protag dies and he ends up becoming a succubus due to miscommunication, and unless he gets the sperm he'll die; however since he prefers women and doesn't want to do it with a man, he can also subsist off of cow milk for whatever reason. He basically has to constantly fend off men getting super horny over his body while living as a succubus.

<!--more-->

it's basically nothing but meme booba
