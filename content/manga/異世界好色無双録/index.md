+++
title = "異世界好色無双録"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/6p0v27r"
raw = "https://yanmaga.jp/comics/異世界好色無双録異世界転生の知恵と力を、ただひたすらするために使う"
md = "https://mangadex.org/title/bcbf9c51-2f3c-462c-a81b-b6e5530ae00f/isekai-koushoku-musou-roku-using-the-wisdom-and-power-of-reincarnation-in-another-world-to-just-xxxx"
bw = "https://bookwalker.jp/series/415733"

[chapters]
released = 14
read = 0

[lists]
recommend = ""
+++



<!--more-->

