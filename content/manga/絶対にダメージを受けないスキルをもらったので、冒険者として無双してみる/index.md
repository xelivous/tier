+++
title = "絶対にダメージを受けないスキルをもらったので、冒険者として無双してみる"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2022-04-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169302"
raw = "https://seiga.nicovideo.jp/comic/48536"
md = "https://mangadex.org/title/878da22c-787f-4a94-b249-73a2d7ff5e9e/zettai-ni-damage-o-ukenai-skill-o-moratta-node-boukensha-to-shite-musou-shite-miru"
bw = "https://bookwalker.jp/series/287958/"

[chapters]
released = 17
read = 8

[lists]
recommend = "D"
+++

Dude dies trying to protect someone, then a god gives him a "negate damage" skill so he can protect more people and then reincarnates him just before he originally died so that he can change his fate and protect everybody and become the literal meatshield of his dreams.

<!--more-->
