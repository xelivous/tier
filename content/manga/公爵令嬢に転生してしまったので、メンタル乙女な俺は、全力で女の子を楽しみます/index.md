+++
title = "公爵令嬢に転生してしまったので、メンタル乙女な俺は、全力で女の子を楽しみます"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2025-01-14
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/7knwps0"
raw = "https://gammaplus.takeshobo.co.jp/manga/mental-otome/"
md = "https://mangadex.org/title/462bd3fc-019c-4f28-8884-d7513d1e5a80/koushaku-reijou-ni-tensei-shiteshimatta-node-mental-otome-na-ore-wa-zenryoku-de-onnanoko-wo"
bw = "https://bookwalker.jp/series/415782"

[chapters]
released = 23.1
read = 23.1

[lists]
recommend = "C"
+++

Typical japanese corporate slave has a heart attack and dies from overwork, but after his dead he is connected by a red string of fate with an isekai loli who is tired of looping her life and experiencing death over and over, and ultimatately gives him a chance to live out her life in case he can do better. The protagonist was already a fudanshi at heart and loved girly things so reincarnating as a loli was actually something he was optimistic about, and he was even reincarnated as a high ranking noble girl. Really makes you wonder how fucked the loli's life got if she still ended up repeatedly dying over and over despite how carefree and healing the protagonist's young life is!

<!--more-->

A vast majority of this life is simple slice of life with the protagonist just doing whatever they like to further their interests and experiencing girly activities that he was "unsuited" for back when he was a man, although since he still had 30+ years of being a man he regularly lapses into improper behaviour for a noble girl to the extent that her parents worry that she'll not have good marriage prospects in the future.

The first major problem this work has is that it feels like the artist doesn't really understand manga art; the backgrounds are nearly non-existent, the character art is rakugaki tier, and it feels like a very amateur twitter-tier comic. The second major problem is that it's adapted very poorly where its webnovel aura exudes through the page, with a majority of the text on most pages being narration stating things happened, instead of actually adapting it to work in a manga format. I suppose really that's the same issue manifesting in multiple ways if you think about it. There is a fair amount of work put into the art that the protagonist creates and at times the background art features actual traditional art portraits/etc, and this does look like the artist's first manga work, so maybe they're just not as familiar with manga techniques which is why most of the pages are just completely lacking screentones and feels generally incomplete. It just comes across as feeling a little odd and lacking.

Content-wise the target demographic of this is either people who are trans, into genderbend works in general, or just want to watch a cute girl do cute things while occasionally eventually talking about warfare, astrology, and insects. Ultimately it's someone writing down a bunch of things they would like to do if they were a loli in an isekai and not much else imo; I don't think there's much of a plot to really critique but it's not like having pure slice of life is a bad thing. But I don't think it is particularly noteworthy outside of that and that this isn't a particularly great of an adaptation in the first place unless you think you'd be particularly fond of this kind of work.