+++
title = "異世界に落とされた…浄化は基本"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-10-17
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172656"
raw = "https://to-corona-ex.com/comics/20000000046280"
md = "https://mangadex.org/title/b3a9c1f8-93d2-49ba-96e2-84727c1031a6/isekai-ni-otosareta-jouka-wa-kihon"
bw = "https://bookwalker.jp/series/260147"

[chapters]
released = 20.2
read = 20.2

[lists]
recommend = "C"
+++

Protag gets run over by a truck along with four other people who end up getting summoned/isekai'd but the protag is left out and just hangs out in the in-between space since he wasn't meant to die, and after a while just ends up just kind of plopping out into a random spot in an isekai. He then wanders the forest he was plopped into but realizes he's basically trapped in a cursed forest with a thick miasma that is slowly wearing him down, so he tries casting a cleansing spell and it kind of just works. He then comes across a gigantic wolf that tries to attack him but is too weak to actually do so due to the miasma, at which point the protag also cleanses the wolf because he loves animals and the wolf is actually like an epic fenrir who follows the protag around after that. The protag then goes back to the fenrir's den, purifying everything in sight with his bottomless magic, and gets to hang out with tons of wolves.

<!--more-->

The protag is a disney princess who solves all problems by simply wishing for it and lives among the animals making modern technology with the help of magic bullshit. The protag never really meets any other humans, but just kind of lives his life in a massive forest surrounded by animals occasionally sending out purification waves that are overthrowing plots done by the Evil Humans in the world. Half of the work is focused on the protagonist playing with his animals, and the other half focuses on the rest of the world freaking out over the forest resisting the curse placed upon it and having all of their schemes thwarted to keep all of the powerful creatures at bay. The perspective swaps constantly and you even regularly hear the thoughts of the animals surrounding the protag, but the protag will never hear the thoughts of anybody aside from himself. As a result the protag just talks to himself for the entire work while everybody else just kind of has to deal with his whims which is very bizarre.
