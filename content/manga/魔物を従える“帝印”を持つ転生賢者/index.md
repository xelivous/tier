+++
title = "魔物を従える“帝印”を持つ転生賢者"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165484"
raw = "https://magazine.jp.square-enix.com/joker/series/teiin/"
md = "https://mangadex.org/title/339a6d49-9cc3-4435-98b6-59ddec3a6c0d/mamono-wo-shitagaeru-teiin-wo-motsu-tensei-kenjya-katsute-no-maho-to-jyuma-de-hissori-saikyo-no-bokensha-ninaru"
bw = "https://bookwalker.jp/series/243989/"

[chapters]
released = 38.5
read = 37

[lists]
recommend = "D"
+++

The protagonist is the king of an empire whose unique ability is being able to control demons, yet everybody (including his siblings) fear his ability and consider him to be a cursed demon child and thus plot to overthrow his reign and come for his head. However his siblings are turbo trash who basically ruined the country in the first place, and still are vying for his spot on the throne. However the protag just wants to live in peace and end the fued between humans and demons for good, and is just tired of life, so he commands his demon underlines to skedaddle away against their will and allows himself to get killed by the peasants that were revolting due to his sibling's schemes. However upon dying he ends up being reincarnated (with his memories intact) to a peasant family. The story then timeskips to when he's basically a young adult and picks up from there, however for some reason nobody in this world seems to know how to use magic aside from him so he starts slowly teaching everybody around him. Then when he finally becomes of age he sets out to become an adventurer, and actually manages to find at least one of the demons he set free back when he died in his previous life; but it's been 1000 years since his death.

<!--more-->

The main plot of this work is about creating a world where humans and demons can live together in peace, but the protagonist is simply a hetare wimp of a shounen protag who is a wishy washy coward unable to really do anything meaningful, to the extent that he's already died and came back to life and is still useless. However he ends up meeting a girl with similar powers as him, who looks up to the ideal legendary self that he never actually was, and he ends up supporting her from the shadows so that he can chill out in the country with his demon friends to live happy ever after and maybe farm or something while said demon friends are exasperated on how much of a donkan hetare the protag truly is. Also every woman wants his dick for some reason.
