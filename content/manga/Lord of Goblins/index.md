+++
title = "Lord of Goblins"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["english"]
schedules = []
lastmod = 2024-01-20
tags = [
    
]

[links]
md = "https://mangadex.org/title/54a1ae14-505f-4459-ace1-24a08ceea8c3/lord-of-goblins"
wt = "https://www.webtoons.com/en/fantasy/lord-of-goblins/list?title_no=5227"

[chapters]
released = 48
read = 14

[lists]
recommend = "F"
+++

Protag is epic rebel badass who gets assassinated for trying to overthrow society, only to get reincarnated at the bottom rung of goblin society. He then tries to overthrow this society as well to make his living situation better.

<!--more-->

There's something about the writing that doesn't mesh with me at all. The art style flops between two distinct styles early on. Idk i just don't want to read this at all.
