+++
title = "転生賢者の異世界ライフ"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150779"
raw = "http://www.ganganonline.com/contents/tensei/"
md = "https://mangadex.org/title/30744/tensei-kenja-no-isekai-life-daini-no-shokugyou-wo-ete-sekai-saikyou-ni-narimashita"
bw = "https://bookwalker.jp/series/172921/"

[chapters]
released = 60.1
read = 62.2

[lists]
recommend = "C"
+++

Protag works at a black company and does nothing but work and sleep with nothing else in his life and doesn't even have time to play any videogames, however he gets a weird prompt on his computer one day asking if he wants to be isekai'd and by rebooting his computer during the process he get's isekai'd regardless. As a result he wakes up in a forest and his first thought is literally to kill himself in his dream so he can go back to working and meet his deadline since it's no time to be sleeping. So he tames a slime by accident, finds a nearby house, and tries to think up various ways to kill himself, yet also ends up learning super powerful magic, and then tames like 80 slimes all at once who then read all of the magic books for him which then transfer the knowledge to himself which makes him even more epically powerful.

<!--more-->

It's kind of amusing(?) that the protag's first thought is to kill himself. I'd say this is more of a comedy than anything, but the protag kind of ruins a lot of the comedy by being too much of straightman. A little too much skill autism as well. The tamed monsters are amusing enough though and the wolf is the main highlight of this manga. coders rise up. ch37 is turbo based. I really don't like the school arc that gets added in the late ch50s but it's an isekai so of course it had to shove it in there somehow... but the rest of the work is enjoyable enough. All of the chapters are very long and there's a lot of content out there released for this, and it's still chugging along. Of all of the kenja works this is probably one of the better ones.
