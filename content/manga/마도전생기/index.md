+++
title = "마도전생기"
title_en = "Chronicles of the Demon Faction"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-04-18
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/kuw7coe/chronicles-of-the-demon-faction"
raw = "https://comic.naver.com/webtoon/list?titleId=807777"
md = "https://mangadex.org/title/365d291e-277e-45fd-b364-956d0c6b11c8/chronicles-of-the-demon-faction"
wt = "https://www.webtoons.com/en/action/chronicles-of-the-demon-faction/list?title_no=5832"

[chapters]
released = 57
read = 24

[lists]
recommend = "D"
+++



<!--more-->

Extremely mid murim/cultivation manwha where the protagonist is an epic assassin who gets killed, reincarnates as a prince who got backstabbed, then immediately becomes the most powerful chadgod-emporer as if he was a pay2win soshage player. The plot is contrived and boring, and you're better off reading any of the other murim stores instead. It's not irredeemable garbage but I don't really see anything outstandingly unique about it in any way.
