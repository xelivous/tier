+++
title = "그 악녀 인생, 제가 한번 살아 볼게요"
title_en = "I Will Live the Life of a Villainess"
categories = ["isekai", "reincarnation"]
demographics = ["josei"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-03
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/ut0hhxo/i-will-live-the-life-of-a-villainess"
raw = "https://comic.naver.com/webtoon/list?titleId=813396"
md = "https://mangadex.org/title/2b8ea244-30be-4c4b-b3dd-d136b4b7b443/that-villain-s-life-i-ll-live-it-once"
wt = "https://www.webtoons.com/en/romance/i-will-live-the-life-of-a-villainess/list?title_no=5954"

[chapters]
released = 45
read = 12

[lists]
recommend = "B"
+++

Protagonist gets hit by a car and reincarnates into the body of a noble lady who is betrothed to an asshole of a pretty boy (the crown prince) and her first course of action is to slap him silly and give him the middle finger. Apparently the protagonist reincarnated into a villainess novel she read in her past life, and desperately wants to avoid a beheading ending after sabotaging the moe heroine of the story, thus she immediately sets out to girlboss her way to fame and fortune as an independent woman instead.

<!--more-->

This work seems to greatly focus on women's indepedence in the "noble age" where women typically existed solely to marry off to a man and go along with his whims, set after a time period shortly after women are finally able to inherit their households but there's been very few  instances of it happening yet. There's also a few subplots of multiple reincarnators trying to probe each other out, along with the typical villainess plotlines you see in these kinds of works. The author also doesn't rely on writing in dumb characters for the sake of extending out plotlines nor fostering misunderstandings; they will make obvious logical deductions and then move forward with their ideas while only questioning that which needs questioning.

Overall this has a really strong start and i'm looking forward to how it can develop the story in the future. It's a little too early on to really tell if the author can keep up this pace but i'm tentatively optimistic on it.
