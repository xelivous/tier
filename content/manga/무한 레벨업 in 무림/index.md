+++
title = "무한 레벨업 in 무림"
title_en = "Infinite Leveling: Murim"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=178219"
raw = "https://series.naver.com/comic/detail.nhn?productNo=5038382"
md = "https://mangadex.org/title/298cb7b7-4a1a-407f-b479-d157a9b33d9a/infinite-leveling-murim"
wt = "https://www.webtoons.com/en/action/infinite-leveling-murim/list?title_no=2676"

[chapters]
released = 131
read = 74

[lists]
recommend = "D"
+++

This story is about gamifying hard work. Would you try harder in life if you had numbers go up when you did menial tasks? ch63 is good i guess.

<!--more-->

