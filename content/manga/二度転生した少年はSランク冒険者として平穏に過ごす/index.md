+++
title = "二度転生した少年はSランク冒険者として平穏に過ごす"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153902"
raw = "https://magazine.jp.square-enix.com/top/comics/detail/9784757563056/"
md = "https://mangadex.org/title/36610/nido-tensei-shita-shounen-wa-s-rank-boukensha-toshite-heion-ni-sugosu-zense-ga-kenja-de-eiyuu-datta-boku-wa-raisede-wa-jimini-ikiru"
bw = "https://bookwalker.jp/series/219010/"

[chapters]
released = 31
read = 31

[lists]
recommend = "D"
+++

Protag reincarnates twice, originally a sage and then a hero and now just some random dude who will clearly not be broken OP. The work timeskips immediately to age 15 so it doesn't need to spend any time on growth or training and so it can go right into the protag blastin' fools.

<!--more-->

Fairly standard oretueeee where protag teaches some random newbs how to actually fight using Lost Abilities all while the protag continues to state that everything in this era is too easy. and unfortunately even though this is the protag's 3rd life he's still brainlet. mina and a few of the later girls are hot at least even if their clothing kind of detracts from their emotional backstories/etc and just makes you wonder why they're basically wearing meme mmo armor although eventually she wears actual armor. booba.

Honestly the protagonist being an excessive brainlet really lowers the rating of this work by a substantial amount.
