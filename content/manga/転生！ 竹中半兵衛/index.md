+++
title = "転生！ 竹中半兵衛"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=159086"
raw = "https://piccoma.com/web/product/19968"
md = "https://mangadex.org/title/7139976f-57ef-4530-8668-89cba8c2164a/tensei-takenaka-hanbei-maina-bushou-ni-tensei-shita-nakama-tachi-to-sengokuranse-wo-ikinuku"
bw = "https://bookwalker.jp/series/220577"

[chapters]
released = 29
read = 0

[lists]
recommend = ""
+++



<!--more-->

