+++
title = "水属性の魔法使い"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2025-01-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/0yarwf7/mizu-zokusei-no-mahoutsukai-comic"
raw = "https://to-corona-ex.com/comics/20000000055002"
md = "https://mangadex.org/title/bfbecb6e-8a6f-4b31-a040-5928a7635894/mizu-zokusei-no-mahoutsukai"
bw = "https://bookwalker.jp/series/336111/"

[chapters]
released = 31
read = 10

[lists]
recommend = "D"
+++

The protagonist is orphaned at a young age from his parents getting caught in an accident, then a few years later he himself is also run over by a car and dies; However he gets whisked away into a white void and told that he was chosen to be one of the lucky few to be able to reincarnate into another world along with his memories intact. He even gets to reincarnate into a world of fantasy, swords, and magic, and he's even a part of the lucky 20% that is able to use magic at all! After a ton of very detailed lore dump-tier explanations from the corporate-slave-like angel, he eventually gets isekai'd according to his vague preferences out in the middle of nowhere to live out his "slow life" with a nice grace period where he can train himself up to not immediately die.

<!--more-->

The early portions of the work are extremely weird and almost entirely narration as if was stripped out directly from the webnovel with little thought on how to adapt it to manga format. On top of that the art is unbelievably abysmally bad, with an insane amount of terrible CG use and everything else being either white voids or bad rakugaki-tier sketches. The panelling sucks, the art sucks, it feels like reading a really poorly adapted webnovel. There is no merit in reading this; either read the LN or wait for the anime. Content-wise it's actually fairly fine with a nice dual-protag setup and nice pacing, but it's hard to appreciate it when it's in the form of this shit adaptation.
