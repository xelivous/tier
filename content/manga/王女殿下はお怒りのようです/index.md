+++
title = "王女殿下はお怒りのようです"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["josei"]
statuses = ["completed"]
furigana = ["full"]
sources = ["light-novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155606"
raw = "https://comic-gardo.com/episode/10834108156673596521"
md = "https://mangadex.org/title/efa1e330-e318-4669-b9a7-878ac2618cab/her-royal-highness-seems-to-be-angry"
bw = "https://bookwalker.jp/series/236821"

[chapters]
released = 28
read = 28

[lists]
recommend = "A"
+++

The protagonist is a tomboy princess of a certain small country that has been ravaged by wars, who regularly goes out into the wilderness to fight against beasts with her ice magic. One day a random japanese guy appears out of nowhere from the sky and falls down in front of the princess, and she brings him back and tries taking care of him despite not being able to understand his language at all. Essentially it's an isekai told from the point of view of the princess who discovers the isekai'd dude as they slowly enjoy what life has to offer after her country has been devastated by war.

<!--more-->

There's an extremely hot twinbraid who becomes friends with the protagonist and every panel with her is great even if she gets less and less screentime as the work goes on. Both the characters and the background art is beautiful though. It's ultimately an extremely standard josei plot that mixes in shounen isekai plotlines from the other perspectives to make a cool mix between the two. I feel it rushes and skips over a few plotlines a little too much and kind of just assumes you'll roll with some of the events that happen and fill in the plotlines yourself (maybe they're in the source material?). As far as I can tell the story also diverges(?) or is completely different from the lightnovel to kind of just give an ending while the lightnovel continues onwards. It's cool enough of a work on its own in its current form although i'm slightly interested in how different the source material is; if anybody ends up reading it before I do let me know thanks
