+++
title = "영주님의 코인이 줄지 않음"
title_en = "The Lord of Coins"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=180577"
raw = "https://page.kakao.com/content/56998709"
md = "https://mangadex.org/title/87eab128-54a8-4fcf-a5e2-016a39413216/the-lord-of-coins"

[chapters]
released = 81
read = 77

[lists]
recommend = "D"
+++

Protag gets scammed repeatedly in his last life after dealing in the Interdimensional Marketplace filled with scrupulous merchants with years of experience, and manages to go back in time with multiple of his wishes granted by a random wish-granting mcguffin he bought in the past that he thought was also a scam. He sets out to get back and all of the scammers that wronged him and prevent all of the downfalls he had in his previous life by beating them at their own game in the marketplace since somehow him doing absolutely basic merchant shit is actually working for him in this life now that he's no longer falling for the scams he had in the previous life!!! It's also basically a murim series in disguise for some reason? It doesn't really do anything interesting with its core premise, and mostly just exists as a vehicle to make the protag have random powerups to dab over people in his original world.

<!--more-->
