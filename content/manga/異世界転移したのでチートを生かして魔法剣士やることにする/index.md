+++
title = "異世界転移したのでチートを生かして魔法剣士やることにする"
title_en = ""
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-05
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=171556"
raw = "https://magazine.jp.square-enix.com/mangaup/original/isekai_teni/"
md = "https://mangadex.org/title/9c4f1b25-c224-4346-990a-f18c77f4ab9e/isekai-cheat-magic-swordsman"
bw = "https://bookwalker.jp/de02912b12-be97-41c6-be51-2e183777a559"

[chapters]
released = 28.2
read = 28.2

[lists]
recommend = "D"
+++

Protag is a hardcore MMO gamer who had just finished college and ended up staying up all night for days on end until he eventually dies, and ends up reincarnating into a game similar to the MMO he was playing. He has ultra broken levels of MP, can cast any spell just by imagining it, and immediately starts leveling up and enjoying his time in this isekai.

<!--more-->

Fairly run-of-the-mill game-like isekai where the protag is OP and there's basically no conflict in anything. Has extrmely hot twinbraids in like every town he visits fawning over him but obtains a loli to go on adventures with him instead... tfw will never have cute isekai twinbraid gf.... why does a work with so many kami twinbraids in it have none of them join the protag....
