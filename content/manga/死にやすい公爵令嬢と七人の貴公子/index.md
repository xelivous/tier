+++
title = "死にやすい公爵令嬢と七人の貴公子"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["josei"]
statuses = ["axed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169129"
raw = "https://gaugau.futabanet.jp/list/work/5dd4fe3077656139da070000"
md = "https://mangadex.org/title/30b68b83-d8cd-46c2-ad6c-e2eca355ab36/deathbound-duke-s-daughter-and-seven-noblemen"
bw = "https://bookwalker.jp/series/291304"

[chapters]
released = 27
read = 22

[lists]
recommend = "C"
+++

Protag was regularly stalked by men in her previous life and was eventually stabbed by a crazy stalker, and then reincarnates into the otomege she liked as the villainess. However this game has tons of bad ends with brutal deaths in it, and unfortunately the protag never actually finished the game before she died.

<!--more-->

Overall an okay entry where the protag is proactive and tomboyish wearing men's clothing and not afraid to step into danger to save her friends. However the way the characters just give tons of random backstory at convenient moments is kind of jarring; it just doesn't flow well plotwise. It also got axed without really going anywhere. Could rec if you want a standard otomege isekai work with an outgoing protag and a slightly different magic system.
