+++
title = "田舎のホームセンター男の自由な異世界生活"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150742"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01200628010000_68/"
md = "https://mangadex.org/title/f5b69b9e-c41f-4d12-8f62-fa7eba9d9b6c/inaka-no-home-center-otoko-no-jiyuu-na-isekai-seikatsu?tab=chapters"
bw = "https://bookwalker.jp/series/193475"

[chapters]
released = 54.1
read = 42

[lists]
recommend = "D"
+++

I swear the artist has some kind of open-mouth-with-teeth-visible fetish. Protag also makes some like really creepy faces near the start. It's a city building isekai where the protagonist uses his encyclopedic knowledge of working at "basically home depot" to make anything and everything. Protag is also broken OP and has goddess waifus who fawn over him, on top of a bunch of non-goddess waifus who also fawn over him. Protag is a naive dumbass who lets bandits go after healing them too. Every girl in the series has massive breasts as well. Protag basically just invents anything and everything without issue and then teaches it to the peasants of this world while they fawn over him for his amazing otherworld knowledge.

<!--more-->

If you want a town-building manga that is kind of like slime dattaken then it's probably okay. Same level of wankery.
