+++
title = "ヘルモード　～やり込み好きのゲーマーは廃設定の異世界で無双する～"
title_en = ""
categories = ["reincarnation", "isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-12-21
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=173268"
raw = "https://www.comic-earthstar.jp/detail/hellmode/"
md = "https://mangadex.org/title/ee4bf782-9fe9-42a6-be7f-3809293818c2/hell-mode-yarikomi-suki-no-gamer-wa-hai-settei-no-isekai-de-musou-suru"
bw = "https://bookwalker.jp/def7687242-81c7-433a-8f9e-15d64c769425/"

[chapters]
released = 72
read = 71

[lists]
recommend = "C"
+++

Protag was playing an online game that got shuttered, was bored of all of the online games since they were all too easy for this chaddest of chad gamers, and wanted a kamige that lasts for eternity and is also hard. He comes across a cool game that touts it as "never ending" and also has a "hell mode" difficulty so of course he starts that shit right up.

<!--more-->

However "hell mode" is really just secret code for "how much do you want to grind? yes", there isn't really much difficulty, it's simply that you need to put in 20000% effort to menial chores to be at the baseline that other people are at. To top it off he has what is generally considered to be a broken class in most mmos/games/etc, a summoner, and he doesn't even have any real cap on the amount he can summon at one time. He can just obliterate anything while his summons do all of the work while he sits in relative safety.

I could probably only recommend this to someone who really loves stat/skill spam and excessive grinding in mmos or some shit, because that's 90% of this work, and the other 10% is like weird political shit. Although it's decent enough at that if that's all you want.
