+++
title = "異世界居酒屋「のぶ」"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-02-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=123149"
raw = "https://comic-walker.com/contents/detail/KDCW_KS02000029010000_68/"
md = "https://mangadex.org/title/4710b856-0585-40d2-a87e-099fc032511f/isekai-izakaya-nobu"
bw = "https://bookwalker.jp/series/57544"

[chapters]
released = 101
read = 44

[lists]
recommend = "B"
+++

This is probably one of the better cooking series out there, and it does a good job fleshing out the world and intertwining a large cast of characters that come in and out of the shop, even if it does handwave out some of the finer points like money to actually get the ingredients (it does touch on it a bit, but barely). The art is good. The regularly reoccuring hilariously evil dude who keeps coming back for revenge before blasting off again like team rocket is kind of annoying and seems to exist solely to drive whatever random drama is needed when the author seems to get into a slump to make it seem more interesting.

<!--more-->
