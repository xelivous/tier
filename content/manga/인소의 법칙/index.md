+++
title = "인소의 법칙"
title_en = "My Life as an Internet Novel"
categories = ["isekai"]
demographics = ["shoujo"]
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-11
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154214"
raw = "https://page.kakao.com/content/52610652"
md = "https://mangadex.org/title/8a2e20cc-bdc8-47f1-80fd-b6da4b2703df/my-life-as-an-internet-novel"

[chapters]
released = 170
read = 119

[lists]
recommend = "C"
+++

So this is basically a Denpa isekai where the protag all of a sudden wakes up in a parallel universe that is based off of random tropes from webnovels, and she has been inserted as the friend of the "female lead" where everybody has falsely planted memories of the past of her existing there since the beginning. The work is largely about the protag trying to deal with this insane world she is living in and where she constantly struggles to be Geniune to people she can only consider to be "Characters", along with all of these "Character's" thoughts on how their friend seemingly acts like a stranger to them from time to time. It does a pretty good job of conveying the insecurity/uncertainty of the protag's reality, with the possibility of waking up one day and all of your newfound friends disappearing and completely forgetting you even existed. 

<!--more-->

The title being what it is however, it does lean into "webnovel tropes" a little too hard which makes everything seem a little too superficial which cheapens all of the good parts of the work that make up every other part of it. I feel like if it didn't try to play for jokey comedy with all of the absurd "webnovel events" it would've been a really good work overall and much higher in the list, but unfortunately it does play into them too much and I have to put it a lot lower.