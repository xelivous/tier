+++
title = "외과의사 엘리제"
title_en = "Doctor Elise: The Royal Lady With the Lamp"
categories = ["reincarnation", "isekai"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149861"
raw = "https://series.naver.com/comic/detail.series?productNo=8542770"
md = "https://mangadex.org/title/8368162b-a57e-4374-877a-926d42c683df/doctor-elise"

[chapters]
released = 143
read = 143

[lists]
recommend = "C"
+++

This story is about a girl who became the empress, died because she was a bitch, reincarnated as someone in korea who became a doctor, died again and went back to the first life with all of her knowledge about both lives intact. She then uses this newfound surgical knowledge to become a doctor in her original life and right her wrongs. The author puts in a lot of gorey surgical operations in quite a lot of detail, interspersed with an extremely brainlet-tier romance. "I was a massive bitch in my past life and basically forced the prince to marry me and thus he hated me, but in this current life where i'm perfect and knowledgeable and capable and humble the prince actually likes me for some reason??? He's probably secretly plotting my downfall!!!" The protag repeats that over and over again throughout the story unironically. 

<!--more-->

Since she has future knowledge of everything and modern surgical knowledge she just breezes through everything while everything perfectly aligns. It's kind of bizarre that her first world has basically modern-grade equipment like xrays, surgical gear, scalpels, the whole works because of what is assumedly another dude who isekai'd earlier and set up everything for the protag. It really feels like the author wanted to write a story about doctors, but also wanted some kind of power fantasy where the protag girlboss'd everybody, and the only way they could think to do it was by double reincarnating the protag...

Somewhere around ch100 the chapters become smaller and smaller with less actual content happening each chapter, as if to drag out the story until the ending; maybe this is when covid hit which affected every other serialization as well. Ch 122 is the most pain inducing dumb shit i've read; there's a limit to how contrived a plot point should be. It never resolved the plot point of his alternate persona either.

C tier if you fall into the intersection of wanting to read about surgical operations and shoujo romance simultaneously.
