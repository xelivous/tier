+++
title = "四天王最弱だった俺"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-12-28
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=170057"
raw = "https://seiga.nicovideo.jp/comic/53868"
md = "https://mangadex.org/title/f17a64c1-5582-48fa-a8f2-ed3e3158a3a6/i-was-the-weakest-of-the-four-heavenly-kings-since-i-have-reincarnated-i-want-to-lead-a-peaceful-life"
bw = "https://bookwalker.jp/series/268730/"

[chapters]
released = 21
read = 10

[lists]
recommend = "D"
+++


<!--more-->
