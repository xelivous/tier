+++
title = "이계 검왕 생존기"
title_en = "Latna Saga: Survival of a Sword King"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=160778"
raw = "https://page.kakao.com/home?seriesId=54204320"
md = "https://mangadex.org/title/478e6926-b8bc-465c-9694-2bae1dbaf32b/the-survival-story-of-the-sword-king-in-another-world"

[chapters]
released = 155
read = 141

[lists]
recommend = "D"
+++

TLDR protagonist and a bunch of others get summoned somewhere, get gifted the System, and then for some reason only the protag gets a bugged system  that's corrupted as hell, and the gods are like "well whatever just ship it to prod". As a result of having a bugged system the protagonist is incapable of leaving the tutorial zone, and basically has an infinite level up glitch which constantly resets his levels without increasing the exp costs but still keeping the stats for some godawful reason. Then after literal decades of being trapped in tutorial hell, slowly losing his mind and grinding the shit out of everscaling mobs that match his powerlevel exactly, the gods stumble across a bizarre low level hanging out in the tutorial and decide to just slap him into the real thing to hide the mistake(?) of letting someone be in there that long since he'll probably just immediately die in the main area since he's so low leveled obvs.

<!--more-->

That's the extremely elaborate prologue/setup for why the protagonist is so overwhelmingly broken OP but the only thing they ever really do with it is make the protag One Punch Man and constantly have characters mention/joke about his low level and then constantly be surprised at how strong he is. That's the plot.

According to random comments the artist of this kind of just went off and did their own thing separate from the original novel, and as a result the further it goes on the dumber it becomes despite the beginning being decent. That could be a sideeffect of the original work just not knowing what to do with this meme OP protagonist, or it could just be that this adaptation is extremely shit; not like i'll ever know since i don't know korean.

The start of s2 is absolute turbo garbage trash tier and I don't want to read this any longer. Fuck why are there so many dumb expositions and info dumps between characters who already know everything about the topics? Is this the part where the artist just went off and did their own thing? I still managed to somehow read it until the hiatus point but like why. I actually enjoyed it up until s2!!!

At least the work does some interesting things with the System, the psychological side effects that happens to humans in stressful situations and the intrinsic Rewards they get from seeing numbers go up from doing anything the shady System tells them to do, etc. And the art is great too.
