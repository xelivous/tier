+++
title = "ダィテス領攻防記"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["josei"]
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-04-16
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=137480"
raw = "https://www.alphapolis.co.jp/manga/official/725000154"
md = "https://mangadex.org/title/cb3ef946-c718-49e1-8f71-15014e15254d/daites-ryou-koubouki"
#bw = ""

[chapters]
released = 36
read = 11

[lists]
recommend = ""
+++

Protag is a fujoshi who reincarnated as a duke's daughter and introduces tons of scientific knowledge from japan and greatly advances her territory by leaps and bounds, until one day she gets forcibly married to a prince who failed the battle for succession. She immediately forks up that she was reincarnated to the prince during their first night together after they get married, and then he basically forces himself on her after he reads one of her self-made BL books out loud (basically rape).

<!--more-->

She invented so much fucking technology that you would think she literally just brought wikipedia back with her and she was an ultra supergenius in her past life. In actuality she *just* has photographic memory and *conveniently* saw tons of detailed blueprints and schematics in her life to hand off to all of the people who can conveniently build all of this shit. And then when her prince husband (who raped her btw) cheats on her with his right-hand man in hot steamy yaoi sex she's all over that shit and hootin' and hollerin' like it's the best thing ever with spit flying out of her mouth like a rabid dog. 

The series is ridiculous and constantly shoves in modern technology everywhere only to have the native inhabitants of the world react to the absurd technology she reproduced, and somehow managed to keep confined within the borders of her duchy without ever letting any of it leak out.
