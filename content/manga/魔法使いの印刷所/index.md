+++
title = "魔法使いの印刷所"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = []
languages = []
schedules = []
lastmod = 2023-09-24
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=145292"
#raw = ""
md = "https://mangadex.org/title/c1de44c5-0efc-4eb5-9117-c791774eb8a8/mahoutsukai-no-insatsujo"
bw = "https://bookwalker.jp/series/162339"

[chapters]
released = 37
read = 0

[lists]
recommend = ""
+++
