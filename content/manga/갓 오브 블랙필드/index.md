+++
title = "갓 오브 블랙필드"
title_en = "God of Blackfield"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=164892"
raw = "https://page.kakao.com/home?seriesId=54498159"
md = "https://mangadex.org/title/1a008e7e-0885-4830-92c6-16b3bbe4f5d4/god-of-blackfield"

[chapters]
released = 0
read = 145

[lists]
recommend = "D"
+++

Protag is a Cool Hardcore Military Badass who kills his enemies without mercy and is the strongest cooldude ever, but he gets betrayed and dies, and then reincarnates as a generic bullied korean kid who killed himself. This hardcore military badass then slowly works out all of the problems around him, and slowly the problems increase in scope until it's multi-national world-scale world-war level while the protag is like "19". The romance is unbearable since the protag basically gets new additions to his harem constantly and occasionally goes out on dates with tons of women which doesn't really add anything to the story at all other than more damsels in distress he has to save later on, which he does because he's such a badass. I can only recommend this if you want to see endless action/fight scenes and increasingly brainlet-tier political nonsense where the protag will always come out on top and the villains are always comically evil/dumb.

<!--more-->

