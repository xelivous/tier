+++
title = "俺んちに来た女騎士と"
title_en = ""
categories = ["isekai", "reverse-isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["every other week"]
lastmod = 2024-10-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149691"
raw = "https://www.mangabox.me/reader/78226/episodes/"
md = "https://mangadex.org/title/20e4c17d-3741-46dd-a8bb-cbc132ee52d1/orenchi-ni-kita-onna-kishi-to-inakagurashi-surukotoninatta-ken"
bw = "https://bookwalker.jp/series/189815"

[chapters]
released = 139
read = 136

[lists]
recommend = "B"
+++

The protagonist's (M32) parents died so he quits being a salaryman and moves out to the countryside to inherit their farm, and spends years honing his craft ryanmaxxing. When all of a sudden one day a random blond knight (F18) straight out of medieval times opens up his house's door and asks if she can take shelter while speaking nonsense about another world, yet for some reason can perfectly speak japanese. He lets her stay the night after considering the state she's in and how frantic she's persisting, but who is she and is she pranking him??

<!--more-->

So ultimately this work is just the wish fulfillment of a japanese salaryman to break free of it all and just live the happy farm life with his cute foreign blond gf half his age, and the early chapters are kind of just mediocre at best as a result, but I do feel like it has merits the longer it goes on and evolves past that. A large portion of work after the early chapters dives into what the JP government might do once an isekai'd person might appear there, takes them into quarantine, does a ton of testing on them, etc. It's actually a fairly interesting angle for a work like this to take and it really goes all in on it. However at the same time parts of the government intervention at the beginning of the story are fairly unrealistic and downright terribly done in some respects, notably that they essentially torture her psychologically for her entire hospital visit.

I'd say one of the notable parts about this work is that it's largely a more adult-oriented romance that has a protagonist with an Ex (that even still regularly appears in the story). The romance is a little childish at times and maybe a little bit too much of ossan wish fulfillment but still kind of cute overall. Though the ex is so much hotter without the ponytail...

Later on it even starts to dive deeply into the original world of the knight, and simultaneously stretches plotlines between the two worlds back and forth while the protagonist has his happy homelife with a wife half his age. It semi-regularly switches perspectives every few chapters to try and flesh out what a certain character is thinking about and their thoughts on the situations overall which gives it a nice pace overall. It even manages to mix in a small story in the first-chapter-pages later on.

The character art is a little awkward at times but nothing particularly egregious, and there's a good fair amount of effort put into the background without too many blank white voids inserted that overall I think the art is pretty good.

It's a fairly enjoyable slow burn of a farming manga with some romance and isekai elements on the side, and I do enjoy reading it. It's unique enough in the space that I feel like it's worth reading.
