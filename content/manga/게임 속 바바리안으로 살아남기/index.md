+++
title = "게임 속 바바리안으로 살아남기"
title_en = "Surviving the Game as a Barbarian"
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2023-12-23
tags = [
    "male-protagonist",
    "warrior-protagonist",
    "intelligent-protagonist",
    "traditional-roguelike",
]

[links]
mu = "https://www.mangaupdates.com/series/ly5au35/surviving-the-game-as-a-barbarian"
raw = "https://comic.naver.com/webtoon/list?titleId=808482"
wt = "https://www.webtoons.com/en/fantasy/surviving-the-game-as-a-barbarian/list?title_no=5515"

[chapters]
released = 44
read = 44

[lists]
recommend = "A"
+++

Protagonist is a gamer who has gamed so hard he is now bored of just about every game, when he finds a random indie pixel art traditional roguelike (with a party system) in the time of WindowsXP, and gets utterly addicted to it despite it only being available in english and him not understanding english at all and thus needs to translate everything into the game into korean. There were also no strategy guides online for it since it was universally panned so the protagonist spends literally 9 years grinding/strategizing the game until he finally gets close to beating it. However right before the door to the final boss he gets isekai'd into the game as the class he was playing as, but as a newbie fresh and almost about to start the game. There's even another guy here who apparently got isekai'd into the game but is immediately beheaded as soon as it's found out that he is possessed by an "evil spirit", so the protag has to lay low as much as possible and blend in.

<!--more-->

There's a lot of aspects of the work where it feels like the author doesn't understand all too much about games, and then also does actually understand a ton about games, maybe it's a weird translation issue? Either way the work is a very entertaining take on a fantasy dungeon exploring series based on a traditional roguelike. I don't even really want to explain or spoil anything about it since i just think the entire thing is cool and would rec trying out the first few chapters if you're at all interested in a dude desperately trying to survive in a realistic dungeon that is trying to kill him, all while getting discriminated against for being an uncivilized unintelligent barbarian, with all of his knowledge from basically full clearing the game with multiple different characters.
