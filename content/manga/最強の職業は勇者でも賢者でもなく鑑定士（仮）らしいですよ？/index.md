+++
title = "最強の職業は勇者でも賢者でもなく鑑定士（仮）らしいですよ？"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-03
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144201"
raw = "https://www.alphapolis.co.jp/manga/official/648000181"
md = "https://mangadex.org/title/22104/saikyou-no-shokugyou-wa-yuusha-demo-kenja-demo-naku-kanteishi-kari-rashii-desu-yo"
bw = "https://bookwalker.jp/series/156494/"

[chapters]
released = 54
read = 41

[lists]
recommend = "F"
+++

Protag is late to class and opens up his classroom door but immediately gets teleported to the middle of nowhere in an isekai. He's so in the middle of nowhere that there is no living creatures at all even after walking for days barely surviving from the water/bread he conveniently had in his bag at the time of teleportation. However after multiple days he finally sees a rabbit that almost gores him with its horn immediately after encountering it, and apparently the protag has the totally epic standard skill of "you can see status info of things like a game haha lol".

<!--more-->

"you should join a party to increase your survivability. How about this B rank party that is fairly capable?"  
"No i don't want to trouble anybody."  
"Okay, i know just what you need then, *slaves*."  
it keeps happening pls why. 

love how the protag keeps going on about how deplorable it is to have slaves but also reasonable and totally the most rational decision, instead of like, you know, joining a normal party with normal people on equal grounds. Especially when he has the fucking Contract skill that would force people to be bound to be on equal/fair terms if both parties wished it. At the very least the constant Macho Man fanboying is kind of interesting, as is all of the oneshota. too bad the protag is an extreme brainlet and he just constantly gets given everything he needs without really any work thanks to god just inventing random skills for him on the spot constantly.

One of the biggest problems with this work is the art; While it is relatively competent there's a massive issue of sameface and a large majority of the work is distinguishing certain people by their facial features which is quite literally impossible when everybody looks the same. Well aside from the big furry wolfman who is just about the only distinct entity in the work outside of general clothing differences. The girls are cute though at least even if they all look the same.
