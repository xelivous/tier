+++
title = "계모인데 딸이 너무 귀여워"
title_en = "Not-Sew-Wicked Stepmom"
categories = ["isekai", "reincarnation"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-10-02
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=179566"
raw = "https://page.kakao.com/home?seriesId=56714856"
md = "https://mangadex.org/title/e618f14a-58c0-448c-9eb7-ef30d2422f6f/not-sew-wicked-stepmom"

[chapters]
released = 99
read = 70

[lists]
recommend = "C"
+++

It's a cute romance series where the protag tries to Fix the male lead that has mommy issues, and the general relationship between him and his daughter, while she slowly learns to Love.

<!--more-->

