+++
title = "転生貴族の異世界冒険録"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147856"
raw = "https://magcomi.com/episode/10834108156766450183"
md = "https://mangadex.org/title/28612/tensei-kizoku-no-isekai-boukenroku-jichou-wo-shiranai-kamigami-no-shito"
bw = "https://bookwalker.jp/series/181916/"

[chapters]
released = 54
read = 54

[lists]
recommend = "D"
+++

Protag gets stabbed by a deranged dude while protecting some cute girls in japan and wakes up as a noble shounen in medieval society, filled with magic and monsters and even status windows! He immediately sets out to learn magic as soon as he knows it exists and trains in secret for years up until he can get appraised at the church, whereupon he meets like every god simultaneously and explains to him that he's a super cool dude who should make the world a better place for them so he gets ultra powered up by all of the gods to be the very best there ever was. Even even has a 10000x exp/status multiplier so he can be super ultra godtier broken as a shota and nothing will ever get in the way of his godlike duties!!!!

<!--more-->

Okay so like the average top-level magician has an MP count of 10k right, and the protagonist gets well over 94 million shortly into the work which is how you know the author has absolutely no inclination to write in any kind of struggles for the protag. He even gets basically 2 princesses engaged to him before he even hit puberty and starts building up a harem. The work then kind of goes off the rails a bit with an epic training montage of him getting older and then reverting back to his younger appearance again for Reasons which was kind of awkward. If anything that entire arc could've been left for much later since it has basically no plot relevance until way later down the line and they can just hyperbolic time chamber him at literally any moment anyways.

The paraplegic foxgirls were an interesting addition to the cast; it's fairly rare to see anime girls outright missing limbs excluding in katawa shoujo, so that in itself is fairly unique, aside from the fact that he almost immediately heals their missing limbs because he's literally god incarnate. All of the forced drama with this recurring noble dude is also just incredibly brainlet-tier as well.
