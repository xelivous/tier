+++
title = "よくわからないけれど異世界に転生していたようです"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-19
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158385"
raw = "https://pocket.shonenmagazine.com/episode/10834108156721092932"
md = "https://mangadex.org/title/42868/i-don-t-really-get-it-but-it-looks-like-i-was-reincarnated-in-another-world"
bw = "https://bookwalker.jp/series/243518/"

[chapters]
released = 111
read = 57

[lists]
recommend = "D"
+++

Protag is on getting transported on a wagon to the orphanage director's house when she gets attacked by bandits, falls down a cliff etc, everybody else is dead except for her, when she awakens to her past life's memories where she was some 30 year old male scientist in japan. As a result she uses her big intj brain and past life's memories to enhance her chance of survival until she makes it out of the forest. And just as she's about to die from hunger/thirst she finds out she has ultra broken skills/stats for seemingly no reason and can just create anything out of thin air at no cost and no restrictions!!

<!--more-->

tldr Big booba braid girl masturbates excessively while giving out drugs to random peasants. It's not particularly well-written, the plot progression is kind of wack, and it's mostly just excessive wish fulfillment on top of the normal oretueeee. ~~maybe if she was twinbraid it would be a C haha lol~~. The よくわからないけれど refers to the author not knowing what to write tbh. It's largely a cooking manga with excessive over the top "oh my god this food is soooo gooood aaaa i'm cumming" panels over and over. The only redeeming part about this work is the art, which is wasted on the plot, and has some kind of weird deformed-ness to it at times. I'd rather read twinbraid porn than read this.
