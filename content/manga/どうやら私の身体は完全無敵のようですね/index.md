+++
title = "どうやら私の身体は完全無敵のようですね"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-24
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149486"
raw = "https://comic-walker.com/contents/detail/KDCW_FS01200196010000_68"
md = "https://mangadex.org/title/29166/douyara-watashi-no-karada-wa-kanzen-muteki-no-you-desu-ne"
bw = "https://bookwalker.jp/series/168951/"

[chapters]
released = 60
read = 48

[lists]
recommend = "C"
+++

Protag is sickly and wishes for a body that won't be as sickly in her next life, which of course god grants in a monkey paw-esque way where she becomes ultra broken powerful in her reincarnated life. As a result she constantly struggles to control her powers while living day to day life, and gets wrapped up in tons of weird schenanigans while trying to not stand out.

<!--more-->

The few ecchi scenes where they're naked in the shower is kind of creepy esp when they're like, 10 years old.

has same energy has noukin with an overpowered white haired protagonist that doesn't want to stand out but can't help but standing out, with slight yuri vibes. very wareyacore but i'm not sure if i'd rec it to other people.
