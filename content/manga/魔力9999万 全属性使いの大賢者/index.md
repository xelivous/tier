+++
title = "魔力9999万 全属性使いの大賢者"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["original"]
languages = ["japanese"]
schedules = ["weekly"]
lastmod = 2025-01-12
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/2elqfd0"
raw = "https://hykecomic.com/titles/魔力9999万%E3%80%80全属性使いの大賢者/"
md = "https://mangadex.org/title/4017de66-fe69-4e46-baa8-787c399de6d2/magic-level-99990000-all-attribute-great-sage"
#bw = ""

[chapters]
released = 104
read = 80

[lists]
recommend = "F"
+++

Protag is the epic magedude who is cooler than everybody in existence and the pinnacle of chads with the mostest magical power imaginable in a world that was invaded by gate outbreaks, but ends up dying after defeating the endboss of the gates. Only to wake up in the body of some random dude 50 years in the future with the exact same given name and all of his memories/abilities/magic intact. The world is now at peace after he saved it, except it's not since all of a sudden the gate outbreaks are starting back up again conveniently as soon as he reincarnated and he has to save the world yet again because the level of magic in this peaceful era is absolutely abysmal causing him to be even more of an epic chad with exponentially more coolness than everybody else.

<!--more-->

Unbelievably horrible work that somehow makes all of the korean gate outbreak slop look great in comparison. How are japanese-native webtoons so shit? The family with water magic are blue, the family with wind magic are green, the comically evil dude with dark magic is black and also literally has the name Kuroi Akuto. Every single character is either brain damaged, comically evil and over the top, or glazing the protagonist; sometimes all 3 at once. The only redeeming aspect of this work is that the heroines are kind of cute, and that the protag got confessed to by a cute twinbraid girl for like a single chapter (only for her to disappear from the story entirely after that). I am unable to say anything positive about this work, and i'd rather not write a full on rage essay about this so i'll stop here. Don't read this. Please. I wish I didn't read this.
