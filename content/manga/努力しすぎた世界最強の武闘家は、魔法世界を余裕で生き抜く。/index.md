+++
title = "努力しすぎた世界最強の武闘家は、魔法世界を余裕で生き抜く。"
title_en = ""
statuses = ["completed"]
demographics = ["seinen"]
furigana = ["none"]
categories = ["reincarnation", "isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-22
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151624"
raw = "http://seiga.nicovideo.jp/comic/35630"
md = "https://mangadex.org/title/29402/doryoku-shisugita-sekai-saikyou-no-butouka-ha-mahou-sekai-wo-yoyuu-de-ikinuku"
bw = "https://bookwalker.jp/series/179497/"

[chapters]
released = 75
read = 28

[lists]
recommend = "B"
+++

Protag lived as a martial artist in his previous life who really really loved fantasy anime with magic, and luckily managed to reincarnate into a world with magic in it. However for some reason the protag just has no magic power due to his parents having so little to begin with, and thus they like abandon him in the forest and basically leave him to die. However he has meme strength and just blasts away the gigantic monster who was going to eat him and gets taken in by a grand sorcerer, who then tries to train the protag as a successor. Except the protag can't use magic, so he ends up mimicking the magic attacks by just punching really hard, and gets tricked into believing that if he simply trains super hard then one day he'll finally be a grand sorcerer too and can use more magic than simply making the air move fast by punching.

<!--more-->

This is a comedy where the protag is an extreme brainlet. Main highlight of this work is the heroines, and simply how optimistic the protag is in every aspect of life. The age regression plotline is kind of weirdge.
