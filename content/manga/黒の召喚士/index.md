+++
title = "黒の召喚士"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-21
tags = [
    "amnesia-protag",
    "engages-in-slavery",
    "appraisal-skill",
    "skill-window-spam",
    "battle-maniac-protagonist",
    "frequent-battles",
    "villain-of-the-week",
    "incompetent-hero",
    "mentor-protagonist",
    "goddess-as-heroine",
    "letter-rank-adventurer-guild",
    "adventurer-guild",
    "male-protagonist",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146577"
raw = "https://over-lap.co.jp/Form/Product/ProductDetail.aspx?pid=ZG0017"
md = "https://mangadex.org/title/23042/kuro-no-shoukanshi"
bw = "https://bookwalker.jp/series/164413/"

[chapters]
released = 127
read = 7

[lists]
recommend = "C"
+++

Protagonist wakes up in an isekai without any of his memories, however his previous self chose all of the skills for himself and ended up getting bonus skills at the cost of his memories thinking that his future self would know what to do even without his memories. The protag is so chad he even convinced the reincarnation goddess to come down with him and join him on his journey in the isekai, although he has no memories of it.

<!--more-->

Protagonist just kind of goes around fighting everything he can because he really loves to fight, and you just get inundated with epic skill spam constantly. One of the first things he does is go get a cute elf slave and even spends tons of his skill points for her to be free from her curse. He even gets a random booba demon pretty soon afterwards as well. However the villains are just extremely meme and too prevalent; basically meme villain of the week series. Protag and supporting characters are interesting enough though at least if you can tolerate all of the battles. Kind of a series to just turn your brain off and enjoy meme fights.
