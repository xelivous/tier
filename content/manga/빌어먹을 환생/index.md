+++
title = "빌어먹을 환생"
title_en = "Damn Reincarnation"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-10-02
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/j7x0kkd"
raw = "https://page.kakao.com/home?seriesId=59617058"
md = "https://mangadex.org/title/6e769716-df1e-4d66-89cc-32f3b46e17dc/damn-reincarnation"

[chapters]
released = 69
read = 69

[lists]
recommend = "C"
+++

The protagonist is a member of the hero's party on their quest to slay the Demon King, but unfortunately dies just before they meet the final boss while taking a blow for the Hero. He then reincarnates 300 years later as the hero's descendent and tries to figure out what happened after he died and where everybody ended up while training his body as if he was Asta from black clover. 

<!--more-->

The girls in this are hot ngl; Need to check out the artist's other works. Or at least i've tried finding the artist's like twitter or whatever but his name is so generic that I can't find anything and it's even harder when I don't understand korean.
