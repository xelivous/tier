+++
title = "Re:Monster"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
sources = []
furigana = []
categories = []
languages = ["japanese"]
schedules = []
lastmod = 2023-01-02
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=109745"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/609000058"
md = "https://mangadex.org/title/12171/re-monster"
bw = "https://bookwalker.jp/series/62182/"

[chapters]
released = 87
read = 86

[lists]
recommend = "D"
+++

Dude gets murdered by a yandere in his previous life and wakes up as a goblin, and he even gets to keep his innate ability from his previous life for some reason which allows him to gain the abilities of anything he eats no matter what it is! Which means that as long as he continues to exist he just gets more and more broken OP as time goes on with basically no downsides! The rank up / evolving system is relatively fun although this series is mostly just a long string of stat/skill spam, which to be honest I just almost fully skip over reading anytime i pops up and gloss over by knowing that he's getting even more OP every time. The weirdest part though is that this story is almost exclusively told through narration as if it was 1:1 adapted from the novel instead of reworking it to work as a manga, so it's basically just a novel picture book.

<!--more-->

I feel like one of the weirder aspects of this series is the 5 human women he takes in and they just kind of chill in the goblin cave without ever really trying to escape, and eventually just become part of his harem. He also just basically calls them by their characteristic instead of their names until really late into the story. There's also the fact that the protag just plops his dopplegangers everywhere leaving them to endlessly toil away at menial labor for all eternity like living in business cards or just chilling in clothing to fetch information. which feels kind of weird if you think about it for more than a second since a clone would normally have the same thoughts/aspirations as the main body, and most people wouldn't want to be trapped like that for eternity.

The princess is the best part about this series though tbh.

Later chapters (80+) start to get a little tedious/boring to read and I found myself not caring too much about the story at that point, but it's at least commendable to have a monthly series reach that many chapters in the first place.
