+++
title = "FFF급 관심용사"
title_en = "FFF-Class Trash Hero"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2024-07-26
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158360"
raw = "https://page.kakao.com/home?seriesId=53665498"
md = "https://mangadex.org/title/1a4bc1ca-2ed8-425b-8235-d9372a446d93/fff-class-trashero"

[chapters]
released = 172
read = 172

[lists]
recommend = "B"
+++

So basically the protagonist gets thrown into an isekai and finally after 10 long years of fighting along with his antagonistic asshole "companions" he finally manages to defeate the demon lord. However right before defeating the demon lord he ended up killing his companions because they abused him enough times to tip him over the edge, and as a result he got "graded" after everything was done and was deemed a failure, doomed to repeat the isekai from the start again as if nothing ever happened. He then sets out to try and free himself from this isekai once more, but he's so sociopathic and broken from his first playthrough that is very unlikely!

<!--more-->

This is the single most sociopathic protag to exist in the isekaiverse. The story is off the rails from the start and has no intention of ever going back onto the rails. It's also basically a timeloop series. There's a section partways into the work where it will just shove in western memes (like starwars/etc) constantly but it doesn't last all too long overall. It's a relatively fun work that explores the absurdity of how Wrong a traditional "isekai" story can go when the protagonist is incentivized to kill anything and everything to get more experience, with friendly NPCs with high levels being the main source of lots of EXP. 

Ch18 is kino poggers. Ch101 is also peak fiction. I feel like it's recommendable to most people although it does have tons of skill spam wankery and an excessively sociopathic protag which might turn off some people.

Also the official english title of this makes it seem like something else; I originally parsed it as "trash ero" but it's actually just "trash hero" and there's nothing particularly ero in this work, so it's a pretty bad title all things considered. Although apparently the original novel has sex scenes/etc which aren't in the webtoon.
