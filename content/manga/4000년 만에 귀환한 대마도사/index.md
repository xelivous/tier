+++
title = "4000년 만에 귀환한 대마도사"
title_en = "The Archmage Returns After 4000 Years"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2022-12-26
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166376"
md = "https://mangadex.org/title/ff14c29a-3456-4047-828d-10c9671cac4c/the-archmage-returns-after-4000-years"
raw = "https://webtoon.kakao.com/content/4000년-만에-귀환한-대마도사/2385"

[chapters]
released = 151
read = 129

[lists]
recommend = "D"
+++

The start of this manga is actual F tier, but ch104/105 are pretty good for whatever that's worth (lol). Protag gets locked in stasis for 4000 years and for some reason gets reincarnated into the body of a dude who killed himself since he was so weak, and protag just buttblasts his weakness away and starts becoming as strong as he was before. His goal is to kill the gods that locked him into stasis and wrench back control over the destiny of humans from them. Protag regularly gets the macguffin needed to power up substantially every time he needs it, and he just bitchslaps everybody with very little to no setbacks while going on his journey and everybody revereing him for being so cool and eventually he reveals his true identity to some people and they revere him even more.

<!--more-->
