+++
title = "獣医さんのお仕事in異世界"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=141307"
raw = "https://www.alphapolis.co.jp/manga/official/266000135"
md = "https://mangadex.org/title/48592786-4c75-43ed-b697-59f8beb79bc4/jui-san-no-oshigoto-in-isekai"
bw = "https://bookwalker.jp/series/108463"

[chapters]
released = 68
read = 51

[lists]
recommend = "B"
+++

The protagonist is a veterinarian who regularly goes out into the field to try and save as many animals as possible, when all of a sudden he gets sucked into an isekai while out driving. The otherworlders were looking for someone to save them and their magical beasts / etc, and ended up summoning the protagonist due to his desire to save as many beasts/people as possible.

<!--more-->

The protagonist didn't get any power when transferring over since he was summoned by humans, and the only thing he has going for him is his occupation and knowledge, which to be fair is PHD-level veterinarian knowledge, biology, etc. I feel like the story should've focused a little more on advancing the technology directly instead of going out on Epic shounen-battle-tier fights constantly. Sera is a terrible character who ruins every scene she is in but ch27 is good and honestly that dude is the best character in the entire series so it's nice he's added somewhat shortly after sera. If you want a battle shounen that is more like batman than superman then i guess this one is a rec.
