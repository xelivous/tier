+++
title = "황비님의 비밀수업"
title_en = "The Secret Life of Empress Isana"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=157107"
raw = "https://page.kakao.com/link/54065772"
md = "https://mangadex.org/title/a8b7e935-66ef-4577-ad42-66078b91da0b/the-secret-life-of-empress-isana"

[chapters]
released = 100
read = 73

[lists]
recommend = "C"
+++

The art at the beginning of this series is kind of extremely bad (but actually improves quite a lot by the time of s2), and the general setting of this is kind of cringe what with the whole basically dominatrix shoujo lead. However it kind of gets endearing over time up until the end of s1 which actually wraps up in a relatively satisfactory way that could be the "ending" of most normal series. I actually do somewhat appreciate that the protag {{% spoiler %}}even gets kids with the male lead{{% /spoiler %}}. Then S2 starts and the author decides to do a kind of strange what-if scenario using timey-wimey bullshit.

<!--more-->
