+++
title = "먹을수록 강해짐"
title_en = "I Get Stronger the More I Eat"
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=182900"
raw = "https://webtoon.kakao.com/content/먹을수록-강해짐/2829"
md = "https://mangadex.org/title/513c1c8f-bf6a-4fe0-a9d3-0c26dfb5b5f0/i-grow-stronger-by-eating"
wt = "https://www.webtoons.com/en/action/i-get-stronger-the-more-i-eat/list?title_no=3199"

[chapters]
released = 80
read = 53

[lists]
recommend = "F"
+++

Okay so basically the protag is a villianous asshole who doesn't give a shit about anything other than eating anything possible to powerup, then gets isekai'd and gets to power up even more without anybody being able to challenge him since in the isekai he's immune to everything but nothing is immune to him (lol). His eventual goal is to eventually head back to earth and rule over everything as the peak of all life after absorbing every single creature's abilities.

<!--more-->

