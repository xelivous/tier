+++
title = "회귀했는데 FFF급 부두술사"
title_en = "The Voodooist's Wrathful Return"
categories = ["reincarnation", "game"]
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["korean"]
schedules = []
lastmod = 2024-06-05
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/4nv68x4/the-voodooist-s-wrathful-return"
raw = "https://page.kakao.com/content/61625443"
md = "https://mangadex.org/title/1be53773-752f-4dc7-b540-03bb99b46cd7/the-voodooist-s-wrathful-return"
#wt = ""

[chapters]
released = 77
read = 58

[lists]
recommend = "D"
+++

The Protagonist is one of the top players of a VRMMO that he played for 10+ years after neglecting everything in his real life including disappointing his parents, and he finally got the opportunity to participate in one of the main quests of the storyline after all of those years and reaching one of the top ranks. However shortly before defeating the boss of that questline he gets betrayed/PK'd by one of the "righteous chads" in his party who everbody thought was a super good benevolent person but turned out to be an asshole who was just using them all in order to clear the quest. Yet for some reason right before the protag dies he gets offered an opportunity by the system to take revenge, gets sent back in time 10 years before the game launched, and the system is popping up IRL coercing him to play the game or he'll die IRL. To make matters worse now if he dies in-game he will also die IRL as a result of this bugged system, but to make up for it he has turned into what is essentially the final boss of the game.

<!--more-->

The author doesn't really understand even the most fundamental basics of mmos or game systems. This is little more than yet another shounen manwha power fantasy work, although the protagonist focusing more on DoT/poison is at least slightly different enough compared to the 99999th spear master. Although mostly just an opportunity to have the protagonist be naked for most of the time and show off his abs in 90% of the panels so it's not really all that different in the end. Protagonist just flies through the game with his knowledge of future events dabbing on everybody with his knowledge while trying to figure out how to rid himself of the system's effects of him dieing IRL if he were to ever die in-game. Only really noteworthy part is the protagonist trying to invest into cryptocurrency, immediately getting burned/bogged on it, then swearing off crypto for good despite him knowing it will grow exponentially in the coming years.

There's other better VRMMO works out there that you can read instead but I could maybe recommend this to you if you just want to look at a dude's abs 90% of the time while he just throws poison darts at people occasionally and stacks his skills up to x999999 times in a second because that's how skill cooldowns work. The official translation has some mistakes here and there, and is otherwise kind of mediocre; didn't look at the fan translations.