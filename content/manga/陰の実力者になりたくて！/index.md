+++
title = "陰の実力者になりたくて！"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
categories = ["isekai", "reincarnation"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152254"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04200967010000_68/"
md = "https://mangadex.org/title/33085/kage-no-jitsuryokusha-ni-naritakute"
bw = "https://bookwalker.jp/series/212232/"

[chapters]
released = 32
read = 26

[lists]
recommend = "A"
+++

If the style of comedy works for you, then this is 神. Otherwise it might be a bad rec. I do feel like most people would enjoy it though.

<!--more-->
