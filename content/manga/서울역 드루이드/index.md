+++
title = "서울역 드루이드"
title_en = "The Druid of Seoul Station"
categories = ["post-isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=183386"
raw = "https://comic.naver.com/webtoon/list?titleId=773476"
md = "https://mangadex.org/title/946ff973-95b5-4f57-8e81-7c4674ea50dc/the-druid-of-seoul-station"
wt = "https://www.webtoons.com/en/action/the-druid-of-seoul-station/list?title_no=3453"

[chapters]
released = 82
read = 39

[lists]
recommend = "F"
+++

Protag gets transported to some crazy ass world, lives there for like literally an eternity and becomes king of the animal kingdom basically, and then eventually returns to a fucked up broken earth and he has to deal with a bunch of brainlets who don't believe him for some reason. Another situation where the protag is put into an arbitrary F rank because they're brainlets who don't have basic power measurement tests, and then you spend countless chapters reading about how people are surprised how powerful the protag is despite being F rank. Honestly the worst kind of OP protag story.

<!--more-->
