+++
title = "격투몬스터"
title_en = "Fighting Monster"
categories = ["isekai", "reverse-isekai", "game"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = []
sources = []
languages = ["korean"]
schedules = []
lastmod = 2024-06-16
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/gc29i5u/fighting-monster"
#raw = ""
#md = ""
#bw = ""

[chapters]
released = 48
read = 48

[lists]
recommend = "D"
+++

The protagonist is a cocky asshole of an MMA champion who is slowly rising up the ranks and increasing in weight classes and think's he's the best of the best. Simultaneously a large game corporation is mad that Nintendo is hogging all of the sales and is developing a VR Fighting Game/world and reaches out to the protagonist to take part in training the game's data, to which he refuses since he only cares about fighting and not games.

<!--more-->

Something like 80% of the work is about MMA, and the other 20% is about the game world subplot. Only read this if you want to essentially just watch dudes beat them up like an MMA PPV. There is little to this work beyond that, the isekai elements happen extremely late into the work, and the work gets axed before it ever does anything with it really. Also it starts having more and more blood/gore as the series goes on.
