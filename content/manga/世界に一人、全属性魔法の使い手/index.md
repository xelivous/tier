+++
title = "世界に一人、全属性魔法の使い手"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["every other week"]
lastmod = 2024-12-20
tags = [
    "male-protagonist",
    "reincarnated-as-baby",
    "black-company-worker",
    "run-over-by-truck",
    "died-while-saving-child",
    "school-arc",
    "immeasurable-power-level",
    "falsely-put-into-lowest-rank",
]

[links]
mu = "https://www.mangaupdates.com/series/115zryh"
raw = "https://tonarinoyj.jp/episode/316190247028743837"
md = "https://mangadex.org/title/7a47c571-7567-471d-bc65-ff942e3186f8/sekai-ni-hitori-zenzokusei-mahou-no-tsukaite"
bw = "https://bookwalker.jp/series/415638"

[chapters]
released = 47
read = 36

[lists]
recommend = "D"
+++

Protag overworks himself to death in a black company, gets run over a truck while saving a child, and gets told by god that the reason why his life was so unfortunate and filled with unpleasing things is not the result of him being a failure that makes bad decisions but because there was an oopsie-woopsie and he was born in the wrong world, and god will make it all better and reincarnate him into the world that he belongs with super-duper epic magical powers instead like it always meant to be! However upon being reincarnated and his magic getting appraised, his super epic magical powers has all of the colors combine and become black due to additive color space and they mistake him for being a demon!! However since this is the ideal world for him everything works out and nothing will ever go wrong for him and he gets adopted by parents who will do everything to raise him right and pamper him and give him everything.

<!--more-->

Ugh. "haha lol he has every magic attribute and also his power is so strong he overflows the magic meter and is assumed to have 0 magic power instead despite breaking the machine!!"

The protag is playing on easy mode with a near infinite amount of magic power and all attributes while virtually everybody else has a single element with way less magic power. The least the author could do was make him have a tiny amount of magic power but make up for it in versatility or something but no, he hides all of his powers outside of "wind" and just brute forces every fight with his insane magic capacity. Most of the work is the protag acting as a teacher/etc to his "friends" off-screen to improve their standing since of course he was put into "F" class for breaking all of the machines with his overwhelming unprecedented power and being the disciple of one of the strongest mages alive who has never taken a disciple before. Of course they would do a haunted house and everybody would be enthused and surprised at the suggestion since it has never been done before in this isekai and the protag is so smart and cool for suggesting it.

Please. Stop.
