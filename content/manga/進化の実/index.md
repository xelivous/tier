+++
title = "進化の実"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147435"
raw = "http://seiga.nicovideo.jp/comic/34133"
md = "https://mangadex.org/title/24123/shinka-no-mi"
bw = "https://bookwalker.jp/series/152447/"

[chapters]
released = 30.3
read = 26

[lists]
recommend = "F"
+++

<!--more-->
