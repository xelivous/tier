+++
title = "２９歳独身は異世界で自由に生きた……かった"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-01-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153827"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01200814010000_68/"
md = "https://mangadex.org/title/36514/29-sai-dokushin-wa-isekai-de-jiyuu-ni-ikita-katta"
bw = "https://bookwalker.jp/series/212477/"

[chapters]
released = 33.1
read = 27

[lists]
recommend = "F"
+++

Protag is a fat salaryman who goes to sleep and wakes up in an isekai as an ikemen and has a weird conversation infodump with god about the systems and how everything is just like a game and you can level up and get skills!!! He's also no longer 29 but 19!!! For some reason a random cute girl immediately falls for the protag and basically stalks him, and then confesses to him without them even knowing each other. She also uses and emotionally manipulates the (probably virgin) protag while being absolutely useless. She also then proceeds to literally drug and rape him. And the protag forgives her because she's a little cute..... It's like, why. Actually why. The work also tends to dip into those ecchi/sex scenes fairly frequently which is kind of cringe.

<!--more-->

The story starts to pick up a bit when it starts heading into the truth beyond Heroes in this world, and then it heads back into memeville almost immediately afterwards. Sparing a cute girl assassin and then later buying her as a slave who then craves for his dick is so far down the scale that i don't even know what to say at this point. And then another princess literally tries to forcibly rape him. Seriously what the fuck is this series.

I feel like there's a lot of other better series you would be better off reading instead of this.
