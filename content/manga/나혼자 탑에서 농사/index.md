+++
title = "나혼자 탑에서 농사"
title_en = "The Top Dungeon Farmer"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-03
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/fdbhhx7/the-top-dungeon-farmer"
raw = "https://series.naver.com/comic/detail.series?productNo=9632355"
md = "https://mangadex.org/title/f461e344-2385-46b5-83b8-8776f5ea65d0/solo-farming-in-the-tower"
wt = "https://www.webtoons.com/en/fantasy/the-top-dungeon-farmer/list?title_no=5656"

[chapters]
released = 50
read = 24

[lists]
recommend = "D"
+++

The world has towers spawn all across it in every major city on earth, and humans randomly get whisked away inside of it unable to leave until they reach certain checkpoints in the tower. However returnees can also come back with entrance tickets, which they then sell for astronomical prices for people wishing to head into the tower themselves. The protagonist desperately wants to head into the tower to hopefully turn his life around and get some of the untold riches within, but he'll likely have to save up for many years and likely will never reach his goal before he dies as he comes from a poor family. Yet one day a portal randomly opens up behind him, and he jumps inside of it without thinking, only to end up stuck in a random cave with nothing else around outside of the groceries he was carrying at the time. Unfortunately for him he entered the tower in an irregular fashion and is now essentially stuck inside until he can figure out a way down, all while farming to survive with the smaller critters around him.

<!--more-->

This is kind of ryancore in that it's literally just endless farming and almost nothing else, but i'm not sure it's even good ryancore since the protagonist is literally just locked in a small area while planting crops that grow really quickly.
