+++
title = "물고기로 살아남기"
title_en = "Surviving as a Fish"
categories = ["reincarnation", "game"]
demographics = ["seinen"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-12-25
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/gkgv4rz/surviving-as-a-fish"
raw = "https://comic.naver.com/webtoon/list?titleId=793887"
md = "https://mangadex.org/title/530df556-6f87-4bf9-a7d3-1f2fcfb86558/surviving-as-a-fish"
wt = "https://www.webtoons.com/en/fantasy/surviving-as-a-fish/list?title_no=4634"

[chapters]
released = 60
read = 60

[lists]
recommend = "C"
+++

The president of totally-not-samsung is dedicated to his job and living the life, when all of a sudden one day his business partner from ages ago comes by to kill him off since he knows tons of dirty secrets that he doesn't want found out. As a result he gets thrown into the lake and left to die however just before losing concious a random voice calls out to him asking if he wants to reincarnate, to which he responds yes, and gets offered to reincarnate into various species of fish. However he took too long to decide and gets randomly assigned to reincarnate as a bass. After successfully reincarnating he starts seeing system messages, stat windows, and even has quests where if he becomes the biggest and best king fish of the pond he'll be granted a wish by the system. He now has to work his way up from the bottom, just like he did as a human, in order to hopefully get a shot at becoming a human again.

<!--more-->

This is this author's debut work, and based off of the epilogue i'm not sure they'll ever get around to actually making another one. They originally wanted to create some kind of action/fantasy or survival/zombie work in the past using humans after covid but didn't really click with any of the stories and never found the motivation to actually start a series with it. Then they had the idea to mix something they are super passionate about (fishing) with various popular genres, and the end result is that they essentially made a "Zombie Survival" series where all of the characters are fish. 

They had very little planning on where the work will end up while starting it outside of making up a general end goal, and fleshed it out as they went along. As a result there were a lot of ideas that were kind of left to the wayside and glossed over, and large portions of the work kind of drag on with endless fights back to back, which is kind of a downside of using fish in a "zombie survival" setting. It's basically nonstop action/fights with very little else. The author alleviated this by going back to the human side of the story a few times to fix the pacing and fill out some additional information about the story, which I feel like was handled well enough. The art is also fairly good throughout and the author's love for fishing really shines through.

I feel like the author could eventually make some pretty good works in the future since even this one is pretty good as a debut work. I'll likely try out his work if he ever does at least, even if it's not isekai/reincarnation.
