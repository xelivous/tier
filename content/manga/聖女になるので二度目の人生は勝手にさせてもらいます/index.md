+++
title = "聖女になるので二度目の人生は勝手にさせてもらいます"
title_en = ""
statuses = ["axed"]
demographics = ["shoujo"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152751"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00200667010000_68/"
md = "https://mangadex.org/title/35167/since-i-became-a-saint-i-ll-do-whatever-i-want-with-my-second-life"
bw = "https://bookwalker.jp/series/201638/"

[chapters]
released = 16.3
read = 11

[lists]
recommend = "D"
+++

Feels very uninspired unfortunately. I don't know who I would rec it to unless someone had already ran out of all of the other shoujo romance series somehow. On indefinite hiatus for years now so very likely turbo axed.

<!--more-->
