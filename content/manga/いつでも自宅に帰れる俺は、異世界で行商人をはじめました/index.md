+++
title = "いつでも自宅に帰れる俺は、異世界で行商人をはじめました"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-08-12
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=170598"
raw = "https://seiga.nicovideo.jp/comic/49186?track=rank"
md = "https://mangadex.org/title/b252e636-0143-42d7-903c-de0c9772d5fb/itsudemo-jitaku-ni-kaerareru-ore-wa-isekai-de-gyoushounin-o-hajimemashita"
bw = "https://bookwalker.jp/series/291752/"

[chapters]
released = 50
read = 50

[lists]
recommend = "D"
+++

Protag discovers another world in his closet after his grandmother died and left him the house in her will. His grandmother was from another world and left behind a ring that can translate speech, along with 2 skillbooks to get him started off on his adventures in her original world. One of the skills allows him to convert yen to the currency of this world and back, as a result he ends up trying to make a lot of money by bringing Basic Goods from japan to this isekai, selling them for gold, converting it back to yen, and slowly building up his funds.

<!--more-->

The work is primarily about a dude selling things from the dollar store in isekai land without questioning the logistics of taxes and without even going out of his way to buy from a bulk supplier instead of just walking into a store and buying like 60000 matches from a random clerk in line and nothing else. Most of the events in the story are the protag introducing superior goods manufactured from japan to this isekai, including using bear spray + lighters to kind of spray some fire at creatures occasionally, while amassing a ton of wealth and doing /r/thathappened-tier reddit spergs dabbing on comically evil dudes. The protagonist is extremely dumb in general and the only reason why the story even barely works is because a majority of the people he meets are nice goodhearted people who don't fully take advantage from him, outside of literally everybody just reselling his goods elsewhere for a massive markup, because he's dumb. You have to completely turn your brain off in order to enjoy this series, the rabbitgirl that is in charge of the adventurer's guild is especially egregiously bad. 

Basically one of the very first people the protag meets in the story is a 9 year old girl selling flowers she picked to try and make money to save her sick mother, and the protag decides to spend a majority of his starting capital (30000 yen) (he's broke and unemployed btw) on flowers for his dead grandma to help her out. This scene exists solely because the author wanted to have this girl be in the story and it's the best way to introduce a reason for why a 9 year old girl would hang out with a 25 year old isekai'd dude without resorting to slavery that the author could think of I guess, which is fine, and then he offers to use her as a salesperson for his imported Matches and give her a portion because why wouldn't he just sponsor a random cute isekai girl as if she's his daughter? They make tons of money, and she gets tons of money, and she eventually asks the protag if she can fix her sick mother since she isn't getting any better, to which he agrees to help since he's already essentially taken her in as a daughter anyways why not. So her heads over, immediately knows that's it's Vitamin B12 deficiency almost at a glance or whatever, buys her supplements from not-walmart, and she's almost immediately cured like magic. The mom then basically almost completely disappears from the story since at that point since the only thing that matters is the little girl I guess, and the few times she's brought up later she's being an irresponsible parent and super melodramatic, existing solely to make the little girl worry if she did something wrong. Just like the mother, virtually every character in this story is just an empty set piece for the protag to reddit sperg and show how cool he is for importing basic shit from walmart without any consideration.

However ch18 is kind of kino actually in that it's actually told from the PoV of a little girl and the art style changes to match her view of the world and her maturing too quickly, and almost singlehandedly redeems every chapter before it and actually made me like this work a little bit. Except the work is still kind of bad/mediocre outside of that chapter. The adventurer mage is kind of hot though.
