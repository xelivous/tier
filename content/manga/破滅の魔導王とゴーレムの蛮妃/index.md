+++
title = "破滅の魔導王とゴーレムの蛮妃"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-10-25
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154172"
raw = "https://comic-walker.com/contents/detail/KDCW_KS04200784010000_68/"
md = "https://mangadex.org/title/de3f1db5-8de4-4f19-b218-0c62f0a63ea7/the-sorcerer-king-of-destruction-and-the-golem-of-the-barbarian-queen"
bw = "https://bookwalker.jp/series/212387"

[chapters]
released = 12
read = 12

[lists]
recommend = "D"
+++

Axed before it ever really got anywhere, and the later chapters are extremely short while being action-filled, then ends in a really awkard spot. I guess it could have potential but you'd likely have to read the LN...

<!--more-->
