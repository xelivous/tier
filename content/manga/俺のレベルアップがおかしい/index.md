+++
title = "俺のレベルアップがおかしい"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["original"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2025-01-13
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/w21vw6z/ore-no-level-up-ga-okashii-dekiru-otoko-no-isekai-tensei"
raw = "https://comic-ragchew.jp/comics/levelup/"
md = "https://mangadex.org/title/434fd5af-e06c-4827-a690-d4ef31d7c720/ore-no-level-up-ga-okashii-dekiru-otoko-no-isekai-tensei"
bw = "https://bookwalker.jp/series/370887"

[chapters]
released = 35
read = 16

[lists]
recommend = "D"
+++

Protagonist reincarnates into an isekai as a random dude, tries to become an adventurer and make it in the world but never manages to level up even once the entire time until 5 years later he retires. He uses all of his saved up money to finally lose his virginity by spending it in a brothel and upon impregnating the prostitute he finally manages to level up. Apparently the way he levels up is by having sex with women and impregnating them, bypassing any birth control they have and rendering it ineffective.

<!--more-->

I could be fine with the premise on some level but the way the protagonist is written is horrible. He had a crush on a woman in his party for ages until they eventually separated, then a few years later they happen upon each other by chance and she confesses to him, has sex with him, he gets a status message telling him he impregnated her, she tells him that she loves him, and he does not reciprocate at all despite loving her. He literally just sits there never telling her that he loves her in return, never reaffirming her feelings, simply using her to have sex occasionally while basically taking advantage of her. 

This work is like 80% sex scenes 20% meme adventurer plot to the point that i'm not sure it's even worth putting on this list at all. It's not well-written at all and you're likely better off reading any actual h-doujins instead. The only redeeming aspect of this work is that it's fully colored and in manga format instead of webtoon format. The actual h-scenes and non-hscene content are fairly terrible by any normal standards so I can't really rec this to anybody.
