+++
title = "異世界に救世主として喚ばれましたが、アラサーには無理なので、ひっそりブックカフェ始めました"
title_en = ""
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = ["none"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158549"
raw = "https://urasunday.com/title/834"
md = "https://mangadex.org/title/43375/the-savior-s-book-caf-in-another-world"
bw = "https://bookwalker.jp/series/209170/"

[chapters]
released = 26
read = 16

[lists]
recommend = "C"
+++

Rational protag destroys god with facts and logic. Fairly standard shoujo romance except it's a speedrun. The main characters are older so there's less dumb drama and it's more of a Healing Manga. I have no problem recommending it to people who want that, but if you don't want that there isn't much there for you.

<!--more-->
