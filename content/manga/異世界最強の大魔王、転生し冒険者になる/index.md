+++
title = "異世界最強の大魔王、転生し冒険者になる"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-10-01
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=172911"
raw = "https://comic-walker.com/contents/detail/KDCW_AM00201878010000_68/"
md = "https://mangadex.org/title/9f47af4e-ab8d-4a77-8a00-2acea81573fc/isekai-saikyou-no-daimaou-tensei-shi-boukensha-ni-naru"
#bw = ""

[chapters]
released = 28
read = 12

[lists]
recommend = "D"
+++

Demon lord sacrifices himself to save all of the demons, and vows to resurrect in 1000 years so he can get revenge on the angels. During that time the demons he left behind basically turned their land into a scifi cityscape with modern technology and then he revives with the collective knowledge of the demons he resided in during the 1000 years so he became ultra OP with tons of knowledge of everything. Also his adopted daughters are extremely horny for him, as is most of the women in the series.

<!--more-->

The art is both good and bad at the same time which is kind of bizarre.　I feel like I'd rec it to someone that wants a moderately horny power fantasy with scifi elements and an ikemen protagonist that acts like a virgin constantly. Which honestly, who wants that?
