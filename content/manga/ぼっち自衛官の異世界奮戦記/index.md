+++
title = "ぼっち自衛官の異世界奮戦記"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"] #https://ncode.syosetu.com/n1545dr/
languages = ["japanese"]
schedules = ["every other month"]
lastmod = 2024-08-20
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/li3u7xb"
raw = "https://comic-boost.com/series/295"
md = "https://mangadex.org/title/57dbd8a6-86c3-40af-bb28-9bfb931e7d0a/bocchi-jieikan-no-isekai-funsenki"
bw = "https://bookwalker.jp/series/327650"

[chapters]
released = 25.2
read = 16

[lists]
recommend = "D"
+++

Protag's a young 🇯🇵JSDF🇯🇵 soldier walking about the city when a girl almost gets run over by a truck, so he ends up jumping out and saving her, but ends up dying as a result. And apparently it's because god fucked up! So as an apology he gets to go to an isekai with his body/knowledge intact! He also has all of his soldier equipment in a spatial inventory along with guns/etc and his spatial inventory can replenish whatever it lost every day (including bullets etc)! 🇯🇵🇯🇵🇯🇵

<!--more-->

LMAO at the town IDs being literal military dogtags. And his military dogtags are actually registered in this world and belongs to IsekaiJapan out in the sea that looks identical to japan. Also shoutout to there being an ancient isekai'd military dude in the past who introduced a ton of random technology to this world and fought against the demon lord in literal tanks🇯🇵.

Shoutout to the cute twinbraids-but-not archer.

The art could be better, but ultimately this is just 🇯🇵 wankery and an excuse to introduce isekai people to superior japanese technology and indoctrinate them into the military.
