+++
title = "異世界の貧乏農家に転生したので、レンガを作って城を建てることにしました"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2023-09-29
tags = [
    "reincarnated-as-baby",
    "male-protagonist",
    "mage-protagonist",
    "farmer-protagonist",
    "city-building",
    "war",
    "arbitrary-magic-system",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=162104"
raw = "https://to-corona-ex.com/comics/20000000045505"
md = "https://mangadex.org/title/829d27e8-3d50-48bc-ab1d-9d1c3bfba615/i-was-reincarnated-as-a-poor-farmer-in-a-different-world-so-i-decided-to-make-bricks-to-build-a-castle"
bw = "https://bookwalker.jp/series/260148"

[chapters]
released = 20.2
read = 12.2

[lists]
recommend = "B"
+++

Protagonist reincarnates as a baby in an isekai where everybody can use magic, but he's just a random farmer's child. However he diligently practices magic every day by obeserving his parent's simple usage of it so that one day he too can handle magic, and at a much higher level than anybody else. However until he reaches the age of 6 he won't be able to actually perform any magic properly due to Reasons, except he manages to perform magic without chanting simply through manipulating the magic inside of and around him regardless. He then sets out and crossbreeds/farms plants constantly while training his magic, slowly improving/experimenting with the crops. However while he's having fun experimenting with his crops, one day his dad has to go off and fight in a war, which sparks a desire for the protagonist to start preparing himself for the future and the rest of his life with the knowledge that he too might have to fight in a war some day.

<!--more-->

It's primarily a city-building work that quickly escalates in scale. It's just generally fun to read through but isn't particularly outstanding in any way in particular. Chapter 12.1 is quite simply good though.
