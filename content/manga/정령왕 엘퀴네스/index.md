+++
title = "정령왕 엘퀴네스"
title_en = "Spirit King Elqueeness"
categories = ["isekai", "reincarnation"]
demographics = []
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-02-12
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143900"
raw = "https://page.kakao.com/home?seriesId=49361421"
md = "https://mangadex.org/title/53c12cd2-f7ff-4976-b16a-c83efdeedb6f/elqueeness"

[chapters]
released = 248
read = 197

[lists]
recommend = "C"
+++

Protag lives an absolutely shit life getting abused by his parents for inexplicable reason and then eventually dies from a truck, but then is just left to wander around as a ghost for a while until he stumbles across some grim reapers accidentally who are taking care of other people's deaths but for some reason not his. The reason for this is because he was never supposed to exist in this world in the first place, all of the suffering he went through was for nothing, and a mistake was made haha lol.

<!--more-->

This is probably the most straightforward escapist fantasy i've seen in this reincarnation/isekai spree. "Wouldn't it be great if all of the suffering I had to go through with shit parents was because there was a mistake and I should've been born in a world of fantasy and magic, and if I was born there I would be the greatest existence that everybody would worship and love unconditionally?". The work is largely about the protagonist learning to accept his new surroundings/family while "healing" everybody around him with how nice and perfect he is. 

The closest comparison to this work is maybe like kubera? It's a largely emotion-driven work where the plot doesn't make too much sense and most of the focus is on character interactions and general relationship building. There's tons of random interwoven plotlines of mythical beings/gods but most of the focus of the work is following the plotline of a civil war in a single kingdom and the boy emperor who contracts the protagonist as basically a familiar. There's some good chapters sprinkled here or there but you have to turn off your brain and resist the urge to frodopost near constantly.
