+++
title = "세계관 최강자들이 내게 집착한다"
title_en = "The Most Powerful Characters in the World Are Obsessed With Me"
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-10-02
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=189471"
raw = "https://page.kakao.com/home?seriesId=57944386"
md = "https://mangadex.org/title/66f6c059-9c92-4fc3-aee7-92e0e663a87a/the-strongest-characters-in-the-world-are-obsessed-with-me"

[chapters]
released = 91
read = 78

[lists]
recommend = "A"
+++

Title is extremely dumb, I dislike the first chapter, but everything else is really good. The art is 11/10. The protag is cute. It's mostly a cute work about familial love and the protag's relationships with basically sociopaths in an otomege.

<!--more-->

Main highlight of this work is that the protagonist just constantly has tons of new outfits and hairstyles. Like every single chapter has a new outfit/hairstyle it's insane. Not just the protagonist but just about every major character has new outfits regularly as well, as what you'd normally expect from Nobles, but most works don't actually go through the effort of constantly changing their outfits. Hell a lot of the time the protag changes outfits and hairstyles multiple times in a single chapter.
