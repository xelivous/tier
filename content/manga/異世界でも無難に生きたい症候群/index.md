+++
title = "異世界でも無難に生きたい症候群"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152746"
raw = "https://magcomi.com/episode/10834108156766386086"
md = "https://mangadex.org/title/6a769300-091e-49ab-8cd3-cc32c360ecc0/isekai-demo-bunan-ni-ikitai-shoukougun"
bw = "https://bookwalker.jp/series/217685"

[chapters]
released = 29.2
read = 29.2

[lists]
recommend = "S"
+++

Protagonist wakes up in another world and skips his life's backstory and how he got isekai'd in the first place because reasons, almost gets eaten by a bear almost immediately, but gets saved by a random slime which eats the bear instead. The work then does an absolutely speedrun through content, he can't communicate with the natives of this world, he ends up in jail, and meets a pretty female knight.

<!--more-->

lawful neutral sociopathic INTJ protag with no cheat skills or power of his own defeats evil with logic and rationality, while everybody else around him is excessively powerful. The art in this manga is actually really good tbh despite the faces occasionally being a little wonky. Also the wolfgirl is cute and a good plotline that actively elevated the manga. It's a really cool work with a really well-defined protagonist and the inner struggles he faces, along with all of the other character's observations of the main character; we get a lot of inner thoughts from a lot of characters but all of the important details are left out about the protagonist.

The artist gave birth to a baby in late 2022 so the manga is on hiatus until they can get back into manga again which could be years.
