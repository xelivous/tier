+++
title = "レベル1だけどユニークスキルで最強です"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=148847"
raw = "http://seiga.nicovideo.jp/comic/34653"
md = "https://mangadex.org/title/27554/level-1-dakedo-unique-skill-de-saikyou-desu"
bw = "https://bookwalker.jp/series/188249/"

[chapters]
released = 53
read = 37

[lists]
recommend = "D"
+++

Protag spawns as a dungeon drop from a slime all of a sudden one day, in a world where everything comes from monster drops; food, air, water, materials, everything. However the protag has a max level of 1, and all of his stats are F tier, except for his drop chance/quantity, which is S for some reason. As a result he has to slowly build up his funds/skills to be able to make do with shit stats and OP drop rates, while slowly getting closer with the cute waifu who spawned him from the slime in the first place. However shortly into the series they give him a deus ex bullshit dungeon that exists solely to level up the protag in other ways with bonus stats so that he can actually survive, making the entire point of him being stuck at lvl1 meaningless.

<!--more-->

It kind of goes a little too hard on the innuendo/eroticism at times. The mage girl in ch9 is the greatest thing in this manga and she disappears almost immediately never to be seen again which is the greatest travesty known to man.

Honestly I kind of like the art, it has a weird kind of charm to it. Kind of a dumb work overall and the only reason I continued on is because the girls are kind of cute at times. Kind of brainlet tier otherwise.
