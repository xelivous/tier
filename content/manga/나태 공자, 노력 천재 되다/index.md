+++
title = "나태 공자, 노력 천재 되다"
title_en = "The Lazy Lord Masters the Sword"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2023-12-21
tags = [
    "male-protagonist",
    "swordsman-protagonist",
    "hyperbolic-time-chamber",
]

[links]
mu = "https://www.mangaupdates.com/series/jz0mbfy/the-lazy-lord-masters-the-sword"
raw = "https://comic.naver.com/webtoon/list?titleId=772853"
wt = "https://www.webtoons.com/en/fantasy/the-lazy-lord-masters-the-sword/list?title_no=3349"

[chapters]
released = 104
read = 89

[lists]
recommend = "S"
+++

The work starts out from the perspective of a random villager who is minding his business when a random dude ends up joining the village and does nothing but swing his sword day in and out. He continues swinging his sword for decades even after the villger has become an old grandpa, and eventually dies swinging his sword having accomplished nothing but swinging it. Then the protagonist wakes up having seen this dream and is inspired to actually make something out of his life, as up until this moment he has spent that last 10+ years of his life wasting away doing nothing but sleeping away his depression after his mother died. 

<!--more-->

This is probably one of the best traditional battle shounens in korean manwha. It's a story about the protagonist getting his life back on track and putting in boundless effort to improve himself solely to make his family happy after wasting away his life, all while having to face ridicule being called a lazy no-good loser the entire time he's trying to change himself. The work largely follow four main characters, two of which are men and two are female, all striving to be the very best swordmasters on the continent. It's a work about coming of age, growth, friendship, and striving for self-improvement. The side-characters are also really well done and cool.

Chapter 1-7 has a decent translation, and then chapter 8 and beyond has some of the most abysmal grammar imaginable; it's baffling. The english grammar starts to improve again some chapters later and it's back up to decent quality around 30+ and stays there, but you have to really press on past those initial chapters...

