+++
title = "악역의 엔딩은 죽음뿐"
title_en = "Villains Are Destined to Die"
categories = ["isekai", "reincarnation", "game"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-10-02
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166368"
raw = "https://page.kakao.com/content/54845189"
md = "https://mangadex.org/title/14569f2f-f66a-4c67-ac7f-a37823a0fa23/villains-are-destined-to-die"

[chapters]
released = 141
read = 134

[lists]
recommend = "A"
+++

Imagine you get trapped in a dating sim where you have very little input outside of saying choices that will inevitably lead you down a path towards your death, where you're not allowed to even speak unless it's a part of the prebuilt scenarios and you are forced to look on in horror while you try and contemplate how to navigate a bunch of choices written by a sadistic asshole who only wants everybody to suffer. That's what the beginning of this work is. In fact a large majority of this work is simply for people who enjoy watching others suffer.

<!--more-->

And then it does away with that and gives the protagonist the freedom of choice to say anything she wants to without being bound by the choice window, and since she's no longer roleplaying as the dumb ungrateful asshole things start to actually work out kind of effortlessly from there onwards. However she's still trapped within the confines of the game; She is still forced to reduce every relationship and interaction with every person she meets down to whether or not a small number over their heads will increase or decrease and whether or not that will end up immediately killing her or not. It's a work about living life as a sociopath.

However if she wasn't forced to see everything as a game, if she wasn't forced to analyze and fret over every interaction to maintain imaginary numbers over other people's heads, the situations and hardships she faces isn't all that worse than any other typical Noble Lady work will all of the political intrigue and drama that normally involves. If the work ever actually goes down the path of freeing her from the confines of the game and getting rid of the "Systems" and treating it as actual sociopathy then it would likely be an actually really good work (it actually kind of does this later on and the only real relationship she creates is when she starts ignoring the number and just treats the other person as an actual person). It's still pretty good even with it playing it all straight though.

However the "heros" are all almost exclusively masochists who wants a sadistic mommy to step on them. Seemingly the only way to raise their affection is to excessively 💅 girlboss 💅 and belittle them and handle everything without their help since they're all also assholes themselves.
