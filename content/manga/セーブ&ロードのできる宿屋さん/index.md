+++
title = "セーブ&ロードのできる宿屋さん"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-10-05
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154703"
raw = "http://seiga.nicovideo.jp/comic/38840"
md = "https://mangadex.org/title/54dce902-6873-4033-8828-176fe5ce8f9f/save-load-no-dekiru-yadoya-san"
bw = "https://bookwalker.jp/series/208755"

[chapters]
released = 55
read = 18

[lists]
recommend = "C"
+++

Kind of an odd entry since the protagonist isn't really the one who has isekai/reincarnated, but the dude has already basically newgame+'d and finished everything and he's just chilling in postgame training newbies as if it were rezero.

<!--more-->
