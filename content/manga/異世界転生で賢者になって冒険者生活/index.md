+++
title = "異世界転生で賢者になって冒険者生活"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=175909"
raw = "https://www.ganganonline.com/title/1204"
md = "https://mangadex.org/title/258d2691-ffea-4e77-8968-ce5c24ffc6f7/isekai-tensei-de-kenja-ni-natte-boukensha-seikatsu"
bw = "https://bookwalker.jp/series/295787/"

[chapters]
released = 20
read = 20

[lists]
recommend = "C"
+++

Protagonist lives in a world with in strange scifi post-apocalyptic world where offensive magic is banned but all other kinds of magic is allowed. And despite being in a building way up high in the sky he get's space truck'd and reincarnates/isekais into a world where he's actually able to use offensive magic freely.

<!--more-->

It's a fairly simplistic work where the protag is broken OP compared to everybody else simply from actually taking the time to try and understand the magic formulas, experiment with them, optimize them. There's also a few fun subplots with this being a very scifi-oriented background setting both in his previous world and his current world, and all of the similarities and parellels between the two worlds. Could probably recommend to someone who wants a very scifi-focused fantasy work that still feels like a normal fantasy isekai.