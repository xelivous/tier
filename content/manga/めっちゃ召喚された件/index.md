+++
title = "めっちゃ召喚された件"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=170491"
raw = "https://magcomi.com/episode/13933686331682099976"
md = "https://mangadex.org/title/802bf297-b156-4053-8542-f12057c4a7b9/meccha-shoukan-sareta-ken"
bw = "https://bookwalker.jp/series/275215"

[chapters]
released = 26
read = 26

[lists]
recommend = "C"
+++

Edgy chuuni protag gets repeatedly summoned and then eventually ends up in some random world as a 'useless hero' alongside the other actual heroes while being an Epic Badass unknowing to everybody. 

<!--more-->

The protag is an actual sociopath and this manga is filled with tons of gore. The art style is simplistic but effective. ch12 is dank and ch15 is actually pretty good, but the rest of the work is just kind of pointless meme gore. Probably a fun rec if you just want to see a protag dab on everybody.
