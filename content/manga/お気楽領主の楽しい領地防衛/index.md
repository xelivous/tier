+++
title = "お気楽領主の楽しい領地防衛"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-10-16
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=187259"
raw = "https://comic-gardo.com/episode/3269754496561190983"
md = "https://mangadex.org/title/9afe47ee-5c8b-4572-b7c0-efa4f83da432/okiraku-ryoushu-no-tanoshii-ryouchi-bouei"
bw = "https://bookwalker.jp/series/342851"

[chapters]
released = 31.2
read = 31.1

[lists]
recommend = "B"
+++

Protag basically works at a black company all day long and occasionally reads novels in the small amount of spare time he gets when all of a sudden he dies in his sleep one day, and wakes up in an isekai as a young noble boy living in a mansion. Because he's in a prestigious house all of his siblings are tasked with a rigourous schedule to make them befitting of their stations, but since the protagonist is basically two years old he doesn't really have any immediate obligations. Yet since he has his past life's knowledge he sets out trying to be a prodigy and learns everything possible without hiding his skills / knowledge, until he realizes that he would show up his older brothers and possibly get an assassination target on his back as a result, so he starts to slack off and become a lazy goodfornothing instead. However once he becomes old enough to get his magic aptitude it's found out that he had nothing to worry about because he had a useless "crafting" aptitude instead of the lauded magic/healing aptitudes, is almost killed by his father so he doesn't become an "embarassment", but manages to get shipped off to a remote region of the land thanks to his eldest brother intervening. Luckily the protag is now free from having to worry about "dumb noble things" and can just have fun crafting and building up a village in a remote region free of all obligations.

<!--more-->

The work timeskips forwards and backwards a lot in the initial chapters which is really annoying to read through. He also almost immediately gets a slave randomly for no reason which is Epic. The maid has some nice almost pseudo-twinbraids at least; very cute.

This work is primarily about the protagonist building up a small village to a large city solely with his charismatic attitude and being polite to all people making them want to serve him. However he does have a kind of broken cheat of a skill in production magic with a near infinite amount of magic capacity along with all of his isekai knowledge so ultimately it's still an oretueee work.

cute brown mermaid loli wife...

Fairly enjoyable citybuilding series overall; would rec to fans of Slime.
