+++
title = "物語の黒幕に転生して"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-07-26
tags = [
    "male-protagonist",
    "reincarnated-into-jrpg",
    "status-window",
    "reincarnated-as-baby",
    "timeskip-to-childhood",
    "reincarnated-as-villain",
    "the-original-plots-fate-is-strong",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=13552961046"
raw = "https://comic-walker.com/contents/detail/KDCW_KS01203437010000_68"
md = "https://mangadex.org/title/39c9c088-3a83-4e61-a129-35fe299bbe3c/monogatari-no-kuromaku-ni-tensei-shite"
bw = "https://bookwalker.jp/series/400546"

[chapters]
released = 21
read = 17

[lists]
recommend = "C"
+++

Protag played the 2nd installment of a game in a trilogy and upon completion he decides to play it again while waiting for the new one to release, but gets a weird prompt on NG+ and ends up getting isekai'd into the game. He then spends years enjoying his new life with an actual family (since he was neglected in his previous life due to divorce/etc) and enjoying father/son bonding time every day. However even with the events of the story slapping the protagonist in the face time and time again, the protagonist remains obstinate in his desire to live the happy country life with his family.

<!--more-->

Heroine is pretty moe tbh and the romance is kind of cute so far at least.
