+++
title = "まだ魔法なんかに頼ってらっしゃるのでしょうか"
title_en = ""
categories = []
demographics = []
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=160299"
raw = "https://comic-walker.com/contents/detail/KDCW_AM01200589010000_68/"
md = "https://mangadex.org/title/a873026d-2de3-4721-a865-b07b72fd9834/you-still-rely-on-magic"
#bw = ""

[chapters]
released = 12
read = 0

[lists]
recommend = ""
+++



<!--more-->

