+++
title = "最近入った白魔導士がパーティークラッシャーで、俺の異世界冒険者生活が崩壊の危機な件について"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = ["sporadic"]
lastmod = 2024-02-29
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/cnrijjs/saikin-haitta-shiro-madoushi-ga-party-crusher-de-ore-no-isekai-boukensha-seikatsu-ga-houkai-ni-kiki-na-ken-ni-tsuite"
raw = "https://www.comicride.jp/book/crusher/"
md = "https://mangadex.org/title/ff73a7a3-0f65-47d6-9f71-6c3c529dcd9b/saikin-haitta-shiro-madoushi-ga-party-crusher-de-ore-no-isekai-boukensha-seikatsu-ga-houkai-ni-kiki"
#bw = ""

[chapters]
released = 23
read = 18

[lists]
recommend = "C"
+++

Protag has trauma from his previous life where he kept getting harrassed by women who kept breaking up his male friend groups so he decides to make an all-male party after he gets reincarnated into an isekai. However he is once again met with the same fate of having his party almost ruined by a woman who they let in due to Reasons. The work eventually turns into a "woman of the week" type deal where more and more women keep gravitating to the party in an effort to cause them to fight amongst each other or whatever while the bros try to bro each other up.

<!--more-->

The reason for this is because the main person that harrassed the protagonist in his previous life also reincarnated alongside with him and is also harassasing him in this life!! Why is she going to all of these lengths just to harrass the protagonist? Reasons. This is a really bizarre ecchi comedy and i'm not sure what to really think about it. It depicts p much all men as horny idiots and all women as manipulative bitches but slowly tries to redeem the cast while slowly building up a harem for the protagonist filled exclusively with basically yanderes.

The "main" antagonist/heroine seems extremely mellycore and maybe this entire work is just ultra mellycore in general?

Third volume cover kinda hot tho ngl.
