+++
title = "S급 헌터는 악역 공녀가 되기 싫습니다"
title_en = "The S-Class Lady"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2025-01-28
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/7pvgyx3/the-s-class-lady"
raw = "https://page.kakao.com/content/63555003"
md = "https://mangadex.org/title/d76a350d-c6b5-42bb-bfba-e521a3964a6a/the-s-class-lady"
#bw = ""

[chapters]
released = 52
read = 20

[lists]
recommend = "D"
+++

The protagonist is your average korean lady who got trapped in a novel after getting run over by a car, a novel where humanity was forced to climb a tower or the world gets destroyed. However she has repeated her life over and over again after failing to reach the top due to regression, then after many attempts 500 years pass and she's finally able to defeat the final boss of the tower and go home. Except the final boss curses her and she ends up being forced into another novel instead; One where she's forced to be the villainess in a work she has never read before. However thankfully she has retained a majority of her power from her previous novel so theoretically she should be fine, right?

<!--more-->

The system popup window where the "gods" constantly talk to her is terrible and drags down the work considerably. They constantly interrupt every conversation to talk over them while adding no decent commentary of their own, and exist to make a mockery of the already brainlet-tier characters. This work is primarily a "mystery" series where the protag needs to figure out what happened in the past while turning around everyone's opinions about her, and she spends countless chapters flailing around before literally just reading the diary that the original villainess had in her room which gives a majority of the information needed. The writing sucks, the art is filled with those meme sunglasses, the only thing it has going for it is that it has the exact same "everybody is so pretty and attractive" artstyle that literally every other manwha has, so it's pointless to read. I suppose the only merit is that the protagonist isn't a pushover and is a ""girlboss"", but she's written identically to every other korean battle fantasy male lead so it's all the same.
