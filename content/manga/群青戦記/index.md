+++
title = "群青戦記"
title_en = ""
statuses = ["completed"]
demographics = ["seinen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=103373"
md = "https://mangadex.org/title/12588/gunjou-senki"
bw = "https://bookwalker.jp/series/16059/"

[chapters]
released = 177
read = 0

[lists]
recommend = ""
+++

<!--more-->
