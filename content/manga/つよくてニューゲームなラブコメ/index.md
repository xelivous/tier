+++
title = "つよくてニューゲームなラブコメ"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-18
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=195953"
raw = "https://www.pixiv.net/user/1032496/series/122597"
md = "https://mangadex.org/title/591b6e03-c02b-4362-aa75-b9066e21f46e/tsuyokute-new-game-na-love-come"
#bw = ""

[chapters]
released = 112
read = 6

[lists]
recommend = "F"
+++

I gave it a try even though it is a genre that I know I already hate, and I ended up hating it to nobody's surprise. The chapters are extremely short and it is mostly just the protag being an old guy in his young body constantly telling a girl that she is cute which is excessively creepy while said young girl keeps getting embarrased and verbally abuses the guy. I don't know who the target demographic of this is but it's probably pedophiles.

<!--more-->
