+++
title = "異世界チートサバイバル飯"
title_en = ""
categories = ["isekai"]
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2024-07-28
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=150722"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000081010000_68/"
md = "https://mangadex.org/title/35199/isekai-cheat-survival-meshi"
bw = "https://bookwalker.jp/series/193346/"

[chapters]
released = 66
read = 41

[lists]
recommend = "C"
+++

The protagonist is your average highschooler who loves camping and the outdoors when he gets hit by a truck and isekai'd into a random forest, along with some of the camping gear he just happened to have in his school backpack at the time. He then sets out to survive using all of the tricks he learned from the TV and internet, but since he's a massive camping otaku he has no issues at all and quickly adapts to his situation. On top of that he even has a special ability to discern dangerous foods so that he is less likely to get sick. On top of that he has the ability to gain an ability from anything that he eats!!

<!--more-->

This is like 70% cooking/slice of life, and 30% battles where the protag dabs on everybody with his epic powers while the harem occasionally helps out. The cooking doesn't really go into much detail and it's more like japanese tv where the characters just say it's delicious constantly, and the battles have about that much depth to them as well. I have an extreme negative bias towards cooking manga in general but maybe someone who enjoys them could enjoy this.
