+++
title = "今度こそ幸せになります"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-06-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=145711"
raw = "https://www.alphapolis.co.jp/manga/official/657000190"
md = "https://mangadex.org/title/faf88355-e0d9-41bb-a091-cc599faf0123/this-time-i-will-definitely-be-happy"
bw = "https://bookwalker.jp/series/168087"

[chapters]
released = 26.5
read = 26.5

[lists]
recommend = "S"
+++

The protag continues to reincarnate as the childhood friend of the hero, and as a result ends up having to separate from her ikemen crush repeatedly as he promises to come back after their journey is done (but never does), and ends up growing old alone while waiting for him to return after he inevitably marries the princess or gets a harem of big booba witch girls. As a result the protag decides to set out to become an independent woman that is self-sufficient, but still can't quite get over her love of her childhood crush who will inevitably betray her like all of the other Heros before him.

<!--more-->

The first chapters seem a little meme at first glance but it's a well done work overall and probably best-in-class for this particular subgenre. It's rather rare to get a full story with an actually complete ending in a small amount of chapters that still fully resolves everything it set out to do. Good romance tbh.
