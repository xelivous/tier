+++
title = "聖女のはずが、どうやら乗っ取られました"
title_en = ""
categories = ["isekai", "game"]
demographics = ["shoujo"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2022-03-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169400"
raw = "https://magazine.jp.square-enix.com/mangaup/original/seijonohazuga/"
md = "https://mangadex.org/title/4d73682d-ea8e-4b73-b90b-11006b051fb5/i-was-summoned-to-be-the-saint-but-i-was-robbed-of-the-position-apparently"
bw = "https://bookwalker.jp/series/275440"

[chapters]
released = 0
read = 6

[lists]
recommend = "D"
+++

Protag gets summoned, except her bully gets summoned along with her and tries to do everything she can to fuck up the protag. The faces sure are meme at times, and the antagonist is meme-tier evil to the point that it's not even really interesting.

<!--more-->
