+++
title = "악녀 18세 공략기"
title_en = "Rewriting the Villainess"
categories = ["reverse-isekai"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-12-22
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/dpk76up/rewriting-the-villainess"
raw = "https://comic.naver.com/webtoon/list?titleId=783525"
md = "https://mangadex.org/title/3aeb9b6e-39c4-45c5-ac8c-644fa84fd034/rewriting-the-villainess"
wt = "https://www.webtoons.com/en/fantasy/rewriting-the-villainess/list?title_no=4093"

[chapters]
released = 63
read = 63

[lists]
recommend = "A"
+++

A "villainess" girlboss (Liza) in a fantasy land got her prince fiance stolen by some random girl and ends up drinking her sorrows away, only to wake up in modern korea in the body of some unknown girl. It turns out this girl (Deoun) is the author of the webcomic that Liza was a "villainess" in and she now has to figure out how to get back into her own body/world and figure out how to survive in a modern world. However Deoun isn't making it easy for Liza, since she doesn't want to return to her body since she's done with the world and just wants to end it all.

<!--more-->

A very nice coming-of-age reverse-isekai shoujo romance drama. It puts a lot of effort into all of the art, the backgrounds have tons of detail in them and the characters are fairly nice as well. The author clearly had an ending in mind when they started out and fully fleshed out the story they wanted to tell on the way there, and it managed to pack each of the chapters with tons of content which made most of the chapters feel at least 2x as long as most other manwha. I do feel like it could've maybe done more with a few of the characters but I feel that's more of a nitpick than anything. I enjoyed reading it from start to finish and I now want to check out the author's other works.

No real issues with the official translation either.
