+++
title = "復讐完遂者の人生二周目異世界譚"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["partial"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=153322"
raw = "http://comicride.jp/avenger/"
md = "https://mangadex.org/title/35479/fukushuu-kansuisha-no-jinsei-nishuume-isekaitan"
bw = "https://bookwalker.jp/series/223291/"

[chapters]
released = 45
read = 19

[lists]
recommend = "D"
+++

The protag sought revenge, killed everybody involved, then killed himself. However after killing himself he gets isekai'd to some random ruins somewhere with only the knife he used to kill himself, and the first thing he does is try to kill himself again but is stopped by a cute girl using magic. She then explains that most people who die unfortunate deaths end up reincarnating to this place to try and redeem themselves or have a second chance at life. The world has heroes, dungeons, skills, etc, just like you'd expect from an isekai. Additionally since this is where unfortunate people end up the protag's sister might be in this world, since the entire reason he was seeking revenge in the first place was due to her murder.

<!--more-->

It's an edgy battle shounen where the protag fights with the power of edge, and the more he fights the edgier he gets until his edginess goes out of control and he becomes maximum edge. The artstyle/characters look pretty derpy at times. Can probably rec to the target demographic of arifureta enjoyers since it's that same level of the author not being able to commit to a psychotic revenge MC.
