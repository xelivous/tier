+++
title = "부서진 성좌의 회귀"
title_en = "The Divine Twilight's Return"
categories = ["reincarnation", "post-isekai"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=192302"
raw = "https://page.kakao.com/home?seriesId=58274684"
md = "https://mangadex.org/title/8d12bb9f-dd9a-45ab-b6d7-fa26e52c91b5/return-of-the-broken-constellation"

[chapters]
released = 60
read = 35

[lists]
recommend = "D"
+++

protag get's isekai'd then becomes like a god after hundreds of years (or something idk since it's not really explained at all), then he gets condemned by the gods in that world and gets sents to the afterlife, then gets sent back to original world around the time he originally left his first world on orders of the ruler of the afterlife to go after rogue gods in his world. So you have an broken OP protag doing weird as shit but all of the gods watching him and saying dumb shit don't really catch on to all of the weird shit the protag is doing despite him eventually coming to kill a lot of them.

<!--more-->
