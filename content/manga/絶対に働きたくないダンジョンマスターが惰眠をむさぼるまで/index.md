+++
title = "絶対に働きたくないダンジョンマスターが惰眠をむさぼるまで"
title_en = ""
categories = []
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149649"
raw = "https://comic-gardo.com/episode/10834108156661710098"
md = "https://mangadex.org/title/29336/lazy-dungeon-master/"
bw = "https://bookwalker.jp/series/195865/"

[chapters]
released = 53
read = 27

[lists]
recommend = "D"
+++

Irrational Sociopath Leg-Fetishist Protag slaughters bandits with reckless abandon while ""doing nothing"" all day. The early chapters go relatively deep into tower/base defense autism and then it just scales back and becomes relatively normal with no risk, and subsequently becomes rather boring. Might be worth it for the earlier bit although maybe just play a tower defense game instead of being forced to see lolis constantly pee themselves.

<!--more-->
