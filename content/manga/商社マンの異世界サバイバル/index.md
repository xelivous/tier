+++
title = "商社マンの異世界サバイバル"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2023-10-06
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/qg5n9g7"
raw = "https://comic-walker.com/contents/detail/KDCW_FS00201832010000_68/"
md = "https://mangadex.org/title/2c96dd59-388b-47cb-aefd-a09ddd2ddcf2/shoushaman-no-isekai-survival-zettai-hito-to-wa-tsurumanee"
bw = "https://bookwalker.jp/series/295144"

[chapters]
released = 27
read = 23

[lists]
recommend = "A"
+++

Protag is an antisocial dude who just wants to delete all of his social accounts and be free from social obligation when all of a sudden one day he wins the lottery and buys a cabin out in the middle of nowhere. And then on top of that he even gets isekai'd somehow along with his house/chickens!! And for some reason he has an appraisal skill and status windows keep popping up?? How wacky!! After living in this world for a while (timeskipped over with no explanation) he's gone back and forth to town to sell various things he made in town but he can't understand anybody in this world since he doesn't have magical translation abilities.

<!--more-->

Since protag is so antisocial he legit goes out of his way to not learn the isekai language so he doesn't have to like get deeply involved with them, which is kind of fascinating in its own right since most language barriers in these works disappear within a few chapters usually. However the work still prominently features a few characters who regularly interact with the protagonist and tries to communicate with him, since he's a primary source of magical equipment from his skills which is seemingly rare in this world.

The work seems to be primarily about the protagonist finally realizing that being social with a select group of Friends is actually a great feeling and isn't really all that worth avoiding in the first place. Stop being antisocial, yo! Also i swear juno's gf man... it should've been me, not him... The work goes back and forth between death spirals of the protag just losing himself to depression and being antisocial, and then swaps back to him being full of childlike wonder that talking to people is actually fun. Funniest part about this work is that the protag is so antisocial that he gets infatuated with a random girl at a cabaret bar since she's nice and listens to him (since that's her job).

There's also X.5 chapters that delve into the perspectives of other people where you can actually understand what the hell they're saying which is kind of necessary for the work to not be too annoying. 11.5 in particular is truly a kino chapter.

However honestly I do enjoy this work. After having read so much of the same shit over and over again it's actually kind of fresh having adults just shoot the shit and deal with their relationships constantly. Unlike a lot of other works people other than the protagonist actually get GFs/married/etc and there's a wide dynamic of relationships at play, on top of the constantly present language barrier mixing everything up even further.
