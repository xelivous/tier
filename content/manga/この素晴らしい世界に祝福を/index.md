+++
title = "この素晴らしい世界に祝福を"
title_en = ""
categories = ["isekai"]
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=113872"
raw = "https://dragonage-comic.com/product/konosuba/"
md = "https://mangadex.org/title/bcfa196d-d162-45f5-a224-61d26b04a077/kono-subarashii-sekai-ni-shukufuku-wo"
bw = "https://bookwalker.jp/series/42841"

[chapters]
released = 73
read = 0

[lists]
recommend = ""
+++



<!--more-->

