+++
title = "NGライフ"
title_en = ""
categories = []
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=6524"
#raw = ""
md = "https://mangadex.org/title/12332/ng-life"
bw = "https://bookwalker.jp/series/42264/"

[chapters]
released = 51
read = 0

[lists]
recommend = ""

+++

Dude died during the volcanic eruption at pompeii and reincarnates into modern japan, along with just about everybody else he knew in that time period as someone in his new life, but he's the only one who knows their past lives because Reasons so he keeps getting them confused and doing weird actions.

<!--more-->

I've tried reading this multiple times and I just get filtered every time.
