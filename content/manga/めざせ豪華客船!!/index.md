+++
title = "めざせ豪華客船!!"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-09
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=179149"
raw = "https://magazine.jp.square-enix.com/gfantasy/story/mezasegoukakyakusen/"
md = "https://mangadex.org/title/830b4cdd-df77-4a48-975a-aa2bc0684738/striving-for-the-luxury-liner?tab=chapters"
bw = "https://bookwalker.jp/series/299274/"

[chapters]
released = 0
read = 29

[lists]
recommend = "D"
+++

The protagonist is a little too lustful but i guess it's understandable for someone his age. It's like a comedy without the comedy and the protag isn't interesting. Dude gets summoned to another world with the ability to summon ships so he tries to be an adventurer, fails at that, then tries to be a merchant, but keeps getting scammed but still somehow succeeding since he has cheats anyways. The work is basically just the protag fawning over the cute women and spawning tons of magical ships out of nothing while doing ""commerce"".

<!--more-->
