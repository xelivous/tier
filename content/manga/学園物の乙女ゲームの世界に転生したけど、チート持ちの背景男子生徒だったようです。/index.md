+++
title = "学園物の乙女ゲームの世界に転生したけど、チート持ちの背景男子生徒だったようです。"
title_en = ""
categories = ["isekai", "reincarnation", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-07-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=198766"
raw = "https://gaugau.futabanet.jp/list/work/615fb68877656145b30a0000"
md = "https://mangadex.org/title/78687be0-7487-4671-ac55-a6c6bbdb449b/gakuenmono-no-otome-game-no-sekai-ni-tensei-shita-kedo-cheat-mochi-no-haikei-danshi-seitodatta-you"
bw = "https://bookwalker.jp/series/344738/"

[chapters]
released = 19.2
read = 5.3

[lists]
recommend = "D"
+++

It's porn. In an otome game villianess setting. Where every single woman wants the protag. Probably even the game's heroine, who is also the protag's sister from his previous life.

<!--more-->
