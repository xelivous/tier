+++
title = "その無能、実は世界最強の魔法使い"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["every other week"]
lastmod = 2024-12-07
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/ocpdlih/sono-munou-jitsu-wa-sekai-saikyou-no-mahoutsukai-munou-to-sagesumare-kizokuka-kara-oidasareta-ga-gift-tenseisha-ga-kakusei-shite-zense-no-nouryoku-ga-yomigaetta"
raw = "https://yanmaga.jp/comics/その無能、実は世界最強の魔法使い_無能と蔑まれ、貴族家から追い出されたが、ギフト転生者が覚醒して前世の能力が蘇った"
md = "https://mangadex.org/title/d21e52cf-50f1-48c7-8de7-69f280c05be4/sono-munou-jitsu-wa-sekai-saikyou-no-mahoutsukai"
bw = "https://bookwalker.jp/series/359356"

[chapters]
released = 64
read = 28

[lists]
recommend = "D"
+++

Protag gets reincarnated as the child of a famous 賢者 in a world that derives around set classes given by the goddess/etc, and as a result the protag and all of his siblings end up having magic-based professions, and the protag tries his best to be the very best mage there ever was. At least until the day he gets his class, or rather, when he doesn't get his class, ends up classless, and disowned by his family. Except he does have the gift of reincarnation, gets all of the skills from his past life along with all of his memories, which conviently was a super powerful OP top magician that knew he had reincarnation and set himself up for his second life.

<!--more-->

Is the protagonist stupid? lmao @ this "NTR" plot too.

All of the antagonists are comically evil charicatures and all of the "good guys" are boring wet blankets with no personality. The only redeeming aspect of this work is that the girls have cute designs, but this is also slightly ruined by the artist not really ever studying poses so everybody looks like they're constipated constantly with really strange arm placement/faces/etc. The plot makes no sense at all and all of the drama is manufactured garbage that comes about solely from the characters being dumber than a sack of rocks.
