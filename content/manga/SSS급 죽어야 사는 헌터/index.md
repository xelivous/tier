+++
title = "SSS급 죽어야 사는 헌터"
title_en = "SSS-Class Revival Hunter"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-10-02
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=175793"
raw = "https://webtoon.kakao.com/content/SSS급-죽어야-사는-헌터/2454?tab=episode"
md = "https://mangadex.org/title/4a973243-952e-44d7-a50f-883b4b7c9cc2/sss-class-suicide-hunter"

[chapters]
released = 96
read = 70

[lists]
recommend = "A"
+++

The work starts off kind of mediocre but it slowly improves over time; floors 11-20 were decent, and the martial arts world is kino. It just does a really good job of crafting a really super detailed/nuanced world filled all kinds of inhabitants. The title of this work is fucking dumb but I really enjoy it.

<!--more-->
