+++
title = "強くてニューサーガ"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=109553"
raw = "http://www.alphapolis.co.jp/manga/viewOpening/830000061"
md = "https://mangadex.org/title/9e80c192-6870-4500-afe9-e72e322689fe/tsuyokute-new-saga"
bw = "https://bookwalker.jp/series/83958"

[chapters]
released = 107
read = 89

[lists]
recommend = "C"
+++

Protag defeats the demon lord but in the process loses all of the friends he brought with him and eventually dies himself, but before he drew his last breath he reached out for the item the demon lord was protecting and it ended up sending him back into the past, back before all of his loved ones died. He then sets out to change the horrible future and ensure everybody survives.

<!--more-->

The plot is kind of annoying since it keeps introducing stuff like "hey i didn't tell you earlier but X happened in the past and that's why Y is happening now", or it goes to a flashback of his past and it's like "oh right that thing was there too". The author just keeps making shit up and then retconning it into the past as needed to advance the plot. It's still entertaining enough to be on the author's wild ride of bullshit but I personally don't consider it to be more than popcorn material.

I remember reading this one like almost a decade ago and then I got caught up and then stopped reading manga for a long while, so it's nice to come across it again at least.

:kannapraise: Twinbraid childhood friend :kannapraise:

loli mom is kind of cringe tho.
