+++
title = "ニトの怠惰な異世界症候群"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-13
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158142"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000310010000_68/"
md = "https://mangadex.org/title/30eb9ee7-4cfe-4036-95f9-4aac5e05b505/nito-no-taidana-isekai-shoukougun-saijaku-shoku-healer-nano-ni-saikyou-wa-cheat-desu-ka"
bw = "https://bookwalker.jp/series/245039"

[chapters]
released = 35.2
read = 35.1

[lists]
recommend = "D"
+++

Protag is excessively bullied  and commits suicide, but while he's literally falling from his death after jumping from his school rooftop, his entire class gets summoned to another world. To top it off all of his bullies get super top class stats and great jobs, but the protag gets the detestable (lol) healer class. Everybody treats him like contemptable garbage and go out of their way to make him feel even more like shit!

<!--more-->

What's the point of having excessively over the top revenge stories if it more or less drops the revenge after a few chapters and just becomes happy harem homelife. Is the message of this story that if you kill yourself you will get a cute isekai waifu and overpowered skills immediately afterwards and everything will be all right in the world, so you may as well off yourself now? Because that's what i'm getting from this.

It does eventually go back to the revenge plot after a while, but the protag is already so broken OP and the motivations seem so lackluster that it's not even really satisfying after he's spent all of that time flirting with his harem.

Ch10 has an epically cool and metal attack though fr.
