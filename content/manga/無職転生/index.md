+++
title = "無職転生"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=110083"
raw = "https://comic-walker.com/contents/detail/KDCW_MF01000009010000_68"
md = "https://mangadex.org/title/12108/mushoku-tensei-isekai-ittara-honki-dasu"
bw = "https://bookwalker.jp/series/153747/"

[chapters]
released = 93
read = 0

[lists]
recommend = ""
+++


<!--more-->
