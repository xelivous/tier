+++
title = "말단 병사에서 군주까지"
title_en = "Becoming the Monarch"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-03-28
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/99h7oxs/becoming-the-monarch"
raw = "https://series.naver.com/comic/detail.series?productNo=5912441"
md = "https://mangadex.org/title/899b5de6-9ad8-40bd-bc32-0fac4f7af86c/the-story-of-a-low-rank-soldier-becoming-a-monarch"
wt = "https://www.webtoons.com/en/fantasy/becoming-the-monarch/list?title_no=5549"

[chapters]
released = 138
read = 34

[lists]
recommend = "C"
+++

The protagonist is an average middle-aged mercenary who has spent his life dabbling in everything imaginable trying to make ends meet after losing his arm when he was very young. One day while escorting merchants on a mission everybody gets wiped out due to a brigand coveting the Ancient Artifact the merchants were secretly hiding, but not before the protagonist tries to sabotage the brigand in a last ditch effort before he dies. This results in the protagonist swallowing the artifact out of spite to make it worthless, which causes the protagonist to absorb the powers and essentially send him back in time to before he lost his arm. He now has an opportunity to change his life for the better by preventing the loss of his arm, and using all of his various life experience to keep all of his friends alive.

<!--more-->

The work does have a little bit of skill/stat spam popup windows but since the protagonist isn't an Isekai'd person he doesn't really understand what the terms mean other than knowing that "bigger numbers are better". Beyond that it's a relatively fun yet simple military-esque manwha. There aren't too many chapters adapted into english (officially) yet so maybe my opinion of it will change as it adapts more.

The art style and the protagonist's hairstyle in particular is kind of bad but wcyd.
