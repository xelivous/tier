+++
title = "魔法使いの婚約者"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2022-10-23
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146997"
raw = "https://comic-walker.com/contents/detail/KDCW_FL00000006010000_68/"
md = "https://mangadex.org/title/d45ba05a-a218-4c91-9f02-5efacb54e6e3/mahoutsukai-no-konyakusha"
bw = "https://bookwalker.jp/series/151095"

[chapters]
released = 24
read = 24

[lists]
recommend = "D"
+++

I do appreciate that the perspective keeps swapping between the two main protags like every few chapters in order to build up and compare their thoughts back and forth. However both of the protags are extreme brainlets and a lot of the decisions in the latter half of the story are extremely dumb. It honestly feels like they just wanted to shove as much drama into the end as possible no matter how farfetched. Kind of an overall unsatisfactory work.

<!--more-->
