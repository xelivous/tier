+++
title = "ポンコツが転生したら存外最強"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["completed"]
furigana = ["full"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    "male-protagonist",
    "multiple-isekaiers",
    "gore",
    "brainlet-heroine",
    "comedy-focused",
    "absurdist",
    "adult-loli",
    "beastiality",
    "body-swap",
    "ecchi",
    "nudity",
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151098"
raw = "https://seiga.nicovideo.jp/comic/36394"
md = "https://mangadex.org/title/14ee6a1f-fa8f-4b32-82e5-6648a2825c34/ponkotsu-ga-tensei-shitara-zongai-saikyou"
bw = "https://bookwalker.jp/series/192180"

[chapters]
released = 27
read = 27

[lists]
recommend = "C"
+++

Protag has an absolute brainlet of a childhood friend, along with another girl that clearly has a crush on him, when they're all out and about on a day off when the brainlet friend pushes them all into the path of a truck and they all get isekai'd together into the bodies of the heroes of that world who were on the run from the demon lord. They also all got powers specific to their natural abilities they had in their previous lives, although all of them are extremely meme.

<!--more-->

This is like an extremely bizarre comedy-focused manga where the entire point is thinking up more and more absurd events. The under-pinning of this work is "Anime Logic" at its utmost. And a lot of the absurd anime logic dips into sexual topics/etc quite frequently, like healing boobs, or being able to control all of the hair on your body to trap someone especially the pubic hair, or restoring someone from death by putting boob-shaped mushrooms on a mandarake and mixing that with the body.

This also seems to be a spinoff of a "normal" series featuring the same characters set on earth, which might give more of a backstory of the actual characters than this story.

If the style of comedy doesn't click with you then this will probably be unbearable.
