+++
title = "성좌들이 나만 좋아해"
title_en = "The Stars Are on My Side"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=187863"
raw = "https://webtoon.kakao.com/content/성좌들이-나만-좋아해/2518"
md = "https://mangadex.org/title/80bfed02-1f24-47d0-9fac-6fdb50b1aac6/i-m-the-only-one-loved-by-the-constellations"

[chapters]
released = 67
read = 58

[lists]
recommend = "B"
+++

Dude dies, and finds out he regresses except he doesn't retain anything when he does so, and meets some of his more OP past-selves in the afterlife. He then gets to watch yet another version of himself go through life with minor rng seed changes, but this time this new incarnation might have a way to actually break out of this pointless cycle of rebirth. It's a relatively decent series honestly. The title doesn't really make any sense at all, since he's not particularly loved by the constellations at all aside from himself.

<!--more-->
