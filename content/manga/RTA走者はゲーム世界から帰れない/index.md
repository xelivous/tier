+++
title = "RTA走者はゲーム世界から帰れない"
title_en = ""
categories = ["isekai", "game"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["original"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2023-10-22
tags = [
    "male-protagonist",
    "letter-rank-adventurers-guild",
    "trapped-in-a-game",
    "buggy-game",
    "comedy-focus",
]

[links]
mu = "https://www.mangaupdates.com/series/sc54mnx"
raw = "https://comic-walker.com/contents/detail/KDCW_MF01203736010000_68/"
md = "https://mangadex.org/title/ac743453-99ef-49e5-93e5-265cfff01fe0/rta-sousha-wa-game-sekai-kara-kaerenai"
bw = "https://bookwalker.jp/series/426415"

[chapters]
released = 8.1
read = 7

[lists]
recommend = "B"
+++

Protag is a speedrunner who basically speedruns soulslikes and regularly goes to major events like GDQ/etc, however one day when he's practicing his speedruns he blanks out and gets isekai'd into the game he has speedrun over and over again. He immediately starts speedrunning the game but in-person instead and starts live commentating to himself as if he was streaming. However the "NPC" companion and everybody else in the world is just bugging out of their minds at the protag's actions.

<!--more-->

The art is good, the girls are cute, and it's a good fun twist on the isekai/game genre.
