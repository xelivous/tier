+++
title = "6歳の賢者は日陰の道を歩みたい"
title_en = ""
categories = ["reincarnation"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-12-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=174549"
raw = "https://www.ganganonline.com/contents/rokusai/"
md = "https://mangadex.org/title/9913f21f-d531-458f-a980-c6657981f0cd/6-sai-no-kenja-wa-hikage-no-michi-wo-ayumitai"
bw = "https://bookwalker.jp/series/258914/"

[chapters]
released = 14.1
read = 14.1

[lists]
recommend = "D"
+++

Protag is an epic wizard in his previous life who managed to reincarnate himself with Magic before he died, and ended up in the body of some random kid in a good family. This world has a status window, skills, levels, etc, and for some reason reincarnating gives you an ultra OP bonus to your stats as if you're playing NG+ and everything is trivial. He comes across some random knight being harrased by orcs telling her to strip (lol), and he saves her then puts her to sleep so he can be even more of an cool badass, but not after she constantly blushes at the protag. The demon lord even comes straight to him since he realizes the sage that was a pain in his butt reincarnated, but gets obliterated by the protag with his NG+ powers!! The work is basically about the protag being super ultra powerful and can do anything, but just wants to be loved and no longer lonely like he was in his previous life. So he tries to desperately hide his abilities while striving to be normal and average and wacky hijinks ensue!

<!--more-->

If that sounds appealing to you, go ahead and read it I guess.
