+++
title = "언브레이커블"
title_en = "Unbreakable"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=184654"
raw = "https://series.naver.com/comic/detail.series?productNo=6273131"
md = "https://mangadex.org/title/0692faef-3726-4732-91fb-6e493344a051/unbreakable"

[chapters]
released = 52
read = 8

[lists]
recommend = "F"
+++

Random towers appear all across the world randomly one day, and everybody stops caring about them like 3 weeks later, but then they randomly activate and a world-wide death game starts and everybody seemingly has knowledge of their previous life except the protag!! And the protag was an epic badass in his previous life so everybody is out to kill him!!! And it's gory and epic!!!!!!

<!--more-->

I couldn't stomach more than 8 chapters of this, good luck if you try to go to the end. It hasn't been updated in a very long while and s2 will probably never happen so I consider it to be axed at this point.
