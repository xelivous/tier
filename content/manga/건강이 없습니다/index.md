+++
title = "건강이 없습니다"
title_en = "I'm All Out of Health!"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2025-01-16
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/rs44pu6/i-m-all-out-of-health"
raw = "https://page.kakao.com/content/57458770"
md = "https://mangadex.org/title/cd899926-20df-4f4b-b400-3b9917cf2882/i-have-no-health"
#bw = ""

[chapters]
released = 94
read = 94

[lists]
recommend = "B"
+++

The protagonist dies while reading a novel, and ends up reincarnating into an incredibly sick side character that is doomed to die soon that exists solely to serve as backstory for the real protagonist of the story. Her body is so deathly sick that it's painful to even eat and she can barely move. She now has to figure out a way to survive longer than her imminent death sentence, and she might need to change the entire story to do so.

<!--more-->

A majority of the work is the protagonist trying to somehow survive dying in any way she can, which inspires the broody-mcbrooderson autistic tall handsome super cool powerful male lead that wants to die and hates humans due to past trauma, to try living, and to try to get past his hatred for humans since she's so beautiful and radiant and inspiring. It's a work about a sick heroine that will die any moment, and a broken man that is deathly afraid of human touch due to past traumas. A work about a protagonist that keeps wanting the male lead's dick and thinking he's handsome but constantly pushing him away while he aggressively pursues her. This is a work about 2 characters trauma dumping on them while they slowly get hornier about each other until they remove all obstacles from their lives until they're left alone and have sex with each other.

This is a pure romance series with some minor political drama on the side for spice, and a ton of random trauma dumping for good measure. Main highlight of the work is that all of the characters are competent, and nothing really happens without a grand master plan to drive it. As a result it's a fairly quick and well-paced work, although the early chapters feel kind of scuffed with the protag sleeping or being in pain 80% of the time. There were also a few other events that the story could've explored but didn't, largely because it had no reason to when it didn't contribute to the romance with Mr. Tall Dark and Autistic. However if all you want is watching a cute romance between a beautiful woman/man then this clears that mark at least. I feel like there's much better and more compelling romances out there yet it's a nice "comfy" pick in case you've already read the other better ones.
