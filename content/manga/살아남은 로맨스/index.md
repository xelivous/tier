+++
title = "살아남은 로맨스"
title_en = "Surviving Romance"
categories = ["isekai", "reincarnation"]
demographics = ["josei"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2023-09-29
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=176458"
raw = "https://comic.naver.com/webtoon/list.nhn?titleId=764129"
md = "https://mangadex.org/title/9be148dc-e8fc-4ca5-bd78-e0a142a27ca8/survive-romance"
wt = "https://www.webtoons.com/en/horror/survive-romance/list?title_no=2607"

[chapters]
released = 102
read = 102

[lists]
recommend = "S"
+++

the rezero of gakkou gurashi of shoujo romance.

<!--more-->
