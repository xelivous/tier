+++
title = "地味な剣聖はそれでも最強です"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-02
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=151025"
raw = "https://pash-up.jp/content/00000194"
md = "https://mangadex.org/title/31002/jimi-na-kensei-wa-sore-demo-saikyou-desu"
bw = "https://bookwalker.jp/series/197788/"

[chapters]
released = 77
read = 77

[lists]
recommend = "D"
+++

Protag is crossing the street without paying attention, gets hit by a truck, and gets isekai'd. And apparently the protag died because the protag's name sounded so oldschool that god just decided to blow out his candle for funsies and decides to allow him to reincarnate into an isekai haha lol. But instead of just giving the protag OP powers outright, he has to train in the way of the blade and become immortal first through hard work through menial tasks for hundreds of years first!! After training for hundreds of years a random family walks into the forest, dies, and leaves their baby behind, at which point the protag gets tasked with raising her among normal humans until she comes of age and to become her "father".

<!--more-->

It's like one punch man but even more mediocre and without the good side characters and not really a parody so unironically somehow worse than something that is already bad. Protag swings his sword once, other people call him the most boring existence alive, they sigh, the story moves on, repeat. That's the entire work.
