+++
title = "この世界の攻略本を拾ってしまいました"
title_en = ""
categories = []
demographics = []
statuses = ["hiatus"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-25
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/4813il1/kono-sekai-no-kouryakuhon-wo-hirotte-shimaimashita"
raw = "https://comic-walker.com/contents/detail/KDCW_CW01203739010000_68/"
md = "https://mangadex.org/title/98017a70-5580-40fb-9d69-69e377325484/kono-sekai-no-kouryakuhon-wo-hirotte-shimaimashita"
bw = "https://bookwalker.jp/series/406714"

[chapters]
released = 7
read = 0

[lists]
recommend = ""
+++



<!--more-->

