+++
title = "ネタキャラ転生とかあんまりだ"
categories = ["isekai", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-05
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165603"
raw = "https://magcomi.com/episode/13933686331623324202"
md = "https://mangadex.org/title/fd41659a-e4ce-458f-b087-8032b7a5fab9/neta-chara-tensei-toka-anmarida"
bw = "https://bookwalker.jp/series/266743"

[chapters]
released = 20
read = 13.5

[lists]
recommend = "A"
+++

VRMMOs kept popping up left and right in the protag's world until all of a sudden some madman made an ultra massive superrealistic world with permadeath in it. Protag loves this game and even has multiple characters, a main knight, an alt that's a mage, and also has a third meme character for roleplay. Unfortunately the protag reincarnates as his chuuni meme character that is female (he's male). The reason for him reincarnating as his meme character is because it's the only time he was actually having fun and enjoying life, since it was a form of escapism from his boring as hell dead-end life.

<!--more-->

The art is kami; My initial gut reaction was "the faces look weird" but then i realized that it's so stylized that it's kino and good actually. The overall panelling/character deformation and general character writing carries it super hard to the point that I really enjoy this. I feel like it'd do really well as an anime if it managed to keep this artstyle intact somehow. This artist hasn't worked on anything else as far as I can see but I want them to since they're great. It's a great pairing overall with the tone/character writing of the series