+++
title = "神眼の勇者"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=144994"
raw = "http://seiga.nicovideo.jp/comic/34131"
md = "https://mangadex.org/title/22372/shingan-no-yuusha"
bw = "https://bookwalker.jp/series/148906/"

[chapters]
released = 57.2
read = 31

[lists]
recommend = "B"
+++

The humor is good.

<!--more-->
