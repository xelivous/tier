+++
title = "장르를 바꿔보도록 하겠습니다"
title_en = "The Villainess Flips the Script"
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2025-01-15
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/347ithy/the-villainess-flips-the-script"
raw = "https://page.kakao.com/content/56934318"
md = "https://mangadex.org/title/ccf19b16-e6c5-4f16-b327-8399e61c2e5b/it-s-time-to-change-the-genre"
#bw = ""

[chapters]
released = 119
read = 107

[lists]
recommend = "A"
+++

The protagonist regularly enjoys reading tragedy works where the protag suffers only to one day get run over by a car and wakes up inside one of the tragedy novels she read, but as a side character who gets killed off almost immediately shortly after the work starts. Specifically she's the "Evil" aunt of the story's protagonist who had to reluctantly take care of him after his parents died, and she now has to try to figure out a way to reverse her outcome and live beyond her destined death.

<!--more-->

This work is kind of strange since it regularly hints that there's more to the story than anybody is letting on but it continues to string it out for a long time leaving the protagonist to kind of just struggle along. The male lead is kind of excessively baffling and almost exists to be the ideal man to the point it's hyper unrealistic and feels like little more than an NPC, probably even moreso than a lot of random heroines in male harem works. He's like a full 2 feet taller than her, the highest nobility you can be without literally being the king, rich enough that he can buy literally everything 1billion times over, super considerate and also nice and sweet and handsome with a nice voice and super hot and epic, super strong military commander dude, and just immediately falls in love with the protag almost immediately upon first meeting her despite always giving the cold shoulder to every other woman previously because she's just so magical. Although he is at least somewhat extremely austistic, which is about his only "flaw".

Shoutout to the protag choosing a really cute twinbraid maid.

Even despite all of that it's still a really cute work overall with a very nice competent protagonist, and a ton of really nice supporting characters that are a joy to read. All of the misunderstandings are solved with outright communication (because the male lead is too autistic for anything else) and despite being like completely and utterly filled with the typical Noble drama it's actually not like unbelievable or overdramatic or drawn out for the sake of the story; Every bit of drama is solved relatively quickly in a satisfactory manner while still being engaging and well done.

It really feels like the author had the entire work plotted out before they began writing the original web novel, or at least it feels that way from reading this adaptation. Everything is foreshadowed well, everything is very consistent, it's just well done overall. It also has pretty great art overall. I enjoyed reading it a fair bit even my slight dislike for the male lead. It's a nice simple shoujo that has been done many times before, but with really great execution. Would def rec to fans of shoujo/josei romance.
