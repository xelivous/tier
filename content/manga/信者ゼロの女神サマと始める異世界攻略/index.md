+++
title = "信者ゼロの女神サマと始める異世界攻略"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166047"
raw = "https://comic-gardo.com/episode/13933686331628922960"
md = "https://mangadex.org/title/9937f7da-7913-49cb-a354-2e4ecd9cbd52/clearing-an-isekai-with-the-zero-believers-goddess"
bw = "https://bookwalker.jp/series/280875/"

[chapters]
released = 42.3
read = 31.5

[lists]
recommend = "D"
+++

Protag's entire class gets stranded in the middle of a snowstorm on some kind of field trip in a bus, and end up freezing to death alongside his childhood friend (girl) while playing video games on what is probably a psp. However a majority of his class reincarnates/isekais into a fantasy world and almost every single one of them has broken OP skills, except for the protagonist, who has absolutely garbage stats and a shit job, but also gets a unique skill about being an RPG gamer (gamers rise up). He makes the most of his abilities by just grinding goblins all day long until he has ultra tier magic control and slowly builds up a harem of cute girls.

<!--more-->

It's arifureta without the cringe edgyness and without the meme powerup to make him ultra godtier. an actual bottom tier dude just trying his best to make the most out of his absolute shit powers. The art is kind of subpar and the story is pretty lackluster as well. This manga asks hard hitting questions like "would you sell your soul to the devil if she was a cute anime girl?"

tfw no cute elf mage gf. 
