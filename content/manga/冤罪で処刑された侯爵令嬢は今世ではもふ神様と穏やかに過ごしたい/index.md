+++
title = "冤罪で処刑された侯爵令嬢は今世ではもふ神様と穏やかに過ごしたい"
title_en = ""
categories = ["reincarnation"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-06-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=166159"
raw = "https://freeserial.cmoa.jp/title/202003165/"
md = "https://mangadex.org/title/e66968c0-c42a-44ef-96d0-52047036352f/the-daughter-of-the-marquis-who-was-executed-under-false-accusation-wants-to-spend-a-peaceful-life"
#bw = ""

[chapters]
released = 59
read = 21

[lists]
recommend = "F"
+++

The faces get a little derpy at times but otherwise the art is fine. Fairly standard entry in the noble girl reincarnation series where a girl is set to be executed (this time she's innocent) and then gets sent back in time for reasons and tries to change her fate. The antagonist is looney and is kind of just over the top, and having a bunch of gods just handing out powers willy nilly all over the place makes for a weird story. The protagonist is also just a complete dumbass that constantly makes bad decisions. Just assassinate the antagonist and all of the issues would be solved she's just a powerless non-attribute what she's gonna do frfr.

<!--more-->

The work also constantly shoves in random japanese culture, like the one dude is literally just from isekai-japan, tons of random japan food that people like, japanese school events, etc. Also check out that high quality florescent lighting and traditional japanese classroom in ch19 lmao. Like I understand that this is a japanese manga for a japanese audience but when you're making a reincarnation setting that doesn't have any isekai influence at all (seemingly) it just feels like even more of a cheap writing fallback so that the author doesn't have to think about actually properly building up and defining an entirely new world for their characters to live in.

The somewhat poor art, the braindead protag, the gods that do basically nothing but exist to send out powers to everybody, the dumb plot developments, the excessively over the top antagonist, just about everything to do with this work is annoying or dumb so it's really hard to recommend to anybody.
