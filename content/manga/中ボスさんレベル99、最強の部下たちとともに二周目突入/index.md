+++
title = "中ボスさんレベル99、最強の部下たちとともに二周目突入"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=156959"
raw = "https://comic-walker.com/contents/detail/KDCW_AM05201142010000_68/"
md = "https://mangadex.org/title/78ccd6df-51db-46d2-8f9f-1a55a13bfac2/ch-boss-san-level-99-saikyou-no-buka-tachi-to-tomo-ni-nishuume-totsunyuu"
#bw = ""

[chapters]
released = 11
read = 0

[lists]
recommend = ""
+++



<!--more-->

