+++
title = "地獄の業火で焼かれ続けた少年。最強の炎使いとなって復活する。"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-25
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=169085"
raw = "https://pocket.shonenmagazine.com/episode/13933686331666633377"
md = "https://mangadex.org/title/3edb0966-0a32-4afc-8107-637d03c26d3a/jigoku-no-gouka-de-yaka-re-tsuzuketa-shounen-saikyou-no-honou-tsukai-to-natte-fukkatsu-suru"
bw = "https://bookwalker.jp/series/275655"

[chapters]
released = 105
read = 41

[lists]
recommend = "C"
+++

Protag basically does nothing but exercise and physical training all day long and lives in a village guarding a mysterious gate, and one day it's decided that he will be sacrificed to the gate. As a result his body burns to ashes, and then his soul also burns to ashes. However he doesn't die and instead becomes one with the flames after an eternity of suffering.

<!--more-->

The protag is pretty enjoyable and the character designs are good too. The art overall is nice. However the protagonist doesn't really have a goal and instead is just tagging along with the very first people he sees and protects them with them unable to do anything of value repeatedly. You could replace those characters with a rock with a face painted on it and the story wouldn't really be any different, and in fact might even be better due to meme value. Not enough emotional support pet rocks in reincarnation stories.
