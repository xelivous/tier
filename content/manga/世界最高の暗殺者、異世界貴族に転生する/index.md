+++
title = "世界最高の暗殺者、異世界貴族に転生する"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152574"
raw = "https://web-ace.jp/youngaceup/contents/1000117"
md = "https://mangadex.org/title/34221/sekai-saikyou-no-assassin-isekai-kizoku-ni-tensei-suru"
bw = "https://bookwalker.jp/series/221175/"

[chapters]
released = 26.1
read = 24

[lists]
recommend = "C"
+++

Protag is a chad assassin who is nearing retirement from old age, but in his last mission he gets "betrayed" assassinated by his guild and kind of regrets being so loyal. As a result he decides to only live for himself if he ever gets another chance; lo and behold he gets reincarnated by the bimbo sneed goddess who wants the protag to assassinate the hero.

<!--more-->

The scene with his magic teacher loli is a little too pointlessly erotic. Also one of the first things the protag does when he learns how to make magic is to create guns... Also it goes all in on the ecchi fanservice with his assistant etc. However the core work is fairly fun to read regardless especially when the protag goes up against the unstoppable force that is the Hero.

I feel like you're best off watching the anime adaptation and then maybe reading the LN if you still care about the series after that; the manga skips over a fair amount of detail to shove in more ecchi scenes which I guess if you want that then read this version instead? Although if you want that just read the author's other work which is just bad sex scenes constantly.
