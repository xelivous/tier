+++
title = "境界迷宮と異界の魔術師"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation", "game"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149230"
raw = "https://comic-gardo.com/episode/10834108156661710153"
md = "https://mangadex.org/title/28536/kyoukai-meikyuu-to-ikai-no-majutsushi"
bw = "https://bookwalker.jp/series/192924/"

[chapters]
released = 61
read = 32

[lists]
recommend = "F"
+++

Protag was playing a VRMMO when one day he got literally knifed by a dude while he was in the game, died, and as a result he became trapped in the game. However he's in the body of some random noble kid who gets bullied by his older brothers, but that won't stop the protag from just buttblasting them and setting off on his own in the span of like 2 panels. The protag's backstory and overall plot in the game is just completely glossed over with a panel or two of flashbacks that aren't expanded on, and the focus is instead on the protag just doing whatever he wants with his ultra tier magic knowledge from the game and his broken OP maid that was conveniently by his side, devoted to him, simply sealed to prevent her power from going out of control.

<!--more-->

The beginning is just a bizarre speedrun of a story, and the rest of it is just the protag picking up harem members left and right while Owning hilariously evil villains and/or demons. The noble society aspects are largely superficial and not particularly interesting. The art is also fairly lazy in that there's basically no backgrounds a majority of the time.
