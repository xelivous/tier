+++
title = "なぜ僕の世界を誰も覚えていないのか"
title_en = ""
categories = ["reincarnation"]
demographics = []
statuses = ["ongoing"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-07
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=148547"
raw = "https://comic-walker.com/contents/detail/KDCW_MF02200222010000_68/"
md = "https://mangadex.org/title/91503d4c-16d6-4962-9eca-93557667066c/naze-boku-no-sekai-wo-daremo-oboeteinai-no-ka"
bw = "https://bookwalker.jp/series/164792"

[chapters]
released = 45.2
read = 3

[lists]
recommend = ""
+++



<!--more-->

This work is kind of hard to categorize on this list since it's kind of parallel-world where the entire world except for the protag reincarnates after someone changes the timeline, which is also kind of an isekai but also not.
