+++
title = "賢者の弟子を名乗る賢者"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=134730"
raw = "http://comicride.jp/pupil/"
md = "https://mangadex.org/title/19517/kenja-no-deshi-wo-nanoru-kenja"
bw = "https://bookwalker.jp/series/112079/"

[chapters]
released = 68
read = 30

[lists]
recommend = "C"
+++

Honestly a lot of the earlier chapters aren't that interesting but there's a few later on that are decent. Basic starting point, and on a surface level premise, is similar to Overlord.

<!--more-->
