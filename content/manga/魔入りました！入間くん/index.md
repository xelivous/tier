+++
title = "魔入りました！入間くん"
title_en = ""
categories = ["isekai"]
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2024-10-07
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143087"
md = "https://mangadex.org/title/21063/mairimashita-iruma-kun"
bw = "https://bookwalker.jp/series/115382/"

[chapters]
released = 366
read = 366

[lists]
recommend = "S"
+++

kino shounen about ambitions, dreams, and friendship. every once in a while there's some strings of basically filler chapters where i assume the author is taking a breather to hash out the next kino plot to drop, but it's worth it since the highs hit high.

<!--more-->
