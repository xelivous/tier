+++
title = "魔力0で最強の大賢者"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=174058"
raw = "https://pocket.shonenmagazine.com/episode/3269754496479646471"
md = "https://mangadex.org/title/d1173b5a-7c10-4762-8890-f7f7e3986c98/the-greatest-philosopher-with-zero-magic-power"
bw = "https://bookwalker.jp/series/283628/"

[chapters]
released = 37.3
read = 10

[lists]
recommend = "F"
+++

A dude hailed as a great magician gets reincarnated, except he's always had 0 magic power and can't use any magic at all, and all he's doing is "abusing physics" to create supernatural phenomena. You know, like moving your arms super fast to create shockwaves to slice metal dummies in half, or increasing your poison antibodies in your body to extreme levels then shooting it out of your body to kill tough creatures who don't have the antibodies. Best part is keeping your antibodies you developed in your previous life into your new body. You know, normal physics things. It's supposed to be a comedy but when the execution itself seems fundamentally flawed it just feels weird. Also it tends to steal its ideas for powers/character-traits from really popular shounen series so it feels like you're just reading a really cheap knockoff the entire time.

<!--more-->

Never before have I had the urge to frodopost a manga so hard. You need to completely turn off your brain or you can and will be harmed while reading this.

The only redeeming feature of this manga is the art, which is unfortunate that the artist was put onto this project and not lierally any other in existence. Also there's a cute pseudo-twinbraid girl that shows up in ch4/5 which is really cute at least so it has that going for it at least. Cover art for vol2 has some meme eyes tho.