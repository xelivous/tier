+++
title = "천마육성"
title_en = "Murim RPG Simulation"
categories = ["reincarnation", "game"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-04-13
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/0sl9clp/murim-rpg-simulation"
raw = "https://series.naver.com/comic/detail.series?productNo=6651735"
md = "https://mangadex.org/title/c8f0f000-35be-4f16-9b68-f2aea7e67fdb/murim-rpg-simulation"
wt = "https://www.webtoons.com/en/action/murim-rpg-simulation/list?title_no=3779"

[chapters]
released = 118
read = 109

[lists]
recommend = "C"
+++

The protagonist is some low level stooge of the Demon Sect off on a mission where he was set up to die, after trying to sneak a glance at a superior officer's office in an attempt to get stronger. However as soon as he dies he's brought to some weird alternate dimension with nothing around and is offered the chance to restart from the beginning as if it were a game. At first he doesn't understand the system (or what a game is) but slowly learns how to gain more lives while he slowly builds up his power in order to get further in his life and not end up dying a pointless death.

<!--more-->

This work is both terrible and kino, sometimes simultaneously. This is the most system-core work imaginable where everything is extremely gamified and the other characters almost feel entirely like NPCs at times. The pacing is amazing and there's almost never any dead time, the chapters are long and filled with content, and the art is probably some of the best in webtoons. But it gets so bogged down in RPG and power fantasy that it's sometimes a little hard to take it seriously. It does do a relatively good of balancing the protagonist being overpowered while also regularly encountering people that will slap his shit silly regularly, and it doesn't delve all too much into the politics; it's just endless dudes being bros and working out while gaining +9999 stats.

Also pretty much all of the female characters are the curviest kpop idols in existence despite a few of them being "trained warriors" which is excessively meme and kind of cringe. The heroine/romance aspect that is hinted early on doesn't really go anywhere since she just becomes an almost inconsequential NPC as the story goes on, since the only person with any real agency in this story in the protagonist.

I feel like this is the best possible way to write a story that goes all in on "turn-based real-time action combat in a murim fantasy scenario" which is why it's kino but it's also just kind of really bad at times which makes it hard to recommend.
