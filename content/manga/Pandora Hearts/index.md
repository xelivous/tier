+++
title = "Pandora Hearts"
title_en = ""
statuses = ["completed"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=9709"
md = "https://mangadex.org/title/1057/pandora-hearts"
bw = "https://bookwalker.jp/series/2167/"

[chapters]
released = 104
read = 0

[lists]
recommend = ""
+++

<!--more-->

