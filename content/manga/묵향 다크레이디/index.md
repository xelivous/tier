+++
title = "묵향 다크레이디"
title_en = "Mookhyang: Dark Lady"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["hiatus"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-10
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155073"
raw = "https://page.kakao.com/home?seriesId=53216022"
md = "https://mangadex.org/title/4ffd1f8f-ab82-4de4-9dea-e0608c043d7b/mookhyang-dark-lady"

[chapters]
released = 167
read = 99

[lists]
recommend = "F"
+++

Protag is like a super powerful martial artist so some of his opponents in his original world decide to isekai him to get him out of the picture. he spends 2 years learning the language at some random villager's house then goes out into the world and tries to just find a way back to his original world. He fights and he fights. And then eventually some random magician blindsides him with a spell that causes him to turn into something he "dislikes", and he currently really dislikes one of his travelling companions so he loses all of his fighting skill and turns into basically a duplicate of a small girl. There is of course no way to undo this spell for whatever reason, so now the protag that used to be super top tier is now complete bottom tier and has to work back to the top to become the very best once again, like those extremely shit RPGs that have you test drive the high level character for the first 20 minutes and then slap you back down into peasant tier slowly to build back up to the top tier again. Protag does weird handwavey bullshit and quickly builds up his power to be ultra tier again and then there's this weird plotline where he's serving the country of that same magician who cursed him in the first place along with tons of weird mecha fights.

<!--more-->

Apparently this work is an adaptation of part 2 of some random novel, and then they later went back and adapted part 1 (which isn't an isekai), but it's bad as well.
