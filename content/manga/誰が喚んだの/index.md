+++
title = "誰が喚んだの!?"
title_en = ""
categories = []
demographics = []
statuses = ["axed"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-29
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series.html?id=153444"
raw = "http://seiga.nicovideo.jp/comic/37654"
md = "https://mangadex.org/title/392c247a-43b2-4cca-aa92-940e323f5182/dare-ga-yonda-no-isekai-to-game-dzukuri-to-recruit-shoukan"
bw = "https://bookwalker.jp/series/176287"

[chapters]
released = 12
read = 0

[lists]
recommend = ""
+++



<!--more-->

