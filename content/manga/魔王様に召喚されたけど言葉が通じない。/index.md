+++
title = "魔王様に召喚されたけど言葉が通じない。"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["original"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-18
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=159148"
raw = "https://comic-meteor.jp/maotujinai/"
md = "https://mangadex.org/title/bcab1d1d-029f-439b-893e-6e8553b007ba/i-was-summoned-by-the-demon-lord-but-i-can-t-understand-her-language"
bw = "https://bookwalker.jp/series/263792"

[chapters]
released = 30.5
read = 30.5

[lists]
recommend = "A"
+++

Protagonist is just minding his own business as a normal college dude when all of a sudden he wakes up in a castle and a blue-haired loli with horns is smiling at him, but he is unable to understand what she is saying at all. He now has to learn to live with a massive language barrier between them and not really understanding much beyond the fact that she is seemingly the demon lord.

<!--more-->

It's a moe romance work that goes all in on the language barrier. It's good. Why did this series have to get axed... it's not fair...

Twinbraid mage in ch6 :pray: and absolutely kami kino twinbraids in ch20.

