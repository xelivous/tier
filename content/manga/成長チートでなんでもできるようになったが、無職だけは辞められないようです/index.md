+++
title = "成長チートでなんでもできるようになったが、無職だけは辞められないようです"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-28
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=145875"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000053010000_68/"
md = "https://mangadex.org/title/e881168f-ef99-41ff-88cf-1cf21c152635/seichou-cheat-de-nandemo-dekiru-you-ni-natta-ga-mushoku-dake-wa-yamerarenai-you-desu"
bw = "https://bookwalker.jp/series/154003"

[chapters]
released = 83
read = 53

[lists]
recommend = "B"
+++

Protag is an orphan living with his younger sister, desperately trying to find a stable job so he can provide for the both of them, but hasn't had much luck and repeatedly gets denied. One day when heading to an interview he almost gets run over by a truck, which was transporting horses, and then gets killed by the horses (proxy-truck'd) and ends up, but gets offered to get isekai'd by random chance along with some basic abilities of his choosing. However the goddess is lazy and forgets to actually send him to the isekai after giving him an ability, so another goddess comes along (who is even lazier), gives him another ability assuming he's a fresh arrival, and sends him on his way, essentially giving him his desired ability twice over.

<!--more-->

One of the first things he does in this work is buy a cute big booba wolfgirl slave for various skill/job-related reasons since he's basically sidestepping all kinds of common knowledge in this world with the broken cheats he received from the two goddesses. This work is ultimately a skill/stat spam fiesta at the beginning where he does meme system abuse and becomes stronger than everybody else with little to no effort. There isn't really an overarching plot that the protagonist is working for besides raising up his levels and seeing all of the weird broken shit he gets from doing so, and as a result this work becomes extremely character-focused, which is fine since all of the characters are well-written and memorable. There's also a good amount of reincarnated people in this story all with their own weird blessings.

I thought the art was kind of bad at the start but it definitely grew on me over time, and the plot is similar in that i've grown more attached to it each chapter. キャロ's smile must be protected tbh. I'm not really sure how to feel about the transgender clothing shopkeep since it borders on the charicature/meme portrayals you often see in japanese media but it's not outright disrespectful at least. The "team rocket duo" are fun. You can really tell the author/artist has a wolfgirl fetish though, and I also now too understand the appeal of wolfgirls after being brainwashed from all of the panels (the sniffing hugging scenes are literally ojisan bait fr). The artist also doesn't shy away from having girls make incredibly ugly faces at times which is based.

On a re-read the beginning of this work is aggravating, and the protagonist constantly falling back on "but i don't have a job (unemployed) uwu..." for everything and causing misunderstandings is super annoying. Easily the worst part of the work. I still enjoy it regardless and feel like it does improve a fair bit over time, so it's less that it grew on me over time but moreso that the beginning of this work is just godawful. However I still do feel that キャロ's arc and her character in general is pretty good and worth reading this work for (outside of the wolfgirl fetish brainwashing as well). In fact most of the plot events and characters introduced after the starting arc (with its terrible art) is good, so you really just have to push through the garbage start. Honestly thinking about it, this work is very similar to [異世界迷宮でハーレムを](/manga/異世界迷宮でハーレムを) (just with worse art, no explicit sex scenes, and more of a focus on comedy). The doujinshi arc is also based. 

The further I read the more I remember why I thought this work was good enough to get a B despite the beginning arc(s) being turbo trash, it actually becomes pretty good. The work goes through a lot of evolutions throughout its runtime which makes it constantly feel fresh and engaging. It regularly builds up a ton of varying characters and at times just completely ignores the main protagonist's happy harem slave homelife and instead goes off on an adventure for many chapters with various "side" characters while greatly expanding the world and plot. Only to go back to the protagonist and have based ecchi antics again.
