+++
title = "레벨업 닥터 최기석"
title_en = "Level-Up Doctor"
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2023-01-09
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/8v0wzfs"
raw = "https://page.kakao.com/home?seriesId=59495324"
md = "https://mangadex.org/title/6d342a6b-d2ac-4a82-a3e6-233fc1cc9ff6/level-up-doctor"

[chapters]
released = 50
read = 37

[lists]
recommend = "F"
+++

The levelup portion of this is extremely in your face and meme and almost feels like it's making light of the entire experience cheapening the work overall. The work would be quite a fair bit better if it just didn't have that aspect at all. There's other better works that go all in on "wikipedia-tier medical knowledge protag operates on patients repeatedly while everybody is impressed with them" that there isn't much of a reason to really read this one unless you want to compare to the others.

<!--more-->

