+++
title = "異世界迷宮の最深部を目指そう"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-05
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=155332"
raw = "https://comic-gardo.com/episode/10834108156668670658"
md = "https://mangadex.org/title/39510/isekai-meikyuu-no-saishinbu-o-mezasou"
bw = "https://bookwalker.jp/series/233711/"

[chapters]
released = 28
read = 25

[lists]
recommend = "S"
+++

The protagonist wakes up in a dungeon without knowing how he got there and almost gets himself killed before he manages to find his way out into the great unknown. Additionally he has weird skills that don't display properly in his status window that keep forcibly supressing his emotions.

<!--more-->

The protag is interesting, he has a severe complex about the world and his thoughts of what he should be doing. dia is pretty cool as well; Her complex with wanting to be the ideal shounen protagonist and general gender identity is moe. The characters carry this series hard and the world itself is decent enough that this is kind of pretty cool.