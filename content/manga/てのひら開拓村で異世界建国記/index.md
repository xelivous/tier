+++
title = "てのひら開拓村で異世界建国記"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = ["light-novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-23
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152212"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000072010000_68/"
md = "https://mangadex.org/title/ad59c8bb-e645-41d2-bc16-65b1b94cafd6/tenohira-kaitaku-mura-de-isekai-kenkokuki-fueteku-yome-tachi-to-nonbiri-mujintou-life"
bw = "https://bookwalker.jp/series/185885/"

[chapters]
released = 42
read = 41

[lists]
recommend = "D"
+++

It's a village building series with tons of convenient asspull abilities mixed in. Also the church is evil, as always. Also beastman discrimination, also like usual. He keeps getting more and more women in his harem as well. There's very little in the way of satisfying drama or actual civilization building, so it just feels weird to read. The protag also just kind of becomes more and more passive over time and tries to complete everything with the power of love and peace and friendship, except for when it doesn't work out and ends up bitchslapping everybody out of his way regardless.

<!--more-->
