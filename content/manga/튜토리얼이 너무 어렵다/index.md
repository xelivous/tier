+++
title = "튜토리얼이 너무 어렵다"
title_en = "The Tutorial Is Too Tough!"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-08-18
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=177212"
raw = "https://page.kakao.com/home?seriesId=56443245"
md = "https://mangadex.org/title/5c0a6150-502b-4d88-a46e-50b83768fe8f/the-tutorial-is-too-hard"

[chapters]
released = 179
read = 162

[lists]
recommend = "C"
+++

It's a sao-like where you have to fight through 100 floors, and you can't leave until you complete them, and since the protag thinks he is hot shit he chose the highest level difficulty and has to find some way to clear it basically solo. This mostly involves lots of self-harm and grinding of basic resistances/etc while the protag slowly loses his mind. 

<!--more-->

One of the bad parts about this manga is that it likes doing random timeskips to future events to kind of teases the reader about what is going to happen in the future but it's done in the dumbest conceivable way that doesn't really enhance the story at all, and it also skips over floors/events only to bring them up later when the protag reminisces about them. It's like the worst way to do a non-linear story when the core foundation of the story is linearly increasing the floor number the protag is on. Which is unfortunate since it's somewhat entertaining beyond that.

That being said the "martial art dojo" world with the combat orcs is actually pretty good all things considered. Barely passable. The amogus arc is kind of meme but overall decent as well. Okay honestly outside of the work constantly flopping between timelines constantly this work is actually pretty good and kino. Shoutout to ch161+ being traditional isekai.
