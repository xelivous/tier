+++
title = "邪竜転生"
title_en = ""
statuses = ["axed"]
demographics = ["shounen"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-16
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=136131"
raw = "https://www.alphapolis.co.jp/manga/official/7000146"
md = "https://mangadex.org/title/20255/jaryuu-tensei"
bw = "https://bookwalker.jp/series/111659/"

[chapters]
released = 47
read = 33

[lists]
recommend = "D"
+++

Protag gets run over by a distracted lovey-dovey couple basically having sex at the wheel and reincarnates as a despicable evil dragon, and regularly gets challenged by tons of high level mooks wanting to slay him for being so evil. Except since he's just a normal japanese salaryman he's not evil and instead just takes care of the slimes in the neighborhood etc. However one day his slimes get killed by the demon lord's goons, he kills the demon lord, and obtains a pill to become human and he can even transform back and forth between being a human or dragon willy nilly.

<!--more-->

It's a work about being an evil dragon but he's in human form for a large majority of it and he's wholesome chungus so what's even the point really.
