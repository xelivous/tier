+++
title = "老後に備えて異世界で８万枚の金貨を貯めます"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["isekai"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147516"
raw = "http://seiga.nicovideo.jp/comic/27620"
md = "https://mangadex.org/title/22843/saving-80-000-gold-coins-in-the-different-world-for-my-old-age"
bw = "https://bookwalker.jp/series/139071/"

[chapters]
released = 95
read = 62

[lists]
recommend = "A"
+++

badass competent protag destroys medieval peasants with Guns and Science

<!--more-->
