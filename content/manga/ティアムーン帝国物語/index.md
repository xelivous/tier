+++
title = "ティアムーン帝国物語"
statuses = ["ongoing"]
demographics = ["shoujo"]
furigana = ["partial"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-18
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=157049"
raw = "http://seiga.nicovideo.jp/comic/43303"
md = "https://mangadex.org/title/40472/tearmoon-empire-story"
bw = "https://bookwalker.jp/series/230301/"

[chapters]
released = 26
read = 25

[lists]
recommend = "A"
+++

Protag was an antagonistic bitch in her previous life and got guillotine'd after a rebellion, but miraculously managed to go back in time to when she was younger. As a result she learns to not take anything she has for granted and tries her best to actually change for the better. I'd say the strong point of this series is that while the protagonist is repentant, she's still as brainlet as ever; Magically reincarnating doesn't immediately make you smarter if you never did anything to improve yourself in your last life, and instead of skipping over this process like a lot of series with a Training Montage/arc, it goes all in on the brainlet status. also freckles are justice; Anne-sama i kneel.

<!--more-->

It's a fairly entertaining series overall and I do enjoy reading it. It does get a little too simplistic/repetitive at times with the narrative tsukkomi but beyond that it moves at a decent pace and doesn't stick too long on any one plot point so you never really get tired of the individual plot points. The romance is pretty good as well.
