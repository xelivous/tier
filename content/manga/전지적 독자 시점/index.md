+++
title = "전지적 독자 시점"
title_en = "Omniscient Reader's Viewpoint"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = ["weekly"]
lastmod = 2024-08-07
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=167681"
raw = "https://comic.naver.com/webtoon/list?titleId=747269"
md = "https://mangadex.org/title/9a414441-bbad-43f1-a3a7-dc262ca790a3/omniscient-reader-s-viewpoint"
wt = "https://www.webtoons.com/en/action/omniscient-reader/list?title_no=2154"

[chapters]
released = 216
read = 214

[lists]
recommend = "B"
+++

The protagonist spends 10 years reading a shitty webnovel to the and after persevering being the only view and comment (somehow) on all of the chapters, he finishes it right as the world ends, and the end of the world follows the plot of that random webnovel 1:1. Essentially Earth stops being free-to-play and becomes pay-to-play, and the only way to pay is to do missions for the fickle constellations and the murder-hungry dokkaebi that are trying to put on an entertaining show for all of the intergalactic onloonkers.

<!--more-->

The early translation for this work is kind of weird, since it tries localizing it for an american audience but still keeps a ton of things extremely korean and constantly mixes everything a way that ends up making no sense. A ton of later chapters have baffling grammar mistakes and typos in them as well, which is kind of disappointing for one of the highest views works on webtoons.com.

This series has the same overarching themes as a typical "timeloop" story without the protagonist being in an actual timeloop himself; he knows one iteration of the future but he also only has one attempt and if he changes anything it's possible his knowledge will be completely useless as well from changing too much. The early chapters are kind of cringe and it's a little too reliant on skills/stats for meaningless things but overall it utilizes the meta elements of the story well. Also this is kind of just dragon ball z ultimately.

The work has very good pacing early on and is generally pleasant to read through, however somewhere before ep100 it starts to get lost in the sauce and everything starts to get kind of weird.
