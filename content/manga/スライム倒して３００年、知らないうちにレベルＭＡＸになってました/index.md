+++
title = "スライム倒して３００年、知らないうちにレベルＭＡＸになってました"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-16
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=146564"
raw = "https://www.ganganonline.com/contents/slime/"
md = "https://mangadex.org/title/23197/slime-taoshite-300-nen-shiranai-uchi-ni-level-max-ni-nattemashita"
bw = "https://bookwalker.jp/series/146298/"

[chapters]
released = 69.3
read = 29

[lists]
recommend = "B"
+++

Protag nolifes the corporate lifestyle and never has fun or does anything that is not work related, and then ends up dying due to overwork. When reincarnating she asks the goddess for immortality, and it's just given to her (along with a 17 year old body). The protag then gets an idyllic house in a countryside with a farm near a village and kills slimes or slacks off everyday, until 300 years pass uneventfully. As a result of her epic daily grindset she ends up leveling up to max level and becoming broken overpowered. Now everybody wants to challenge the strongest witch who has attained max level, and tons of wacky hijinks ensue!!!

<!--more-->

The work is basically a slice of life where the protag keeps getting wrapped up in more ridiculous events over time against her will but manages to resolve it effortlessly while instilling the respectable mantra of "just fucking chill and stop overworking".

The blue dragon has a top tier design.
