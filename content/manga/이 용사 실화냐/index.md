+++
title = "이 용사 실화냐"
title_en = "Is This Hero for Real?"
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = []
sources = ["web novel"]
languages = ["korean"]
schedules = []
lastmod = 2024-07-26
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=196799"
raw = "https://page.kakao.com/home?seriesId=58800646"
md = "https://mangadex.org/title/c6db0d4c-a0e0-401a-8503-3c84ce30e346/is-this-hero-for-real"

[chapters]
released = 101
read = 101

[lists]
recommend = "C"
+++

An entire class gets isekai'd at once and gets told to save another world by a goddess, and all of the kids in the class start asking basic questions of the goddess on what they can expect, although the goddess starts to get annoyed after a while from all of their questions. Near the end of the questioning, one of the kids in the class ask to be returned to earth since he doesn't want to abandon his loved ones, to which the goddess obliges and sends him back. Except she doesn't, and he ends up in the middle of nowhere in the forest while the rest of his classmates end up teleported to royalty as expected. The work then follows the story of the classmates that were sent to royalty, along with the single protagonist sent to the outskirts that was cursed by the goddess and doomed to his death for wanting to head back to his parents.

<!--more-->

it's like a cross between hitori bocchi and tsukimichi where the protag gets cursed by the goddess and can only talk to the enemies of humans, but he meets a sassy lost child who stalks him and gives him her blessing. he kills and massacres a ton while slowly trying to gain experience except he can't gain experience because of the blessing and curse he obtained. It's a fairly entertaining "the full class of students isekai at once" series where it follows and swaps back between multiple viewpoints and storylines. It's very much a "stat spam" series though, and it constantly bombards you with the constellation/god popup windows too. Ultimately I do enjoy a large majority of this work since it's just relatively chill popcorn in the same sense that tsukimichi is, but the ending is rushed/axed and kind of terrible which makes the entire thing not really as worth it.

The official translation is baffling since it uses the korean termiology as-is without attempting to translate it, so they end up using "assa" which just sounds funny in english.
