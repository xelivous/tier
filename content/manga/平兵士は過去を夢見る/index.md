+++
title = "平兵士は過去を夢見る"
title_en = ""
statuses = ["axed"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-10
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=135109"
raw = "https://www.alphapolis.co.jp/manga/official/357000139"
md = "https://mangadex.org/title/19589/hiraheishi-wa-kako-wo-yumemiru"
bw = "https://bookwalker.jp/series/108462/"

[chapters]
released = 62
read = 62

[lists]
recommend = "D"
+++

Protag is just a random dude in the human army in the fight against the demon lord, and in the final battle he witnesses the actual Heroes defeating but demon lord but ends up getting backstabbed/dying right afterwards as well. However a random creepy girl saves him from death and reincarnates him by sending him back to when he was a baby.

<!--more-->

The work regularly flops back and forth between the present time and the protag's previous life, so if you are allergic to flashbacks you might not like this work. However the pacing is kind of weird, it never fully fleshes out the story properly, and a lot of the plot events ultimately feel extremely weird. There's occasionally some nice twinbraids in this work but it's usually relegated to characters that barely appear since this work is largely focused on the bromance instead so it's just disappointing overall. The work feels like it was turboaxed near the end but up until that point it really felt like it might have something worth reading underneath but in the end nothing really matters. Probably worth checking out if you don't mind being possibly disappointed.
