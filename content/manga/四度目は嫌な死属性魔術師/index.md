+++
title = "四度目は嫌な死属性魔術師"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["partial"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-07-01

[links]
mu = "https://www.mangaupdates.com/series.html?id=149072"
raw = "https://comic-walker.com/contents/detail/KDCW_MF00000076010000_68/"
md = "https://mangadex.org/title/28544/yondome-wa-iya-na-shizokusei-majutsushi"
bw = "https://bookwalker.jp/series/185891/"

[chapters]
released = 47
read = 46

[lists]
recommend = "C"
+++

Protag is on a cruise ship with all of his classmates on a class trip when it gets attacked by a terrorist and they all die/drown, and all get reincarnated/isekai'd together with all of the people on the cruise. Unfortunately when in the process of reincarnation the dumb god gave all of the protag's powers/etc to some other random dude with a similar name. As a result he ends up getting sold by his parents shortly after birth to be involved in human experiments and tortured for a large majority of his life, and basically gets forced to live in eternal suffering for years on end. He then eventually dies, gets reincarnated again and it's just as bad as the first time, then dies and gets reincarnated again, and this time even gets cursed by the god on top of still having a shit time!!! Uooooh suffering!!!

<!--more-->

Main highlight of this work is that it has tons of cute dark skinned anime girls. The protag just slowly builds up a society of dark skinned cute girls and their wolfman counterparts and slowly amasses power to eventually receive and annihilate his classmates that will eventually arrive in this world, and maybe to eventually kill the god who caused him to suffer in the first place.
