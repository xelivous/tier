+++
title = "前世は剣帝。今生クズ王子"
title_en = ""
categories = ["reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["light-novel"]
languages = ["japanese"]
schedules = []
lastmod = 2024-02-11
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=183655"
raw = "https://www.alphapolis.co.jp/manga/official/615000314"
md = "https://mangadex.org/title/9660c79b-0b65-4f13-956c-03364651800b/zense-wa-kentei-konjou-kuzu-ouji"
bw = "https://bookwalker.jp/series/261918"

[chapters]
released = 39
read = 39

[lists]
recommend = "S"
+++

The protag desperately struggled to survive back in his old world after living on the battlefield for decades and watching all of his close friends die, but finally manages to reincarnate into the cushy life of a prince that doesn't need to pick up a sword anymore.

<!--more-->

overall it's a pretty cool work about a broken protagonist who is tired of the world and just wants to chill out and sleep all day, until he comes across someone who manages to change his mind somewhat and he starts putting in a basic amount of effort to try and move forward to healing the mind that was demolished from everything that happened in his past life. The art is kind of bad though at times but I got used to it and/or the artist improved over time. It's still a power fantasy of the protag overwhelming most people with near absolute power, but it doesn't fall into being a wish fulfilment fantasy. Just a dude trying to move on from the suffering in his previous life, the regrets he had, along with learning to become human again while protecting the new people in his life.

Shoutout to the protag's first crush being a really cute twinbraid who regularly pops up in flashbacks really appreciate it.
