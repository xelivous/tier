+++
title = "王妃様は逃亡中"
title_en = ""
categories = ["isekai"]
demographics = ["shoujo"]
statuses = ["completed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-20
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=142006"
raw = "https://www.alphapolis.co.jp/manga/official/762000165"
md = "https://mangadex.org/title/53b1e6d1-b740-464c-a7fe-ce9793bc8135/ouhi-sama-wa-toubouchuu"
bw = "https://bookwalker.jp/ded13a711f-d6ca-4706-95c5-962600680653/"

[chapters]
released = 7
read = 7

[lists]
recommend = "F"
+++

Protagonist gets summoned to an isekai, had a baby with the prince, and then shortly afterwards almost gets forcibly sent back to japan after giving birth by the priest. However she manages to just narrowly avoid the magic circle and goes on the run in the isekai.

<!--more-->

I don't even know what to say about this. It's extremely shuojo, is very short, doesn't really make any sense, and i'm not surprised it was axed; or well it's not even axed it's just so short and rushed that it only has 7 chapters and is "complete". It has like multiple levels of bang starts all misfiring over each other. I do not know who the target demographic is, I do not know who this would appeal to, and although it isn't an affront to my senses and actually deplorable it's just boring and basically nonsensical.
