+++
title = "エルフ嫁と始める異世界領主生活"
title_en = ""
categories = ["reverse-isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-03-08
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=140405"
#raw = ""
md = "https://mangadex.org/title/58a21951-e916-442e-a21f-ba3b3924bae0/elf-yome-to-hajimeru-isekai-ryoushu-seikatsu"
bw = "https://bookwalker.jp/series/153513"

[chapters]
released = 25
read = 25

[lists]
recommend = "D"
+++

Cute elf girl and her entire island isekais into japan and the Average Protag who lives out in the middle of nowhere who ends up meeting her first becomes basically the lord of the region and gets a harem of cute girls because reasons and he has to solve mundane issues while the japanese government faffs about. the end.

<!--more-->
