+++
title = "失格紋の最強賢者"
title_en = ""
statuses = ["ongoing"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["reincarnation"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2021-09-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=143878"
raw = "http://www.ganganonline.com/contents/shikkaku/"
md = "https://mangadex.org/title/21901/shikkaku-mon-no-saikyou-kenja-sekai-saikyou-no-kenja-ga-sara-ni-tsuyokunaru-tame-ni-tensei-shimashita"
bw = "https://bookwalker.jp/series/140366/"

[chapters]
released = 71.3
read = 46

[lists]
recommend = "B"
+++

I really like the dragon girl's design. It's top tier. The series gets excessively better every time she's there and worse when she's not there.

<!--more-->
