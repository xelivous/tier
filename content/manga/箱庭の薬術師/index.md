+++
title = "箱庭の薬術師"
title_en = ""
categories = ["isekai"]
demographics = ["josei"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-09-30
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=168695"
raw = "https://gaugau.futabanet.jp/list/work/5dd5003c77656175b3040000"
md = "https://mangadex.org/title/2a7ccd70-60e7-4ce6-820b-9adf4a9a0e40/boxed-garden"
bw = "https://bookwalker.jp/series/246587"

[chapters]
released = 43.2
read = 14

[lists]
recommend = "D"
+++

The protagonist was an average highschool jk when her sister was struck with a terminal illness and hospitalized, and despairs over being unable to do anything for the sister she loves. Four years pass and the sister has to undergo a dangerous surgery so the protagonist prays to god to help out her sister offering to do anything in return, to which god replies that they'll fix her but the protagonist will have to do god's bidding and become his possession (are you sure it's not a deal with the devil?). As a result she gets sent to an isekai where she has to gather a ton of good deed points in this other world (which she can use to buy some things from god) for an unknown reason and he also makes her younger again. She then gets whisked away to the isekai along with a fancy house, barrier, tons of materials, tons of broken OP lifestyle skills, etc. One day she saves some nobles who strayed into her forest after living there for months, and they get shocked at how epic her powers are and she ends up making potions and encountering more and more people as time goes on!!

<!--more-->

As soon as she meets someone for the first time in the world one of the very first actions she takes is to go buy a slave. She goes out of her way to find someone that's broken so she can go "i can fix him". But then god gets jealous because she's his posession and she should only have eyes for him and she shouldn't get any ideas teehee. He's a cute ikemen possessive shounen who regularly teases her uwu.

From what i've read of the story there's nothing particularly outstanding about it, although if anything ch14/15 were so far out of left field that maybe there's more interesting stuff later on in the story but it just immediately goes back to happy-go-lucky shoujo life right afterwards that it just feels weird.
