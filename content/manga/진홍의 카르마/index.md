+++
title = "진홍의 카르마"
title_en = "Crimson Karma"
categories = ["reincarnation", "isekai"]
demographics = ["josei"]
statuses = ["completed"]
furigana = []
sources = ["original"]
languages = ["korean"]
schedules = []
lastmod = 2022-10-24
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series.html?id=158184"
raw = "http://page.kakao.com/link/53809611"
md = "https://mangadex.org/title/fe840243-5b68-447a-ab4b-c094eed7d6af/crimson-karma"

[chapters]
released = 118
read = 118

[lists]
recommend = "A"
+++

It's somewhere in-between a military, romance, and political-intrigue work that focuses on a female soldier who's ikemen af. I don't feel like any part of the work is bad in any way and I enjoyed it throughout the entire thing to the point that I just marathoned it straight up. The pacing is pretty great throughout without any superfluous elements while expanding on everything that needed to be expanding on, and ending in a satisfying way that didn't feel contrived.

<!--more-->
