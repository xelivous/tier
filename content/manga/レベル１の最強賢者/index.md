+++
title = "レベル１の最強賢者"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-05-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=160098"
raw = "https://comic.pixiv.net/works/6195"
md = "https://mangadex.org/title/b48a41e8-7072-49eb-a5ed-aab1d98f47af/level-1-no-saikyou-kenja-noroi-de-sai-kakyuu-mahou-shika-tsukaenaikedo-kami-no-kanchigai-de-mugen-no-maryoku-o-te-ni-ire-saikyou-ni"
bw = "https://bookwalker.jp/series/255532"

[chapters]
released = 27
read = 27

[lists]
recommend = "C"
+++

The hero beat the demon lord so quickly that the evil god didn't recoup enough mana from the entire process, so he decides to reincarnate the "hero" with trash skills and tons of curses as a pre-emptive measure. As a result he curses him to never level up, and so that his status never changes. However this backfires and as a result the protag is basically invincible because his HP can't lower, and his MP is infinite haha lol.

<!--more-->

Time goes on and he trains with his booba-maid who used to be a companion of the previous hero from 100+ years ago, and eventually goes to school and dabs on fools. the twins have an extremely wareyacore design.

Basically the protag just overlaps tons of easy spells with low mana cost to make complex skills that seem like they're ultimate magic to get around his low mana cap and since he literally can't die there's very little stakes other than having to protect his harem of cute girls.
