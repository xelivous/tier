+++
title = "転生したら兵士だった？！"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["seinen"]
statuses = ["axed"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=176589"
raw = "https://www.comic-earthstar.jp/detail/tenseiheishi/"
md = "https://mangadex.org/title/fa3c24af-fea0-4d44-b8da-131c69646bb1/tensei-shitara-heishi-datta-akai-shinigami-to-yobareta-otoko"
bw = "https://bookwalker.jp/series/318029/"

[chapters]
released = 10.2
read = 10.2

[lists]
recommend = "D"
+++

Protagonist wakes up on the middle of a battlefield with his past life's memories and is immediately thrust into a life-or-death situation against a gigantic enemy. He manages to slightly outwit it but still gets blasted and ends up in the hospital. Apparently he's part of the military and has been quickly getting promoted for his epic skillz and now that he has the knowledge of the super advanced japanese society he will become even more epic.

<!--more-->

The work is blazing fast with basically nonstop fights, it leaves everything as vague as possible, and then just ends from being axed. There's also an utterly bizarre age gap romance/marriage plot later on. If you want some military dudes doing military things against magical creatures with little to no explanation then i'd rec this I guess. Also the protag is the only super special boy who wears special black clothing while everybody else wears white for some reason!!
