+++
title = "不遇職とバカにされましたが、実際はそれほど悪くありません？"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["none"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2024-08-18
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=160570"
raw = "https://www.alphapolis.co.jp/manga/official/894000312"
md = "https://mangadex.org/title/44221/people-looked-down-on-me-for-having-a-crummy-job-but-it-really-isn-t-all-that-bad"
bw = "https://bookwalker.jp/series/253168/"

[chapters]
released = 41
read = 26

[lists]
recommend = "D"
+++

Protag gets sucked through a crack in the spacetime flux and can't go back to earth, and as a result gets reborn as a baby in an isekai with magic and cool fun isekai rpg stuff! Except apparently he's a cursed child who is outcast and gets relegated to live in an obscure mansion away with his hot as fuck twinbraid mom. The reason why he was thrown out is because his class/etc was shit so he's useless to lead as a potential king of this country (lol). But he has his emotional support angel to guide him to become broken OP and obtain all of the skills!

<!--more-->

for the most part it's just skill autism. oh godddddd i'm levellinggggggg. The emotional support angel that he constantly asks questions of kind of drags down the story since any time the protag is in a pinch she just pops in and tells him everything he needs to know so he never has to think for himself. She literally just exists for the author to loredump random shit like a cute wikipedia mascot; No reason to give the protag encyclopedic knowledge when you can have a tulpa give him the info constantly. Maybe it would've been a decent work without that.

I appreciate that the guild master is unconventially a bulky gigantic human woman who went all in on greatswords despite most people looking down on those big weapons.
