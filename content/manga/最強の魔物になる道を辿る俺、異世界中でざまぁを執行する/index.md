+++
title = "最強の魔物になる道を辿る俺、異世界中でざまぁを執行する"
title_en = ""
categories = ["reincarnation", "isekai"]
demographics = ["seinen"]
statuses = ["hiatus"]
furigana = ["partial"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=187801"
raw = "https://www.yomonga.com/title/1582"
md = "https://mangadex.org/title/1e8d04cf-400c-4ffb-af6b-108d851f1b7d/saikyou-no-mamono-ni-narumichi-wo-tadoru-ore-isekaijuu-de-zamaa-wo-shikkou-suru"
bw = "https://bookwalker.jp/dec63d9ac8-75de-4c4f-9563-7bda1c62d1ac/"

[chapters]
released = 16
read = 12

[lists]
recommend = "F"
+++

Protag's father adopts a girl after her family dies, and the protag tries her best to be the very best oneesan possible to her. However the adopted girl is a massive conniving bitch who lies to/manipulates everybody possible to hate the protag and basically make her life miserable, because reasons. However in the depths of despair she comes across a small black dragon who for some reason helps her out, and it turns out it's because he's a japanese dude who killed himself by jumping in front of a train. The perspective then swaps from the girl protag to the isekai'd dragon while the dragon helps get back at the conniving bitch.

<!--more-->

I never thought i'd come across an edgy isekai manga that makes kaiyari man's writing seem good but here we are.