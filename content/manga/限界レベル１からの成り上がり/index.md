+++
title = "限界レベル１からの成り上がり"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["partial"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-17
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=165513"
raw = "https://seiga.nicovideo.jp/comic/46451"
md = "https://mangadex.org/title/00b64b8f-cb7e-4322-9855-3669fe210ac7/genkai-level-1-kara-no-nariagari-saijaku-level-no-ore-ga-isekai-saikyou-ni-naru-made"
bw = "https://bookwalker.jp/series/264159"

[chapters]
released = 28
read = 27.2

[lists]
recommend = "B"
+++

Protagonist is getting beat up by delinquents while this highschool girl looks on when both of them along with the four delinquents end up getting isekai'd together, and apparently the four delinquents are the destined Heroes of this world and the protag/girl are just accidental summons that tagged along with. In this world level caps mean everything and if your level cap is trash, you are trash, and wouldn't you know it the protag's level cap is literally 1 so he's ultimate trash and immediately gets the death penalty for being so trash haha. But unknowing to everybody else he actually has a super epic ultra skill that is still powerful enough to ignore his shitty level cap so he's actually like the strongest imaginable.

<!--more-->

I'll give it bonus points for having a guild system that isn't letter ranks but a more vague 'trainee' -> 'regular' system that mostly acts as the militia/mercenary being shipped out to various warfields. It's a refreshingly chuuni work where the power in his right hand is actually realdeal and the worldbuilding/characters is fairly unique in the isekai-verse.
