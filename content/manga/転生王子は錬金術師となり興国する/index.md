+++
title = "転生王子は錬金術師となり興国する"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-14
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series/7ggbijj/tensei-ouji-wa-renkinjutsushi-to-nari-koukoku-suru"
raw = "https://www.ganganonline.com/title/1057"
md = "https://mangadex.org/title/6fc21db2-490c-4cf5-a207-f27e0e864662/the-reincarnated-prince-becomes-an-alchemist-and-brings-prosperity-to-his-country"
bw = "https://bookwalker.jp/series/283904/"

[chapters]
released = 12.2
read = 11.5

[lists]
recommend = "D"
+++

Protag was born as the third prince of a basically starving/ruined country, his older sister gets married off to some lecherous douche of a prince, and his goal is to bring prosperity to his nation since he's likely the one to inherit it since his two older brothers are basically useless as well. Also his parents literally named him hero haha lol. Protag also has the power of Bullshit Nonsense on his side, aka Magical Wikipedia that answers his every question magically whenever he thinks it (once every 30 days). Also his method of becoming an alchemist (despite it being a super big taboo thanks to the epic evil church) is through this wikipedia power who gives a Special Exception and grants the knowledge that the macguffin he needs is in his very own territory and all he needs to do it to set out to get it! Also alchemy is a programming language with compilers and things because coders rise up!!

<!--more-->

Also where the fuck is the king/queen/his parents in all of this?

One of the good parts about this work is that all of the relationships are very straightforward. His brothers just want to save the country but they can only think of solutions they can bring about, and they're perfectly fine supporting each other and talking it out instead of prolonging dumb drama. The heroine wants the D and she's not afraid to outright say it without beating around the bush. Etc.

It's honestly just a bizarre series overall.
