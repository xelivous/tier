+++
title = "チート薬師のスローライフ"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-04-18
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=152496"
raw = "http://gammaplus.takeshobo.co.jp/manga/cheat_yakushi/"
md = "https://mangadex.org/title/33136/cheat-kusushi-no-slow-life-isekai-ni-tsukurou-drugstore"
bw = "https://bookwalker.jp/series/210332/"

[chapters]
released = 48.2
read = 45

[lists]
recommend = "C"
+++

Protag gets isekai'd, comes across a wolf bleeding out in the middle of the forest, crafts a healing potion from scratch, and gives it to the wolf. She then mounts him, transforms into a wolfgirl loli, licks him, calls him ご主人様, and follows him home to be his pet.

<!--more-->

The protag lives with his ghost wife and wolfgirl doggo pet, while occasionally solving the problems that plague the villagers around him by making whatever random potion of the day that directly solves their problem perfectly. "Why would the leader of the mercenaries wear that?" -jackvoice

It's a relatively decent laidback comedy with moeblobs.
