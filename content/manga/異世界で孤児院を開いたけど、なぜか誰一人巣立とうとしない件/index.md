+++
title = "異世界で孤児院を開いたけど、なぜか誰一人巣立とうとしない件"
title_en = ""
categories = ["isekai"]
demographics = ["seinen"]
statuses = ["ongoing"]
furigana = ["none"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-08-27
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=149006"
raw = "http://gammaplus.takeshobo.co.jp/manga/isekai_kojiin/"
md = "https://mangadex.org/title/57a981a8-fa23-48c5-971b-1329e3d9bb0b/isekai-de-kojiin-wo-hiraitakedo-nazeka-darehitori-sudatou-to-shinai-ken"
bw = "https://bookwalker.jp/series/187188"

[chapters]
released = 49.2
read = 29

[lists]
recommend = "F"
+++

i'll give the art an 8/10 and the story a -7/10 for an overall score of 1/10. check out the artist's [twitter](https://twitter.com/tomosane_ariike)/pixiv/whatever and hope they've drawn other stuff, and/or check out their other series if they have any. The only redeeming point of this series is if you like ecchi loli panels and even then they throw in a booba character relatively early on so they can't even stick to their one demographic of loli-lovers. The plot is both nonsensical and brainlet-tier and basically nothing happens and there's still no plot after 30+ chapters of what's basically a monthly series.

<!--more-->
