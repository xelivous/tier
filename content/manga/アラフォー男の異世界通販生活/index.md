+++
title = "アラフォー男の異世界通販生活"
title_en = ""
categories = ["isekai"]
demographics = ["shounen"]
statuses = ["axed"]
furigana = ["full"]
sources = ["web novel"]
languages = ["japanese"]
schedules = ["monthly"]
lastmod = 2025-01-25
tags = [
    
]

[links]
mu = "https://www.mangaupdates.com/series/v5vp4fa/around-40-otoko-no-isekai-tsuuhan-seikatsu"
raw = "https://magazine.jp.square-enix.com/gfantasy/story/arafo_isekaitsuhan"
md = "https://mangadex.org/title/18f5549a-9d1a-44a2-a8ca-aed2448e37c0/the-daily-life-of-a-middle-aged-online-shopper-in-another-world"
bw = "https://bookwalker.jp/series/222775/"

[chapters]
released = 60
read = 60

[lists]
recommend = "F"
+++

Protag tires of his corporate life as an illustrator and moves out to the countryside to farm while taking illustration jobs on the side, when all of a sudden one day he winds up in an isekai forest with a status window. In the status window he's able to access an online store to buy literally anything he'd ever want, an item box to store anything, and can even sell anything he sees as currency to buy from this online shop. He then basically pay2win's his way to a safe and easy life.

<!--more-->

Lmao at the 1st chapter. Meets a girl (half his age) working at an inn, asks her to teach him to read/write, and then she almost immediately strips afterwards and pushes him down and end up having sex. Isekai Sex Speedrun Any% World Record frfr. He then continues to introduce more and more insane shit to the world without care, and eventually runs away from this starting village (after a few other events) to not get wrapped up in noble disputes. The work then becomes a strange child-raising story and the ecchi contents almost completely disappear from the story; I have no idea what the author is cooking and nothing feels planned at all. Okay nevermind it just eventually becomes harem-ville again later on after a while.

Dude skips town because he doesn't want to get involved with nobles and doesn't want to stand out, then in the next town stands out and intentionally gets involved with nobles. Is the author stupid? "don't show your massive item box to nobility young man", and he immediately shows his massive item box to the first nobility he meets (who is also super ultra duper horny for him despite being married).

This work is literally just the protag getting bossed around by a bunch of women as a massive pushover doing anything they want without any of his own opinions or needs. The protagonist has no drive at all, no will of his own, and exists solely as a vending machine for the women's desires. Every single woman in the work then naturally wants to have sex with him regardless of their age; his special isekai power is clearly "super pheromones".

The art is actually very high quality, and the first page of each chapter is really nice more often than not (although I legit feel like the artist is a hardcore loli porn artist based off of some of the contents on them). There's good attention to detail in all of the backgrounds, the overall composition is nice, the art itself is high quality all around. If anything the only reason why I managed to stick around to the end is because the art was good enough and I knew there was an ending in sight (since this got axed).

The ending is abysmally bad, the last arc is terrible, and this work got axed in a really bad spot as well. Honestly there's nobody I could even rec this to outside of someone who wants to see sneed lolis craving this 40 year old dude, since at least the art is great, although maybe just read one of those loli doujins instead idk?
