+++
title = "皇帝つき女官は花嫁として望まれ中"
title_en = ""
statuses = ["ongoing"]
demographics = ["josei"]
furigana = []
categories = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-06
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154873"
raw = "https://online.ichijinsha.co.jp/zerosum/comic/koute"
md = "https://mangadex.org/title/38702/the-emperor-s-court-lady-is-wanted-as-a-bride"
bw = "https://bookwalker.jp/series/224738/"

[chapters]
released = 25
read = 15

[lists]
recommend = "D"
+++

While it's nice that the protag is at least somewhat competent, constantly being told she shouldn't do anything so as to not hurt herself is rather annoying.

<!--more-->
