+++
title = "任侠転生"
title_en = ""
statuses = ["ongoing"]
demographics = ["seinen"]
furigana = ["none"]
categories = ["isekai", "reincarnation"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-10-04
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=154821"
raw = "https://www.sunday-webry.com/detail.php?title_id=964"
md = "https://mangadex.org/title/43476/yakuza-reincarnation"
bw = "https://bookwalker.jp/series/233391/"

[chapters]
released = 94
read = 25

[lists]
recommend = "B"
+++

A little heavy on the Fights and action with little else. Would rec to anyone who is fine with wanking over how Cool and Badass yakuza are, since everything else about the series is solid so far.

<!--more-->
