+++
title = "豚公爵に転生したから、今度は君に好きと言いたい"
title_en = ""
statuses = ["completed"]
demographics = ["shounen"]
furigana = ["full"]
categories = ["isekai", "reincarnation"]
sources = ["web novel"]
languages = ["japanese"]
schedules = []
lastmod = 2023-09-13
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=147947"
raw = "https://comic-walker.com/contents/detail/KDCW_MF02200210010000_68/"
md = "https://mangadex.org/title/24206/buta-koushaku-ni-tensei-shitakara-kondo-wa-kimi-ni-suki-to-iitai"
bw = "https://bookwalker.jp/series/173741/"

[chapters]
released = 46
read = 46

[lists]
recommend = "B"
+++

Protag is your average otaku wanting to get reincarnated into an LN fantasy series as an ikemen protag to have a harem with tons of cute girls, when one day while watching anime he falls asleep and wakes up in an isekai in another body. Except he's a chubby fatass the series he was watching who is doomed to never be liked by any of the girls. However thankfully he has a super cute girl as an attendant so his luck isn't all that bad in the long run, and he seeks to do whatever he can to improve his situation starting with shedding a lot of his useless body fat.

<!--more-->

It's a fairly wholesome chungus shounen manga that is primarily a battle shounen, about a dude slowly trying to repair his absolutely shit reputation slowly over time. A self-improvement propoganda manga if you will. The art itself is fairly good although there's a lot of blank / talking-head panels which is fairly lazy all things considered. If you imagine that booba princess is actually booba patchouli the work becomes far better tho; she's hot fr (the artist used to do a lot of touhou fanart back in the day too). Unfortunately most of the characters don't really get developed aside from the main character, and a lot of the women in the manga are also fairly useless and exist simply to be cute/hot. Ch 44 is kind of kino-meme I respect it; peak fiction. It's kind of unfortunate that this work got pseudo-axed since I was somewhat fond of the work but I suppose it's better to end and drag out until I begin to hate it, and now the artist can work on something else which may or may not be better.
