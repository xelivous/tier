+++
title = "最強スキル「命乞い」で悔しいけど無双しちゃう元魔王様の世界征服活動"
title_en = ""
categories = []
demographics = []
statuses = ["ongoing"]
furigana = []
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2023-09-30
tags = [
    
]
draft = true

[links]
mu = "https://www.mangaupdates.com/series/myk2dvz"
raw = "https://viewer.heros-web.com/episode/4856001361370797022"
md = "https://mangadex.org/title/ed35f841-5e6d-485c-a902-78f11e2975ac/saikyou-skill-inochigoi-de-kuyashii-kedo-musou-shichau-moto-maou-sama-no-sekai-seifuku-katsudou"
#bw = ""

[chapters]
released = 4
read = 0

[lists]
recommend = ""
+++



<!--more-->

