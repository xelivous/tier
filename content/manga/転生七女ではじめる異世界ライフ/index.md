+++
title = "転生七女ではじめる異世界ライフ"
title_en = ""
categories = ["isekai", "reincarnation"]
demographics = ["shoujo"]
statuses = ["axed"]
furigana = ["full"]
sources = []
languages = ["japanese"]
schedules = []
lastmod = 2022-12-03
tags = [

]

[links]
mu = "https://www.mangaupdates.com/series.html?id=182997"
#raw = ""
md = "https://mangadex.org/title/2193e82e-ae7c-4f5e-b498-f96090784688/tensei-nanajou-de-hajimeru-isekai-life"
bw = "https://bookwalker.jp/series/323597/"

[chapters]
released = 18
read = 18

[lists]
recommend = "D"
+++

Maybe you'd like this if you were very into yuri but it doesn't do much at this point beyond that. Also it got axed before it ever really began so there's not much of a point in reading it to begin with. The art is good at least.

<!--more-->
