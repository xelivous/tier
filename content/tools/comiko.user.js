// ==UserScript==
// @name     Comiko page reader
// @version  1.0
// @grant    none
// @match https://comiko.net/chapter/*
// @run-at document-end
// ==/UserScript==

let css = `

#viewer {
  display: flex !important;
  flex-wrap: wrap !important;
  flex-direction: row !important;
  direction: rtl;
}

#viewer .item {
  width: unset !important;
  height: unset !important;
  flex: 1;
  max-height: 100vh;
}

#viewer .item img {
  width: unset !important;
  height: unset !important;
  position: relative !important;
  max-height: 100vh;
}

#viewer .item:nth-child(odd) {
  text-align: right;
}
#viewer .item:nth-of-type(even) {
  text-align: left;
}

#viewer .item:first-child {
  flex: 1 100%;
  text-align: center;
}

`;

let body = document.body || document.getElementsByTagName("body")[0];

let style = document.createElement("style");
style.setAttribute("type","text/css");
style.appendChild(document.createTextNode(css));

body.appendChild(style);