// ==UserScript==
// @name     Mangagohan page reader
// @version  1.1
// @grant    none
// @match https://mangagohan.com/manga/*
// @run-at document-end
// ==/UserScript==

let css = `

.container {
  max-width: 100vw;
}

.reading-content {
  display: flex;
  flex-flow: row wrap;
  direction: rtl;
}

.reading-content .page-break {
  flex: 50%;
}

.reading-content .page-break img {
	max-height: 100vh;
}

.reading-content .page-break:nth-of-type(1n) {
    text-align: right;
}

.reading-content .page-break:nth-of-type(2n) {
    text-align: left;
}

.reading-content .page-break:first-of-type {
  flex: 100%;
  text-align: center;
}

`;

let body = document.body || document.getElementsByTagName("body")[0];

let style = document.createElement("style");
style.setAttribute("type","text/css");
style.appendChild(document.createTextNode(css));

body.appendChild(style);