+++
title = "Tools"

[menu.main]
title = "Various javascripts / tools to use on other sites"
weight = 100
+++

Here's some manga related tools:

1. [A userscript to read MangaGohan in 2page format as intended](mangagohan.user.js) because vertical scrolling exclusively is brainlet tier
1. [A userscript to read Comiko in 2page format as intended](comiko.user.js) because vertical scrolling exclusively is brainlet tier. 
  * However it breaks horribly on stitched double pages. Assumes you set it to All Pages / Original.
