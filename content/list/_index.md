+++
title = "Lists"

[menu.main]
title = "The main lists to categorize by"
weight = 5
+++

All of the classifications/rankings of the works listed on this site.
