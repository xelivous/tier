+++
title = "Isekai/Reincarnation Manga Recommendability"
description = "How much I would recommend a particular isekai/reincarnation work"

aliases = [
    "/list/overall", 
    "/list/recommend",
]
+++

One day I set out to try to read every isekai/reincarnation manga that existed because it felt like it was a large enough "genre" to have a ton of cool stories within it but small enough that I could still reasonably finish it within a reasonable timespan. I have since read a bunch of works and created this tier list of the works and how much I would recommend other people to also read them in a general sense.

I've written comments for a majority of the works, with the first paragraph typically being a basic summary around the first chapters/arc, and every paragraph after that being overall thoughts or quips about the work overall or why I would/wouldn't recommend it. Some works have far more comments about them than others and it's not always related to how good the work itself is itself, and there's a few works I still haven't gotten around to actually writing comments for. I try to use spoiler tags for some basic comments that I think would always spoil someone but you may be better off just reading the comments after you finish a work yourself and see if your thoughts align with mine to kind of calibrate the rest of the list; If my comments seem fully off then maybe all of my other recs are fully off and this list wouldn't be as useful for you.

There's some filters above the chart where you can drill down a bit to see only certain works if you'd like to I guess. Some of the manga still don't have a full set of taxonomies set properly so you might get weird results.

## Extra descriptions for the tiers:

TLDR: The higher the tier the more easily i'd be able to recommend it to other people.

* **SS**: Please, just read it.
* **S**: S tier is basically the same as A tier but I personally want to shill the works for one reason or another. I consider them works that just about anybody would enjoy, or that they're the best in their subcategories, and generally i'd want an anime made of them.
* **A**: Works that are generally competent/good and appealing to just about everybody. They typically have well thought out and fleshed stories/characters with decent or better art, good panelling, etc. I feel like I could recommend them wholeheartedly to anybody and they wouldn't dislike it.
* **B**: Works that are mostly competent, but might have a few flaws that could cause at least certain demographics to not like it all that much.
* **C**: This tier mostly houses anything from middling works that only appeal to certain demographics, to somewhat mediocre works with a decent amount of good moments mixed in. Check out the synopsis/descriptions of the works, or even the cover arts/name of the comic, and if it sounds appealing to you then you may as well give it a shot.
* **D**: It's hard to recommend these works in any general capacity either due to large sweeping issues with their story/content/characters, or just other general issues, but it's possible that it fits directly into the niche of someone i'd want to recommend to and would be a perfect fit for them. I personally like a few series in this tier but I have no idea who I would really rec them to at the moment aside from myself.
* **F**: Read these if you hate yourself I guess.
