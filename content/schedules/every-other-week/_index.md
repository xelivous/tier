+++
title = "Every Other Week"
per-year = 24
+++

Work gets updated every other week, or twice a month, or about 24 times a year.
