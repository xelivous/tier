+++
title = "Sporadic"
per-year = 1
+++

Work gets updated whenever the author feels like it, maybe once or twice a year.
