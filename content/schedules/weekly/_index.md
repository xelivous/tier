+++
title = "Weekly"
per-year = 52
+++

Work gets updated once a week, or about 52 times a year minus holiday weeks/etc.
