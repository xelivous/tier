+++
title = "Every 10th Day"
per-year = 36
+++

Work gets updated on the 1st, 11th, and 21st day of the month, or 36 times a year.
