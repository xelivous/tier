+++
title = "Villain of the Week"

type = "plot"
+++

Work primarily features just random villains constantly in the protagonist's way. The protagonist overcomes them and then another one appears like a cockroach.
