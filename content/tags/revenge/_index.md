+++
title = "Revenge"

type = "plot"
+++

A major plot point in this work is that the protag or a main character goes off and seeks revenge for a prior transgression.
