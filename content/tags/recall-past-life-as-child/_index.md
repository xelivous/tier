+++
title = "Recall Past Life as Child"

type = "plot"
+++

The protagonist recalls their past life when they are a child, instead of retaining their memories at birth or otherwise.
