+++
title = "Timeskip From Baby Age"

type = "plot"
+++

Story doesn't go through the whole process of aging up from a baby/etc, and instead timeskips past to an age where the protagonist can do things more reasonably.
