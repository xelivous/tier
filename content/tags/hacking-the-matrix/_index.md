+++
title = "Hacking the Matrix"

type = "plot"
+++

The fabric of reality can no longer constrain the protagonist.
