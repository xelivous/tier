+++
title = "Monster Taming Through Naming"

type = "world"
+++

You tame monsters in this work by giving them a name and usually offering some of your magic power to them. This in turn makes them stronger, and also makes them loyal to you, usually.
