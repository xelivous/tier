+++
title = "Arbitrary Magic System"

type = "world"
+++

Magic has no hard and fast rules, it's just whatever the author wants it to be at that moment. New techniques can be thought up and used in battle at any moment with no prior explanation.
