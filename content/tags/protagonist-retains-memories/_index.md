+++
title = "Protagonist Retains Memories"

type = "character"
+++

When reincarnating, the protagonist retains his memories.
