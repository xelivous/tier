+++
title = "Language Barrier"

type = "plot"
+++

A part of the plot is that the Protagonist has to overcome a language barrier in order to communicate with others, or regularly comes across people they cannot communicate with.
