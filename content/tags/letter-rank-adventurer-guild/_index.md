+++
title = "Letter Rank Adventurer Guild"

type = "world"
+++

Work features an adventurer's guild that uses a ranking system that typically goes from F -> S as the protagonist "ranks up" or similar.
