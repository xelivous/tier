+++
title = "Immeasurable Power Level"

type = "character"
+++

So strong that their power level can't even be measured by the tools of this world.
