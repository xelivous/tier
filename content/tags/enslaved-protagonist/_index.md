+++
title = "Enslaved Protagonist"

type = "character"
+++

Protagonist is enslaved in the story and is either robbed of their free will or has some kind of restrictions placed upon them.
