+++
title = "Reincarnated as Baby"

type = "plot"
+++

Protagonist reincarnates as a baby and as at least a small portion of the work is dedicated to being a baby.
