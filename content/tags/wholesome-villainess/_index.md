+++
title = "Wholesome Villainess"

type = "character"
+++

The villainess should normally be evil and mean, but our reincarnated goody-two-shoes can't possibly be mean.
