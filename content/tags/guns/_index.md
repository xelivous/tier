+++
title = "Guns"

type = "world"
+++

Guns are in this world, either as a result of the protagonist introducing them, some previous reincarnator introducing them, or they were independently developed.
