+++
title = "Thrown Out for Being Too Weak"

type = "plot"
+++

Protagonist gets thrown out or disregarded for being too weak. This could be brought about from the protag hiding their power levels, or being too strong that they wrap around to having a trash status, or may they really are just weak.
