+++
title = "Non-Blood-Related Sibling"

type = "character"
+++

Protagonist has a sibling that isn't related by blood, either from the protagonist being adopted, or the sibling being adopted.
