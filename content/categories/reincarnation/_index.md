+++
title = "Reincarnation"
+++

Protagonist reincarnates then either has their past life's memories at birth, or regains the memories at a later point in time (possibly due to some traumatic shock).
