+++
title = "Isekai"
+++

In general the protagonist (and/or many people) gets transported to another world and is either forced to live there, gets randomly phased back into their original world periodically, or can freely move back and forth between the two worlds.