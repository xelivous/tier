# Tierlist Site

I wanted a space to rank some manga/etc that  I was reading based on recommendability so I made this site over a long period of time tweaking it slowly as I went along.

## Cloning / Building

This repo uses [git LFS](https://git-lfs.com/) so you'll need to install that first. After that's done it's pretty normal

```sh
git clone https://gitlab.com/xelivous/tier.git tier
cd tier
hugo serve -D
```

